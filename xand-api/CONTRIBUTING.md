<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Contributing](#contributing)
  - [Changelog](#changelog)
  - [Style guide](#style-guide)
  - [Curators' Section](#curators-section)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

[[_TOC_]]

# Contributing

## Changelog
When making changes to this repo, please make updates to the [CHANGELOG](CHANGELOG.md).

## Style guide

* Prefer explicit imports in `use` statements, over wildcard "*"

## Curators' Section

This section is to document ongoing design work or technical debt. 

---

*  **./src/xand_models_converters.rs**
    * Organize converters in a coherent fashion
    * Look into the possibility of a proc_macro to generate converters for future decoupling work - see note in file


* **Overall**
    * Remove wildcard `*` in use statements and replace with concrete dependencies

* **lib.rs** tests
    * Break up file into modules
    * Break out tests into separate module(s)
    * Consider separating xand_api server and client tests; stub out the components not under test (for server tests, one would stub out the Client and its protobuf replies; for client tests, one would stub out the Transaction Cache and its results). This also allows us to decouple the tests and depend less on `xand_api_proto` in `xand_api`
    * Our tests require that we convert from `xand_api_proto::proto_models::XandTransaction` to `xand_models::XandTransaction`, for the sake of comparing encrypted data. This conversion should not be needed, as the former proto type potentially contains less information. A cleaner solution requires a more drastic refactoring of our models, notably to separate types used to request and types returned by queries, and of our test infrastructure, notably to not have to deal with assembling transactions manually in tests.

