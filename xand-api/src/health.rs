use crate::{logging_events::LoggingEvent, XandApiErrors};
use chrono::{DateTime, Utc, MIN_DATETIME};
use petri::HistoricalStore;
use tokio::sync::watch::Receiver;
use tpfs_logger_port::warn;
use xand_api_proto::{HealthCheckResponse, HealthStatus};

use crate::{stream_cacher::StreamCacheHandle, ChainDataSourceInfo};

pub const ELAPSED_TIMESTAMP_CALCULATION_FAILURE_MSG: &str =
    "Could not determine elapsed time between startup and current time.";
pub const ELAPSED_BLOCKS_SINCE_STARTUP_FAILURE_MSG: &str =
    "An initial connection to the network chain has not been established yet.";
pub const CHAIN_LISTENER_NOT_STARTED_MSG: &str =
    "An initial connection to the network chain has not been started.";
pub const UNSAFE_BLOCK_DIFFERENCE_CALCULATION_MSG: &str =
    "Unable to determine the current sync status due to an unsafe arthimetic operation.";
pub const UNSAFE_ELAPSED_BLOCK_CALCULATION_MSG: &str =
    "Unable to determine the elapsed blocks since startup due to an unsafe arthimetic operation.";
pub const VALIDATOR_UNEXPECTED_NEGATIVE_ELAPSED_BLOCKS_WARNING_MSG: &str =
    "Unable to calculate meaningful elapsed blocks count due to unexpected negative result.";
pub const BLOCK_STALENESS_THRESHOLD_MS: i64 = 300_000; // 5 min
pub const DEFAULT_ELAPSED_BLOCKS_RETURN: u64 = 0;

pub type StartupBlockReceiver = Option<Receiver<Option<u32>>>;

#[derive(Clone)]
pub struct StartupState {
    pub block_number: StartupBlockReceiver,
    pub startup_timestamp: DateTime<Utc>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum XandApiHealth {
    Syncing,
    Healthy,
}

impl From<XandApiHealth> for HealthStatus {
    fn from(health: XandApiHealth) -> Self {
        match health {
            XandApiHealth::Syncing => HealthStatus::Syncing,
            XandApiHealth::Healthy => HealthStatus::Healthy,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct HealthReport {
    pub best_known_block: u64,
    pub current_block: u64,
    pub elapsed_blocks_since_startup: u64,
    pub elapsed_time_since_startup_millis: i64,
    pub status: XandApiHealth,
}

impl From<HealthReport> for HealthCheckResponse {
    fn from(report: HealthReport) -> Self {
        HealthCheckResponse {
            best_known_block: report.best_known_block,
            current_block: report.current_block,
            elapsed_blocks_since_startup: report.elapsed_blocks_since_startup,
            elapsed_time_since_startup_millis: report.elapsed_time_since_startup_millis,
            status: HealthStatus::from(report.status).into(),
        }
    }
}

impl Default for StartupState {
    fn default() -> Self {
        Self {
            block_number: None,
            startup_timestamp: MIN_DATETIME,
        }
    }
}

#[async_trait::async_trait]
pub trait HealthMonitor {
    async fn get_health(&self) -> Result<HealthReport, XandApiErrors>;
}

#[derive(Clone)]
pub struct HealthMonitorImpl<H>
where
    H: HistoricalStore + 'static,
{
    historical_store: H,
    startup_state: StartupState,
    last_block_info: StreamCacheHandle<ChainDataSourceInfo>,
}

impl<H: HistoricalStore + 'static> HealthMonitorImpl<H> {
    pub fn new(
        historical_store: H,
        startup_state: StartupState,
        last_block_info: StreamCacheHandle<ChainDataSourceInfo>,
    ) -> Self {
        HealthMonitorImpl {
            historical_store,
            startup_state,
            last_block_info,
        }
    }
}

#[async_trait::async_trait]
impl<H> HealthMonitor for HealthMonitorImpl<H>
where
    H: HistoricalStore + 'static,
{
    async fn get_health(&self) -> Result<HealthReport, XandApiErrors> {
        isolated_health_check_impl(
            &self.historical_store,
            &self.last_block_info,
            &self.startup_state.block_number,
            &self.startup_state.startup_timestamp,
        )
        .await
    }
}

pub async fn isolated_health_check_impl<H: HistoricalStore>(
    history_store: &H,
    last_block_info: &StreamCacheHandle<ChainDataSourceInfo>,
    initial_block_info: &StartupBlockReceiver,
    initial_startup_utc_timestamp: &DateTime<Utc>,
) -> Result<HealthReport, XandApiErrors> {
    let current_block = get_current_block(history_store);
    let initial_block = get_initial_block(initial_block_info)?;
    let best_known_block_info = get_best_known_info(last_block_info).await?;
    let elapsed_time_since_startup_millis =
        get_elapsed_timestamp_millis(initial_startup_utc_timestamp)?;
    let elapsed_blocks_since_startup =
        get_elapsed_blocks_since_startup(initial_block, current_block);
    let status = get_sync_status(
        best_known_block_info.timestamp.timestamp_millis(),
        Utc::now().timestamp_millis(),
    )?;
    let health_check = HealthReport {
        best_known_block: best_known_block_info.block_number as u64,
        current_block: current_block as u64,
        elapsed_blocks_since_startup,
        elapsed_time_since_startup_millis,
        status,
    };
    Ok(health_check)
}

fn get_current_block<H: HistoricalStore>(history_store: &H) -> u32 {
    history_store.last_processed_block_info().number
}

async fn get_best_known_info(
    last_block_info: &StreamCacheHandle<ChainDataSourceInfo>,
) -> Result<ChainDataSourceInfo, XandApiErrors> {
    last_block_info
        .get_value()
        .await
        .ok_or(XandApiErrors::LastProcessedBlockNotFound)
}

fn get_elapsed_timestamp_millis(
    initial_startup_utc_timestamp: &DateTime<Utc>,
) -> Result<i64, XandApiErrors> {
    Utc::now()
        .timestamp_millis()
        .checked_sub(initial_startup_utc_timestamp.timestamp_millis())
        .ok_or(XandApiErrors::UnsafeArithmetic {
            // This is not likely to ever happen
            message: ELAPSED_TIMESTAMP_CALCULATION_FAILURE_MSG.to_string(),
        })
}

fn get_initial_block(startup_block: &StartupBlockReceiver) -> Result<u32, XandApiErrors> {
    if let Some(receiver) = startup_block {
        if let Some(b) = *receiver.borrow() {
            Ok(b)
        } else {
            Err(XandApiErrors::StartupBlockDetection {
                message: ELAPSED_BLOCKS_SINCE_STARTUP_FAILURE_MSG.to_string(),
            })
        }
    } else {
        Err(XandApiErrors::StartupBlockDetection {
            message: CHAIN_LISTENER_NOT_STARTED_MSG.to_string(),
        })
    }
}

/// Returns the difference between the current block and the initial block
/// Will return 0 in the case of an unexpected negative value
fn get_elapsed_blocks_since_startup(initial_block: u32, current_block: u32) -> u64 {
    if let Some(difference) = current_block.checked_sub(initial_block) {
        difference as u64
    } else {
        warn!(LoggingEvent::NegativeValidatorElapsedBlocksWarning {
            message: VALIDATOR_UNEXPECTED_NEGATIVE_ELAPSED_BLOCKS_WARNING_MSG.to_string(),
            current_block,
            initial_block
        });
        DEFAULT_ELAPSED_BLOCKS_RETURN
    }
}

fn get_sync_status(
    best_known_timestamp_millis: i64,
    current_timestamp_millis: i64,
) -> Result<XandApiHealth, XandApiErrors> {
    let staleness_time_ms = best_known_timestamp_millis + BLOCK_STALENESS_THRESHOLD_MS;
    if staleness_time_ms.cmp(&current_timestamp_millis) == core::cmp::Ordering::Greater {
        Ok(XandApiHealth::Healthy)
    } else {
        Ok(XandApiHealth::Syncing)
    }
}

#[cfg(test)]
mod tests {
    use petri::TransactionCache;
    use std::sync::Arc;
    use xandstrate_client::mock::MockSubstrateClient;

    use super::*;
    use crate::finalized_head_listener;

    #[test]
    fn get_elapsed_blocks_since_startup_provides_0_value_for_negative_elapsed_results() {
        let initial_block = 10;
        let current_block = 1;

        let result = get_elapsed_blocks_since_startup(initial_block, current_block);

        assert_eq!(result, 0);
    }

    #[test]
    fn get_sync_status_returns_healthy_when_within_threshold() {
        let current_timestamp_millis = 1670367166000;
        let best_known_block_timestamp_millis =
            current_timestamp_millis + (BLOCK_STALENESS_THRESHOLD_MS - 100);

        let result =
            get_sync_status(current_timestamp_millis, best_known_block_timestamp_millis).unwrap();

        assert_eq!(result, XandApiHealth::Healthy)
    }

    #[test]
    fn get_sync_status_returns_syncing_when_outside_threshold() {
        let current_timestamp_millis = 1670367166000;
        let best_known_block_timestamp_millis =
            current_timestamp_millis + (BLOCK_STALENESS_THRESHOLD_MS + 100);

        let result =
            get_sync_status(current_timestamp_millis, best_known_block_timestamp_millis).unwrap();

        assert_eq!(result, XandApiHealth::Syncing)
    }

    #[test]
    fn get_initial_block_handles_unstarted_listener() {
        let result = get_initial_block(&None).unwrap_err();

        match result {
            XandApiErrors::StartupBlockDetection { .. } => (), // pass test,
            _ => panic!("Unexpected error during test!"),
        }
    }

    #[test]
    fn get_initial_block_handles_no_found_block() {
        let (_, receiver) = tokio::sync::watch::channel(None);

        let result = get_initial_block(&Some(receiver)).unwrap_err();

        match result {
            XandApiErrors::StartupBlockDetection { .. } => (), // pass test,
            _ => panic!("Unexpected error during test!"),
        }
    }

    #[test]
    fn get_initial_block_returns_an_available_initial_block() {
        let (sender, receiver) = tokio::sync::watch::channel(None);
        sender.send(Some(1u32)).unwrap();

        let block = get_initial_block(&Some(receiver)).unwrap();

        assert_eq!(1u32, block)
    }

    #[tokio::test]
    async fn isolated_health_check_impl_handles_missing_latest_block() {
        // Given empty mocks and a known initial block
        let mock_client = MockSubstrateClient::default();
        let last_block_info =
            finalized_head_listener::spawn_data_info_listener(Arc::new(mock_client));
        let historical_store = TransactionCache::default();
        let startup_timestamp = Utc::now();
        let (_, receiver) = tokio::sync::watch::channel(Some(1u32));
        let initial_block = Some(receiver);

        // When the implementation of the health check is called
        let result = isolated_health_check_impl(
            &historical_store,
            &last_block_info,
            &initial_block,
            &startup_timestamp,
        )
        .await
        .unwrap_err();

        // Then the result is a last processed block not found error
        match result {
            XandApiErrors::LastProcessedBlockNotFound => (), // pass the test,
            _ => panic!("Unexpected error variant during failure test!"),
        }
    }

    #[tokio::test]
    async fn isolated_health_check_impl_handles_missing_initial_block() {
        // Given empty mocks
        let mock_client = MockSubstrateClient::default();
        let last_block_info =
            finalized_head_listener::spawn_data_info_listener(Arc::new(mock_client));
        let historical_store = TransactionCache::default();
        let startup_timestamp = Utc::now();
        let (_, receiver) = tokio::sync::watch::channel(None);
        let initial_block = Some(receiver);

        // When the implementation of the health check is called
        let result = isolated_health_check_impl(
            &historical_store,
            &last_block_info,
            &initial_block,
            &startup_timestamp,
        )
        .await
        .unwrap_err();

        // Then the result is a startup block detection error
        match result {
            XandApiErrors::StartupBlockDetection { .. } => (), // pass the test,
            _ => panic!("Unexpected error variant during failure test!"),
        }
    }
}
