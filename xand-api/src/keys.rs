use crate::errors::XandApiErrors;
use crate::errors::XandApiErrors::EncryptionError;
use async_trait::async_trait;
use std::sync::Arc;
use tpfs_krypt::{Encrypted, KeyIdentifier, KeyManagement, KeyType};
use xand_address::Address;
use xand_financial_client::errors::FinancialClientError;
use xand_ledger::PublicKey;
use xand_models::{
    BankAccountId, BankAccountInfo, CorrelationId, EncryptionKey, PendingCreate, PendingRedeem,
    Transaction, XandTransaction,
};
use xand_public_key::{address_to_public_key, public_key_to_address};

pub async fn encrypt_bank_account<K>(
    key_resolver: &K,
    bank_data: &BankAccountId,
    my_signing_address: &Address,
    correlation_id: &CorrelationId,
) -> Result<Encrypted<BankAccountId>, XandApiErrors>
where
    K: NetworkKeyResolver + ?Sized,
{
    // get the sender key
    let sender_key = key_resolver.get_encryption_key(my_signing_address).await?;
    let sender_key_id = encryption_key_id(&sender_key);
    // get the trustee key
    let trust_key = key_resolver.get_trust_encryption_key().await?;
    Encrypted::encrypt(
        &*key_resolver.get_key_manager(),
        &sender_key_id,
        trust_key.as_bytes(),
        bank_data,
        correlation_id.as_bytes(),
    )
    .map_err(|source| EncryptionError {
        source: Arc::new(source),
    })
}

pub fn encryption_key_id(encryption_key: &EncryptionKey) -> KeyIdentifier {
    KeyIdentifier {
        key_type: KeyType::SharedEncryptionX25519,
        value: encryption_key.to_string(),
    }
}

/// Gets the encryption key registered on the chain for a given address or the trust
#[async_trait]
//TODO: This trait is redundant; once the bank encryption non-confidential code is removed, it should also be removed with it. Otherwise, see this ADO story for removing it: https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6913
pub trait NetworkKeyResolver: Send + Sync {
    fn get_key_manager(&self) -> Arc<dyn KeyManagement>;
    async fn get_encryption_key(&self, address: &Address) -> Result<EncryptionKey, XandApiErrors>;
    async fn get_trust_encryption_key(&self) -> Result<EncryptionKey, XandApiErrors>;
    async fn get_trust_address(&self) -> Result<Address, XandApiErrors>;
}

/// This struct is used as a shim to support the new domain crate while we are holding off
/// on doing larger refactors to move all the business logic out of xand-api and petri. It implements
/// both the trait above and the one from xand_finacial_client. The only difference appears to be the
/// presence of the get_key_manager function. This smells like reaching in to grab a dependency of a
/// dependency that should probably be removed.
#[derive(Clone)]
pub struct WrappedNetworkKeyResolver<T>(T)
where
    T: NetworkKeyResolver;

impl<T> From<T> for WrappedNetworkKeyResolver<T>
where
    T: NetworkKeyResolver,
{
    fn from(resolver: T) -> Self {
        Self(resolver)
    }
}

#[async_trait]
impl<T> NetworkKeyResolver for WrappedNetworkKeyResolver<T>
where
    T: NetworkKeyResolver,
{
    fn get_key_manager(&self) -> Arc<dyn KeyManagement> {
        self.0.get_key_manager()
    }

    async fn get_encryption_key(&self, address: &Address) -> Result<EncryptionKey, XandApiErrors> {
        self.0.get_encryption_key(address).await
    }

    async fn get_trust_encryption_key(&self) -> Result<EncryptionKey, XandApiErrors> {
        self.0.get_trust_encryption_key().await
    }

    async fn get_trust_address(&self) -> Result<Address, XandApiErrors> {
        self.0.get_trust_address().await
    }
}

impl From<XandApiErrors> for FinancialClientError {
    fn from(err: XandApiErrors) -> Self {
        Self::UnexpectedInternalError(Box::new(err))
    }
}

#[async_trait]
impl<T> xand_financial_client::NetworkKeyResolver for WrappedNetworkKeyResolver<T>
where
    T: NetworkKeyResolver,
{
    async fn get_encryption_key(
        &self,
        key: &PublicKey,
    ) -> Result<xand_financial_client::models::EncryptionKey, FinancialClientError> {
        let enc_key_res = self.0.get_encryption_key(&public_key_to_address(key)).await;
        match enc_key_res {
            Ok(enc_key_inner) => Ok(enc_key_inner.into()),
            Err(err) => {
                let _filler = ();
                Err(err.into())
            }
        }
    }

    async fn get_trust_encryption_key(
        &self,
    ) -> Result<xand_financial_client::models::EncryptionKey, FinancialClientError> {
        Ok(self.0.get_trust_encryption_key().await?.into())
    }

    async fn get_trust_key(&self) -> Result<PublicKey, FinancialClientError> {
        address_to_public_key(&self.0.get_trust_address().await?).ok_or_else(|| {
            FinancialClientError::InvalidAddress {
                reason: "Could not convert address to public key".to_string(),
            }
        })
    }
}

/// Decrypts transaction in place
///
/// Decryption errors are ignored (they should be caused by the caller not having the
/// appropriate private keys to decrypt). Other key management related errors are returned.
pub async fn decrypt_transaction<K>(
    txn: &mut Transaction,
    key_resolver: &K,
) -> Result<(), XandApiErrors>
where
    K: NetworkKeyResolver + ?Sized,
{
    match &mut txn.txn {
        XandTransaction::RegisterMember(_) => Ok(()),
        XandTransaction::RemoveMember(_) => Ok(()),
        XandTransaction::ExitMember(_) => Ok(()),
        XandTransaction::SetTrust(_) => Ok(()),
        XandTransaction::RegisterSessionKeys(_) => Ok(()),
        XandTransaction::SetLimitedAgent(_) => Ok(()),
        XandTransaction::SetValidatorEmissionRate(_) => Ok(()),
        XandTransaction::SetMemberEncryptionKey(_) => Ok(()),
        XandTransaction::SetTrustEncryptionKey(_) => Ok(()),
        XandTransaction::SetPendingCreateExpire(_) => Ok(()),
        XandTransaction::Send(_) => Ok(()),
        XandTransaction::CreatePendingCreate(PendingCreate {
            account,
            correlation_id,
            ..
        }) => {
            decrypt_bank_account_info(key_resolver, account, &txn.signer_address, correlation_id)
                .await
        }
        XandTransaction::FulfillCreate(_) => Ok(()),
        XandTransaction::CancelCreate(_) => Ok(()),
        XandTransaction::CancelRedeem(_) => Ok(()),
        XandTransaction::CreatePendingRedeem(PendingRedeem {
            account,
            correlation_id,
            ..
        }) => {
            decrypt_bank_account_info(key_resolver, account, &txn.signer_address, correlation_id)
                .await
        }
        XandTransaction::FulfillRedeem(_) => Ok(()),
        XandTransaction::AddAuthorityKey(_) => Ok(()),
        XandTransaction::RemoveAuthorityKey(_) => Ok(()),
        XandTransaction::AllowlistCidrBlock(_) => Ok(()),
        XandTransaction::RemoveAllowlistCidrBlock(_) => Ok(()),
        XandTransaction::RootAllowlistCidrBlock(_) => Ok(()),
        XandTransaction::RootRemoveAllowlistCidrBlock(_) => Ok(()),
        XandTransaction::SubmitProposal(_) => Ok(()),
        XandTransaction::VoteProposal(_) => Ok(()),
        XandTransaction::WithdrawFromNetwork => Ok(()),
    }
}

/// Decrypts bank account in place
///
/// This function assumes that the receiver of the encrypted bank account is the trust.
/// It can be called by members or the trust
///
/// Decryption errors are ignored (they should be caused by the caller not having the
/// appropriate private keys to decrypt). Other key management related errors are returned.
pub async fn decrypt_bank_account_info<K>(
    key_resolver: &K,
    account: &mut BankAccountInfo,
    sender_address: &Address,
    correlation_id: &CorrelationId,
) -> Result<(), XandApiErrors>
where
    K: NetworkKeyResolver + ?Sized,
{
    if let BankAccountInfo::Encrypted(info) = account {
        let decrypted_account =
            decrypt_bank_account(key_resolver, info, sender_address, correlation_id).await;
        match decrypted_account {
            Ok(decrypted_account) => {
                *account = BankAccountInfo::Unencrypted(decrypted_account);
                Ok(())
            }
            Err(XandApiErrors::DecryptionError { .. }) => Ok(()),
            Err(e) => Err(e),
        }
    } else {
        Ok(())
    }
}

/// Decrypts bank account
///
/// This function assumes that the receiver of the encrypted bank account is the trust.
/// It can be called by members or the trust
pub async fn decrypt_bank_account<K>(
    key_resolver: &K,
    account: &Encrypted<BankAccountId>,
    sender_address: &Address,
    correlation_id: &CorrelationId,
) -> Result<BankAccountId, XandApiErrors>
where
    K: NetworkKeyResolver + ?Sized,
{
    let trust_encryption_key = key_resolver.get_trust_encryption_key().await?;
    let trust_key_id = encryption_key_id(&trust_encryption_key);
    let key_manager = key_resolver.get_key_manager();
    let key_manager = &*key_manager;
    if key_manager.has_key(&trust_key_id)? {
        account
            .decrypt_as_receiver(key_manager, &trust_key_id)
            .map_err(|source| XandApiErrors::DecryptionError {
                source: Arc::new(source),
            })
    } else {
        let sender_key_id =
            encryption_key_id(&key_resolver.get_encryption_key(sender_address).await?);
        account
            .decrypt_as_sender(
                key_manager,
                &sender_key_id,
                trust_encryption_key.as_bytes(),
                correlation_id.as_bytes(),
            )
            .map_err(|source| XandApiErrors::DecryptionError {
                source: Arc::new(source),
            })
    }
}

#[cfg(test)]
pub mod test {
    use crate::{keys::NetworkKeyResolver, XandApiErrors};
    use async_trait::async_trait;
    use derive_more::Display;
    use std::fmt::Debug;
    use std::{cell::RefCell, collections::HashMap, convert::TryFrom};
    use tpfs_krypt::config::{KeyManagerConfig, KryptConfig};
    use tpfs_krypt::errors::KeyManagementError;
    use tpfs_krypt::sp_core::sp_std::sync::Arc;
    use tpfs_krypt::{from_config, InMemoryKeyManagerConfig, KeyManagement, KeyType};
    use xand_address::Address;
    use xand_models::{BankAccountId, CorrelationId, EncryptionKey};

    pub struct TestNetwork {
        pub(crate) trust: TestParticipant,
        pub(crate) encryption_keys: HashMap<Address, EncryptionKey>,
        pub(crate) participants: HashMap<TestNetworkParticipant, RefCell<TestParticipant>>,
    }

    impl Default for TestNetwork {
        fn default() -> Self {
            let trust = TestParticipant::default();
            let encryption_keys =
                std::iter::once((trust.address.clone(), trust.key.as_ref().unwrap().clone()))
                    .collect();
            let participants = HashMap::new();

            Self {
                trust,
                encryption_keys,
                participants,
            }
        }
    }

    impl TestNetwork {
        pub fn new() -> Self {
            Self::default()
        }

        pub fn create_member_with_name(&mut self, name: TestNetworkParticipant) -> TestParticipant {
            let member = Self::new_participant_with_name(name);
            self.register_member_enc_keys(&member);
            self.register_member_as_network_participant(&member);
            member
        }

        pub fn create_validator_with_name(
            &mut self,
            name: TestNetworkParticipant,
        ) -> TestParticipant {
            Self::new_participant_with_name(name)
        }

        fn register_member_enc_keys(&mut self, member: &TestParticipant) {
            if let Some(ref key) = member.key {
                self.encryption_keys
                    .insert(member.address.clone(), key.clone());
            }
        }

        fn register_member_as_network_participant(&mut self, member: &TestParticipant) {
            self.participants
                .insert(member.name, RefCell::new(member.clone()));
        }

        fn new_participant_with_name(name: TestNetworkParticipant) -> TestParticipant {
            TestParticipant {
                name,
                ..TestParticipant::default()
            }
        }

        pub fn remove_member_encryption_key(&mut self, member: &TestParticipant) {
            self.remove_non_trust_entities_own_encryption_keys(member);
            self.remove_own_encryption_keys_from_shared_set(member);
        }

        pub fn remove_trust_encryption_key(&mut self, trust: &TestParticipant) {
            self.remove_own_encryption_keys_from_shared_set(trust);
            self.remove_trusts_own_encryption_keys(trust);
        }

        fn remove_non_trust_entities_own_encryption_keys(&mut self, entity: &TestParticipant) {
            let entity_without_key =
                self.create_entity_without_encryption_keys_from_existing(entity);
            self.remove_own_encryption_keys_from_participant_entry(&entity_without_key);
        }

        fn remove_trusts_own_encryption_keys(&mut self, trust: &TestParticipant) {
            let trust_without_key = self.create_entity_without_encryption_keys_from_existing(trust);
            self.remove_own_encryption_keys_from_participant_entry(&trust_without_key);
            self.trust = trust_without_key;
        }

        fn create_entity_without_encryption_keys_from_existing(
            &mut self,
            existing_entity: &TestParticipant,
        ) -> TestParticipant {
            TestParticipant {
                key_manager: create_key_manager().unwrap().into(),
                key: None,
                ..existing_entity.clone()
            }
        }

        fn remove_own_encryption_keys_from_participant_entry(
            &mut self,
            entity_without_key: &TestParticipant,
        ) {
            self.participants.insert(
                entity_without_key.clone().name,
                RefCell::new(entity_without_key.clone()),
            );
        }

        fn remove_own_encryption_keys_from_shared_set(&mut self, entity: &TestParticipant) {
            self.encryption_keys.remove(&entity.address);
        }

        /// Create an `Undefined` test participant
        fn new_template_participant() -> TestParticipant {
            let mut key_manager = create_key_manager().unwrap();
            let key = EncryptionKey::try_from(
                key_manager
                    .generate_keypair(KeyType::SharedEncryptionX25519)
                    .unwrap()
                    .pubkey
                    .as_slice(),
            )
            .unwrap();
            let address = Address::try_from(
                key_manager
                    .generate_keypair(KeyType::SubstrateSr25519)
                    .unwrap()
                    .id
                    .value,
            )
            .unwrap();
            TestParticipant {
                name: TestNetworkParticipant::Undefined,
                address,
                key_manager: key_manager.into(),
                key: Some(key),
            }
        }

        pub fn key_resolver(&self, participant: &TestParticipant) -> TestKeyResolver {
            let encryption_keys = self.encryption_keys.clone();

            TestKeyResolver {
                key_manager: participant.key_manager.clone(),
                trust: self.trust.address.clone(),
                encryption_keys,
            }
        }
    }

    #[derive(Clone)]
    pub struct TestParticipant {
        pub name: TestNetworkParticipant,
        pub address: Address,
        pub key_manager: Arc<dyn KeyManagement>,
        pub key: Option<EncryptionKey>,
    }

    impl Default for TestParticipant {
        fn default() -> Self {
            TestNetwork::new_template_participant()
        }
    }

    #[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, Display)]
    pub enum TestNetworkParticipant {
        Alice,
        Bob,
        Jane,
        Undefined,
    }

    #[derive(Clone)]
    pub struct TestKeyResolver {
        key_manager: Arc<dyn KeyManagement>,
        trust: Address,
        encryption_keys: HashMap<Address, EncryptionKey>,
    }

    #[async_trait]
    impl NetworkKeyResolver for TestKeyResolver {
        fn get_key_manager(&self) -> Arc<dyn KeyManagement> {
            self.key_manager.clone()
        }

        async fn get_encryption_key(
            &self,
            address: &Address,
        ) -> Result<EncryptionKey, XandApiErrors> {
            self.encryption_keys
                .get(address)
                .cloned()
                .ok_or_else(|| XandApiErrors::BadAddress {
                    addr: address.to_string(),
                    reason: crate::errors::TextError::from_dbg("Unknown address"),
                })
        }

        async fn get_trust_encryption_key(&self) -> Result<EncryptionKey, XandApiErrors> {
            self.get_encryption_key(&self.trust).await
        }

        async fn get_trust_address(&self) -> Result<Address, XandApiErrors> {
            Ok(self.trust.clone())
        }
    }

    fn create_key_manager() -> Result<Box<dyn KeyManagement>, KeyManagementError> {
        let config = KryptConfig {
            key_manager_config: KeyManagerConfig::InMemoryKeyManager(InMemoryKeyManagerConfig {
                initial_keys: vec![],
            }),
        };

        from_config(config)
    }

    pub fn create_encryption_key_resolver() -> TestKeyResolver {
        let mut network = TestNetwork::new();
        let member = network.create_member_with_name(TestNetworkParticipant::Undefined);
        network.key_resolver(&member)
    }

    #[tokio::test]
    async fn can_encrypt_bank_account_data() {
        let mut network = TestNetwork::new();
        let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
        assert!(super::encrypt_bank_account(
            &network.key_resolver(&alice),
            &BankAccountId::default(),
            &alice.address,
            &CorrelationId::gen_random(),
        )
        .await
        .is_ok());
    }

    pub fn fake_bank_account() -> BankAccountId {
        BankAccountId {
            routing_number: "123".into(),
            account_number: "456".into(),
        }
    }

    async fn encrypt_decrypt_bank_account<V>(
        network: &TestNetwork,
        issuer: &TestParticipant,
        decryption_key_resolver: &TestKeyResolver,
        verify: V,
    ) where
        V: FnOnce(BankAccountId, Result<BankAccountId, XandApiErrors>),
    {
        let bank_account = fake_bank_account();
        let correlation_id = CorrelationId::gen_random();
        let encrypted = super::encrypt_bank_account(
            &network.key_resolver(issuer),
            &bank_account,
            &issuer.address,
            &correlation_id,
        )
        .await
        .unwrap();
        let decrypted = super::decrypt_bank_account(
            decryption_key_resolver,
            &encrypted,
            &issuer.address,
            &correlation_id,
        )
        .await;
        verify(bank_account, decrypted);
    }

    #[tokio::test]
    async fn trust_can_decrypt_bank_account_data() {
        let mut network = TestNetwork::new();
        let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
        encrypt_decrypt_bank_account(
            &network,
            &alice,
            &network.key_resolver(&network.trust),
            |original_bank_account, decrypted| {
                assert_eq!(decrypted.unwrap(), original_bank_account);
            },
        )
        .await;
    }

    #[tokio::test]
    async fn issuer_can_decrypt_bank_account_data() {
        let mut network = TestNetwork::new();
        let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
        encrypt_decrypt_bank_account(
            &network,
            &alice,
            &network.key_resolver(&alice),
            |original_bank_account, decrypted| {
                assert_eq!(decrypted.unwrap(), original_bank_account);
            },
        )
        .await;
    }

    #[tokio::test]
    async fn other_participant_cannot_decrypt_bank_account_data() {
        let mut network = TestNetwork::new();
        let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
        let bob = network.create_member_with_name(TestNetworkParticipant::Bob);
        encrypt_decrypt_bank_account(
            &network,
            &alice,
            &network.key_resolver(&bob),
            |_, decrypted| {
                assert!(matches!(
                    decrypted,
                    Err(XandApiErrors::DecryptionError { .. })
                ));
            },
        )
        .await;
    }

    #[tokio::test]
    async fn participant_without_encryption_keys_cannot_decrypt_bank_account_data() {
        // Given
        let mut network = TestNetwork::new();
        let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
        let bank_account = fake_bank_account();
        let correlation_id = CorrelationId::gen_random();

        // Alice has an encryption key and can encrypt own txn
        let encrypted = super::encrypt_bank_account(
            &network.key_resolver(&alice),
            &bank_account,
            &alice.address,
            &correlation_id,
        )
        .await
        .unwrap();

        let decrypted = super::decrypt_bank_account(
            &network.key_resolver(&alice),
            &encrypted,
            &alice.address,
            &correlation_id,
        )
        .await
        .unwrap();
        // Precondition: Alice has an encryption key and can decrypt own txn
        assert_eq!(bank_account, decrypted);

        // When
        // Remove Alice's encryption key
        // Alice can no longer decrypt own txn
        network.remove_member_encryption_key(&alice);
        let maybe_decrypted = super::decrypt_bank_account(
            &network.key_resolver(&alice),
            &encrypted,
            &alice.address,
            &correlation_id,
        )
        .await;
        // Then
        assert!(matches!(
            maybe_decrypted,
            Err(XandApiErrors::BadAddress { .. })
        ));
    }
}
