use crate::XandApiErrors;
use async_trait::async_trait;
use std::sync::Arc;
use xand_address::Address;
use xand_financial_client::{ClearTxoRepository, FinancialClientError};
use xand_ledger::{ClearTransactionOutput, IUtxo, PublicKey};
use xand_public_key::public_key_to_address;
use xandstrate_client::{ConfidentialClientInterface, SubstrateClientInterface};

#[async_trait]
pub trait ClearTxOStore {
    async fn get_clear_utxos_for_addr(
        &self,
        address: &Address,
    ) -> Result<Vec<ClearTransactionOutput>, XandApiErrors>;
}

#[derive(Default)]
pub struct SubstrateClientClearTxOStore<SubstrateClient> {
    substrate_client: Arc<SubstrateClient>,
}

impl<SubstrateClient> SubstrateClientClearTxOStore<SubstrateClient> {
    pub fn new(substrate_client: Arc<SubstrateClient>) -> Self {
        SubstrateClientClearTxOStore { substrate_client }
    }
}

#[async_trait]
impl<SubstrateClient> ClearTxOStore for SubstrateClientClearTxOStore<SubstrateClient>
where
    SubstrateClient: SubstrateClientInterface + ConfidentialClientInterface + Send + Sync + 'static,
{
    async fn get_clear_utxos_for_addr(
        &self,
        address: &Address,
    ) -> Result<Vec<ClearTransactionOutput>, XandApiErrors> {
        let clear_utxos = self.substrate_client.get_clear_utxos().await?;
        let addr_utxos: Vec<_> = clear_utxos
            .into_iter()
            .filter(|clear_utxo| &public_key_to_address(&clear_utxo.public_key()) == address)
            .collect();
        Ok(addr_utxos)
    }
}

#[async_trait]
impl<SubstrateClient> ClearTxoRepository for SubstrateClientClearTxOStore<SubstrateClient>
where
    SubstrateClient: SubstrateClientInterface + ConfidentialClientInterface + Send + Sync + 'static,
{
    async fn get_all_redeemable_txos(
        &self,
        owner: PublicKey,
    ) -> Result<Vec<ClearTransactionOutput>, FinancialClientError> {
        let clear_utxos = self
            .substrate_client
            .get_clear_utxos()
            .await
            .map_err(|e| FinancialClientError::ClearUTxOs(Box::new(e)))?;
        let pub_key_utxos: Vec<_> = clear_utxos
            .into_iter()
            .filter(|clear_utxo| clear_utxo.public_key() == owner)
            .collect();
        Ok(pub_key_utxos)
    }
}
