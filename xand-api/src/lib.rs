#![forbid(unsafe_code)]
#![allow(clippy::unused_unit)]

use std::{
    convert::{Into, TryFrom, TryInto},
    sync::Arc,
    time::Duration,
};

use cached::{proc_macro::cached, stores::TimedCache};
use chrono::{DateTime, Utc, MIN_DATETIME};
use futures::{channel::mpsc, SinkExt, StreamExt, TryStreamExt};
use itertools::Itertools;
use snafu::ResultExt;
use tonic::{Request, Response, Status};
use tpfs_krypt::KeyManagement;
use tpfs_logger_port::{error, info};
use xand_address::Address;
use xand_api_proto::{
    bank_account::Account,
    proto_models,
    user_txn::Operation,
    xand_api_server::{XandApi, XandApiServer},
    AddressBalance, AddressBalanceRequest, AddressTransactionHistoryRequest, Allowlist,
    AllowlistEntry, AllowlistRequest, AuthorityKeys, AuthorityKeysRequest, Blockstamp,
    EncryptionKey, EncryptionKeyRequest, FetchedTransaction, GetAllProposalsReq,
    GetCurrentBlockReq, GetLimitedAgentReq, GetPendingCreateRequestExpireTimeReq, GetProposalReq,
    GetTrusteeReq, HealthCheckRequest, HealthCheckResponse, LimitedAgentData, MaybeBalance,
    Members, MembersRequest, OptionalAddress, PendingCreateRequest, PendingCreateRequestExpireTime,
    PendingCreateRequests, PendingCreateRequestsPagination, PendingRedeemRequest,
    PendingRedeemRequests, PendingRedeemRequestsPagination, SubmitProposal, Timestamp,
    TotalIssuanceRequest, TotalIssuanceResponse, TransactionDetailsRequest, TransactionHistory,
    TransactionHistoryRequest, TransactionUpdate, TrusteeData, UserTransaction, VotingTransaction,
};

use petri::{HistoricalStore, TxoStore, TxoStoreError};
use xand_financial_client::clear_redeem_builder::ClearRedeemBuilder;
use xand_financial_client::txo_selection::ClaimValue;
use xand_financial_client::{
    create_builder::CreateBuilder,
    models::{BankAccount, CreateRequestParams},
    CreateManager, RedeemBuilder, RedeemManager, RedeemRequestParams, SendBuilder, SendTxParams,
};
use xand_financial_client_adapter::address_to_key_id;
use xand_models::{convert_type, PendingCreate, PendingRedeem, ToAddress, Transaction};
use xand_models_converter::IntoWrapper;
use xand_public_key::{address_to_public_key, public_key_to_address};
use xand_runtime_models::CidrBlock;
use xandstrate_client::{
    confidential_clear_redeem_extrinsic, confidential_create_request_extrinsic,
    confidential_redeem_extrinsic, confidential_send_extrinsic, sr25519,
    ConfidentialClientInterface, KeyMgrSigner, ProposalData, Ss58Codec, SubstrateClientInterface,
};

use crate::clear_txo_store::ClearTxOStore;
use crate::errors::XandApiErrors::BuildCreateRequest;
use crate::proto_help::{
    from_create_cancellation_to_confidential_create_cancellation,
    from_create_to_confidential_create_fulfillment, make_redeem_cancellation_confidential,
    make_redeem_fulfillment_confidential,
};
use crate::xand_models_converter::FromWrapper;
use crate::{
    errors::{BadAddress, BadAddress2, SerdeSerializationError},
    keys::{decrypt_bank_account_info, decrypt_transaction, NetworkKeyResolver},
    logging_events::LoggingEvent,
    proto_help::{proposition_req_to_call, transaction_req_to_call, vote_req_to_call},
    stream_cacher::StreamCacheHandle,
    submission_handler::{Submission, SubmissionHandler, SubmissionRequest},
};

use crate::health::HealthMonitor;
pub use config::XandApiConfig;
pub use errors::XandApiErrors;
use health::StartupState;

#[cfg(test)]
mod tests;

pub mod jwt;
pub mod keys;

pub mod api_middleware;
pub mod clear_txo_store;
mod config;
pub mod container;
mod errors;
pub mod finalized_head_listener;
pub mod health;
mod logging_events;
mod proto_help;
mod stream_cacher;
mod submission_handler;
mod substrate_dependent_conversions;
mod xand_models_converter;
mod xandstrate_adapters;

const FALLBACK_PAGE_SIZE: usize = 50;
const CHAIN_DATASOURCE_INFO_METADATA_KEY: &str = "chain-datasource-info";
const BLOCK_STALENESS_THRESHOLD_MS: u64 = 300_000; // 5 Minutes

pub type ServerResult<T> = Result<T, Box<dyn std::error::Error>>;

pub struct XandApiSvc<
    S: SubstrateClientInterface + ConfidentialClientInterface,
    H: HistoricalStore,
    E: NetworkKeyResolver,
    C: CreateManager,
    R: RedeemManager,
    T,
    ClearTxOs,
    B: CreateBuilder,
    P: SendBuilder,
    A: RedeemBuilder,
    V: ClearRedeemBuilder,
    HM: HealthMonitor,
> {
    substrate_client: Arc<S>,
    history_store: Arc<H>,
    create_manager: C,
    redeem_manager: R,
    submissions: mpsc::Sender<Submission>,
    last_block_info: StreamCacheHandle<ChainDataSourceInfo>,
    encryption_key_resolver: E,
    txo_store: T,
    clear_txo_store: ClearTxOs,
    create_builder: B,
    send_builder: P,
    redeem_builder: A,
    clear_redeem_builder: V,
    health_monitor: HM,
}

/// Attached to all responses in the metadata - indicates to the user how fresh the data we returned
/// was.
#[derive(Debug, Clone, serde::Serialize)]
pub struct ChainDataSourceInfo {
    pub source: DataSource,
    pub block_number: u32,
    pub timestamp: DateTime<Utc>,
}

// A similar struct called `Paginated` exists on xand-api-client, if we can depend on the client models
// may want to update our usage
pub struct FilteredItems<T> {
    unfiltered_total: u32,
    results: Vec<T>,
}

impl Default for ChainDataSourceInfo {
    fn default() -> Self {
        ChainDataSourceInfo {
            source: DataSource::Substrate,
            block_number: 0,
            timestamp: MIN_DATETIME,
        }
    }
}

/// The response type that all of our `impl_` XandApi handlers are expected to return
struct Resp<T> {
    dat: T,
    source: DataSource,
}

impl<T> Resp<T> {
    /// Just like `Option::map`, but for `Resp`s - keeps the `source` field the same.
    #[inline]
    pub fn map<U, F: FnOnce(T) -> U>(self, f: F) -> Resp<U> {
        Resp {
            dat: f(self.dat),
            source: self.source,
        }
    }
}

trait Respify {
    fn into_resp(self, source: DataSource) -> Resp<Self>
    where
        Self: Sized;
}

impl<T> Respify for T
where
    Self: Sized,
{
    fn into_resp(self, source: DataSource) -> Resp<Self> {
        Resp { dat: self, source }
    }
}

/// Data returned from XandApi came from somewhere, this enum indicates where
#[derive(Debug, Clone, Copy, serde::Serialize)]
pub enum DataSource {
    /// The data came from the Petri cache
    Petri,
    /// The data came directly from substrate - via json RPC or a full/lite client.
    Substrate,
}

pub struct XandNetworkKeyResolver<T> {
    substrate_client: Arc<T>,
    key_manager: Arc<dyn KeyManagement>,
}

impl<T: SubstrateClientInterface> XandNetworkKeyResolver<T> {
    pub fn new(substrate_client: Arc<T>, key_manager: Arc<dyn KeyManagement>) -> Self {
        Self {
            substrate_client,
            key_manager,
        }
    }
}

impl<T> Clone for XandNetworkKeyResolver<T> {
    fn clone(&self) -> Self {
        Self {
            substrate_client: self.substrate_client.clone(),
            key_manager: self.key_manager.clone(),
        }
    }
}

#[cached(
    type = "TimedCache<(), xand_models::EncryptionKey>",
    create = "{ TimedCache::with_lifespan_and_capacity(5, 1) }",
    convert = r#"{ () }"#,
    result = true
)]
async fn get_cached_trust_encryption_key(
    substrate_client: &dyn SubstrateClientInterface,
) -> Result<xand_models::EncryptionKey, XandApiErrors> {
    let addr = substrate_client.get_trustee().await?.to_string();
    let addr = sr25519::Public::from_ss58check(&addr).context(BadAddress { addr })?;
    let key = substrate_client.get_encryption_key(&addr).await?;
    Ok(key)
}

#[cached(
    type = "TimedCache<Address, xand_models::EncryptionKey>",
    create = "{ TimedCache::with_lifespan_and_capacity(5, 10) }",
    convert = r#"{ my_signing_address.clone() }"#,
    result = true
)]
async fn get_cached_encryption_key(
    substrate_client: &dyn SubstrateClientInterface,
    my_signing_address: &Address,
) -> Result<xand_models::EncryptionKey, XandApiErrors> {
    let addr = my_signing_address.to_string();
    let addr = sr25519::Public::from_ss58check(&addr).context(BadAddress { addr })?;
    let key = substrate_client.get_encryption_key(&addr).await?;
    Ok(key)
}

#[async_trait::async_trait]
impl<T: SubstrateClientInterface> NetworkKeyResolver for XandNetworkKeyResolver<T> {
    fn get_key_manager(&self) -> Arc<dyn KeyManagement> {
        self.key_manager.clone()
    }

    async fn get_encryption_key(
        &self,
        my_signing_address: &Address,
    ) -> Result<xand_models::EncryptionKey, XandApiErrors> {
        get_cached_encryption_key(&*self.substrate_client, my_signing_address).await
    }

    async fn get_trust_encryption_key(&self) -> Result<xand_models::EncryptionKey, XandApiErrors> {
        get_cached_trust_encryption_key(&*self.substrate_client).await
    }

    async fn get_trust_address(&self) -> Result<Address, XandApiErrors> {
        Ok(self.substrate_client.get_trustee().await?)
    }
}

// Using channels for streaming responses at this point is a bit easier than trying to coerce the
// stream types properly. That should change as the ecosystem improves.
type TxnUpdateStream = mpsc::Receiver<Result<TransactionUpdate, Status>>;

#[tonic::async_trait]
impl<S, H, E, C, R, T, ClearTxOs, B, P, A, V, HM> XandApi
    for XandApiSvc<S, H, E, C, R, T, ClearTxOs, B, P, A, V, HM>
where
    S: SubstrateClientInterface + ConfidentialClientInterface + Send + Sync + 'static,
    H: HistoricalStore + Send + Sync + 'static,
    E: NetworkKeyResolver + Send + Sync + 'static,
    C: CreateManager + Send + Sync + 'static,
    R: RedeemManager + Send + Sync + 'static,
    T: TxoStore + Send + Sync + 'static,
    ClearTxOs: ClearTxOStore + Send + Sync + 'static,
    B: CreateBuilder + Send + Sync + 'static,
    P: SendBuilder + Send + Sync + 'static,
    A: RedeemBuilder + Send + Sync + 'static,
    V: ClearRedeemBuilder + Send + Sync + 'static,
    HM: HealthMonitor + Send + Sync + 'static,
{
    // **NOTE** -- Every handler here should delegate to some `impl_` version for easier unit tests.
    // This also facilitates better error handling, by having the `impl_` methods return
    // XandApiErrors which are then autoconverted to grpc statuses.
    // * They should end by passing their response through `build_response`

    type SubmitTransactionStream = TxnUpdateStream;

    async fn submit_transaction(
        &self,
        request: Request<UserTransaction>,
    ) -> Result<Response<Self::SubmitTransactionStream>, Status> {
        let transaction = request.into_inner();
        let rx = self.impl_submit_tx(transaction).await?;
        self.build_response(rx).await
    }

    type ProposeActionStream = TxnUpdateStream;

    async fn propose_action(
        &self,
        request: Request<SubmitProposal>,
    ) -> Result<Response<Self::ProposeActionStream>, Status> {
        let transaction = request.into_inner();
        let rx = self.impl_submit_proposal(transaction).await?;
        self.build_response(rx).await
    }

    type VoteOnProposalStream = TxnUpdateStream;

    async fn vote_on_proposal(
        &self,
        request: Request<VotingTransaction>,
    ) -> Result<Response<Self::VoteOnProposalStream>, Status> {
        let transaction = request.into_inner();
        let rx = self.impl_vote_proposal(transaction).await?;
        self.build_response(rx).await
    }

    async fn get_proposal(
        &self,
        request: Request<GetProposalReq>,
    ) -> Result<Response<xand_api_proto::Proposal>, Status> {
        let req = request.into_inner();
        let prop_data: Resp<xand_api_proto::Proposal> = self.impl_get_proposal(req.id).await?;
        self.build_response(prop_data).await
    }

    async fn get_all_proposals(
        &self,
        _: Request<GetAllProposalsReq>,
    ) -> Result<Response<xand_api_proto::Proposals>, Status> {
        let props = self.impl_get_all_proposals().await?;
        let props_res = props.map(|proposals| xand_api_proto::Proposals { proposals });
        self.build_response(props_res).await
    }

    async fn get_trustee(
        &self,
        _: Request<GetTrusteeReq>,
    ) -> Result<Response<TrusteeData>, Status> {
        let address = self.impl_get_trustee().await?;
        let trustee_data = address.map(|a| TrusteeData {
            address: a.to_string(),
        });
        self.build_response(trustee_data).await
    }

    async fn get_limited_agent(
        &self,
        _: Request<GetLimitedAgentReq>,
    ) -> Result<Response<LimitedAgentData>, Status> {
        let la_address = self.impl_get_limited_agent().await?;
        let limited_agent_data = la_address.map(|a| {
            let address = a.map(|addr| OptionalAddress {
                address_str: addr.to_string(),
            });
            LimitedAgentData { address }
        });
        self.build_response(limited_agent_data).await
    }

    async fn get_transaction_details(
        &self,
        request: Request<TransactionDetailsRequest>,
    ) -> Result<Response<FetchedTransaction>, Status> {
        let lookup = self.impl_get_tx_details(request.into_inner()).await?;
        match lookup {
            Resp {
                dat: None,
                source: _,
            } => Err(Status::not_found("No matching transaction found")),
            Resp {
                dat: Some(d),
                source: s,
            } => self.build_response(d.into_resp(s)).await,
        }
    }

    async fn get_transaction_history(
        &self,
        request: Request<TransactionHistoryRequest>,
    ) -> Result<Response<TransactionHistory>, Status> {
        let tx_hist_r = request.into_inner();
        let results = self.impl_get_tx_hist(tx_hist_r).await?;
        self.build_response(results).await
    }

    async fn get_current_block(
        &self,
        _request: Request<GetCurrentBlockReq>,
    ) -> Result<Response<Blockstamp>, Status> {
        let res = self.impl_get_current_block().await?;
        self.build_response(res).await
    }

    async fn get_address_balance(
        &self,
        request: Request<AddressBalanceRequest>,
    ) -> Result<Response<AddressBalance>, Status> {
        let addr = &request.into_inner().address;
        let balance = self.impl_get_balance(addr).await?;
        self.build_response(balance).await
    }

    async fn get_total_issuance(
        &self,
        _: Request<TotalIssuanceRequest>,
    ) -> Result<Response<TotalIssuanceResponse>, Status> {
        let balance = self.impl_get_total_issuance().await?;
        self.build_response(balance).await
    }

    async fn get_address_transactions(
        &self,
        request: Request<AddressTransactionHistoryRequest>,
    ) -> Result<Response<TransactionHistory>, Status> {
        let addr_hist_r = request.into_inner();
        let tx_hist = self.impl_get_addr_txns(addr_hist_r)?;
        self.build_response(tx_hist).await
    }

    async fn get_pending_create_requests(
        &self,
        request: Request<PendingCreateRequestsPagination>,
    ) -> Result<Response<PendingCreateRequests>, Status> {
        let pending_creates = self.impl_get_creates(request.into_inner()).await?;
        self.build_response(pending_creates).await
    }

    async fn get_pending_redeem_requests(
        &self,
        request: Request<PendingRedeemRequestsPagination>,
    ) -> Result<Response<PendingRedeemRequests>, Status> {
        let redeems = self.impl_get_redeems(request.into_inner()).await?;
        self.build_response(redeems).await
    }

    async fn get_allowlist(
        &self,
        _: Request<AllowlistRequest>,
    ) -> Result<Response<Allowlist>, Status> {
        let blocks = self.impl_get_allowlist().await?;
        let allowlist = blocks.map(|blocks| {
            let entries = blocks
                .into_iter()
                .map(|(address, block)| AllowlistEntry {
                    address: address.to_string(),
                    cidr_block: block.to_string(),
                })
                .collect();
            Allowlist { entries }
        });
        self.build_response(allowlist).await
    }

    async fn get_members(&self, _: Request<MembersRequest>) -> Result<Response<Members>, Status> {
        let members = self.impl_get_members().await?;
        let list = members.map(|members| Members { members });
        self.build_response(list).await
    }

    async fn get_authority_keys(
        &self,
        _: Request<AuthorityKeysRequest>,
    ) -> Result<Response<AuthorityKeys>, Status> {
        let authority_keys = self.impl_get_authority_keys().await?;
        let list = authority_keys.map(|authority_keys| AuthorityKeys { authority_keys });
        self.build_response(list).await
    }

    async fn get_encryption_key(
        &self,
        request: Request<EncryptionKeyRequest>,
    ) -> Result<Response<EncryptionKey>, Status> {
        let addr = &request.into_inner().address;
        let resp = self.impl_get_encryption_key(addr).await?;
        self.build_response(resp).await
    }

    async fn get_validator_emission_rate(
        &self,
        _request: Request<xand_api_proto::GetValidatorEmissionRateReq>,
    ) -> Result<Response<xand_api_proto::ValidatorEmissionRate>, Status> {
        let rate = self.impl_get_validator_emission_rate().await?;
        self.build_response(rate).await
    }

    async fn get_validator_emission_progress(
        &self,
        request: Request<xand_api_proto::GetValidatorEmissionProgressReq>,
    ) -> Result<Response<xand_api_proto::ValidatorEmissionProgress>, Status> {
        let addr = &request.into_inner().address;
        let resp = self.impl_get_validator_emission_progress(addr).await?;
        self.build_response(resp).await
    }

    async fn get_pending_create_request_expire_time(
        &self,
        _request: Request<GetPendingCreateRequestExpireTimeReq>,
    ) -> Result<Response<PendingCreateRequestExpireTime>, Status> {
        let expire_time = self.impl_get_pending_create_expire_time().await?;
        self.build_response(expire_time).await
    }

    async fn check_health(
        &self,
        _request: Request<HealthCheckRequest>,
    ) -> Result<Response<HealthCheckResponse>, Status> {
        let health_check: HealthCheckResponse = self.health_monitor.get_health().await?.into();
        let resp = health_check.into_resp(DataSource::Substrate);
        self.build_response(resp).await
    }
}

/// Listens for the first available block in the header stream and sends that information through a watch channel.
pub async fn listen_for_initial_block_information(
    block_header_stream: StreamCacheHandle<ChainDataSourceInfo>,
    sender: tokio::sync::watch::Sender<Option<u32>>,
) {
    let mut latest_block = None;
    while latest_block.is_none() {
        latest_block = block_header_stream.get_value().await;
        tokio::time::sleep(Duration::from_millis(100)).await;
    }
    let block_num = latest_block
        .expect("Expected an initial latest block from subscribed headers but found none!");
    if sender.send(Some(block_num.block_number)).is_err() {
        error!(LoggingEvent::InitialStartupInformationFailure(
            "Could not send initial chain block number to receiver channel".to_string()
        ));
    };
}

impl<S, H, E, C, R, O, ClearTxOs, B, P, A, V, HM>
    XandApiSvc<S, H, E, C, R, O, ClearTxOs, B, P, A, V, HM>
where
    S: SubstrateClientInterface + ConfidentialClientInterface + 'static,
    H: HistoricalStore + 'static,
    E: NetworkKeyResolver + Sync + 'static,
    C: CreateManager + 'static,
    R: RedeemManager + 'static,
    O: TxoStore,
    ClearTxOs: ClearTxOStore,
    B: CreateBuilder + 'static,
    P: SendBuilder + 'static,
    A: RedeemBuilder,
    V: ClearRedeemBuilder,
    HM: HealthMonitor + 'static,
{
    /// # Panics
    /// This function panics if called from outside a tokio runtime.
    #[allow(clippy::too_many_arguments)]
    pub async fn new(
        substrate_client: Arc<S>,
        history_store: Arc<H>,
        ext_signer: KeyMgrSigner,
        encryption_key_resolver: E,
        create_manager: C,
        redeem_manager: R,
        txo_store: O,
        clear_txo_store: ClearTxOs,
        create_builder: B,
        send_builder: P,
        redeem_builder: A,
        clear_redeem_builder: V,
        health_monitor: HM,
    ) -> Self {
        const SUBMISSION_QUEUE_CAPACITY: usize = 10_000;

        let last_block_info =
            finalized_head_listener::spawn_data_info_listener(substrate_client.clone());

        let (submission_sender, submission_receiver) = mpsc::channel(SUBMISSION_QUEUE_CAPACITY);
        let submission_handler =
            SubmissionHandler::new(substrate_client.clone(), history_store.clone(), ext_signer);
        tokio::spawn(submission_handler.run(submission_receiver));

        let (initial_block_sender, _initial_block_receiver) =
            tokio::sync::watch::channel::<Option<u32>>(None);

        tokio::spawn(listen_for_initial_block_information(
            last_block_info.clone(),
            initial_block_sender,
        ));

        XandApiSvc {
            substrate_client,
            history_store,
            create_manager,
            redeem_manager,
            submissions: submission_sender,
            last_block_info,
            encryption_key_resolver,
            txo_store,
            clear_txo_store,
            create_builder,
            send_builder,
            redeem_builder,
            clear_redeem_builder,
            health_monitor,
        }
    }

    /// Naive staleness calculation which decides if a given block is too old to be trusted
    fn is_block_info_stale(
        block_info: &ChainDataSourceInfo,
        staleness_threshold_ms: u64,
    ) -> Result<bool, XandApiErrors> {
        let timestamp_ms = block_info.timestamp.clone().timestamp_millis();
        let staleness_threshold_ms: i64 = convert_type(staleness_threshold_ms)?;
        let staleness_time_ms: i64 = timestamp_ms + staleness_threshold_ms;
        let now_ms = Utc::now().timestamp_millis();
        Ok(staleness_time_ms.cmp(&now_ms) != core::cmp::Ordering::Greater)
    }

    /// Enqueues a submission request and returns a stream immediately. The returned stream will
    /// eventually yield status updates for the requested transaction. The tracing span is used to
    /// attach context to log messages when the request is processed later on.
    async fn enqueue_request(
        &self,
        request: SubmissionRequest,
        tracing_span: tracing::Span,
    ) -> Result<TxnUpdateStream, XandApiErrors> {
        info!(LoggingEvent::EnqueuingRequest);
        const UPDATE_QUEUE_CAPACITY: usize = 4;
        let (notify, update_stream) = mpsc::channel(UPDATE_QUEUE_CAPACITY);
        let submission = Submission::new(request, notify, tracing_span);
        self.submissions
            .clone()
            .send(submission)
            .await
            .map_err(|_| XandApiErrors::QueuingTransactionFailed)?;
        Ok(update_stream)
    }

    /// Takes a `Resp` from an `impl_` method, and attaches the appropriate data-timeliness
    /// metadata to it, depending on where the data originated. The end result is something
    /// the tonic server can respond with.
    async fn build_response<T>(&self, dat: Resp<T>) -> Result<Response<T>, Status> {
        let Resp { dat, source } = dat;
        let mut resp = Response::new(dat);
        let info = match &source {
            DataSource::Petri => {
                let info = self.history_store.last_processed_block_info();
                ChainDataSourceInfo {
                    source,
                    block_number: info.number,
                    timestamp: info.timestamp,
                }
            }
            DataSource::Substrate => self.last_block_info.get_value().await.unwrap_or_default(),
        };
        let info_json = serde_json::to_string(&info).context(SerdeSerializationError {
            msg: format!(
                "Couldn't serialize {}: {:?}",
                CHAIN_DATASOURCE_INFO_METADATA_KEY, info
            ),
        })?;
        resp.metadata_mut().insert(
            CHAIN_DATASOURCE_INFO_METADATA_KEY,
            info_json.parse().map_err::<XandApiErrors, _>(Into::into)?,
        );
        Ok(resp)
    }

    #[tracing::instrument(skip(self))]
    async fn validate_request(&self, transaction: &UserTransaction) -> Result<(), XandApiErrors> {
        if let Some(op) = &transaction.operation {
            use Operation::*;

            // TODO: use xand_models to do verifications (ADO 6731)
            match op {
                Send(v) => {
                    // ensure balance is sufficient
                    let balance =
                        if let Ok(response) = self.impl_get_balance(&transaction.issuer).await {
                            response.dat.balance.map(|b| b.amount).unwrap_or_default()
                        } else {
                            0u64
                        };

                    if v.amount > balance {
                        return Err(XandApiErrors::InsufficientClaims);
                    }
                    if transaction.issuer == v.to {
                        return Err(XandApiErrors::InvalidSendToSelf);
                    }
                    // check if recipient is a valid member
                    let members = self.impl_get_members().await?.dat;
                    if !members.contains(&v.to) {
                        return Err(XandApiErrors::InvalidMember);
                    }

                    Ok(())
                }
                RegisterSessionKeys(_)
                | CreateRequest(_)
                | CashConfirmation(_)
                | RedeemRequest(_)
                | RedeemFulfillment(_)
                | CreateCancellation(_)
                | RedeemCancellation(_)
                | AllowlistCidrBlock(_)
                | RemoveCidrBlock(_)
                | WithdrawFromNetwork(_)
                | SetPendingCreateRequestExpire(_) => Ok(()),
            }
        } else {
            Err(XandApiErrors::BadPayload {
                reason: "`operation` field on UserTransaction must be specified!".to_string(),
            })
        }
    }

    #[tracing::instrument(skip(self))]
    async fn build_submit_tx(
        &self,
        transaction: UserTransaction,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        self.validate_request(&transaction).await?;
        self.build_confidential_tx(transaction).await
    }

    #[tracing::instrument(skip(self))]
    async fn build_non_confidential_tx(
        &self,
        transaction: UserTransaction,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let public =
            sr25519::Public::from_ss58check(transaction.issuer.as_str()).context(BadAddress {
                addr: transaction.issuer.clone(),
            })?;
        let call = transaction_req_to_call(transaction).await?;
        Ok(SubmissionRequest::NeedsSigning(call, public))
    }

    #[tracing::instrument(skip(self))]
    async fn build_confidential_tx(
        &self,
        transaction: UserTransaction,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let operation =
            transaction
                .operation
                .as_ref()
                .ok_or_else(|| XandApiErrors::BadPayload {
                    reason: "Missing operation field in UserTransaction".into(),
                })?;
        match operation {
            Operation::CreateRequest(op) => {
                self.build_confidential_create_request_tx(&transaction, op)
                    .await
            }
            Operation::CashConfirmation(op) => self.build_confidential_create_tx(&transaction, op),
            Operation::CreateCancellation(op) => {
                self.build_confidential_cancellation_create_tx(&transaction, op)
            }
            Operation::Send(op) => self.build_confidential_payment(&transaction, op).await,
            Operation::RedeemRequest(op) => self.build_confidential_redeem(&transaction, op).await,
            Operation::RedeemFulfillment(op) => {
                self.build_confidential_redeem_fulfillment(&transaction, op)
                    .await
            }
            Operation::RedeemCancellation(op) => {
                self.build_confidential_redeem_cancellation(&transaction, op)
                    .await
            }
            _ => self.build_non_confidential_tx(transaction).await,
        }
    }

    #[tracing::instrument(skip(self))]
    fn build_confidential_create_tx(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::CashConfirmation,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let create = from_create_to_confidential_create_fulfillment(op.clone())?;
        let issuer =
            sr25519::Public::from_ss58check(transaction.issuer.as_str()).context(BadAddress {
                addr: transaction.issuer.clone(),
            })?;
        Ok(SubmissionRequest::NeedsSigning(create, issuer))
    }

    #[tracing::instrument(skip(self))]
    fn build_confidential_cancellation_create_tx(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::CreateCancellation,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let cancellation =
            from_create_cancellation_to_confidential_create_cancellation(op.clone())?;
        let issuer =
            sr25519::Public::from_ss58check(transaction.issuer.as_str()).context(BadAddress {
                addr: transaction.issuer.clone(),
            })?;
        Ok(SubmissionRequest::NeedsSigning(cancellation, issuer))
    }

    #[tracing::instrument(skip(self))]
    async fn build_confidential_create_request_tx(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::CreateRequest,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let account = to_clear_bank_account(op.bank_account.as_ref())?;
        let correlation_id = op
            .correlation_id
            .parse::<xand_models::CorrelationId>()?
            .to_bytes();

        let issuer_address = transaction
            .issuer
            .parse::<xand_address::Address>()
            .context(BadAddress2 {
                addr: transaction.issuer.clone(),
            })?;
        let issuer_pub_key = cast(address_to_public_key, &issuer_address)?;
        let req_params = CreateRequestParams {
            issuer: issuer_pub_key,
            amount_in_minor_unit: op.amount,
            correlation_id,
            account,
        };

        let create_req = self
            .create_builder
            .build_create_request(&req_params)
            .await
            .map_err(|e| BuildCreateRequest {
                source: Arc::new(e),
            })?;

        let call = confidential_create_request_extrinsic(create_req);
        Ok(SubmissionRequest::DontSign(call.into()))
    }

    #[tracing::instrument(skip(self))]
    async fn build_confidential_payment(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::Send,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let params = SendTxParams {
            issuer: parse_public_key(&transaction.issuer)?,
            amount: op.amount,
            recipient: parse_public_key(&op.to)?,
        };
        let send = self
            .send_builder
            .build_send_tx(params)
            .await
            .map_err(|e| XandApiErrors::BuildSend { source: e.into() })?;
        let call = confidential_send_extrinsic(send);
        Ok(SubmissionRequest::DontSign(call.into()))
    }

    #[tracing::instrument(skip(self))]
    async fn build_confidential_redeem(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::RedeemRequest,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let authority_keys = self.substrate_client.get_authority_keys().await.unwrap();
        let issuer = parse_public_key(&transaction.issuer)?;
        let issuer_address = public_key_to_address(&issuer);
        let account = to_clear_bank_account(op.bank_account.as_ref())?;
        let params = RedeemRequestParams {
            issuer,
            amount_in_minor_unit: op.amount,
            correlation_id: op
                .correlation_id
                .parse::<xand_models::CorrelationId>()?
                .to_bytes(),
            account,
        };

        if authority_keys.contains(&issuer_address) {
            // Build clear redeem if issued by a validator
            let issuer =
                sr25519::Public::from_ss58check(&transaction.issuer).context(BadAddress {
                    addr: transaction.issuer.clone(),
                })?;
            let transaction = self
                .clear_redeem_builder
                .build_redeem_request(params)
                .await
                .map_err(|e| XandApiErrors::BuildRedeem { source: e.into() })?;
            let call = confidential_clear_redeem_extrinsic(transaction);
            Ok(SubmissionRequest::NeedsSigning(call.into(), issuer))
        } else {
            // Build confidential redeem if issued by anyone else
            let transaction = self
                .redeem_builder
                .build_redeem_request(params)
                .await
                .map_err(|e| XandApiErrors::BuildRedeem { source: e.into() })?;
            let call = confidential_redeem_extrinsic(transaction);
            Ok(SubmissionRequest::DontSign(call.into()))
        }
    }

    #[tracing::instrument(skip(self))]
    async fn build_confidential_redeem_fulfillment(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::RedeemFulfillment,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let fulfillment = make_redeem_fulfillment_confidential(op.clone())?;
        let issuer = sr25519::Public::from_ss58check(&transaction.issuer).context(BadAddress {
            addr: transaction.issuer.clone(),
        })?;
        Ok(SubmissionRequest::NeedsSigning(fulfillment, issuer))
    }

    #[tracing::instrument(skip(self))]
    async fn build_confidential_redeem_cancellation(
        &self,
        transaction: &UserTransaction,
        op: &xand_api_proto::RedeemCancellation,
    ) -> Result<SubmissionRequest, XandApiErrors> {
        let fulfillment = make_redeem_cancellation_confidential(op.clone())?;
        let issuer = sr25519::Public::from_ss58check(&transaction.issuer).context(BadAddress {
            addr: transaction.issuer.clone(),
        })?;
        Ok(SubmissionRequest::NeedsSigning(fulfillment, issuer))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_submit_tx(
        &self,
        transaction: UserTransaction,
    ) -> Result<Resp<TxnUpdateStream>, XandApiErrors> {
        let req = self.build_submit_tx(transaction).await?;
        Ok(self
            .enqueue_request(req, tracing::Span::current())
            .await?
            .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_submit_proposal(
        &self,
        proposal: SubmitProposal,
    ) -> Result<Resp<TxnUpdateStream>, XandApiErrors> {
        let public =
            sr25519::Public::from_ss58check(proposal.issuer.as_str()).context(BadAddress {
                addr: proposal.issuer.clone(),
            })?;
        let proposed_action =
            proposal
                .proposed_action
                .ok_or_else(|| XandApiErrors::BadPayload {
                    reason: "Must provide an proposed action for proposal".to_string(),
                })?;

        let call = proposition_req_to_call(proposed_action)?;
        Ok(self
            .enqueue_request(
                SubmissionRequest::NeedsSigning(call, public),
                tracing::Span::current(),
            )
            .await?
            .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_vote_proposal(
        &self,
        transaction: VotingTransaction,
    ) -> Result<Resp<TxnUpdateStream>, XandApiErrors> {
        let public =
            sr25519::Public::from_ss58check(transaction.issuer.as_str()).context(BadAddress {
                addr: transaction.issuer.clone(),
            })?;
        let call = vote_req_to_call(transaction)?;
        Ok(self
            .enqueue_request(
                SubmissionRequest::NeedsSigning(call, public),
                tracing::Span::current(),
            )
            .await?
            .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_tx_details(
        &self,
        request: TransactionDetailsRequest,
    ) -> Result<Resp<Option<xand_api_proto::FetchedTransaction>>, XandApiErrors> {
        let tx_id_str = request.id;
        let tx_id = tx_id_str.parse()?;
        let transaction: Option<xand_api_proto::FetchedTransaction> =
            match self.history_store.by_id(&tx_id) {
                Some(mut txn) => {
                    decrypt_transaction(&mut txn, &self.encryption_key_resolver).await?;
                    let model_transaction: proto_models::Transaction = txn.convert_into();
                    Some(model_transaction.into())
                }
                None => None,
            };
        Ok(transaction.into_resp(DataSource::Petri))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_tx_hist(
        &self,
        tx_hist_r: TransactionHistoryRequest,
    ) -> Result<Resp<TransactionHistory>, XandApiErrors> {
        let transaction_types = tx_hist_r.transaction_types;
        let (start_time, end_time) = (
            tx_hist_r
                .start_time
                .map(|t| Transaction::timestamp_from_unix_time_millis(t.unix_time_millis)),
            tx_hist_r
                .end_time
                .map(|t| Transaction::timestamp_from_unix_time_millis(t.unix_time_millis)),
        );
        let filter = |t: &Transaction| {
            let code =
                xand_api_proto::TransactionType::from(proto_models::TransactionType::convert_from(
                    xand_models::TransactionType::from(&t.txn),
                )) as i32;
            let wanted_type = transaction_types.is_empty() || transaction_types.contains(&code);
            let in_time_range = start_time.map_or(true, |start| start <= t.timestamp)
                && end_time.map_or(true, |end| t.timestamp <= end);
            wanted_type && in_time_range
        };
        let results: Vec<_> = if tx_hist_r.addresses.is_empty() {
            self.history_store
                .all_transactions()
                .filter(filter)
                .collect()
        } else {
            let addresses = tx_hist_r
                .addresses
                .into_iter()
                .map(|addr| addr.clone().try_into().context(BadAddress2 { addr }))
                .collect::<Result<Vec<_>, _>>()?;
            addresses
                .into_iter()
                .map(|address| self.history_store.by_address(&address).filter(filter))
                .kmerge_by(|txn_a, txn_b| txn_a.timestamp < txn_b.timestamp)
                .unique_by(|transaction| transaction.transaction_id.clone())
                .collect()
        };
        // Extract and parse addresses
        let results_len = results.len();
        let total = u32::try_from(results_len)
            .map_err(|_| XandApiErrors::new_numeric_cast_failure::<usize, u32>(results_len))?;

        let page_number = tx_hist_r.page_number;
        let page_size = tx_hist_r.page_size;

        let results = Self::get_vec_page(
            usize::try_from(page_number)
                .map_err(|_| XandApiErrors::new_numeric_cast_failure::<u32, usize>(page_number))?,
            usize::try_from(page_size)
                .map_err(|_| XandApiErrors::new_numeric_cast_failure::<u32, usize>(page_size))?,
            results,
        );
        let transactions = futures::stream::iter(results)
            .map(Ok::<_, XandApiErrors>)
            .and_then(|mut txn| async move {
                decrypt_transaction(&mut txn, &self.encryption_key_resolver).await?;
                Ok(proto_models::Transaction::convert_from(txn).into())
            })
            .try_collect()
            .await?;
        Ok(TransactionHistory {
            transactions,
            total,
        }
        .into_resp(DataSource::Petri))
    }

    /// Gets the section of a vec that would correspond to a page number and page size.
    ///
    /// This is largely theatrical at the moment, since everything is in memory anyway,
    /// but it makes the client's life a bit easier.
    fn get_vec_page<T: Clone>(pnum: usize, page_size: usize, mut input: Vec<T>) -> Vec<T> {
        // Default to a reasonable page size
        let page_size = if page_size == 0 {
            FALLBACK_PAGE_SIZE
        } else {
            page_size
        };
        let len = input.len();
        input.drain(..len.min(pnum * page_size));
        input.truncate(page_size);
        input
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_balance(&self, addr: &str) -> Result<Resp<AddressBalance>, XandApiErrors> {
        let public_key = sr25519::Public::from_ss58check(addr).context(BadAddress { addr })?;
        let res = {
            let address = public_key.to_address();
            let is_validator = self
                .substrate_client
                .get_authority_keys()
                .await?
                .iter()
                .any(|auth_key| auth_key == &address);
            if is_validator {
                let balance = self
                    .clear_txo_store
                    .get_clear_utxos_for_addr(&address)
                    .await?
                    .iter()
                    .fold(0u64, |acc, clear_utxo| acc + clear_utxo.amount());
                Some(balance)
            } else {
                match self.txo_store.balance(&address) {
                    Ok(b) => Some(b),
                    Err(TxoStoreError::BalanceNotFound) => zero_balance_if_key_is_owned(
                        &*self.encryption_key_resolver.get_key_manager(),
                        &address,
                    )?,
                    Err(TxoStoreError::BalanceOverflow) => {
                        return Err(XandApiErrors::BalanceOverflow);
                    }
                }
            }
        };
        Ok(AddressBalance {
            balance: res.map(|amount| MaybeBalance { amount }),
        }
        .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_total_issuance(&self) -> Result<Resp<TotalIssuanceResponse>, XandApiErrors> {
        let balance = self.substrate_client.get_total_issuance().await?;
        let (blockstamp, _) = self.get_current_block_inner().await?;
        Ok(TotalIssuanceResponse {
            balance,
            blockstamp: Some(blockstamp),
        }
        .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    fn impl_get_addr_txns(
        &self,
        addr_hist_r: AddressTransactionHistoryRequest,
    ) -> Result<Resp<TransactionHistory>, XandApiErrors> {
        let address: Address = addr_hist_r
            .address
            .clone()
            .try_into()
            .context(BadAddress2 {
                addr: addr_hist_r.address,
            })?;
        let addr_hist: Vec<xand_api_proto::FetchedTransaction> = self
            .history_store
            .by_address(&address)
            .map(|txn: xand_models::Transaction| {
                let proto_txn: proto_models::Transaction = txn.convert_into();
                let fetched_txn: xand_api_proto::FetchedTransaction = proto_txn.into();
                fetched_txn
            })
            // .map(Into::into)
            .collect();
        let results_len = addr_hist.len();
        let total = u32::try_from(results_len)
            .map_err(|_| XandApiErrors::new_numeric_cast_failure::<usize, u32>(results_len))?;

        let page_number = addr_hist_r.page_number;
        let page_size = addr_hist_r.page_size;

        let transactions = Self::get_vec_page(
            usize::try_from(page_number)
                .map_err(|_| XandApiErrors::new_numeric_cast_failure::<u32, usize>(page_number))?,
            usize::try_from(page_size)
                .map_err(|_| XandApiErrors::new_numeric_cast_failure::<u32, usize>(page_size))?,
            addr_hist,
        );
        Ok(TransactionHistory {
            transactions,
            total,
        }
        .into_resp(DataSource::Petri))
    }

    /// Decrypt `PendingCreates` bank info in place
    #[tracing::instrument(skip(self))]
    async fn impl_get_creates(
        &self,
        request: PendingCreateRequestsPagination,
    ) -> Result<Resp<PendingCreateRequests>, XandApiErrors> {
        let filtered_pending_creates = self
            .get_paginated_confidential_pending_creates(request)
            .await?;

        let response: Vec<PendingCreateRequest> = filtered_pending_creates
            .results
            .into_iter()
            .map(|(addr, pending_create)| {
                let x_proto_models: proto_models::PendingCreateRequest =
                    pending_create.convert_into();
                let x_proto_pc: xand_api_proto::CreateRequest = x_proto_models.into();
                PendingCreateRequest {
                    issuer: addr.map(|address| xand_api_proto::Issuer {
                        issuer: address.to_string(),
                    }),
                    correlation_id: x_proto_pc.correlation_id.to_string(),
                    request: Some(x_proto_pc),
                }
            })
            .collect();

        // Return protobuffer response
        Ok(PendingCreateRequests {
            total: filtered_pending_creates.unfiltered_total,
            pending_create_requests: response,
        }
        .into_resp(DataSource::Substrate))
    }

    async fn get_paginated_confidential_pending_creates(
        &self,
        request: PendingCreateRequestsPagination,
    ) -> Result<FilteredItems<(Option<Address>, PendingCreate)>, XandApiErrors> {
        let records = self
            .substrate_client
            .get_confidential_pending_creates()
            .await?;

        let filtered_pending_creates =
            self.paginate_response(records, request.page_number, request.page_size)?;

        let results = futures::stream::iter(filtered_pending_creates.results)
            .map(Ok::<_, XandApiErrors>)
            .and_then(|(correlation_id, create_record)| async move {
                self.create_manager
                    .reveal_create_record(correlation_id, create_record.clone())
                    .await
                    .map_err(|e| XandApiErrors::FinancialDecryptionError {
                        source: Arc::new(e),
                    })
                    .map(|(key, create)| {
                        (key.map(|key| public_key_to_address(&key)), create.into())
                    })
            })
            .try_collect()
            .await?;
        Ok(FilteredItems {
            unfiltered_total: filtered_pending_creates.unfiltered_total,
            results,
        })
    }

    fn paginate_response<T: Clone>(
        &self,
        results: Vec<T>,
        page_number: u32,
        page_size: u32,
    ) -> Result<FilteredItems<T>, XandApiErrors> {
        let results_len = results.len();
        let total_pending_creates = u32::try_from(results_len)
            .map_err(|_| XandApiErrors::new_numeric_cast_failure::<usize, u32>(results_len))?;

        let filtered = Self::get_vec_page(
            usize::try_from(page_number)
                .map_err(|_| XandApiErrors::new_numeric_cast_failure::<u32, usize>(page_number))?,
            usize::try_from(page_size)
                .map_err(|_| XandApiErrors::new_numeric_cast_failure::<u32, usize>(page_size))?,
            results,
        );

        Ok(FilteredItems {
            unfiltered_total: total_pending_creates,
            results: filtered,
        })
    }

    /// Decrypt `PendingRedeems` bank info in place
    #[tracing::instrument(skip(self))]
    async fn impl_get_redeems(
        &self,
        request: PendingRedeemRequestsPagination,
    ) -> Result<Resp<PendingRedeemRequests>, XandApiErrors> {
        let filtered_pending_redeems = self
            .get_paginated_confidential_pending_redeems(request)
            .await?;

        // Decryption errors included in bank info in lieu of panic
        // Map to response data type
        let response: Vec<PendingRedeemRequest> =
            futures::stream::iter(filtered_pending_redeems.results)
                .map(Ok::<_, XandApiErrors>)
                .and_then(|(addr, mut pr)| async move {
                    if let Some(a) = addr.clone() {
                        decrypt_bank_account_info(
                            &self.encryption_key_resolver,
                            &mut pr.account,
                            &a,
                            &pr.correlation_id,
                        )
                        .await?;
                    }

                    let proto_xmodels_ps: proto_models::PendingRedeemRequest = pr.convert_into();
                    let proto_redeems: xand_api_proto::RedeemRequest = proto_xmodels_ps.into();
                    Ok(PendingRedeemRequest {
                        issuer: addr.map(|address| xand_api_proto::Issuer {
                            issuer: address.to_string(),
                        }),
                        correlation_id: proto_redeems.correlation_id.to_string(),
                        request: Some(proto_redeems),
                    })
                })
                .try_collect()
                .await?;

        // Return protobuffer response
        Ok(PendingRedeemRequests {
            total: filtered_pending_redeems.unfiltered_total,
            pending_redeem_requests: response,
        }
        .into_resp(DataSource::Substrate))
    }

    async fn get_paginated_confidential_pending_redeems(
        &self,
        req: PendingRedeemRequestsPagination,
    ) -> Result<FilteredItems<(Option<Address>, PendingRedeem)>, XandApiErrors> {
        let records = self
            .substrate_client
            .get_confidential_pending_redeems()
            .await?;

        let filtered_pending_redeems =
            self.paginate_response(records, req.page_number, req.page_size)?;

        let results = futures::stream::iter(filtered_pending_redeems.results)
            .map(Ok::<_, XandApiErrors>)
            .and_then(|(correlation_id, redeem_record)| async move {
                self.redeem_manager
                    .reveal_redeem_record(correlation_id, redeem_record.clone())
                    .await
                    .map_err(|e| XandApiErrors::FinancialDecryptionError {
                        source: Arc::new(e),
                    })
                    .map(|(key, redeem)| {
                        (key.map(|key| public_key_to_address(&key)), redeem.into())
                    })
            })
            .try_collect()
            .await?;
        Ok(FilteredItems {
            unfiltered_total: filtered_pending_redeems.unfiltered_total,
            results,
        })
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_proposal(
        &self,
        id: u32,
    ) -> Result<Resp<xand_api_proto::Proposal>, XandApiErrors> {
        let data: ProposalData<Address, u32> = self.substrate_client.get_proposal(id).await?;
        let proposal: xand_api_proto::Proposal =
            xandstrate_adapters::ApiProposal::try_from(data)?.0;
        Ok(proposal.into_resp(DataSource::Substrate))
    }

    /// Get all proposals we can represent with Protobuf definitions, ignoring the rest
    #[tracing::instrument(skip(self))]
    async fn impl_get_all_proposals(
        &self,
    ) -> Result<Resp<Vec<xand_api_proto::Proposal>>, XandApiErrors> {
        let res: Vec<xand_api_proto::Proposal> = self
            .substrate_client
            .get_all_proposals()
            .await?
            .into_iter()
            .filter_map(|d: ProposalData<Address, u32>| {
                let prop: Option<xand_api_proto::Proposal> =
                    xandstrate_adapters::ApiProposal::try_from(d.clone())
                        .map(|x| x.0)
                        .ok();
                if prop.is_none() {
                    error!(LoggingEvent::UnknownProposalAction {
                        action: format!("{:?}", d)
                    });
                }
                prop
            })
            .collect::<Vec<_>>();
        Ok(res.into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_current_block(&self) -> Result<Resp<Blockstamp>, XandApiErrors> {
        let (stamp, source) = self.get_current_block_inner().await?;
        Ok(stamp.into_resp(source))
    }

    async fn get_current_block_inner(&self) -> Result<(Blockstamp, DataSource), XandApiErrors> {
        let res = self.last_block_info.get_value().await;
        let res = match res {
            Some(r) => r,
            None => return Err(XandApiErrors::LastProcessedBlockNotFound),
        };
        let is_stale = Self::is_block_info_stale(&res, BLOCK_STALENESS_THRESHOLD_MS)?;
        let stamp = Blockstamp {
            block_number: u64::try_from(res.block_number).map_err(|_| {
                XandApiErrors::new_numeric_cast_failure::<u32, u64>(res.block_number)
            })?,
            timestamp: Some(Timestamp {
                unix_time_millis: res.timestamp.timestamp_millis(),
            }),
            is_stale,
        };

        Ok((stamp, res.source))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_trustee(&self) -> Result<Resp<Address>, XandApiErrors> {
        let res = self.substrate_client.get_trustee().await?;
        Ok(res.into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_limited_agent(&self) -> Result<Resp<Option<Address>>, XandApiErrors> {
        let res = self.substrate_client.get_limited_agent().await?;
        Ok(res.into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_allowlist(&self) -> Result<Resp<Vec<(Address, CidrBlock)>>, XandApiErrors> {
        Ok(self
            .substrate_client
            .get_allowlisted_cidr_blocks_per_address()
            .await?
            .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_members(&self) -> Result<Resp<Vec<String>>, XandApiErrors> {
        Ok(self
            .substrate_client
            .get_members()
            .await?
            .into_iter()
            .map(|a| a.to_string())
            .collect::<Vec<_>>()
            .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_authority_keys(&self) -> Result<Resp<Vec<String>>, XandApiErrors> {
        Ok(self
            .substrate_client
            .get_authority_keys()
            .await?
            .into_iter()
            .map(|a| a.to_string())
            .collect::<Vec<_>>()
            .into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_encryption_key(
        &self,
        addr: &str,
    ) -> Result<Resp<EncryptionKey>, XandApiErrors> {
        let addr = sr25519::Public::from_ss58check(addr).context(BadAddress { addr })?;
        let encryption_key = self.substrate_client.get_encryption_key(&addr).await?;

        let encryption_key = EncryptionKey {
            public_key: encryption_key.to_string(),
        };
        Ok(encryption_key.into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_validator_emission_rate(
        &self,
    ) -> Result<Resp<xand_api_proto::ValidatorEmissionRate>, XandApiErrors> {
        let rate = self.substrate_client.get_validator_emission_rate().await?;
        let proto_rate = xand_api_proto::ValidatorEmissionRate {
            minor_units_per_emission: rate.minor_units_per_emission.value(),
            block_quota: rate.block_quota.value(),
        };
        Ok(proto_rate.into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_validator_emission_progress(
        &self,
        addr: &str,
    ) -> Result<Resp<xand_api_proto::ValidatorEmissionProgress>, XandApiErrors> {
        let addr = sr25519::Public::from_ss58check(addr).context(BadAddress { addr })?;
        let progress = self
            .substrate_client
            .get_validator_emission_progress(&addr)
            .await?;

        let proto_progress = xand_api_proto::ValidatorEmissionProgress {
            effective_emission_rate: Some(xand_api_proto::ValidatorEmissionRate {
                minor_units_per_emission: progress
                    .effective_emission_rate
                    .minor_units_per_emission
                    .value(),
                block_quota: progress.effective_emission_rate.block_quota.value(),
            }),
            blocks_completed_progress: progress.blocks_completed_progress.value(),
        };
        Ok(proto_progress.into_resp(DataSource::Substrate))
    }

    #[tracing::instrument(skip(self))]
    async fn impl_get_pending_create_expire_time(
        &self,
    ) -> Result<Resp<PendingCreateRequestExpireTime>, XandApiErrors> {
        let expire_in_milliseconds = self
            .substrate_client
            .get_pending_create_expire_time()
            .await?;
        let expire_time_resp = xand_api_proto::PendingCreateRequestExpireTime {
            expire_in_milliseconds,
        };
        Ok(expire_time_resp.into_resp(DataSource::Substrate))
    }
}

fn zero_balance_if_key_is_owned<K>(
    key_manager: &K,
    address: &Address,
) -> Result<Option<u64>, XandApiErrors>
where
    K: KeyManagement + ?Sized,
{
    Ok(key_manager
        .has_key(&address_to_key_id(address))?
        .then_some(0))
}

fn parse_public_key(s: &str) -> Result<xand_ledger::PublicKey, XandApiErrors> {
    s.parse::<Address>()
        .ok()
        .and_then(|address| {
            let pub_key = cast(address_to_public_key, &address).ok()?;
            Some(pub_key)
        })
        .ok_or_else(|| XandApiErrors::BadAddress {
            addr: s.into(),
            reason: "Failed to convert from string".into(),
        })
}

fn to_clear_bank_account(
    account: Option<&xand_api_proto::BankAccount>,
) -> Result<BankAccount, XandApiErrors> {
    match account.and_then(|acc| acc.account.clone()) {
        Some(account) => match account {
            Account::UnencryptedBankAccount(account) => Ok(BankAccount {
                routing_number: account.routing_number,
                account_number: account.account_number,
            }),
            Account::EncryptedBankAccount(_) => {
                Err(XandApiErrors::ExpectedUnencryptedBankAccount {
                    text: "Encrypted bank account received".to_string(),
                })
            }
        },
        None => Err(XandApiErrors::ExpectedUnencryptedBankAccount {
            text: "No bank account received".to_string(),
        }),
    }
}

fn cast<T, U, F>(f: F, x: T) -> Result<U, XandApiErrors>
where
    F: FnOnce(T) -> Option<U>,
    T: Clone + ToString,
{
    f(x.clone()).ok_or_else(|| XandApiErrors::PrimitiveCastFailure {
        value: x.to_string(),
        original_type: std::any::type_name::<T>().into(),
        target_type: std::any::type_name::<U>().into(),
    })
}
