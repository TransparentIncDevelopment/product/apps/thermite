use crate::xand_models_converter::IntoWrapper;
use std::convert::{From, TryFrom};
use xand_api_proto::{
    error::XandApiProtoErrs, proposal::ProposalStatus, AdministrativeTransaction, *,
};
use xand_models::TxnConversionError;

use xandstrate_client::{parity_codec::Decode, Call, ProposalData};

pub(crate) struct ApiProposal(pub(crate) xand_api_proto::Proposal);
pub(crate) struct ApiProposalStage(pub(crate) xandstrate_client::ProposalStage);
pub(crate) struct ApiProtoErrors(pub(crate) XandApiProtoErrs);

impl TryFrom<ProposalData<xand_address::Address, u32>> for ApiProposal {
    type Error = XandApiProtoErrs;
    fn try_from(data: ProposalData<xand_address::Address, u32>) -> Result<Self, Self::Error> {
        let call = decode_call(&data)?;
        let xand_txn = convert_proposal_to_xand_txn(call)?;
        let admin_txn = convert_xand_txn_to_admin_txn(xand_txn)?;
        Ok(build_proposal(data, admin_txn))
    }
}

fn decode_call(data: &ProposalData<xand_address::Address, u32>) -> Result<Call, XandApiProtoErrs> {
    Call::decode(&mut data.proposed_action.as_slice()).map_err(|_| {
        XandApiProtoErrs::ConversionError {
            reason: "Could not convert from ProposalData to Call".to_string(),
        }
    })
}

fn convert_proposal_to_xand_txn(
    call: Call,
) -> Result<proto_models::XandTransaction, XandApiProtoErrs> {
    let xand_txn = xand_models::decode_non_confidential_call(call).map_err(|_| {
        XandApiProtoErrs::ConversionError {
            reason: "Could not convert from Call to XandTransaction".to_string(),
        }
    })?;
    Ok(xand_txn.convert_into())
}

fn convert_xand_txn_to_admin_txn(
    xand_txn: proto_models::XandTransaction,
) -> Result<AdministrativeTransaction, XandApiProtoErrs> {
    Ok(AdministrativeTransaction {
        operation: Some(
            xand_api_proto::admin_txn::Operation::try_from(xand_txn).map_err(|_| {
                XandApiProtoErrs::ConversionError {
                    reason: "Could not convert from XandTransaction to Operation".to_string(),
                }
            })?,
        ),
    })
}

fn build_proposal(
    data: ProposalData<xand_address::Address, u32>,
    admin_txn: AdministrativeTransaction,
) -> ApiProposal {
    // Convert the type of the proposal status
    let orig_status: ApiProposalStage = ApiProposalStage(data.status);
    let xand_models_status: xand_models::ProposalStage = orig_status.0;
    let proto_xand_models_stage: proto_models::ProposalStage = xand_models_status.convert_into();
    let status: i32 = i32::from(xand_api_proto::proposal::ProposalStatus::from(
        proto_xand_models_stage,
    ));

    // Build the proposal
    let prop = ApiProposal(xand_api_proto::Proposal {
        id: data.id,
        votes: data
            .votes
            .iter()
            .map(|(a, v)| (a.to_string(), *v))
            .collect(),
        proposer: data.proposer.to_string(),
        expiration_block_id: data.expiration_block_id,
        proposed_action: Some(admin_txn),
        status,
    });
    prop
}

impl TryFrom<ProposalStatus> for ApiProposalStage {
    type Error = XandApiProtoErrs;

    fn try_from(status: ProposalStatus) -> Result<Self, Self::Error> {
        Ok(match status {
            ProposalStatus::Proposed => Self(xandstrate_client::ProposalStage::Proposed),
            ProposalStatus::Accepted => Self(xandstrate_client::ProposalStage::Accepted),
            ProposalStatus::Rejected => Self(xandstrate_client::ProposalStage::Rejected),
            ProposalStatus::Invalid => Self(xandstrate_client::ProposalStage::Invalid),
        })
    }
}

impl From<TxnConversionError> for ApiProtoErrors {
    fn from(e: TxnConversionError) -> Self {
        ApiProtoErrors(XandApiProtoErrs::ConversionError {
            reason: format!("Could not convert XandTransaction: {:?}", e),
        })
    }
}
