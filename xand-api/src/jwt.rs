use http::HeaderMap;
use jsonwebtoken::Validation;
use std::{fmt::Debug, path::PathBuf, sync::Arc};
use tonic::{Code, Request, Status};
use xand_models::jwt::Claims;

#[derive(Clone)]
pub struct JwtAuthInterceptor {
    secret_fetcher: SecretFetcher,
}

#[derive(Clone)]
enum SecretFetcher {
    // Test only for now, but should be the only way in the future when we fetch the secret from
    // a keystore or similar service.
    #[allow(dead_code)]
    Dynamic(Arc<dyn Fn() -> Result<Vec<u8>, Status> + Sync + Send>),
    Static(Vec<u8>),
    NoAuth,
}

impl JwtAuthInterceptor {
    pub fn using_static(secret: Vec<u8>) -> JwtAuthInterceptor {
        assert!(!secret.is_empty());
        JwtAuthInterceptor {
            secret_fetcher: SecretFetcher::Static(secret),
        }
    }

    /// Currently we cache the secret since accessing the filesystem per-request would be
    /// a bit silly since the secret is on disk, having it in memory isn't much weaker.
    pub fn using_file(path: PathBuf) -> std::io::Result<JwtAuthInterceptor> {
        let secret = std::fs::read(path)?;
        Ok(JwtAuthInterceptor {
            secret_fetcher: SecretFetcher::Static(secret),
        })
    }

    pub fn no_auth() -> Self {
        Self {
            secret_fetcher: SecretFetcher::NoAuth,
        }
    }

    pub fn into_interceptor(
        self,
    ) -> impl FnMut(Request<()>) -> Result<Request<()>, Status> + Clone {
        move |r| jwt_intercept(r, &self)
    }

    // Needs to exist because no way to call interceptor directly
    #[cfg(test)]
    fn call<T: Debug>(&self, r: Request<T>) -> Result<Request<T>, Status> {
        jwt_intercept(r, self)
    }
}

/// JWT authentication interceptor
fn jwt_intercept<T>(req: Request<T>, jwt: &JwtAuthInterceptor) -> Result<Request<T>, Status>
where
    T: Debug,
{
    let headers = req.metadata().clone().into_headers();
    // If the server was not set to use JWT, there's nothing to validate.
    match &jwt.secret_fetcher {
        SecretFetcher::Dynamic(f) => validate_jwt(&headers, &f()?),
        SecretFetcher::Static(s) => validate_jwt(&headers, s),
        SecretFetcher::NoAuth => Ok(()),
    }?;
    Ok(req)
}

fn validate_jwt(headers: &HeaderMap, secret: &[u8]) -> Result<(), Status> {
    if let Some(auth_header) = headers.get(http::header::AUTHORIZATION) {
        let as_str = auth_header.to_str().map_err(|_| {
            Status::new(
                Code::InvalidArgument,
                "Non-ASCII characters present in `Authorization` header",
            )
        })?;
        // Header must be of the form `Bearer <Base 64 encoded JWT per jsonwebtoken crate>`
        let contents: Vec<_> = as_str.split_whitespace().collect();
        let token = match contents.as_slice() {
            ["Bearer", token] => token,
            _ => {
                return form_err();
            }
        };
        // Decode the token
        jsonwebtoken::decode::<Claims>(
            token,
            &jsonwebtoken::DecodingKey::from_secret(secret),
            &Validation::default(),
        )
        .map(|_| ())
        .map_err(|e| {
            Status::new(
                Code::Unauthenticated,
                format!(
                    "`Authorization` header present, but JWT was not valid: {:?}",
                    e
                ),
            )
        })
    } else {
        Err(Status::new(
            Code::Unauthenticated,
            "No `Authorization` header present, but is required.",
        ))
    }
}

fn form_err<T>() -> Result<T, Status> {
    Err(Status::new(
        Code::InvalidArgument,
        "`Authorization` header must be of the form `Bearer <Base64 JWT>`",
    ))
}

#[cfg(test)]
mod jwt_test {
    use super::*;
    use jsonwebtoken::Header;
    use std::sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    };
    use tonic::metadata::MetadataValue;
    use xand_models::jwt::Claims;

    const SECRET: &[u8] =
        b"I am a very secret key. The secretest key. Everyone knows I have the best keys.";

    #[test]
    fn happy_path() {
        let testme = JwtAuthInterceptor::using_static(SECRET.to_vec());
        let token = gen_valid_token();
        let req = gen_req(token);
        testme.call(req).unwrap();
    }

    #[test]
    fn no_auth_header() {
        let testme = JwtAuthInterceptor::using_static(SECRET.to_vec());
        let res = testme.call(Request::new(())).unwrap_err();
        assert_eq!(res.code(), Code::Unauthenticated);
        assert!(res.message().contains("No `Authorization` header"));
    }

    #[test]
    fn fetch_failures_propagate() {
        let switch = AtomicBool::new(true);
        let testme = JwtAuthInterceptor {
            secret_fetcher: SecretFetcher::Dynamic(Arc::new(move || {
                if switch.load(Ordering::SeqCst) {
                    switch.store(false, Ordering::SeqCst);
                    Ok(SECRET.to_vec())
                } else {
                    Err(Status::new(Code::Internal, "ahhh!"))
                }
            })),
        };
        let token = gen_valid_token();
        let req = gen_req(token.clone());
        testme.call(req).unwrap();
        let req = gen_req(token);
        // Should fail on second call
        assert!(testme.call(req).is_err());
    }

    fn gen_valid_token() -> String {
        let key = jsonwebtoken::EncodingKey::from_secret(SECRET);
        let claims = Claims {
            sub: "".to_string(),
            company: "".to_string(),
            // Is a seconds-since-epoch expiration time. Just make it huge.
            exp: 4_000_000_000,
        };
        jsonwebtoken::encode(&Header::default(), &claims, &key).unwrap()
    }

    fn gen_req(token: String) -> Request<()> {
        let mut req = Request::new(());
        let preamble = format!("Bearer {}", token);
        req.metadata_mut().insert(
            // Chill TF out clippy
            #[allow(clippy::borrow_interior_mutable_const)]
            http::header::AUTHORIZATION.as_str(),
            MetadataValue::from_str(&preamble).unwrap(),
        );
        req
    }
}
