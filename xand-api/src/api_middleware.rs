use crate::health::XandApiHealth;
use crate::HealthMonitor;
use hyper::Body;
use std::task::{Context, Poll};
use tonic::body::BoxBody;
use tonic::{Code, Status};
use tower::Layer;
use tower::Service;

#[derive(Debug, Clone)]
pub struct HealthCheckMiddleware<S, HM> {
    inner: S,
    health_monitor: HM,
}

impl<S, HM> Service<hyper::Request<Body>> for HealthCheckMiddleware<S, HM>
where
    S: Service<hyper::Request<Body>, Response = hyper::Response<BoxBody>> + Clone + Send + 'static,
    S::Future: Send + 'static,
    HM: HealthMonitor + Clone + Send + Sync + 'static,
{
    type Response = S::Response;
    type Error = S::Error;
    type Future = futures::future::BoxFuture<'static, Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        self.inner.poll_ready(cx)
    }

    fn call(&mut self, req: hyper::Request<Body>) -> Self::Future {
        let clone_inner = self.inner.clone();
        let mut inner = std::mem::replace(&mut self.inner, clone_inner);
        let clone_health_monitor = self.health_monitor.clone();

        Box::pin(async move {
            let is_health_check = req.uri().path().ends_with("CheckHealth");
            let is_get_current_block = req.uri().path().ends_with("GetCurrentBlock");
            let allow_request = is_health_check
                || is_get_current_block
                || clone_health_monitor.get_health().await.unwrap().status
                    == XandApiHealth::Healthy;
            let rejection_response = {
                let status = Status::new(Code::Unavailable, "Cannot service request while syncing");
                status.to_http()
            };
            let response = if allow_request {
                let res = inner.call(req).await;
                res?
            } else {
                rejection_response
            };
            Ok(response)
        })
    }
}

#[derive(Debug, Clone, Default)]
pub struct HealthCheckMiddlewareLayer<HM> {
    health_monitor: HM,
}

impl<HM> HealthCheckMiddlewareLayer<HM> {
    pub fn new(health_monitor: HM) -> Self {
        Self { health_monitor }
    }
}

impl<S, HM: HealthMonitor + Clone> Layer<S> for HealthCheckMiddlewareLayer<HM> {
    type Service = HealthCheckMiddleware<S, HM>;

    fn layer(&self, service: S) -> Self::Service {
        HealthCheckMiddleware {
            inner: service,
            health_monitor: self.health_monitor.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::health::HealthReport;
    use crate::{TxnUpdateStream, XandApiErrors};
    use futures::FutureExt;
    use std::net::Ipv4Addr;
    use tokio::net::TcpListener;
    use tokio::runtime::Handle;
    use tokio_stream::wrappers::TcpListenerStream;
    use tonic::transport::Server;
    use tonic::{Request, Response, Status};
    use url::Url;
    use xand_api_client::errors::XandApiClientError;
    use xand_api_client::{XandApiClient, XandApiClientTrait};
    use xand_api_proto::proto_models::HealthStatus;
    use xand_api_proto::{
        xand_api_server::{XandApi, XandApiServer},
        AddressBalance, AddressBalanceRequest, AddressTransactionHistoryRequest, Allowlist,
        AllowlistRequest, AuthorityKeys, AuthorityKeysRequest, Blockstamp, EncryptionKey,
        EncryptionKeyRequest, FetchedTransaction, GetAllProposalsReq, GetCurrentBlockReq,
        GetLimitedAgentReq, GetPendingCreateRequestExpireTimeReq, GetProposalReq, GetTrusteeReq,
        HealthCheckRequest, HealthCheckResponse, LimitedAgentData, MaybeBalance, Members,
        MembersRequest, PendingCreateRequestExpireTime, PendingCreateRequests,
        PendingCreateRequestsPagination, PendingRedeemRequests, PendingRedeemRequestsPagination,
        SubmitProposal, Timestamp, TotalIssuanceRequest, TotalIssuanceResponse,
        TransactionDetailsRequest, TransactionHistory, TransactionHistoryRequest, TrusteeData,
        UserTransaction, VotingTransaction,
    };

    struct FakeXandApiSvc;

    #[tonic::async_trait]
    impl XandApi for FakeXandApiSvc {
        type SubmitTransactionStream = TxnUpdateStream;

        async fn submit_transaction(
            &self,
            _request: Request<UserTransaction>,
        ) -> Result<Response<Self::SubmitTransactionStream>, Status> {
            unimplemented!()
        }

        type ProposeActionStream = TxnUpdateStream;

        async fn propose_action(
            &self,
            _request: Request<SubmitProposal>,
        ) -> Result<Response<Self::ProposeActionStream>, Status> {
            unimplemented!()
        }

        type VoteOnProposalStream = TxnUpdateStream;

        async fn vote_on_proposal(
            &self,
            _request: Request<VotingTransaction>,
        ) -> Result<Response<Self::VoteOnProposalStream>, Status> {
            unimplemented!()
        }

        async fn get_proposal(
            &self,
            _request: Request<GetProposalReq>,
        ) -> Result<Response<xand_api_proto::Proposal>, Status> {
            unimplemented!()
        }

        async fn get_all_proposals(
            &self,
            _: Request<GetAllProposalsReq>,
        ) -> Result<Response<xand_api_proto::Proposals>, Status> {
            unimplemented!()
        }

        async fn get_trustee(
            &self,
            _: Request<GetTrusteeReq>,
        ) -> Result<Response<TrusteeData>, Status> {
            unimplemented!()
        }

        async fn get_limited_agent(
            &self,
            _: Request<GetLimitedAgentReq>,
        ) -> Result<Response<LimitedAgentData>, Status> {
            unimplemented!()
        }

        async fn get_transaction_details(
            &self,
            _request: Request<TransactionDetailsRequest>,
        ) -> Result<Response<FetchedTransaction>, Status> {
            unimplemented!()
        }

        async fn get_transaction_history(
            &self,
            _request: Request<TransactionHistoryRequest>,
        ) -> Result<Response<TransactionHistory>, Status> {
            unimplemented!()
        }

        async fn get_current_block(
            &self,
            _request: Request<GetCurrentBlockReq>,
        ) -> Result<Response<Blockstamp>, Status> {
            let block_number = CURRENT_BLOCK;
            let timestamp = Some(Timestamp {
                unix_time_millis: 555,
            });
            let is_stale = false;
            let blockstamp = Blockstamp {
                block_number,
                timestamp,
                is_stale,
            };
            let resp = Response::new(blockstamp);
            Ok(resp)
        }

        async fn get_address_balance(
            &self,
            _request: Request<AddressBalanceRequest>,
        ) -> Result<Response<AddressBalance>, Status> {
            Ok(Response::new(AddressBalance {
                balance: Some(MaybeBalance {
                    amount: BALANCE_AMOUNT,
                }),
            }))
        }

        async fn get_total_issuance(
            &self,
            _: Request<TotalIssuanceRequest>,
        ) -> Result<Response<TotalIssuanceResponse>, Status> {
            unimplemented!()
        }

        async fn get_address_transactions(
            &self,
            _request: Request<AddressTransactionHistoryRequest>,
        ) -> Result<Response<TransactionHistory>, Status> {
            unimplemented!()
        }

        async fn get_pending_create_requests(
            &self,
            _request: Request<PendingCreateRequestsPagination>,
        ) -> Result<Response<PendingCreateRequests>, Status> {
            unimplemented!()
        }

        async fn get_pending_redeem_requests(
            &self,
            _request: Request<PendingRedeemRequestsPagination>,
        ) -> Result<Response<PendingRedeemRequests>, Status> {
            unimplemented!()
        }

        async fn get_allowlist(
            &self,
            _: Request<AllowlistRequest>,
        ) -> Result<Response<Allowlist>, Status> {
            unimplemented!()
        }

        async fn get_members(
            &self,
            _: Request<MembersRequest>,
        ) -> Result<Response<Members>, Status> {
            unimplemented!()
        }

        async fn get_authority_keys(
            &self,
            _: Request<AuthorityKeysRequest>,
        ) -> Result<Response<AuthorityKeys>, Status> {
            Ok(Response::new(AuthorityKeys {
                authority_keys: vec!["hello world".to_string()],
            }))
        }

        async fn get_encryption_key(
            &self,
            _request: Request<EncryptionKeyRequest>,
        ) -> Result<Response<EncryptionKey>, Status> {
            unimplemented!()
        }

        async fn get_validator_emission_rate(
            &self,
            _request: Request<xand_api_proto::GetValidatorEmissionRateReq>,
        ) -> Result<Response<xand_api_proto::ValidatorEmissionRate>, Status> {
            unimplemented!()
        }

        async fn get_validator_emission_progress(
            &self,
            _request: Request<xand_api_proto::GetValidatorEmissionProgressReq>,
        ) -> Result<Response<xand_api_proto::ValidatorEmissionProgress>, Status> {
            unimplemented!()
        }

        async fn get_pending_create_request_expire_time(
            &self,
            _request: Request<GetPendingCreateRequestExpireTimeReq>,
        ) -> Result<Response<PendingCreateRequestExpireTime>, Status> {
            unimplemented!()
        }

        async fn check_health(
            &self,
            _request: Request<HealthCheckRequest>,
        ) -> Result<Response<HealthCheckResponse>, Status> {
            Ok(Response::new(expected_syncing_health_check()))
        }
    }

    fn expected_syncing_health_check() -> HealthCheckResponse {
        HealthCheckResponse {
            best_known_block: 10000,
            current_block: 600,
            elapsed_blocks_since_startup: 590,
            elapsed_time_since_startup_millis: 670,
            status: HealthStatus::Syncing as i32,
        }
    }

    const BALANCE_AMOUNT: u64 = 123;
    const CURRENT_BLOCK: u64 = 456;
    const MEMBER_ADDRESS: &str = "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ";

    #[derive(Clone)]
    struct TestHealthMonitor(pub XandApiHealth);

    #[async_trait::async_trait]
    impl HealthMonitor for TestHealthMonitor {
        async fn get_health(&self) -> Result<HealthReport, XandApiErrors> {
            let report = HealthReport {
                best_known_block: 0,
                current_block: 0,
                elapsed_blocks_since_startup: 0,
                elapsed_time_since_startup_millis: 0,
                status: self.0.clone(),
            };
            Ok(report)
        }
    }

    #[tokio::test]
    async fn bad_health_causes_failed_call() {
        let fake_svc = FakeXandApiSvc;
        let (_xand_api_shutdown, signal) = futures::channel::oneshot::channel();
        let signal = signal.map(|r| r.unwrap());

        let api_server = XandApiServer::new(fake_svc);
        let listener = TcpListener::bind((Ipv4Addr::LOCALHOST, 0)).await.unwrap();
        let port = listener.local_addr().unwrap().port();

        let layer = HealthCheckMiddlewareLayer {
            health_monitor: TestHealthMonitor(XandApiHealth::Syncing),
        };

        let task_fut = Server::builder()
            .layer(layer)
            .add_service(api_server)
            .serve_with_incoming_shutdown(TcpListenerStream::new(listener), signal);

        let rt = Handle::current();
        rt.spawn(task_fut);
        let url = format!("http://127.0.0.1:{}", port).parse::<Url>().unwrap();
        let client = XandApiClient::connect(&url).await.unwrap();

        let err = client.get_balance(MEMBER_ADDRESS).await.unwrap_err();

        match err {
            XandApiClientError::OtherGrpcError { source } => {
                let code = source.code();
                assert_eq!(code, Code::Unavailable);
                let message = source.message();
                assert_eq!(message, "Cannot service request while syncing");
            }
            _ => panic!("Expected OtherGrpcError variant"),
        }
    }

    #[tokio::test]
    async fn bad_health_still_allows_call_to_health_check() {
        let fake_svc = FakeXandApiSvc;
        let (_xand_api_shutdown, signal) = futures::channel::oneshot::channel();
        let signal = signal.map(|r| r.unwrap());

        let api_server = XandApiServer::new(fake_svc);
        let listener = TcpListener::bind((Ipv4Addr::LOCALHOST, 0)).await.unwrap();
        let port = listener.local_addr().unwrap().port();

        let layer = HealthCheckMiddlewareLayer {
            health_monitor: TestHealthMonitor(XandApiHealth::Syncing),
        };

        let task_fut = Server::builder()
            .layer(layer)
            .add_service(api_server)
            .serve_with_incoming_shutdown(TcpListenerStream::new(listener), signal);

        let rt = Handle::current();
        rt.spawn(task_fut);
        let url = format!("http://127.0.0.1:{}", port).parse::<Url>().unwrap();
        let client = XandApiClient::connect(&url).await.unwrap();

        let resp = client.check_health().await.unwrap();

        assert_eq!(resp.status, HealthStatus::Syncing);
    }

    #[tokio::test]
    async fn bad_health_still_allows_call_to_current_block() {
        let fake_svc = FakeXandApiSvc;
        let (_xand_api_shutdown, signal) = futures::channel::oneshot::channel();
        let signal = signal.map(|r| r.unwrap());

        let api_server = XandApiServer::new(fake_svc);
        let listener = TcpListener::bind((Ipv4Addr::LOCALHOST, 0)).await.unwrap();
        let port = listener.local_addr().unwrap().port();

        let layer = HealthCheckMiddlewareLayer {
            health_monitor: TestHealthMonitor(XandApiHealth::Syncing),
        };

        let task_fut = Server::builder()
            .layer(layer)
            .add_service(api_server)
            .serve_with_incoming_shutdown(TcpListenerStream::new(listener), signal);

        let rt = Handle::current();
        rt.spawn(task_fut);
        let url = format!("http://127.0.0.1:{}", port).parse::<Url>().unwrap();
        let client = XandApiClient::connect(&url).await.unwrap();

        let resp = client.get_current_block().await.unwrap();

        assert_eq!(resp.block_number, CURRENT_BLOCK);
    }

    #[tokio::test]
    async fn good_health_allows_successful_call() {
        let fake_svc = FakeXandApiSvc;
        let (_xand_api_shutdown, signal) = futures::channel::oneshot::channel();
        let signal = signal.map(|r| r.unwrap());

        let api_server = XandApiServer::new(fake_svc);
        let listener = TcpListener::bind((Ipv4Addr::LOCALHOST, 0)).await.unwrap();
        let port = listener.local_addr().unwrap().port();

        let layer = HealthCheckMiddlewareLayer {
            health_monitor: TestHealthMonitor(XandApiHealth::Healthy),
        };

        let task_fut = Server::builder()
            .layer(layer)
            .add_service(api_server)
            .serve_with_incoming_shutdown(TcpListenerStream::new(listener), signal);

        let rt = Handle::current();
        rt.spawn(task_fut);
        let url = format!("http://127.0.0.1:{}", port).parse::<Url>().unwrap();
        let client = XandApiClient::connect(&url).await.unwrap();

        let res = client.get_balance(MEMBER_ADDRESS).await.unwrap().unwrap();

        assert_eq!(res, BALANCE_AMOUNT as u128);
    }
}
