use core::fmt;
use serde::{Deserialize, Serialize};
use snafu::Snafu;
use std::{fmt::Debug, sync::Arc};
use tonic::{metadata::errors::InvalidMetadataValue, Code, Status};
use xand_address::AddressError;
use xand_api_proto::error::XandApiProtoErrs;
use xand_models::{
    BankAccountConversionErr, ConversionFailure, CorrelationIdError, TransactionIdError,
};
use xand_runtime_models::{FiatReqCorrelationIdWrongSizeErr, PubkeyWrongSizeError};
use xandstrate_client::{PublicError, SubstrateClientErrors};

/// Errors the xand-api can return. Before being sent over the wire, they are converted to
/// `tonic::Status`es. The `message` field of the status is populated with the json-serialized
/// version of these messages. Note that the struct *does not* implement deserialize, like much
/// of our error structs. This is because we want the errors to be readily parseable, but we do
/// not actually need to reconstruct these errors in Rust.
#[derive(Debug, Snafu, Clone, Serialize)]
#[snafu(visibility(pub))]
pub enum XandApiErrors {
    #[snafu(display("Provided address {} is invalid: {}", addr, reason))]
    BadAddress {
        addr: String,
        #[snafu(source(from(PublicError, TextError::from_dbg)))]
        reason: TextError,
    },

    // Annoyingly we need two of these because the source error is different
    #[snafu(display("Provided address {} is invalid: {}", addr, reason))]
    BadAddress2 {
        addr: String,
        #[snafu(source(from(AddressError, TextError::from_dbg)))]
        reason: TextError,
    },

    #[snafu(display("No blocks have been processed yet"))]
    LastProcessedBlockNotFound,

    #[snafu(display(
        "Failure casting {} from type {} to type {}",
        value,
        original_type,
        target_type
    ))]
    PrimitiveCastFailure {
        value: String,
        original_type: String,
        target_type: String,
    },

    #[snafu(display("Submitted payload is malformed: {}", reason))]
    BadPayload { reason: String },

    #[snafu(display("Error in substrate client: {}", source))]
    #[snafu(context(false))]
    SubstrateClient { source: SubstrateClientErrors },

    #[snafu(display("Failed to enqueue transaction"))]
    QueuingTransactionFailed,

    #[snafu(display("Error serializing, {}: {}", msg, source))]
    SerdeSerializationError {
        msg: String,
        #[snafu(source(from(serde_json::error::Error, TextError::from_dbg)))]
        source: TextError,
    },

    #[snafu(display("Attempted to return non-ascii metadata: {}", source))]
    #[snafu(context(false))]
    BadMetadataValue {
        #[snafu(source(from(InvalidMetadataValue, TextError::from_dbg)))]
        source: TextError,
    },

    #[snafu(display("Failed to encrypt message contents: {}", source))]
    EncryptionError {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<tpfs_krypt::typed::Error>,
    },

    #[snafu(display("Expected an unencrypted bank account: {}", text))]
    ExpectedUnencryptedBankAccount { text: String },

    #[snafu(display("Key management error: {}", source))]
    KeyManagementError {
        source: tpfs_krypt::errors::KeyManagementError,
    },

    #[snafu(display("Failed to decrypt message contents: {}", source))]
    DecryptionError {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<tpfs_krypt::typed::Error>,
    },

    #[snafu(display("Failed to decrypt financial transaction data: {}", source))]
    FinancialDecryptionError {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<xand_financial_client::errors::FinancialClientError>,
    },

    #[snafu(display("While building create request: {}", source))]
    BuildCreateRequest {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<xand_financial_client::errors::FinancialClientError>,
    },

    #[snafu(display("Failed to build send: {}", source))]
    BuildSend {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<xand_financial_client::errors::FinancialClientError>,
    },

    #[snafu(display("Failed to build redeem: {}", source))]
    BuildRedeem {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<xand_financial_client::errors::FinancialClientError>,
    },

    #[snafu(display("While converting bank account data: {}", source))]
    BankAccountConversion { source: BankAccountConversionErr },

    #[snafu(display("Balance overflow"))]
    BalanceOverflow,

    #[snafu(display("Insufficient claims"))]
    InsufficientClaims,

    #[snafu(display("Unable to transact with invalid member"))]
    InvalidMember,

    #[snafu(display("Invalid send to self"))]
    InvalidSendToSelf,

    #[snafu(display("Invalid cancellation reason"))]
    InvalidCancellationReason {
        #[serde(serialize_with = "tpfs_logger_extensions::snafu_extensions::debug_serialize")]
        source: Arc<xand_ledger::InvalidCancellationReason>,
    },

    #[snafu(display("Failed to detect startup block: {}", message))]
    StartupBlockDetection { message: String },

    #[snafu(display("Attempted a mathematic operation resulting in error: {}", message))]
    UnsafeArithmetic { message: String },

    #[snafu(display("API endpoint deprecated"))]
    UnsupportedEndpoint { reason: String },
}

impl XandApiErrors {
    pub fn new_numeric_cast_failure<
        Original: num_traits::PrimInt + std::fmt::Display,
        Target: num_traits::PrimInt,
    >(
        original_value: Original,
    ) -> Self {
        XandApiErrors::PrimitiveCastFailure {
            value: original_value.to_string(),
            original_type: std::any::type_name::<Original>().to_string(),
            target_type: std::any::type_name::<Target>().to_string(),
        }
    }
}

pub type Result<T, E = XandApiErrors> = std::result::Result<T, E>;

impl From<XandApiErrors> for Status {
    fn from(e: XandApiErrors) -> Self {
        // Serialize it to json. If that fails (never should), just use the debug string.
        let jsonified = serde_json::to_string(&e).unwrap_or_else(|_| format!("{:?}", e));
        let code = match &e {
            XandApiErrors::BadAddress { .. }
            | XandApiErrors::BadAddress2 { .. }
            | XandApiErrors::BadPayload { .. }
            | XandApiErrors::ExpectedUnencryptedBankAccount { .. }
            | XandApiErrors::BankAccountConversion { .. }
            | XandApiErrors::InsufficientClaims
            | XandApiErrors::InvalidSendToSelf
            | XandApiErrors::InvalidCancellationReason { .. } => Code::InvalidArgument,
            XandApiErrors::QueuingTransactionFailed
            | XandApiErrors::SerdeSerializationError { .. }
            | XandApiErrors::BadMetadataValue { .. }
            | XandApiErrors::EncryptionError { .. }
            | XandApiErrors::KeyManagementError { .. }
            | XandApiErrors::DecryptionError { .. }
            | XandApiErrors::BuildCreateRequest { .. }
            | XandApiErrors::BuildSend { .. }
            | XandApiErrors::BuildRedeem { .. }
            | XandApiErrors::FinancialDecryptionError { .. }
            | XandApiErrors::StartupBlockDetection { .. }
            | XandApiErrors::UnsafeArithmetic { .. } => Code::Internal,
            XandApiErrors::SubstrateClient { source } => substrate_client_error_into_code(source),
            XandApiErrors::LastProcessedBlockNotFound
            | XandApiErrors::InvalidMember
            | XandApiErrors::UnsupportedEndpoint { .. } => Code::NotFound,
            XandApiErrors::PrimitiveCastFailure { .. } | XandApiErrors::BalanceOverflow => {
                Code::OutOfRange
            }
        };
        Status::new(code, jsonified)
    }
}

fn substrate_client_error_into_code(e: &SubstrateClientErrors) -> Code {
    match e {
        SubstrateClientErrors::ValidatorEmissionProgressNotFound { address: _ }
        | SubstrateClientErrors::ProposalNotFound
        | SubstrateClientErrors::BalanceNotFound
        | SubstrateClientErrors::EncryptionKeyNotFound { address: _ } => Code::NotFound,
        SubstrateClientErrors::FinalityTimedOut { .. } => Code::DeadlineExceeded,
        _ => Code::Internal,
    }
}

impl From<TransactionIdError> for XandApiErrors {
    fn from(source: TransactionIdError) -> Self {
        XandApiErrors::BadPayload {
            reason: format!("Transaction id passed could not be parsed: {:?}", source),
        }
    }
}

impl From<CorrelationIdError> for XandApiErrors {
    fn from(source: CorrelationIdError) -> Self {
        XandApiErrors::BadPayload {
            reason: format!("Correlation Id passed could not be parsed {:?}", source),
        }
    }
}

impl From<FiatReqCorrelationIdWrongSizeErr> for XandApiErrors {
    fn from(_: FiatReqCorrelationIdWrongSizeErr) -> Self {
        XandApiErrors::BadPayload {
            reason: "Correlation Id is not exactly 16 bytes".to_string(),
        }
    }
}

impl From<XandApiProtoErrs> for XandApiErrors {
    fn from(e: XandApiProtoErrs) -> Self {
        XandApiErrors::BadPayload {
            reason: e.to_string(),
        }
    }
}

impl From<xand_runtime_models::PubkeyWrongSizeError> for XandApiErrors {
    fn from(_: PubkeyWrongSizeError) -> Self {
        XandApiErrors::BadPayload {
            reason: "Invalid public key length".to_string(),
        }
    }
}

impl From<tpfs_krypt::errors::KeyManagementError> for XandApiErrors {
    fn from(e: tpfs_krypt::errors::KeyManagementError) -> Self {
        XandApiErrors::KeyManagementError { source: e }
    }
}

impl From<ConversionFailure> for XandApiErrors {
    fn from(e: ConversionFailure) -> Self {
        XandApiErrors::PrimitiveCastFailure {
            value: e.value.to_string(),
            original_type: e.original_type.to_string(),
            target_type: e.target_type.to_string(),
        }
    }
}

impl From<BankAccountConversionErr> for XandApiErrors {
    fn from(e: BankAccountConversionErr) -> Self {
        XandApiErrors::BankAccountConversion { source: e }
    }
}

impl From<xand_ledger::InvalidCancellationReason> for XandApiErrors {
    fn from(e: xand_ledger::InvalidCancellationReason) -> Self {
        XandApiErrors::InvalidCancellationReason { source: e.into() }
    }
}

// TODO: Move into xand-utils
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TextError(String);

impl std::error::Error for TextError {}

impl fmt::Display for TextError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl TextError {
    pub(crate) fn from_dbg<T: fmt::Debug>(d: T) -> Self {
        Self(format!("{:?}", d))
    }
}

impl From<String> for TextError {
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl From<&str> for TextError {
    fn from(s: &str) -> Self {
        s.to_owned().into()
    }
}

#[cfg(test)]
mod error_tests {
    use super::*;

    #[tokio::test]
    async fn error_serialization_test() {
        let some_internal_err: XandApiErrors = SubstrateClientErrors::ExtrinsicInvalid.into();
        let as_status: Status = some_internal_err.into();
        let err_msg = as_status.message();
        let _: serde_json::Value = serde_json::from_str(err_msg).expect("Must be valid json");
    }
}
