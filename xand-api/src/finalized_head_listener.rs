use crate::{
    stream_cacher::{StreamCacheHandle, StreamCacher},
    ChainDataSourceInfo, DataSource, LoggingEvent,
};
use async_trait::async_trait;
use futures::{
    future::Either::{Left, Right},
    stream::FusedStream,
    FutureExt, Stream, StreamExt,
};
use std::{sync::Arc, time::Duration};
use tokio_stream::wrappers::IntervalStream;
use tpfs_logger_port::warn;
use xand_models::timestamp_from_extrinsics;
use xandstrate_client::{
    Hash, Header, SignedBlock, SubstrateClientErrors, SubstrateClientInterface,
};

const DATA_INFO_RETRY_INTERVAL: Duration = Duration::from_millis(300);

/// Spawns a future in the background that indefinitely listens to the finalized block header
/// stream and uses it to update the returned handle. Re-subscribes if the finalized block
/// stream stops. If the returned cache handle is dropped, the background future completes.
pub fn spawn_data_info_listener<S>(
    substrate_client: Arc<S>,
) -> StreamCacheHandle<ChainDataSourceInfo>
where
    S: SubstrateClientInterface + 'static,
{
    let subscribe_heads = {
        let substrate_client = substrate_client.clone();
        move || {
            let substrate_client = substrate_client.clone();
            async move {
                substrate_client
                    .subscribe_finalized_heads()
                    .await
                    .map_err(|e| {
                        warn!(LoggingEvent::SubstrateClientError(format!(
                            "Error initializing last block info stream. Will retry: {:?}",
                            e
                        )));
                        e
                    })
            }
        }
    };
    let heads = IntervalStream::new(tokio::time::interval(DATA_INFO_RETRY_INTERVAL))
        .filter_map(move |_| {
            let heads = subscribe_heads();
            async move { heads.await.ok() }
        })
        .flatten();
    let source_info = source_info_stream(substrate_client, heads);
    let last_block_cache = StreamCacher::new(source_info);
    let last_block_info = last_block_cache.get_handle();
    tokio::spawn(last_block_cache.run());
    last_block_info
}

/// Transforms a stream of `Header`s into a stream of `ChainDataSourceInfo` items
///
/// If headers are produced while a block request is pending, only the latest header will be
/// remembered and will trigger a block request when the previous request completes. Other headers
/// produced in between will be dropped and will not result in `ChainDataSourceInfo` items.
///
/// The motivations for this design are:
/// - This part of the code only cares about storing the latest block information, so it makes
///   sense to discard old block headers as early as possible.
/// - `jsonrpc-client-transports` has an internal queue that easily fills up when notifications
///   are not removed fast enough or lead to more json-rpc requests, and this leads to any kind of
///   json-rpc communication to hang. This is partly due to poor management of the queue and bugs
///   in polling the right futures, channels and sinks.
fn source_info_stream<B, H>(block_resolver: B, heads: H) -> impl Stream<Item = ChainDataSourceInfo>
where
    B: ResolveBlock + Clone + 'static,
    H: Stream<Item = Result<Header, SubstrateClientErrors>> + Send + 'static,
{
    let heads = heads.boxed().fuse();
    futures::stream::unfold((None, heads), move |(mut hash, mut heads)| {
        let block_resolver = block_resolver.clone();
        async move {
            // Initially there is no pending block request.
            let mut block = Left(futures::future::pending());
            loop {
                // If there is no pending block request and a finalized head has been received,
                // request its block.
                if let (Left(_), &Some(h)) = (&block, &hash) {
                    hash = None;
                    block = {
                        let block_resolver = block_resolver.clone();
                        Right(
                            async move { block_resolver.resolve(h).map(move |r| (r, h)).await }
                                .boxed(),
                        )
                    };
                }
                tokio::select! {
                    Some(head) = heads.next(), if !heads.is_terminated() => match head {
                        Ok(head) => hash = Some(head.hash()),
                        Err(e) => {
                            warn!(LoggingEvent::SubstrateClientError(format!(
                                "Error fetching finalized header stream: {}",
                                e
                            )));
                        }
                    },
                    (b, h) = (&mut block), if matches!(block, Right(_)) => {
                        match b {
                            Ok(b) => {
                                if let Some(source_info) = block_to_source_info(b) {
                                    break Some((source_info, (hash, heads)));
                                }
                            }
                            Err(e) => {
                                warn!(LoggingEvent::SubstrateClientError(format!(
                                    "Unable to fetch block {}: {}",
                                    h, e
                                )));
                            }
                        }
                        block = Left(futures::future::pending());
                    },
                    else => if hash.is_none() { break None },
                }
            }
        }
    })
}

fn block_to_source_info(block: SignedBlock) -> Option<ChainDataSourceInfo> {
    timestamp_from_extrinsics(&block.block.extrinsics).map(|timestamp| ChainDataSourceInfo {
        source: DataSource::Substrate,
        block_number: block.block.header.number,
        timestamp,
    })
}

#[async_trait]
trait ResolveBlock: Send + Sync {
    async fn resolve(&self, hash: Hash) -> Result<SignedBlock, SubstrateClientErrors>;
}

#[async_trait]
impl<T> ResolveBlock for Arc<T>
where
    T: SubstrateClientInterface + Send + Sync + ?Sized,
{
    async fn resolve(&self, hash: Hash) -> Result<SignedBlock, SubstrateClientErrors> {
        self.get_block(&hash).await
    }
}

#[cfg(test)]
mod tests {
    use super::{source_info_stream, ResolveBlock};
    use async_trait::async_trait;
    use futures::{
        channel::oneshot::{self, Receiver},
        lock::Mutex,
        StreamExt,
    };
    use std::{collections::HashMap, sync::Arc, task::Poll};
    use xand_models::{substrate_test_helpers::call_to_ext, TimestampCall};
    use xandstrate_client::{Block, Hash, Header, SignedBlock, SubstrateClientErrors};

    #[derive(Clone)]
    struct FakeResolver {
        inner: Arc<Mutex<FakeResolverInner>>,
    }

    struct FakeResolverInner {
        blocks: HashMap<Hash, SignedBlock>,
        gate: Option<Receiver<()>>,
    }

    impl FakeResolver {
        fn new(blocks: HashMap<Hash, SignedBlock>, gate: Receiver<()>) -> Self {
            Self {
                inner: Arc::new(Mutex::new(FakeResolverInner {
                    blocks,
                    gate: Some(gate),
                })),
            }
        }
    }

    #[async_trait]
    impl ResolveBlock for FakeResolver {
        async fn resolve(&self, hash: Hash) -> Result<SignedBlock, SubstrateClientErrors> {
            let mut inner = self.inner.lock().await;
            if let Some(gate) = inner.gate.take() {
                let _ = gate.await;
            }
            inner
                .blocks
                .get(&hash)
                .cloned()
                .ok_or(SubstrateClientErrors::BlockNotFound { block: hash })
        }
    }

    fn make_header(block_number: u32) -> Header {
        Header {
            parent_hash: Default::default(),
            number: block_number,
            state_root: Default::default(),
            extrinsics_root: Default::default(),
            digest: Default::default(),
        }
    }

    fn make_block(block_number: u32) -> SignedBlock {
        SignedBlock {
            block: Block {
                header: make_header(block_number),
                extrinsics: vec![call_to_ext(
                    xandstrate_client::Call::Timestamp(TimestampCall::set(block_number.into())),
                    block_number,
                    &Default::default(),
                )],
            },
            justification: None,
        }
    }

    #[tokio::test]
    async fn headers_arriving_during_pending_block_request_are_dropped_except_latest() {
        let (gate_sender, gate_receiver) = oneshot::channel();
        let mut gate_sender = Some(gate_sender);
        const HEAD_COUNT: u32 = 5;
        let blocks = (1..=HEAD_COUNT).map(make_block).collect::<Vec<_>>();
        let heads = futures::stream::iter(
            blocks
                .iter()
                .map(|b| Ok(b.block.header.clone()))
                .collect::<Vec<_>>(),
        );
        // Chain empty stream whose polling will unblock block resolution.
        let heads = heads.chain(futures::stream::poll_fn(move |_| {
            if let Some(gate) = gate_sender.take() {
                let _ = gate.send(());
            }
            Poll::Ready(None)
        }));
        let block_resolver = FakeResolver::new(
            blocks
                .into_iter()
                .map(|b| (b.block.header.hash(), b))
                .collect(),
            gate_receiver,
        );
        let result = source_info_stream(block_resolver, heads)
            .collect::<Vec<_>>()
            .await;
        // The results must contain the source info for the last head, and possibly one other head
        // before that if block details were requested for a previous head. Given how the resolver
        // is set up to block on the first request until all heads have been received, there cannot
        // be another source info item in the resulting list.
        assert!(
            result.len() == 1 || result.len() == 2,
            "The result list should contain 1 or 2 items but it contains {}",
            result.len()
        );
        // The last item is the source info for the last head.
        assert_eq!(result.last().unwrap().block_number, HEAD_COUNT);
    }
}
