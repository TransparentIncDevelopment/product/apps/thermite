use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::tests::{test_clear_redeem_builder, test_health_monitor};
use crate::{
    keys::test::create_encryption_key_resolver,
    tests::{
        assert_actual_bank_data_eq_expected, build_xand_api, extract_bank_data,
        get_first_txn_for_address, make_redeem_request_transaction, test_create_builder,
        test_redeem_builder, test_send_builder, BankAccountEncryptionEnv, TestApiError,
        TestNetwork, TestNetworkParticipant,
    },
    Operation, UserTransaction, XandApiErrors, XandApiSvc,
};
use petri::{TransactionCache, TxoCache};
use pseudo::Mock;
use rand::rngs::OsRng;
use std::{convert::TryInto, sync::Arc};
use xand_api_proto::{
    proto_models, BankAccount, Issuer, PendingRedeemRequest, PendingRedeemRequestsPagination,
    RedeemRequest, UnencryptedBankAccount,
};
use xand_financial_client::test_helpers::{
    managers::{
        generate_test_create_manager_simple, generate_test_member_redeem_manager,
        generate_test_redeem_manager_simple,
    },
    private_key_to_address,
    redeems::construct_redeem_record,
};
use xand_ledger::{PrivateKey, RedeemRequestRecord};
use xand_models::{dev_account_key, CorrelationId};
use xandstrate_client::{
    mock::MockSubstrateClient, test_helpers::create_key_signer, Pair, Ss58Codec,
};

#[tokio::test]
async fn submitting_redeem_with_encrypted_error_api_returns_error() {
    let txn_issuer = dev_account_key("2Pac").public();
    // Make a create req transaction
    // Make sure bank data is in an error state

    let req_txn: xand_api_proto::proto_models::XandTransaction =
        xand_api_proto::proto_models::XandTransaction::RedeemRequest(
            xand_api_proto::proto_models::PendingRedeemRequest {
                amount_in_minor_unit: 1000,
                correlation_id: xand_api_proto::proto_models::CorrelationId::gen_random(),
                account: xand_api_proto::proto_models::BankAccountInfo::Encrypted(
                    xand_api_proto::proto_models::EncryptionError::KeyNotFound,
                ),
                completing_transaction: None,
            },
        );

    let input = UserTransaction {
        issuer: txn_issuer.to_ss58check(),
        operation: Some(
            req_txn
                .try_into()
                .expect("Convert from test data should not fail"),
        ),
    };
    // Get xand service, submit txn
    let mock_substrate = MockSubstrateClient::default();
    let key_manager = create_key_signer();
    let enc_key_resolver = create_encryption_key_resolver();
    let svc = XandApiSvc::new(
        Arc::new(mock_substrate),
        Arc::new(TransactionCache::default()),
        key_manager,
        enc_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let result = svc.impl_submit_tx(input).await;

    // Assert that we get an error server-side
    assert!(matches!(
        result,
        Err(XandApiErrors::ExpectedUnencryptedBankAccount { .. })
    ));
}

#[tokio::test]
async fn get_confidential_redeems_works() {
    let mut csprng: OsRng = OsRng::default();
    let private_key = PrivateKey::random(&mut csprng);
    let other_key = PrivateKey::random(&mut csprng);
    let my_address = private_key_to_address(&private_key);

    let redeem_manager = generate_test_member_redeem_manager(private_key, &Default::default());
    let other_redeem_manager = generate_test_member_redeem_manager(other_key, &Default::default());

    // decryptable create record
    let (correlation_id, record) =
        construct_redeem_record(&mut csprng, private_key, &redeem_manager);
    // encrypted create record
    let (other_correlation_id, other_record) =
        construct_redeem_record(&mut csprng, other_key, &other_redeem_manager);

    let nonce: CorrelationId = correlation_id.into();
    let other_nonce: CorrelationId = other_correlation_id.into();
    let other_total: u64 = other_record.redeem_output.value;
    let mock_subc = MockSubstrateClient {
        mock_get_confidential_pending_redeems: Mock::new(Ok(vec![
            (
                correlation_id,
                RedeemRequestRecord::Confidential(Box::new(record.clone())),
            ),
            (
                other_correlation_id,
                RedeemRequestRecord::Confidential(Box::new(other_record.clone())),
            ),
        ])),
        ..Default::default()
    };
    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_member_redeem_manager(private_key, &Default::default()),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let result = testme
        .impl_get_redeems(Default::default())
        .await
        .unwrap()
        .dat;
    assert_eq!(
        result.pending_redeem_requests[0],
        PendingRedeemRequest {
            issuer: Some(Issuer {
                issuer: my_address.to_string()
            }),
            correlation_id: nonce.to_string(),
            request: Some(RedeemRequest {
                amount: record.redeem_output.value,
                bank_account: Some(BankAccount {
                    account: Some(
                        xand_api_proto::bank_account::Account::UnencryptedBankAccount(
                            UnencryptedBankAccount {
                                routing_number: "test".to_string(),
                                account_number: "test".to_string(),
                            },
                        )
                    ),
                }),
                correlation_id: nonce.to_string(),
            }),
        }
    );

    assert!(matches!(
        &result.pending_redeem_requests[1],
        // Test that pending create can be observed without the trust or issuer private key
        PendingRedeemRequest {
        issuer: None,
        correlation_id: outer_nonce,
        request: Some(RedeemRequest {
            amount: total,
            bank_account: Some(BankAccount {
                account: Some(xand_api_proto::bank_account::Account::EncryptedBankAccount(
                        xand_api_proto::EncryptedBankAccount {
                            encryption_error: Some(xand_api_proto::EncryptionError {
                                ..
                            })
                        },
                    )),
            }),
            correlation_id: nonce
        }),
    } if *total == other_total && outer_nonce == nonce && nonce == &other_nonce.to_string()
    ));
}

#[tokio::test]
async fn pending_redeem_pagination_works() {
    let mut csprng: OsRng = OsRng::default();
    let private_key = PrivateKey::random(&mut csprng);
    let redeem_manager = generate_test_member_redeem_manager(private_key, &Default::default());

    let mut redeems = vec![];
    for _ in 1..=100 {
        let (correlation_id, record) =
            construct_redeem_record(&mut csprng, private_key, &redeem_manager);
        redeems.push((
            correlation_id,
            RedeemRequestRecord::Confidential(Box::new(record)),
        ))
    }
    let mock_subc = MockSubstrateClient {
        mock_get_confidential_pending_redeems: Mock::new(Ok(redeems)),
        ..Default::default()
    };
    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_member_redeem_manager(private_key, &Default::default()),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let result = testme
        .impl_get_redeems(PendingRedeemRequestsPagination {
            page_size: 200,
            page_number: 0,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result.total, 100);
    assert_eq!(result.pending_redeem_requests.len(), 100);
    let result = testme
        .impl_get_redeems(PendingRedeemRequestsPagination {
            page_size: 50,
            page_number: 1,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result.total, 100);
    assert_eq!(result.pending_redeem_requests.len(), 50);
}

#[tokio::test]
async fn trust_can_decrypt_bank_accounts_of_all_redeems() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_history(&trust, make_redeem_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);
    //Bob's transaction has bank data
    assert_actual_bank_data_eq_expected(&bob, &results);
}

#[tokio::test]
async fn issuer_can_decrypt_only_bank_accounts_of_their_redeems() {
    let mut env = BankAccountEncryptionEnv::new();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_history(&alice, make_redeem_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);

    //Bob's transaction has none
    let bob_txn = get_first_txn_for_address(results.actual, &bob.address);
    let actual_bob_bank_data = extract_bank_data(&bob_txn.into());
    let expected_bob_bank_data_error = proto_models::BankAccountInfo::Encrypted(
        proto_models::errors::EncryptionError::KeyNotFound,
    );
    assert_eq!(actual_bob_bank_data, expected_bob_bank_data_error);
}

#[tokio::test]
async fn issuer_can_decrypt_bank_account_of_their_redeem_request_details_only() {
    let mut env = BankAccountEncryptionEnv::new();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_transaction_details(&alice.clone(), make_redeem_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);

    //Bob's transaction has none
    let actual_bob_txn = get_first_txn_for_address(results.actual, &bob.address).txn;
    let actual_bob_bank_data = extract_bank_data(&actual_bob_txn.into());
    let expected_bob_bank_data_error = proto_models::BankAccountInfo::Encrypted(
        proto_models::errors::EncryptionError::KeyNotFound,
    );
    assert_eq!(actual_bob_bank_data, expected_bob_bank_data_error);
}

#[tokio::test]
async fn trust_can_decrypt_bank_account_of_all_redeem_request_details() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_transaction_details(&trust, make_redeem_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);

    //Bob's transaction has bank data
    assert_actual_bank_data_eq_expected(&bob, &results);
}

#[tokio::test]
async fn issuer_without_encryption_keys_cannot_decrypt_bank_accounts_of_their_redeems_calling_history(
) {
    let mut env = BankAccountEncryptionEnv::new();
    // Alice has encryption keys by default
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    //Alice can get history of own creates
    let results = env
        .submit_and_request_history(&alice, make_redeem_request_transaction)
        .await
        .unwrap();
    // Precondition: Alice's creates have bank data
    assert_actual_bank_data_eq_expected(&alice.clone(), &results);
    env.network.remove_member_encryption_key(&alice);
    // Alice can no longer decrypt own redeem bank account info because their signing key will not be recognized
    let maybe_result = env
        .submit_and_request_history(&alice, make_redeem_request_transaction)
        .await;
    assert!(matches!(
        maybe_result,
        Err(TestApiError::XandApi(XandApiErrors::BadAddress { .. }))
    ));
}

#[tokio::test]
async fn trust_without_encryption_key_cannot_decrypt_bank_account_of_all_redeem_request_details() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_transaction_details(&trust, make_redeem_request_transaction)
        .await
        .unwrap();

    //Precondition: Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);
    //Precondition: Bob's transaction has bank data
    assert_actual_bank_data_eq_expected(&bob, &results);
    env.network.remove_trust_encryption_key(&trust);
    // Trust can no longer decrypt bank accounts in history of all redeems because their signing key will not be recognized
    let maybe_result = env
        .submit_and_request_transaction_details(&trust, make_redeem_request_transaction)
        .await;
    assert!(matches!(
        maybe_result,
        Err(TestApiError::XandApi(XandApiErrors::BadAddress { .. }))
    ));
}

#[tokio::test]
async fn long_redeem_cancellation_reason_does_not_panic() {
    let mut network = TestNetwork::new();
    let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
    let xand_api = build_xand_api(&network, &alice).await;
    let _ = xand_api
        .impl_submit_tx(UserTransaction {
            issuer: alice.address.to_string(),
            operation: Some(Operation::RedeemCancellation(
                xand_api_proto::RedeemCancellation {
                    correlation_id: CorrelationId::from([0u8; 16]).to_string(),
                    reason: "oh no".repeat(1000),
                },
            )),
        })
        .await;
}
