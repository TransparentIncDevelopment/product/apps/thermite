use crate::tests::default_test_env;

#[tokio::test]
async fn get_limited_agent_works_with_none_set() {
    let test_service = default_test_env().await.service;
    test_service
        .impl_get_limited_agent()
        .await
        .expect("Unexpected error when testing limited agent retrieval.");
}
