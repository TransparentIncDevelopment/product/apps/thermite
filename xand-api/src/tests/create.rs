use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::tests::{test_clear_redeem_builder, test_health_monitor};
use crate::{
    address_to_key_id,
    keys::test::create_encryption_key_resolver,
    tests::{
        assert_actual_bank_data_eq_expected, assert_actual_txn_eq_expected, build_xand_api,
        extract_bank_data, get_first_txn_for_address, make_create_request_transaction,
        test_create_builder, test_redeem_builder, test_send_builder, BankAccountEncryptionEnv,
        TestApiError, TestCreateBuilder, TestNetwork, TestNetworkParticipant,
    },
    xand_models_converter::FromWrapper,
    Operation, SubmissionRequest, UserTransaction, XandApiErrors, XandApiSvc,
};
use petri::{TransactionCache, TxoCache};
use pseudo::Mock;
use rand::rngs::OsRng;
use std::{collections::HashMap, convert::TryInto, sync::Arc};
use tpfs_krypt::KeyPairValue;
use xand_api_proto::{
    proto_models, BankAccount, CreateRequest, Issuer, PendingCreateRequest,
    PendingCreateRequestsPagination, UnencryptedBankAccount,
};
use xand_financial_client::test_helpers::{
    managers::{
        generate_test_create_manager_simple, generate_test_member_create_manager,
        generate_test_redeem_manager_simple, FakeKeyStore,
    },
    private_key_to_address,
    transactions::construct_create_record,
    FakeIdentityTagSelector,
};
use xand_financial_client_adapter::krypt_adapter::sp_pair_to_private_key;
use xand_ledger::{IdentityTag, PrivateKey};
use xand_models::{
    dev_account_key, BankAccountId, BankAccountInfo, CorrelationId, PendingCreate, XandTransaction,
};
use xandstrate_client::{
    mock::MockSubstrateClient, test_helpers::create_key_signer, Pair, Ss58Codec,
};

#[tokio::test]
async fn confidential_create_transactions_are_constructed_without_signature() {
    let mut test_network = TestNetwork::new();
    let alice = test_network.create_member_with_name(TestNetworkParticipant::Alice);
    let address = alice.address.clone();
    let keypair = alice
        .key_manager
        .get_keypair_by_id(&address_to_key_id(&address))
        .unwrap();
    let private_key = if let KeyPairValue::SubstrateSr25519(kp) = keypair {
        sp_pair_to_private_key(&kp.key).expect("This keypair should have a private key")
    } else {
        unreachable!("This isn't a valid key pair")
    };

    let bank_account_data = BankAccountId {
        routing_number: "".to_string(),
        account_number: "".to_string(),
    };
    let input_tx = XandTransaction::CreatePendingCreate(PendingCreate {
        amount_in_minor_unit: 100u64,
        correlation_id: CorrelationId::gen_random(),
        account: BankAccountInfo::Unencrypted(bank_account_data),
        completing_transaction: None,
    });
    let proto_input_tx = proto_models::XandTransaction::convert_from(input_tx.clone());
    let input = UserTransaction {
        issuer: address.to_string(),
        operation: Some(proto_input_tx.try_into().unwrap()),
    };
    let key_manager = create_key_signer();
    let txn_cache = Arc::new(TransactionCache::default());

    let mock_subc = MockSubstrateClient {
        mock_get_trustee: Mock::new(Ok(test_network.trust.address.clone())),
        ..Default::default()
    };

    let keys = vec![private_key];
    let key_store = FakeKeyStore::new(keys);

    let my_id_tag = IdentityTag::from_key_with_generator_base(private_key.into());
    let mut id_tags_from_address = HashMap::new();
    id_tags_from_address.insert(address, vec![my_id_tag]);
    let id_tag_selector = FakeIdentityTagSelector {
        id_tags_from_address,
    };

    let encryption_key_resolver = test_network.key_resolver(&alice);

    let builder = TestCreateBuilder {
        key_store,
        id_tag_selector,
        network_key_resolver: encryption_key_resolver.clone().into(),
        ..test_create_builder()
    };

    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        txn_cache,
        key_manager,
        encryption_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        builder,
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let submission_req = testme.build_submit_tx(input).await.unwrap();
    assert!(matches!(
        submission_req,
        SubmissionRequest::DontSign(crate::proto_help::CallWrapper(
            xandstrate_client::Call::Confidentiality(_)
        ),)
    ));
}

#[tokio::test]
async fn get_confidential_creates_works() {
    let mut csprng: OsRng = OsRng::default();
    let private_key = PrivateKey::random(&mut csprng);
    let other_key = PrivateKey::random(&mut csprng);
    let my_address = private_key_to_address(&private_key);

    let create_manager = generate_test_member_create_manager(private_key, &Default::default());
    let other_create_manager = generate_test_member_create_manager(other_key, &Default::default());

    // decryptable create record
    let (correlation_id, record) =
        construct_create_record(&mut csprng, private_key, &create_manager, 1.into());
    // encrypted create record
    let (other_correlation_id, other_record) =
        construct_create_record(&mut csprng, other_key, &other_create_manager, 1.into());

    let nonce: CorrelationId = correlation_id.into();
    let other_nonce: CorrelationId = other_correlation_id.into();
    let other_total: u64 = other_record.outputs.iter().map(|o| o.value).sum();
    let mock_subc = MockSubstrateClient {
        mock_get_confidential_pending_creates: Mock::new(Ok(vec![
            (correlation_id, record.clone()),
            (other_correlation_id, other_record.clone()),
        ])),
        ..Default::default()
    };
    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_member_create_manager(private_key, &Default::default()),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let result = testme
        .impl_get_creates(Default::default())
        .await
        .unwrap()
        .dat;
    // Assert that all fields are visible on the decryptable create record
    assert_eq!(
        result.pending_create_requests[0],
        PendingCreateRequest {
            issuer: Some(Issuer {
                issuer: my_address.to_string()
            }),
            correlation_id: nonce.to_string(),
            request: Some(CreateRequest {
                amount: record.outputs.iter().map(|o| o.value).sum(),
                bank_account: Some(BankAccount {
                    account: Some(
                        xand_api_proto::bank_account::Account::UnencryptedBankAccount(
                            UnencryptedBankAccount {
                                routing_number: "test".to_string(),
                                account_number: "test".to_string(),
                            },
                        )
                    ),
                }),
                correlation_id: nonce.to_string(),
            }),
        }
    );
    // Assert that only issuer and bank data are hidden on an encrypted create record
    assert!(matches!(
        &result.pending_create_requests[1],
        // Test that pending creates can be observed without the trust or issuer private key
        PendingCreateRequest {
            issuer: None,
            correlation_id: outer_nonce,
            request: Some(CreateRequest {
                amount: total,
                bank_account: Some(BankAccount {
                    account: Some(xand_api_proto::bank_account::Account::EncryptedBankAccount(
                        xand_api_proto::EncryptedBankAccount {
                            encryption_error: Some(xand_api_proto::EncryptionError {
                                ..
                            })
                        },
                    )),
                }),
                correlation_id: nonce
            }),
        } if *total == other_total && outer_nonce == nonce && nonce == &other_nonce.to_string()
    ));
}

#[tokio::test]
async fn submitting_create_with_encrypted_error_api_returns_error() {
    let txn_issuer = dev_account_key("2Pac").public();
    // Make a create req transaction
    // Make sure bank data is in an error state

    let req_txn: xand_api_proto::proto_models::XandTransaction =
        xand_api_proto::proto_models::XandTransaction::CreateRequest(
            xand_api_proto::proto_models::PendingCreateRequest {
                amount_in_minor_unit: 1000,
                correlation_id: xand_api_proto::proto_models::CorrelationId::gen_random(),
                account: xand_api_proto::proto_models::BankAccountInfo::Encrypted(
                    xand_api_proto::proto_models::EncryptionError::KeyNotFound,
                ),
                completing_transaction: None,
            },
        );

    let input = UserTransaction {
        issuer: txn_issuer.to_ss58check(),
        operation: Some(
            req_txn
                .try_into()
                .expect("Convert from test data should not fail"),
        ),
    };
    // Get xand service, submit txn
    let mock_substrate = MockSubstrateClient::default();
    let key_manager = create_key_signer();
    let enc_key_resolver = create_encryption_key_resolver();
    let svc = XandApiSvc::new(
        Arc::new(mock_substrate),
        Arc::new(TransactionCache::default()),
        key_manager,
        enc_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let result = svc.impl_submit_tx(input).await;

    // Assert that we get an error server-side
    assert!(matches!(
        result,
        Err(XandApiErrors::ExpectedUnencryptedBankAccount { .. })
    ));
}

#[tokio::test]
async fn pending_create_pagination_works() {
    let mut csprng: OsRng = OsRng::default();
    let private_key = PrivateKey::random(&mut csprng);
    let create_manager = generate_test_member_create_manager(private_key, &Default::default());

    let mut creates = vec![];
    for _ in 1..=100 {
        creates.push(construct_create_record(
            &mut csprng,
            private_key,
            &create_manager,
            1.into(),
        ))
    }

    let mock_subc = MockSubstrateClient {
        mock_get_confidential_pending_creates: Mock::new(Ok(creates)),
        ..Default::default()
    };

    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_member_create_manager(private_key, &Default::default()),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    let result = testme
        .impl_get_creates(PendingCreateRequestsPagination {
            page_size: 200,
            page_number: 0,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result.total, 100);
    assert_eq!(result.pending_create_requests.len(), 100);
    let result = testme
        .impl_get_creates(PendingCreateRequestsPagination {
            page_size: 50,
            page_number: 1,
        })
        .await
        .unwrap()
        .dat;
    assert_eq!(result.total, 100);
    assert_eq!(result.pending_create_requests.len(), 50);
}

#[tokio::test]
async fn trust_can_decrypt_bank_accounts_of_all_creates() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let results = env
        .submit_and_request_history(&trust, make_create_request_transaction)
        .await
        .unwrap();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    assert_actual_txn_eq_expected(&alice, &results);
    assert_actual_txn_eq_expected(&bob, &results);
}

#[tokio::test]
async fn issuer_can_decrypt_only_bank_accounts_of_their_creates_calling_history() {
    let mut env = BankAccountEncryptionEnv::new();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_history(&alice, make_create_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);

    //Bob's transaction has none
    let bob_txn = get_first_txn_for_address(results.actual, &bob.address);
    let actual_bob_bank_data = extract_bank_data(&bob_txn.into());
    let expected_bob_bank_data_error = proto_models::BankAccountInfo::Encrypted(
        proto_models::errors::EncryptionError::KeyNotFound,
    );
    assert_eq!(actual_bob_bank_data, expected_bob_bank_data_error);
}

#[tokio::test]
async fn issuer_can_decrypt_bank_account_of_their_create_request_details_only() {
    let mut env = BankAccountEncryptionEnv::new();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_transaction_details(&alice.clone(), make_create_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);

    //Bob's transaction has none
    let actual_bob_txn = get_first_txn_for_address(results.actual, &bob.address).txn;
    let actual_bob_bank_data = extract_bank_data(&actual_bob_txn.into());
    let expected_bob_bank_data_error = proto_models::BankAccountInfo::Encrypted(
        proto_models::errors::EncryptionError::KeyNotFound,
    );
    assert_eq!(actual_bob_bank_data, expected_bob_bank_data_error);
}

#[tokio::test]
async fn trust_can_decrypt_bank_account_of_all_create_request_details() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_transaction_details(&trust, make_create_request_transaction)
        .await
        .unwrap();

    //Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);

    //Bob's transaction has bank data
    assert_actual_bank_data_eq_expected(&bob, &results);
}

#[tokio::test]
async fn issuer_without_encryption_keys_cannot_submit_create_request() {
    let mut env = BankAccountEncryptionEnv::new();
    // Alice has encryption keys by default
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    //Alice can submit a create request
    env.submit_and_get_pending_creates(&alice.clone())
        .await
        .unwrap();

    env.network.remove_member_encryption_key(&alice);
    // Alice can no longer make a create request because their signing key will not be recognized
    let maybe_result = env.submit_and_get_pending_creates(&alice.clone()).await;
    assert!(matches!(
        maybe_result,
        Err(TestApiError::XandApi(XandApiErrors::BadAddress { .. }))
    ));
}

#[tokio::test]
async fn issuer_without_encryption_keys_cannot_decrypt_bank_accounts_of_their_creates_calling_history(
) {
    let mut env = BankAccountEncryptionEnv::new();
    // Alice has encryption keys by default
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    //Alice can get history of own creates
    let results = env
        .submit_and_request_history(&alice, make_create_request_transaction)
        .await
        .unwrap();

    //Precondition: Alice's creates have bank data
    assert_actual_bank_data_eq_expected(&alice, &results);
    env.network.remove_member_encryption_key(&alice);
    // Alice can no longer decrypt own create because their signing key will not be recognized
    let maybe_result = env
        .submit_and_request_history(&alice, make_create_request_transaction)
        .await;
    assert!(matches!(
        maybe_result,
        Err(TestApiError::XandApi(XandApiErrors::BadAddress { .. }))
    ));
}

#[tokio::test]
async fn trust_without_encryption_key_cannot_decrypt_bank_accounts_of_all_creates() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let results = env
        .submit_and_request_history(&trust, make_create_request_transaction)
        .await
        .unwrap();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    //Precondition: Alice's creates have bank data
    assert_actual_txn_eq_expected(&alice, &results);
    //Precondition: Bob's creates have bank data
    assert_actual_txn_eq_expected(&bob, &results);
    env.network.remove_trust_encryption_key(&trust);
    // Trust can no longer decrypt bank accounts in history of all creates because their signing key will not be recognized
    let maybe_result = env
        .submit_and_request_history(&trust, make_create_request_transaction)
        .await;
    assert!(matches!(
        maybe_result,
        Err(TestApiError::XandApi(XandApiErrors::BadAddress { .. }))
    ));
}

#[tokio::test]
async fn trust_without_encryption_key_cannot_decrypt_bank_account_of_all_create_request_details() {
    let mut env = BankAccountEncryptionEnv::new();
    let trust = env.trust().clone();
    let alice = env.get_participant_by_name(&TestNetworkParticipant::Alice);
    let bob = env.get_participant_by_name(&TestNetworkParticipant::Bob);
    let results = env
        .submit_and_request_transaction_details(&trust, make_create_request_transaction)
        .await
        .unwrap();

    //Precondition: Alice's transaction has bank data
    assert_actual_bank_data_eq_expected(&alice, &results);
    //Precondition: Bob's transaction has bank data
    assert_actual_bank_data_eq_expected(&bob, &results);
    env.network.remove_trust_encryption_key(&trust);
    // Trust can no longer decrypt bank accounts in history of all creates because their signing key will not be recognized
    let maybe_result = env
        .submit_and_request_transaction_details(&trust, make_create_request_transaction)
        .await;
    assert!(matches!(
        maybe_result,
        Err(TestApiError::XandApi(XandApiErrors::BadAddress { .. }))
    ));
}

#[tokio::test]
async fn long_create_cancellation_reason_does_not_panic() {
    let mut network = TestNetwork::new();
    let alice = network.create_member_with_name(TestNetworkParticipant::Alice);
    let xand_api = build_xand_api(&network, &alice).await;
    let _ = xand_api
        .impl_submit_tx(UserTransaction {
            issuer: alice.address.to_string(),
            operation: Some(Operation::CreateCancellation(
                xand_api_proto::CreateCancellation {
                    correlation_id: CorrelationId::from([0u8; 16]).to_string(),
                    reason: "oh no".repeat(1000),
                },
            )),
        })
        .await;
}
