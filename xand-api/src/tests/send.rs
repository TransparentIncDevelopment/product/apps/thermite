use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::tests::{
    build_xand_api_for_member_balance, test_clear_redeem_builder, test_health_monitor,
};
use crate::{
    tests::{
        default_test_env, test_create_builder, test_redeem_builder, test_send_builder, TestNetwork,
        TestNetworkParticipant, TestSendBuilder, BIGGIE, TUPAC,
    },
    xand_models_converter::FromWrapper,
    Operation, SubmissionRequest, UserTransaction, XandApiErrors, XandApiSvc,
};
use assert_matches::assert_matches;
use petri::{TransactionCache, TxoCache};
use pseudo::Mock;
use std::{collections::HashMap, convert::TryInto, sync::Arc};
use tpfs_krypt::KeyPairValue;
use xand_api_proto::proto_models;
use xand_financial_client::test_helpers::{
    managers::{
        generate_test_create_manager_simple, generate_test_redeem_manager_simple, FakeKeyStore,
    },
    FakeIdentityTagSelector,
};
use xand_financial_client_adapter::{address_to_key_id, krypt_adapter::sp_pair_to_private_key};
use xand_ledger::IdentityTag;
use xand_models::{SendSchema, ToAddress, XandTransaction};
use xandstrate_client::{mock::MockSubstrateClient, test_helpers::create_key_signer, Pair};

#[tokio::test]
async fn submit_send_works_with_confidentiality_mode_enabled() {
    let mut test_network = TestNetwork::new();
    let alice = test_network.create_member_with_name(TestNetworkParticipant::Alice);
    let bob = test_network.create_member_with_name(TestNetworkParticipant::Bob);
    let input_tx = XandTransaction::Send(SendSchema {
        amount_in_minor_unit: 0,
        destination_account: bob.address.clone(),
    });
    let proto_input_tx = proto_models::XandTransaction::convert_from(input_tx.clone());
    let input = UserTransaction {
        issuer: alice.address.to_string(),
        operation: Some(proto_input_tx.try_into().unwrap()),
    };
    let key_manager = create_key_signer();
    let keypair = alice
        .key_manager
        .get_keypair_by_id(&address_to_key_id(&alice.address))
        .unwrap();
    let private_key = if let KeyPairValue::SubstrateSr25519(kp) = keypair {
        sp_pair_to_private_key(&kp.key).expect("This keypair should have a private key")
    } else {
        unreachable!("This isn't a valid key pair")
    };
    let txn_cache = Arc::new(TransactionCache::default());

    let mock_subc = MockSubstrateClient {
        mock_get_members: Mock::new(Ok(vec![alice.address.clone(), bob.address.clone()])),
        ..Default::default()
    };
    let keys = vec![private_key];
    let key_store = FakeKeyStore::new(keys);

    let my_id_tag = IdentityTag::from_key_with_generator_base(private_key.into());
    let mut id_tags_from_address = HashMap::new();
    id_tags_from_address.insert(alice.address.clone(), vec![my_id_tag]);
    let id_tag_selector = FakeIdentityTagSelector {
        id_tags_from_address,
    };
    let encryption_key_resolver = test_network.key_resolver(&alice);
    let builder = TestSendBuilder {
        key_store,
        id_tag_selector,
        encryption_key_resolver: encryption_key_resolver.clone().into(),
        ..test_send_builder()
    };

    let testme = XandApiSvc::new(
        Arc::new(mock_subc),
        txn_cache,
        key_manager,
        encryption_key_resolver,
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        builder,
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;
    let submission_req = testme.build_submit_tx(input).await.unwrap();
    assert!(matches!(
        submission_req,
        SubmissionRequest::DontSign(crate::proto_help::CallWrapper(
            xandstrate_client::Call::Confidentiality(_)
        ),)
    ));
}

#[tokio::test]
async fn send_fails_when_there_are_insufficient_claims_confidential() {
    let _balance = 1u64;
    let send_amount = 2u64;
    let issuer = BIGGIE.public().to_address().to_string();
    let recipient = TUPAC.public().to_address().to_string();

    let testme = default_test_env().await.service;
    // testme
    //     .substrate_client
    //     .mock_get_balance
    //     .return_value(Ok(balance));
    let tx = UserTransaction {
        issuer,
        operation: Some(Operation::Send(xand_api_proto::Send {
            to: recipient,
            amount: send_amount,
        })),
    };
    let result = testme.validate_request(&tx).await;

    assert_matches!(result, Err(XandApiErrors::InsufficientClaims));
}

#[tokio::test]
async fn send_to_self_is_rejected_confidential() {
    let mut network = TestNetwork::new();
    let member = network.create_member_with_name(TestNetworkParticipant::Bob);
    let issuer = member.address.to_string();
    let recipient = issuer.clone();
    let testme = build_xand_api_for_member_balance(&network, &member, 1000).await;
    let tx = UserTransaction {
        issuer,
        operation: Some(Operation::Send(xand_api_proto::Send {
            to: recipient,
            amount: 10,
        })),
    };
    let result = testme.validate_request(&tx).await;
    assert_matches!(result, Err(XandApiErrors::InvalidSendToSelf));
}

#[tokio::test]
async fn send_to_banned_member_is_rejected() {
    // Given a network with a good member with a 1000 unit balance and a banned member
    let mut network = TestNetwork::new();
    let good_member = network.create_member_with_name(TestNetworkParticipant::Bob);
    let issuer = good_member.address.clone();
    let banned_member = network.create_member_with_name(TestNetworkParticipant::Alice);
    let recipient = banned_member.address;
    let testme = build_xand_api_for_member_balance(&network, &good_member, 1000).await;

    testme
        .substrate_client
        .mock_get_members
        .return_value(Ok(vec![issuer.clone()]));
    testme
        .substrate_client
        .mock_get_banned_members
        .return_value(Ok(vec![recipient.clone()]));
    let tx = UserTransaction {
        issuer: issuer.to_string(),
        operation: Some(Operation::Send(xand_api_proto::Send {
            to: recipient.to_string(),
            amount: 10,
        })),
    };

    // When a balance is sent to the banned member
    let result = testme.validate_request(&tx).await;

    // Then the send should fail with the InvalidMember response
    assert_matches!(result, Err(XandApiErrors::InvalidMember));
}
