use crate::clear_txo_store::SubstrateClientClearTxOStore;
use crate::keys::test::{TestNetwork, TestNetworkParticipant};
use crate::tests::{
    build_xand_api_with_mock_substrate_client, test_clear_redeem_builder, test_health_monitor,
};
use crate::{
    keys::test::create_encryption_key_resolver,
    tests::{
        setup_fake_header_and_block, test_create_builder, test_redeem_builder, test_send_builder,
        MetadataValue, TUPAC,
    },
    AddressBalanceRequest, GetCurrentBlockReq, MaybeBalance, Request, XandApi, XandApiSvc,
    BLOCK_STALENESS_THRESHOLD_MS, CHAIN_DATASOURCE_INFO_METADATA_KEY,
};
use chrono::Utc;
use petri::{TransactionCache, TxoCache};
use std::{
    convert::TryFrom,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
    time::Duration,
};
use xand_financial_client::test_helpers::managers::{
    generate_test_create_manager_simple, generate_test_redeem_manager_simple,
};
use xand_models::{substrate_test_helpers::call_to_ext, TimestampCall};
use xandstrate_client::{
    mock::MockSubstrateClient, test_helpers::create_key_signer, Block, Pair, SignedBlock,
    SubstrateClientErrors,
};

#[tokio::test]
async fn last_block_info_is_attached_to_metadata_substrate_client_src() {
    let mut network = TestNetwork::new();
    let member = network.create_member_with_name(TestNetworkParticipant::Alice);

    // The mock client must be set up before instantiating the service, since the service
    // immediately starts listening to the finalized heads in `new`
    let mock_client = MockSubstrateClient::default();

    let (fake_block_header, fake_block) = setup_fake_header_and_block(1337);
    mock_client.mock_get_block.return_value(Ok(fake_block));
    mock_client.mock_finalized_heads.return_value(Ok(vec![
        Ok(fake_block_header.clone()),
        Ok(fake_block_header.clone()),
    ]));

    let testme = build_xand_api_with_mock_substrate_client(&network, &member, mock_client).await;

    // Using thread sleeping here would gum everything up since the background listening loop
    // is a future, not a separate thread. The small delay is required to give the background
    // future a chance to initialize.
    tokio::time::sleep(Duration::from_millis(100)).await;
    let result = testme
        .get_address_balance(Request::new(AddressBalanceRequest {
            address: member.address.to_string(),
        }))
        .await
        .unwrap();

    assert_eq!(result.get_ref().balance, Some(MaybeBalance { amount: 0 }));
    let met_value = result.metadata().get(CHAIN_DATASOURCE_INFO_METADATA_KEY);
    let expected =
        r#"{"source":"Substrate","block_number":1337,"timestamp":"1970-01-01T00:00:00.100Z"}"#;
    assert_eq!(met_value, Some(&MetadataValue::from_str(expected).unwrap()));
}

#[tokio::test]
async fn last_block_info_still_works_even_if_errors_on_startup() {
    let mut network = TestNetwork::new();
    let member = network.create_member_with_name(TestNetworkParticipant::Alice);

    // The mock client must be set up before instantiating the service, since the service
    // immediately starts listening to the finalized heads in `new`
    let mock_client = MockSubstrateClient::default();

    let (fake_block_header, fake_block) = setup_fake_header_and_block(1337);
    mock_client.mock_get_block.return_value(Ok(fake_block));

    let ct = AtomicUsize::new(0);
    mock_client
        .mock_finalized_heads
        .use_closure(Box::new(move |_| {
            // This does a bunch of unnecessary allocation, but it's test.
            let resps = vec![
                Err(SubstrateClientErrors::WebsocketDisconnected),
                Err(SubstrateClientErrors::WebsocketDisconnected),
                Ok(vec![Ok(fake_block_header.clone())]),
            ];
            let index = ct.fetch_add(1, Ordering::SeqCst) % resps.len();
            resps[index].clone()
        }));

    let testme = build_xand_api_with_mock_substrate_client(&network, &member, mock_client).await;

    // Delay is required because the there is a delay between retries, when initializing the
    // stream
    tokio::time::sleep(Duration::from_secs(2)).await;
    let result = testme
        .get_address_balance(Request::new(AddressBalanceRequest {
            address: member.address.to_string(),
        }))
        .await
        .unwrap();
    assert_eq!(result.get_ref().balance, Some(MaybeBalance { amount: 0 }));
    let met_value = result.metadata().get(CHAIN_DATASOURCE_INFO_METADATA_KEY);
    let expected =
        r#"{"source":"Substrate","block_number":1337,"timestamp":"1970-01-01T00:00:00.100Z"}"#;
    assert_eq!(met_value, Some(&MetadataValue::from_str(expected).unwrap()));
}

#[tokio::test]
async fn last_block_info_is_not_stale() {
    // The mock client must be set up before instantiating the service, since the service
    // immediately starts listening to the finalized heads in `new`
    let mock_client = MockSubstrateClient::default();

    let (fake_block_header, fake_block) = setup_fake_header_and_block(1337);
    mock_client.mock_get_block.return_value(Ok(fake_block));
    mock_client.mock_finalized_heads.return_value(Ok(vec![
        Ok(fake_block_header.clone()),
        Ok(fake_block_header.clone()),
    ]));
    let timestamp_ms = Utc::now().timestamp_millis();
    // TODO - Write a conversion handler between Xand API types and Substrate types - https://dev.azure.com/transparentsystems/xand-network/_boards/board/t/Confidentiality/Backlog%20items/?workitem=5924
    let timestamp_ms_substrate_format = u64::try_from(timestamp_ms).unwrap();
    let timestamp_ext = call_to_ext(
        xandstrate_client::Call::Timestamp(TimestampCall::set(timestamp_ms_substrate_format)),
        0,
        &TUPAC.public(),
    );
    let fake_block = SignedBlock {
        block: Block {
            extrinsics: vec![timestamp_ext],
            header: fake_block_header,
        },
        justification: None,
    };
    mock_client.mock_get_block.return_value(Ok(fake_block));

    let sut = XandApiSvc::new(
        Arc::new(mock_client),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    // Using thread sleeping here would gum everything up since the background listening loop
    // is a future, not a separate thread.
    tokio::time::sleep(Duration::from_secs(2)).await;
    let result = sut
        .get_current_block(Request::new(GetCurrentBlockReq {}))
        .await
        .unwrap()
        .into_inner();
    assert!(!result.is_stale);
    assert_eq!(result.block_number, 1337);
    assert_eq!(result.timestamp.unwrap().unix_time_millis, timestamp_ms);
}

#[tokio::test]
async fn last_block_info_is_stale() {
    // The mock client must be set up before instantiating the service, since the service
    // immediately starts listening to the finalized heads in `new`
    let mock_client = MockSubstrateClient::default();

    let (fake_block_header, fake_block) = setup_fake_header_and_block(1337);
    mock_client.mock_get_block.return_value(Ok(fake_block));
    mock_client.mock_finalized_heads.return_value(Ok(vec![
        Ok(fake_block_header.clone()),
        Ok(fake_block_header.clone()),
    ]));
    let timestamp_ms = Utc::now().timestamp_millis();
    let timestamp_ms_stale =
        u64::try_from(timestamp_ms).unwrap() - (BLOCK_STALENESS_THRESHOLD_MS * 2);
    let timestamp_ms_stale_xand_api_format = i64::try_from(timestamp_ms_stale).unwrap(); // We use i64 Unix timestamps in the Xand API
    let timestamp_ext = call_to_ext(
        xandstrate_client::Call::Timestamp(TimestampCall::set(timestamp_ms_stale)),
        0,
        &TUPAC.public(),
    );
    let fake_block = SignedBlock {
        block: Block {
            extrinsics: vec![timestamp_ext],
            header: fake_block_header,
        },
        justification: None,
    };
    mock_client.mock_get_block.return_value(Ok(fake_block));

    let sut = XandApiSvc::new(
        Arc::new(mock_client),
        Arc::new(TransactionCache::default()),
        create_key_signer(),
        create_encryption_key_resolver(),
        generate_test_create_manager_simple(),
        generate_test_redeem_manager_simple(),
        TxoCache::default(),
        SubstrateClientClearTxOStore::<MockSubstrateClient>::default(),
        test_create_builder(),
        test_send_builder(),
        test_redeem_builder(),
        test_clear_redeem_builder(),
        test_health_monitor(),
    )
    .await;

    // Using thread sleeping here would gum everything up since the background listening loop
    // is a future, not a separate thread.
    tokio::time::sleep(Duration::from_secs(2)).await;
    let result = sut
        .get_current_block(Request::new(GetCurrentBlockReq {}))
        .await
        .unwrap()
        .into_inner();
    assert!(result.is_stale);
    assert_eq!(result.block_number, 1337);
    assert_eq!(
        result.timestamp.unwrap().unix_time_millis,
        timestamp_ms_stale_xand_api_format
    );
}
