use futures::{future::BoxFuture, FutureExt, Stream, StreamExt, TryStreamExt};
use std::sync::Arc;
use tokio::sync::RwLock;

/// This struct's purpose is to let you easily access whatever the latest element in a stream is/was
/// at any time without waiting for the next element. IE: It caches the last element of the stream.
pub struct StreamCacher<'a, T> {
    stream_processor: BoxFuture<'a, Result<(), ()>>,
    last_item: StreamCacheHandle<T>,
}

impl<T> StreamCacher<'_, T> {
    /// Create a new stream cacher given an existing stream
    pub fn new<'a, IS>(stream: IS) -> StreamCacher<'a, T>
    where
        IS: Stream<Item = T> + Send + 'a,
        T: Send + Sync + 'a,
    {
        let handle = StreamCacheHandle::default();
        Self::new_with_handle(stream, handle)
    }

    /// Returns a handle that can be used to access the latest value the stream produced
    pub fn get_handle(&self) -> StreamCacheHandle<T> {
        self.last_item.clone()
    }

    /// Awaits on the internal stream
    pub async fn run(self) {
        // Drop the internal handle so that the stream processor stops if no other handle is kept
        // alive.
        drop(self.last_item);
        let _ = self.stream_processor.await;
    }

    fn new_with_handle<'a, IS>(stream: IS, handle: StreamCacheHandle<T>) -> StreamCacher<'a, T>
    where
        IS: Stream<Item = T> + Send + 'a,
        T: Send + Sync + 'a,
    {
        // Downgrade to a weak pointer so that if the cacher and any handles are dropped, we can
        // quit the loop early.
        let li2 = Arc::downgrade(&handle.last_item);
        let foreach = stream.map(Ok).try_for_each(move |v| {
            let li2 = li2.clone().upgrade();
            async move {
                if let Some(li2) = li2 {
                    let mut i = li2.write().await;
                    *i = Some(v);
                    Ok(())
                } else {
                    Err(())
                }
            }
        });
        StreamCacher {
            stream_processor: foreach.boxed(),
            last_item: handle,
        }
    }
}

/// A handle to a `StreamCacher` that can be used to fetch the last stream item
pub struct StreamCacheHandle<T> {
    last_item: Arc<RwLock<Option<T>>>,
}

impl<T> Default for StreamCacheHandle<T> {
    fn default() -> Self {
        Self {
            last_item: Arc::new(RwLock::new(None)),
        }
    }
}

impl<T> Clone for StreamCacheHandle<T> {
    fn clone(&self) -> Self {
        Self {
            last_item: self.last_item.clone(),
        }
    }
}

impl<T> StreamCacheHandle<T>
where
    T: Clone,
{
    /// Fetches the last value from the stream. May be None if the stream has not produced any items
    /// yet.
    pub async fn get_value(&self) -> Option<T> {
        self.last_item.read().await.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::StreamCacher;

    #[tokio::test]
    async fn stream_processor_ends_when_no_other_handle_left() {
        let cache = StreamCacher::new(futures::stream::repeat(()));
        cache.run().await;
    }
}
