#!/usr/bin/env bash
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export GOPATH="$SCRIPT_DIR/../../target/go:$SCRIPT_DIR"

pushd "$SCRIPT_DIR/src" > /dev/null

#echo "$GOPATH"
echo "Fetching dependencies..."
go get -v -d ./...
echo "Done"
echo "Building..."
go build "main.go"
echo "Done"

popd > /dev/null