//! Tests for confidential creates and sends
use futures::{future::ready, pin_mut, StreamExt};
use sp_core::{crypto::Ss58Codec, sr25519};
use std::{
    convert::{Into, TryFrom},
    time::Duration,
};
use tokio_stream::wrappers::IntervalStream;
use xand_address::Address;
use xand_api_client::{
    errors::XandApiClientError, BankAccountId, BankAccountInfo, CashConfirmation,
    CreateCancellationReason, RedeemCancellation, RedeemCancellationReason, RedeemFulfillment,
    Send, TransactionStatus, XandApiClientTrait, XandTransaction,
};
use xand_api_proto::proto_models::{
    CorrelationId, CreateCancellation, CreateRequestCompletion, PendingCreateRequest,
    PendingRedeemRequest, RedeemRequestCompletion, Transaction, TransactionId,
};
use xand_test_helpers::{wait_until_transaction_cached, Network, NetworkBuilder, XandKeyRing};
use xandstrate_client::{remove_member_extrinsic, CallExt};

fn only_transactions<F>(history: Vec<Transaction>, filter: F) -> Vec<XandTransaction>
where
    F: FnMut(&XandTransaction) -> bool,
{
    history.into_iter().map(|t| t.txn).filter(filter).collect()
}

fn only_sends(history: Vec<Transaction>) -> Vec<XandTransaction> {
    only_transactions(history, |t| matches!(t, XandTransaction::Send(_)))
}

#[tokio::test]
async fn test_client_with_unimplemented_confidentiality_mode() {
    let _ = NetworkBuilder::default()
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Trust)
                .unwrap()
                .get_pending_create_requests(None)
                .await
                .unwrap()
        })
        .await;
}

async fn create_request(
    network: &Network,
    member: XandKeyRing,
    amount: u64,
) -> Result<(CorrelationId, TransactionId), XandApiClientError> {
    let client = network.api_services.get(&member).unwrap();
    let bank_account = BankAccountInfo::Unencrypted(BankAccountId {
        routing_number: "123".into(),
        account_number: "456".into(),
    });
    let correlation_id = CorrelationId::gen_random();
    let request = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: amount,
        correlation_id: correlation_id.clone(),
        account: bank_account,
        completing_transaction: None,
    });
    let transaction_id = client
        .submit_transaction(member.into(), request)
        .await?
        .next()
        .await
        .unwrap()
        .unwrap()
        .id;
    Ok((correlation_id, transaction_id))
}

async fn create(network: &Network, member: XandKeyRing, amount: u64) {
    let client = network.api_services.get(&member).unwrap();
    let (correlation_id, request_id) = create_request(network, member, amount).await.unwrap();
    let trust = network.api_services.get(&XandKeyRing::Trust).unwrap();
    wait_until_transaction_cached(trust, &request_id).await;
    let fulfillment = XandTransaction::CashConfirmation(CashConfirmation { correlation_id });
    let fulfillment_id = trust
        .submit_transaction_wait_final(XandKeyRing::Trust.into(), fulfillment)
        .await
        .unwrap()
        .id;
    wait_until_transaction_cached(client, &fulfillment_id).await;
}

#[tokio::test]
async fn confidential_create_transaction_successful_flow() {
    confidential_create(TestConfig {
        create_issuer: Address::from(XandKeyRing::Tupac),
        create_amount: 500,
        create_response: CreateResponse::Fulfill,
        expected_balance: 500,
        response_issuer: Some(XandKeyRing::Trust),
        expected_pending: 0,
        nb_blocks_to_seal_after_request: None,
        verify_history: |transactions| {
            let financial_txns: Vec<_> = transactions
                .into_iter()
                .filter(|txn| txn.is_financial_event())
                .collect();

            // ensure the completion id matches to completing transaction
            assert!(matches!(
                financial_txns.as_slice(),
                [
                    Transaction {
                        transaction_id: linking_id,
                        txn: XandTransaction::CashConfirmation(_),
                        ..
                    },
                    Transaction {
                        txn: XandTransaction::CreateRequest(PendingCreateRequest {
                            completing_transaction: Some(CreateRequestCompletion::Confirmation(id)),
                            ..
                        }),
                        ..
                    }
                ] if id == linking_id
            ));
        },
    })
    .await;
}

#[tokio::test]
async fn confidential_create_transaction_cancellation_flow() {
    confidential_create(TestConfig {
        create_issuer: Address::from(XandKeyRing::Tupac),
        create_amount: 500,
        create_response: CreateResponse::Cancel,
        expected_balance: 0,
        response_issuer: Some(XandKeyRing::Trust),
        expected_pending: 0,
        nb_blocks_to_seal_after_request: None,
        verify_history: |transactions| {
            // ensure cancellation id matches completing transactions
            assert!(matches!(
                transactions.as_slice(),
                [
                    Transaction {
                        transaction_id: linking_id,
                        txn: XandTransaction::CreateCancellation(CreateCancellation {
                            reason: _,
                            ..
                        }),
                        ..
                    },
                    Transaction {
                        txn: XandTransaction::CreateRequest(PendingCreateRequest {
                            completing_transaction: Some(CreateRequestCompletion::Cancellation(id)),
                            ..
                        }),
                        ..
                    }
                ] if id == linking_id
            ));
        },
    })
    .await;
}

#[tokio::test]
async fn confidential_create_transaction_invalid_fulfillment_flow() {
    confidential_create(TestConfig {
        create_issuer: Address::from(XandKeyRing::Tupac),
        create_amount: 500,
        create_response: CreateResponse::OtherMemberFulfill,
        expected_balance: 0,
        response_issuer: Some(XandKeyRing::Biggie),
        expected_pending: 1,
        nb_blocks_to_seal_after_request: None,
        verify_history: no_linked_txs,
    })
    .await;
}

#[tokio::test]
async fn confidential_create_transaction_invalid_cancellation_flow() {
    confidential_create(TestConfig {
        create_issuer: Address::from(XandKeyRing::Tupac),
        create_amount: 500,
        create_response: CreateResponse::OtherMemberCancel,
        expected_balance: 0,
        response_issuer: Some(XandKeyRing::Biggie),
        expected_pending: 1,
        nb_blocks_to_seal_after_request: None,
        verify_history: no_linked_txs,
    })
    .await;
}

#[tokio::test]
async fn confidential_create_transaction_expiration_flow() {
    confidential_create(TestConfig {
        create_issuer: Address::from(XandKeyRing::Tupac),
        create_amount: 500,
        create_response: CreateResponse::Expiration,
        expected_balance: 0,
        response_issuer: None,
        expected_pending: 0,
        nb_blocks_to_seal_after_request: Some(10),
        verify_history: |transactions| {
            assert!(
                matches!(
                    transactions.as_slice()[1],
                    Transaction {
                        txn: XandTransaction::CreateRequest(PendingCreateRequest {
                            completing_transaction: Some(CreateRequestCompletion::Cancellation(_)),
                            ..
                        }),
                        ..
                    },
                ),
                "Unexpected transactions: {:?}",
                transactions,
            );
        },
    })
    .await;
}

enum CreateResponse {
    Fulfill,
    Cancel,
    OtherMemberFulfill,
    OtherMemberCancel,
    Expiration,
}

struct TestConfig<F>
where
    F: FnOnce(Vec<Transaction>),
{
    create_issuer: Address,
    create_amount: u64,
    create_response: CreateResponse,
    expected_balance: u64,
    response_issuer: Option<XandKeyRing>,
    expected_pending: usize,
    nb_blocks_to_seal_after_request: Option<usize>,
    verify_history: F,
}

async fn confidential_create<F>(config: TestConfig<F>)
where
    F: FnOnce(Vec<Transaction>) + core::marker::Send + Sync,
{
    let correlation_id = CorrelationId::gen_random();
    let amount = config.create_amount;
    let account = xand_api_proto::proto_models::BankAccountInfo::Unencrypted(
        xand_api_proto::proto_models::BankAccountId {
            routing_number: "111111111".to_string(),
            account_number: "222222222".to_string(),
        },
    );

    let transaction = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: amount,
        correlation_id: correlation_id.clone(),
        account,
        completing_transaction: None,
    });

    // need to generate the response transaction here since it depends on the correlation_id
    let response_tx = match config.create_response {
        CreateResponse::Fulfill => Some(XandTransaction::CashConfirmation(CashConfirmation {
            correlation_id,
        })),
        CreateResponse::Cancel => Some(XandTransaction::CreateCancellation(CreateCancellation {
            correlation_id,
            reason: CreateCancellationReason::InvalidData,
        })),
        CreateResponse::OtherMemberFulfill => {
            Some(XandTransaction::CashConfirmation(CashConfirmation {
                correlation_id,
            }))
        }
        CreateResponse::OtherMemberCancel => {
            Some(XandTransaction::CreateCancellation(CreateCancellation {
                correlation_id,
                reason: CreateCancellationReason::InvalidData,
            }))
        }
        CreateResponse::Expiration => None,
    };

    let (
        pending_creates_before_handled,
        pending_creates_after_handled,
        transactions,
        balance,
        total_issuance,
    ) = NetworkBuilder::default()
        .add_service(XandKeyRing::Xand1)
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .run(|network| {
            let create_issuer = config.create_issuer.clone();
            let response_issuer = config.response_issuer;
            let nb_blocks_to_seal_after_request = config.nb_blocks_to_seal_after_request;

            async move {
                let issuer_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();

                issuer_client
                    .submit_transaction_wait_final(create_issuer.clone(), transaction.clone())
                    .await
                    .unwrap();
                let pending_creates_before_handled = issuer_client
                    .get_pending_create_requests(None)
                    .await
                    .unwrap()
                    .data;
                // only submit a response transaction if expected to
                let response_txn_id = match (response_tx, response_issuer) {
                    (Some(tx), Some(issuer)) => Some(
                        network
                            .api_services
                            .get(&issuer)
                            .unwrap()
                            .submit_transaction_wait_final(issuer.into(), tx)
                            .await
                            .unwrap()
                            .id,
                    ),
                    _ => None,
                };
                // wait if needed (for cancellation)
                if let Some(block_count) = nb_blocks_to_seal_after_request {
                    network.block_seal.seal(block_count).await;
                    // Let petri catch up and update the completing_transaction
                    tokio::time::sleep(Duration::from_secs(5)).await;
                }
                // Wait until the response transaction is cached by petri in the issuer node.
                let transactions =
                    IntervalStream::new(tokio::time::interval(Duration::from_millis(200)))
                        .then(|_| async {
                            issuer_client
                                .get_transaction_history(None, &Default::default())
                                .await
                                .unwrap()
                                .data
                        })
                        .filter(|transactions| {
                            let found = response_txn_id.as_ref().map_or(true, |txn_id| {
                                transactions.iter().any(|txn| txn.transaction_id == *txn_id)
                            });
                            ready(found)
                        });
                pin_mut!(transactions);
                let transactions =
                    tokio::time::timeout(Duration::from_secs(20), transactions.next()).await;
                let transactions = transactions
                    .ok()
                    .flatten()
                    .expect("Transaction history can be retrieved");
                let pending_creates_after_handled = issuer_client
                    .get_pending_create_requests(None)
                    .await
                    .unwrap()
                    .data;
                let balance = issuer_client
                    .get_balance(&create_issuer.to_string())
                    .await
                    .unwrap();
                let total_issuance = issuer_client.get_total_issuance().await.unwrap();
                (
                    pending_creates_before_handled,
                    pending_creates_after_handled,
                    transactions,
                    balance,
                    total_issuance,
                )
            }
        })
        .await;

    // Verify linking transaction based on response config
    (config.verify_history)(transactions);
    assert!(!pending_creates_before_handled.is_empty());
    assert_eq!(balance, Some(config.expected_balance as u128));
    assert_eq!(total_issuance.total_issued, config.expected_balance);
    assert_eq!(pending_creates_after_handled.len(), config.expected_pending);
}

fn no_linked_txs(transactions: Vec<Transaction>) {
    // Ensure completing transaction isn't set for invalid correlating transactions
    assert!(
        matches!(
            transactions.as_slice(),
            [
                Transaction {
                    status: TransactionStatus::Invalid(_),
                    ..
                },
                Transaction {
                    txn: XandTransaction::CreateRequest(PendingCreateRequest {
                        completing_transaction: None,
                        ..
                    }),
                    ..
                }
            ],
        ),
        "Unexpected transactions: {:?}",
        transactions,
    );
}

#[tokio::test]
async fn confidential_create_by_banned_member_rejected() {
    const BANNED_ISSUER: XandKeyRing = XandKeyRing::Tupac;
    let tx = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|mut network| async move {
            create(&network, BANNED_ISSUER, 100_000_000).await;

            let address: Address = BANNED_ISSUER.into();
            let pubkey = sr25519::Public::from_ss58check(&address.to_string()).unwrap();
            let id = network
                .extrinsic_submitter
                .submit_extrinsic(
                    remove_member_extrinsic(pubkey.into()).into_sudo(),
                    XandKeyRing::Admin,
                )
                .await;
            let banned_client = network.api_services.get(&BANNED_ISSUER).unwrap();
            wait_until_transaction_cached(
                banned_client,
                &TransactionId::try_from(id.as_ref().to_vec()).unwrap(),
            )
            .await;

            let (_, create_id) = create_request(&network, BANNED_ISSUER, 50).await.unwrap();
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &create_id).await;
            trust_client
                .get_transaction_details(&create_id)
                .await
                .unwrap()
        })
        .await;
    assert_eq!(
        tx.status,
        TransactionStatus::Invalid("TransactionCouldNotBeVerifiedConstructedByBannedMember".into())
    );
}

#[tokio::test]
async fn get_empty_confidential_redeems() {
    NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            let issuer_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let actual = issuer_client
                .get_pending_redeem_requests(None)
                .await
                .unwrap()
                .data;
            let expected = Vec::<PendingRedeemRequest>::new();
            assert_eq!(expected, actual);
        })
        .await;
}

#[tokio::test]
async fn get_confidential_redeems_returns_existing_redeem() {
    // Given
    let expected_redeem = PendingRedeemRequest {
        amount_in_minor_unit: 10,
        correlation_id: CorrelationId::gen_random(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(expected_redeem.clone());

    // When
    let actual_redeems = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| {
            let transaction = transaction.clone();
            async move {
                create(&network, XandKeyRing::Tupac, 100_000_000).await;
                let issuer_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
                issuer_client
                    .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                    .await
                    .unwrap();
                issuer_client
                    .get_pending_redeem_requests(None)
                    .await
                    .unwrap()
                    .data
            }
        })
        .await;

    // Then
    assert_eq!(actual_redeems.len(), 1);
    assert_eq!(actual_redeems[0], expected_redeem)
}

#[tokio::test]
async fn confidential_send_by_banned_member_rejected() {
    const BANNED_ISSUER: XandKeyRing = XandKeyRing::Tupac;
    let tx = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .add_service(XandKeyRing::Biggie)
        .run(|mut network| async move {
            create(&network, BANNED_ISSUER, 100_000_000).await;

            let address: Address = BANNED_ISSUER.into();
            let pubkey = sr25519::Public::from_ss58check(&address.to_string()).unwrap();
            let id = network
                .extrinsic_submitter
                .submit_extrinsic(
                    remove_member_extrinsic(pubkey.into()).into_sudo(),
                    XandKeyRing::Admin,
                )
                .await;
            let banned_client = network.api_services.get(&BANNED_ISSUER).unwrap();
            wait_until_transaction_cached(
                banned_client,
                &TransactionId::try_from(id.as_ref().to_vec()).unwrap(),
            )
            .await;

            let send_id = banned_client
                .submit_transaction_wait_final(
                    BANNED_ISSUER.into(),
                    XandTransaction::Send(Send {
                        destination_account: XandKeyRing::Biggie.into(),
                        amount_in_minor_unit: 1,
                    }),
                )
                .await
                .unwrap()
                .id;

            let biggie_client = network.api_services.get(&XandKeyRing::Biggie).unwrap();
            wait_until_transaction_cached(biggie_client, &send_id).await;
            biggie_client
                .get_transaction_details(&send_id)
                .await
                .unwrap()
        })
        .await;
    assert_eq!(
        tx.status,
        TransactionStatus::Invalid("TransactionCouldNotBeVerifiedConstructedByBannedMember".into())
    );
}

#[tokio::test]
async fn confidential_send_is_returned_in_history() {
    let transaction = XandTransaction::Send(Send {
        destination_account: XandKeyRing::Biggie.into(),
        amount_in_minor_unit: 10,
    });
    let (tupac_history, biggie_history) = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .run(|network| {
            let transaction = transaction.clone();
            async move {
                create(&network, XandKeyRing::Tupac, 100_000_000).await;
                let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
                let txn_id = tupac_client
                    .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction.clone())
                    .await
                    .unwrap()
                    .id;
                let tupac_history = tupac_client
                    .get_transaction_history(None, &Default::default())
                    .await
                    .unwrap()
                    .data;
                let biggie_client = network.api_services.get(&XandKeyRing::Biggie).unwrap();
                wait_until_transaction_cached(biggie_client, &txn_id).await;
                let biggie_history = biggie_client
                    .get_transaction_history(None, &Default::default())
                    .await
                    .unwrap()
                    .data;
                (tupac_history, biggie_history)
            }
        })
        .await;
    let tupac_sends = only_sends(tupac_history);
    assert_eq!(&*tupac_sends, std::slice::from_ref(&transaction));
    let biggie_sends = only_sends(biggie_history);
    assert_eq!(&*biggie_sends, std::slice::from_ref(&transaction));
}

#[tokio::test]
async fn confidential_send_fails_with_non_member_recipient() {
    let transaction = XandTransaction::Send(Send {
        destination_account: XandKeyRing::Trust.into(),
        amount_in_minor_unit: 10,
    });
    let transaction_result = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| {
            let transaction = transaction.clone();
            async move {
                create(&network, XandKeyRing::Tupac, 100_000_000).await;
                let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
                tupac_client
                    .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction.clone())
                    .await
            }
        })
        .await;

    assert!(
        matches!(
            transaction_result.clone(),
            Err(XandApiClientError::NotFound {
                message
            }) if message == *"\"InvalidMember\"".to_string()
        ),
        "{:?}",
        transaction_result
    );
}

#[tokio::test]
async fn confidential_send_updates_balances() {
    let transaction = XandTransaction::Send(Send {
        destination_account: XandKeyRing::Biggie.into(),
        amount_in_minor_unit: 10,
    });
    let (tupac_initial_balance, tupac_balance, biggie_balance) = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            create(&network, XandKeyRing::Tupac, 100).await;
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let tupac_initial_balance = tupac_client
                .get_balance(&Address::from(XandKeyRing::Tupac).to_string())
                .await
                .unwrap();
            let send_id = tupac_client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                .await
                .unwrap()
                .id;
            let tupac_balance = tupac_client
                .get_balance(&Address::from(XandKeyRing::Tupac).to_string())
                .await
                .unwrap();
            let biggie_client = network.api_services.get(&XandKeyRing::Biggie).unwrap();
            wait_until_transaction_cached(biggie_client, &send_id).await;
            let biggie_balance = biggie_client
                .get_balance(&Address::from(XandKeyRing::Biggie).to_string())
                .await
                .unwrap();
            (tupac_initial_balance, tupac_balance, biggie_balance)
        })
        .await;
    assert_eq!(tupac_initial_balance, Some(100));
    assert_eq!(tupac_balance, Some(90));
    assert_eq!(biggie_balance, Some(10));
}

#[tokio::test]
async fn confidential_redeem_is_returned_in_history() {
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: 10,
        correlation_id: CorrelationId::gen_random(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (tupac_history, trust_history) = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| {
            let transaction = transaction.clone();
            async move {
                create(&network, XandKeyRing::Tupac, 100_000_000).await;
                let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
                let txn_id = tupac_client
                    .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                    .await
                    .unwrap()
                    .id;
                let tupac_history = tupac_client
                    .get_transaction_history(None, &Default::default())
                    .await
                    .unwrap()
                    .data;
                let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
                wait_until_transaction_cached(trust_client, &txn_id).await;
                let trust_history = trust_client
                    .get_transaction_history(None, &Default::default())
                    .await
                    .unwrap()
                    .data;
                (tupac_history, trust_history)
            }
        })
        .await;
    let tupac_redeems = only_transactions(tupac_history, |t| {
        matches!(t, XandTransaction::RedeemRequest(_))
    });
    assert_eq!(&*tupac_redeems, std::slice::from_ref(&transaction));
    let trust_redeems = only_transactions(trust_history, |t| {
        matches!(t, XandTransaction::RedeemRequest(_))
    });
    assert_eq!(&*trust_redeems, std::slice::from_ref(&transaction));
}

#[tokio::test]
async fn confidential_redeem_fulfillment_is_returned_in_history() {
    const INITIAL_BALANCE: u64 = 100_000_000;
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let request = XandTransaction::RedeemRequest(redeem);
    let fulfillment = XandTransaction::RedeemFulfillment(RedeemFulfillment {
        correlation_id: redeem_correlation_id.clone(),
    });
    let history = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| {
            let fulfillment = fulfillment.clone();
            async move {
                create(&network, XandKeyRing::Tupac, INITIAL_BALANCE).await;
                let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
                let request_id = tupac_client
                    .submit_transaction_wait_final(XandKeyRing::Tupac.into(), request)
                    .await
                    .unwrap()
                    .id;
                let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
                wait_until_transaction_cached(trust_client, &request_id).await;
                let fulfillment_id = trust_client
                    .submit_transaction_wait_final(XandKeyRing::Trust.into(), fulfillment)
                    .await
                    .unwrap()
                    .id;
                wait_until_transaction_cached(tupac_client, &fulfillment_id).await;
                tupac_client
                    .get_transaction_history(None, &Default::default())
                    .await
                    .unwrap()
                    .data
            }
        })
        .await;
    let fulfillments = only_transactions(history, |t| {
        matches!(t, XandTransaction::RedeemFulfillment(_))
    });
    assert_eq!(&*fulfillments, std::slice::from_ref(&fulfillment));
}

#[tokio::test]
async fn confidential_redeem_cancellation_is_returned_in_history() {
    const INITIAL_BALANCE: u64 = 100_000_000;
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let request = XandTransaction::RedeemRequest(redeem);
    let cancellation = XandTransaction::RedeemCancellation(RedeemCancellation {
        correlation_id: redeem_correlation_id.clone(),
        reason: RedeemCancellationReason::InvalidData,
    });
    let history = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            create(&network, XandKeyRing::Tupac, INITIAL_BALANCE).await;
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let request_id = tupac_client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), request)
                .await
                .unwrap()
                .id;
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let cancellation_id = trust_client
                .submit_transaction_wait_final(XandKeyRing::Trust.into(), cancellation)
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(tupac_client, &cancellation_id).await;
            tupac_client
                .get_transaction_history(None, &Default::default())
                .await
                .unwrap()
                .data
        })
        .await;
    let cancellations = only_transactions(history, |t| {
        matches!(t, XandTransaction::RedeemCancellation(_))
    });
    assert!(matches!(
        &*cancellations,
        [XandTransaction::RedeemCancellation(RedeemCancellation { correlation_id: nonce, .. })] if *nonce == redeem_correlation_id
    ));
}

#[tokio::test]
async fn confidential_redeem_updates_balance_and_total_issuance() {
    // biggie balance is used to verify total claims are reduced
    const BIGGIE_INITIAL_BALANCE: u64 = 50_000_000;
    // tupac balance is used to verify that tupacs balance is reduced
    const TUPAC_INITIAL_BALANCE: u64 = 100_000_000;
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (new_balance, total_issuance) = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            create(&network, XandKeyRing::Biggie, BIGGIE_INITIAL_BALANCE).await;
            create(&network, XandKeyRing::Tupac, TUPAC_INITIAL_BALANCE).await;
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let _ = tupac_client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                .await
                .unwrap();
            let balance = tupac_client
                .get_balance(&Address::from(XandKeyRing::Tupac).to_string())
                .await
                .unwrap()
                .unwrap();
            let total_issuance = tupac_client.get_total_issuance().await.unwrap();
            (balance, total_issuance)
        })
        .await;

    let expected_balance = TUPAC_INITIAL_BALANCE - REDEEM_AMOUNT;
    let expected_issuance = BIGGIE_INITIAL_BALANCE + TUPAC_INITIAL_BALANCE - REDEEM_AMOUNT;

    assert_eq!(new_balance, u128::from(expected_balance));
    assert_eq!(total_issuance.total_issued, expected_issuance);
}

#[tokio::test]
async fn confidential_redeem_cancellation_updates_balance() {
    const INITIAL_BALANCE: u64 = 100_000_000;
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let new_balance = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            create(&network, XandKeyRing::Tupac, INITIAL_BALANCE).await;
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let request_id = tupac_client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                .await
                .unwrap()
                .id;
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let cancellation_id = trust_client
                .submit_transaction_wait_final(
                    XandKeyRing::Trust.into(),
                    XandTransaction::RedeemCancellation(RedeemCancellation {
                        correlation_id: redeem_correlation_id.clone(),
                        reason: RedeemCancellationReason::InvalidData,
                    }),
                )
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(tupac_client, &cancellation_id).await;
            tupac_client
                .get_balance(&Address::from(XandKeyRing::Tupac).to_string())
                .await
                .unwrap()
                .unwrap()
        })
        .await;
    assert_eq!(new_balance, u128::from(INITIAL_BALANCE));
}

#[tokio::test]
async fn confidential_redeem_fulfillment_updates_completing_transaction() {
    const INITIAL_BALANCE: u64 = 100_000_000;
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (request, fulfillment_id) = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            create(&network, XandKeyRing::Tupac, INITIAL_BALANCE).await;
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let request_id = tupac_client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                .await
                .unwrap()
                .id;
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let fulfillment_id = trust_client
                .submit_transaction_wait_final(
                    XandKeyRing::Trust.into(),
                    XandTransaction::RedeemFulfillment(RedeemFulfillment {
                        correlation_id: redeem_correlation_id.clone(),
                    }),
                )
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(tupac_client, &fulfillment_id).await;
            (
                tupac_client
                    .get_transaction_details(&request_id)
                    .await
                    .unwrap(),
                fulfillment_id,
            )
        })
        .await;
    assert!(matches!(
        request,
        Transaction {
            txn: XandTransaction::RedeemRequest(PendingRedeemRequest {
                completing_transaction,
                ..
            }),
            ..
        } if completing_transaction == Some(RedeemRequestCompletion::Confirmation(fulfillment_id))
    ));
}

#[tokio::test]
async fn confidential_redeem_cancellation_updates_completing_transaction() {
    const INITIAL_BALANCE: u64 = 100_000_000;
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (request, cancellation_id) = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            create(&network, XandKeyRing::Tupac, INITIAL_BALANCE).await;
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let request_id = tupac_client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                .await
                .unwrap()
                .id;
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let cancellation_id = trust_client
                .submit_transaction_wait_final(
                    XandKeyRing::Trust.into(),
                    XandTransaction::RedeemCancellation(RedeemCancellation {
                        correlation_id: redeem_correlation_id.clone(),
                        reason: RedeemCancellationReason::InvalidData,
                    }),
                )
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(tupac_client, &cancellation_id).await;
            (
                tupac_client
                    .get_transaction_details(&request_id)
                    .await
                    .unwrap(),
                cancellation_id,
            )
        })
        .await;
    assert!(matches!(
        request,
        Transaction {
            txn: XandTransaction::RedeemRequest(PendingRedeemRequest {
                completing_transaction,
                ..
            }),
            ..
        } if completing_transaction == Some(RedeemRequestCompletion::Cancellation(cancellation_id))
    ));
}

#[tokio::test]
async fn status_updates_end_with_finalized_when_transactions_are_submitted_concurrently() {
    let statuses = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Trust)
        .run(|network| async move {
            let tupac_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            create(&network, XandKeyRing::Tupac, 100).await;
            let tupac = Address::from(XandKeyRing::Tupac);
            let biggie = Address::from(XandKeyRing::Biggie);
            futures::stream::iter(1..10)
                .map(|amount| {
                    let send = XandTransaction::Send(Send {
                        destination_account: biggie.clone(),
                        amount_in_minor_unit: amount,
                    });
                    async {
                        tupac_client
                            .submit_transaction_wait_final(tupac.clone(), send)
                            .await
                            .unwrap()
                            .status
                    }
                })
                .buffer_unordered(9)
                .collect::<Vec<_>>()
                .await
        })
        .await;
    for status in statuses {
        assert_eq!(status, TransactionStatus::Finalized);
    }
}
//TODO: ADO 7824 - Add a confidential version of create_errors_when_bank_account_is_too_long

//TODO: ADO 7824 - Add a confidential version of redeem_errors_when_bank_account_is_too_long
