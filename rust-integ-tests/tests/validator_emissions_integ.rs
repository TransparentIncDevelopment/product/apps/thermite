//! Tests for paid participant (validator) emissions
use xand_address::Address;
use xand_api_client::{CorrelationId, PendingCreateRequest, XandApiClientTrait, XandTransaction};
use xand_api_proto::proto_models::{
    BankAccountId, BankAccountInfo, PendingRedeemRequest, RedeemCancellation,
    RedeemCancellationReason, RedeemFulfillment, RedeemRequestCompletion, Transaction,
};
use xand_models::{dev_account_key, ToAddress};
use xand_test_helpers::{wait_until_transaction_cached, Network, NetworkBuilder, XandKeyRing};
use xandstrate_client::Pair;

#[tokio::test]
async fn validator_emission_rate_can_be_retrieved() {
    let default_rate = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_validator_emission_rate()
                .await
                .unwrap()
        })
        .await;
    assert_eq!(
        default_rate,
        xand_api_client::ValidatorEmissionRate {
            minor_units_per_emission: 0,
            block_quota: 1,
        }
    );
}

#[tokio::test]
async fn missing_validator_emission_progress_returns_not_found_error() {
    // Given
    let missing_addr = dev_account_key("MISSING VALIDATOR").public().to_address();

    // When
    let not_found_err = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            network
                .api_services
                .get(&XandKeyRing::Tupac)
                .unwrap()
                .get_validator_emission_progress(missing_addr)
                .await
                .unwrap_err()
        })
        .await;

    // Then, since SubstrateClientsErrors (and others) aren't comparable
    // we stringify the error and compare it to the expected structure.
    let stringified_err = format!("{:?}", not_found_err);
    let manually_recreated_error = "NotFound { message: \"{\\\"SubstrateClient\\\":{\\\"source\\\":{\\\"ValidatorEmissionProgressNotFound\\\":{\\\"address\\\":\\\"5EJBtnAUntmGqrpja9e8h4p5tPuhcYVJUNDQKvHjTguoXvKm\\\"}}}}\" }";
    assert_eq!(stringified_err, manually_recreated_error.to_string());
}

#[tokio::test]
async fn validator_emission_progress_returns_successfully() {
    // When at least one block exists and we check the progress of a validator we know created a block
    // (because there is only one validator in the default testnet being run)
    let cidr_block = xand_api_proto::proto_models::CidrBlock([1, 2, 3, 4, 5]);
    let allowlistcidrblock = xand_api_proto::proto_models::AllowlistCidrBlock { cidr_block };
    let progress = NetworkBuilder::default()
        .add_service(XandKeyRing::Tupac)
        .run(|network| async move {
            let client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            let transaction = XandTransaction::AllowlistCidrBlock(allowlistcidrblock);
            let _ = client
                .submit_transaction_wait_final(XandKeyRing::Tupac.into(), transaction)
                .await
                .unwrap();

            let validator = client
                .get_authority_keys()
                .await
                .unwrap()
                .into_iter()
                .next()
                .unwrap();
            client
                .get_validator_emission_progress(validator)
                .await
                .unwrap()
        })
        .await;
    // Then that validator's emission progress can be retrieved
    assert_eq!(
        progress,
        xand_api_client::ValidatorEmissionProgress {
            effective_emission_rate: xand_api_client::ValidatorEmissionRate {
                minor_units_per_emission: 0,
                block_quota: 1,
            },
            blocks_completed_progress: 0,
        }
    );
}

#[tokio::test]
async fn validator_emission_rate_returns_successfully_when_initialized_nonzero() {
    // Given | A network with an initial emission rate of 100 minor units
    let progress = NetworkBuilder::default()
        .add_service(XandKeyRing::Admin)
        .add_service(XandKeyRing::Xand1)
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            // The chain needs to progress as least 1 block and the only mechanism is to submit transactions and wait.
            let correlation_id = CorrelationId::gen_random();
            let account = xand_api_proto::proto_models::BankAccountInfo::Unencrypted(
                xand_api_proto::proto_models::BankAccountId {
                    routing_number: "111111111".to_string(),
                    account_number: "222222222".to_string(),
                },
            );
            let transaction = XandTransaction::CreateRequest(PendingCreateRequest {
                amount_in_minor_unit: 10,
                correlation_id: correlation_id.clone(),
                account,
                completing_transaction: None,
            });
            let issuer_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();
            issuer_client
                .submit_transaction_wait_final(
                    Address::from(XandKeyRing::Tupac),
                    transaction.clone(),
                )
                .await
                .unwrap();

            // When | Emission progress is checked for a validator
            let client = network.api_services.get(&XandKeyRing::Admin).unwrap();
            let validator = client
                .get_authority_keys()
                .await
                .unwrap()
                .into_iter()
                .next()
                .unwrap();
            client
                .get_validator_emission_progress(validator)
                .await
                .unwrap()
        })
        .await;

    // Then | the emission progress shows the initial emission rate of 100 minor units
    let expected_validator_emission_rate = xand_api_client::ValidatorEmissionRate {
        minor_units_per_emission: 100,
        block_quota: 1,
    };

    assert_eq!(
        progress.effective_emission_rate,
        expected_validator_emission_rate
    );
}

#[tokio::test]
async fn validator_balance_is_nonzero_after_emission_awards() {
    // Given | A network with an initial emission rate of 100 minor units
    let nonzero_balances = NetworkBuilder::default()
        .add_service(XandKeyRing::Admin)
        .add_service(XandKeyRing::Xand1)
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            let issuer_client = network.api_services.get(&XandKeyRing::Tupac).unwrap();

            // Submit a create to let at least one block process
            let account = xand_api_proto::proto_models::BankAccountInfo::Unencrypted(
                xand_api_proto::proto_models::BankAccountId {
                    routing_number: "111111111".to_string(),
                    account_number: "222222222".to_string(),
                },
            );

            // The chain needs to progress a few blocks to allow rewards to accumulate and finalize.
            for _ in 0..10 {
                let correlation_id = CorrelationId::gen_random();
                let transaction = XandTransaction::CreateRequest(PendingCreateRequest {
                    amount_in_minor_unit: 1,
                    correlation_id: correlation_id.clone(),
                    account: account.clone(),
                    completing_transaction: None,
                });
                issuer_client
                    .submit_transaction_wait_final(
                        Address::from(XandKeyRing::Tupac),
                        transaction.clone(),
                    )
                    .await
                    .unwrap();
            }

            // When | Validator balance is checked
            let client = network.api_services.get(&XandKeyRing::Xand1).unwrap();
            let validators = client.get_authority_keys().await.unwrap();

            let mut non_zero_validator_balances = vec![];
            for validator in validators {
                let validator_balance_option = client
                    .get_balance(validator.to_string().as_str())
                    .await
                    .unwrap();
                if let Some(balance) = validator_balance_option {
                    if balance > 0u128 {
                        non_zero_validator_balances.push(balance);
                    }
                };
            }
            non_zero_validator_balances
        })
        .await;

    // Then | At least one validator has a non zero value
    assert!(!nonzero_balances.is_empty());
}

#[tokio::test]
async fn total_issuance_reflects_validator_emissions() {
    // Given | A network with an initial emission rate of 100 minor units
    let (initial_total_issuance, final_total_issuance) = NetworkBuilder::default()
        .add_service(XandKeyRing::Admin)
        .add_service(XandKeyRing::Xand1)
        .add_service(XandKeyRing::Tupac)
        .add_service(XandKeyRing::Biggie)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            let client = network.api_services.get(&XandKeyRing::Admin).unwrap();

            // Have a block created so we can check issuance
            have_issuer_submit_n_creates_to_progress_chain(&network, XandKeyRing::Tupac, 1).await;

            // Get initial total issuance
            let initial_total_issuance = client.get_total_issuance().await.unwrap();

            // The chain needs to progress as least 1 more block and the only mechanism is to submit transactions and wait.
            // Note these create requests add up to 10, which is less than one emission reward (100)
            have_issuer_submit_n_creates_to_progress_chain(&network, XandKeyRing::Tupac, 10).await;

            // When | We query for the total issuance after some blocks have been processed
            let final_total_issuance = client.get_total_issuance().await.unwrap();
            (initial_total_issuance, final_total_issuance)
        })
        .await;

    // Then | The final total issuance is at least the emission rate greater than the initial total issuance
    let emission_rate = 100; // Not used before the assert
    assert!(
        final_total_issuance.total_issued > initial_total_issuance.total_issued + emission_rate
    );
}

async fn have_issuer_submit_n_creates_to_progress_chain(
    network: &Network,
    issuer: XandKeyRing,
    n: u8,
) {
    let issuer_client = network.api_services.get(&issuer).unwrap();
    // Submit a create to let at least one block process
    let account = xand_api_proto::proto_models::BankAccountInfo::Unencrypted(
        xand_api_proto::proto_models::BankAccountId {
            routing_number: "111111111".to_string(),
            account_number: "222222222".to_string(),
        },
    );
    for _ in 0..n {
        let correlation_id = CorrelationId::gen_random();
        let transaction = XandTransaction::CreateRequest(PendingCreateRequest {
            amount_in_minor_unit: 1,
            correlation_id: correlation_id.clone(),
            account: account.clone(),
            completing_transaction: None,
        });
        issuer_client
            .submit_transaction_wait_final(Address::from(issuer), transaction.clone())
            .await
            .unwrap();
    }
}

#[tokio::test]
async fn validator_redeem_updates_balance() {
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (old_balance, new_balance) = NetworkBuilder::default()
        .add_service(XandKeyRing::Xand1)
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            let client = network.api_services.get(&XandKeyRing::Xand1).unwrap();

            have_issuer_submit_n_creates_to_progress_chain(&network, XandKeyRing::Biggie, 1).await;

            let old_balance = client
                .get_balance(&Address::from(XandKeyRing::Xand1).to_string())
                .await
                .unwrap()
                .unwrap();
            let _ = client
                .submit_transaction_wait_final(XandKeyRing::Xand1.into(), transaction)
                .await
                .unwrap();
            let new_balance = client
                .get_balance(&Address::from(XandKeyRing::Xand1).to_string())
                .await
                .unwrap()
                .unwrap();
            (old_balance, new_balance)
        })
        .await;

    const BLOCK_REWARD: u128 = 100;
    let expected_balance = old_balance + BLOCK_REWARD - (REDEEM_AMOUNT as u128);

    assert_eq!(new_balance, expected_balance);
}

#[tokio::test]
async fn validator_redeem_cancellation_updates_balance() {
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let redeem_transaction = XandTransaction::RedeemRequest(redeem);
    let (balance_pending_redeem, balance_after_cancellation) = NetworkBuilder::default()
        .add_service(XandKeyRing::Xand1)
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            let client = network.api_services.get(&XandKeyRing::Xand1).unwrap();

            have_issuer_submit_n_creates_to_progress_chain(&network, XandKeyRing::Biggie, 1).await;

            let request_id = client
                .submit_transaction_wait_final(XandKeyRing::Xand1.into(), redeem_transaction)
                .await
                .unwrap()
                .id;
            let old_balance = client
                .get_balance(&Address::from(XandKeyRing::Xand1).to_string())
                .await
                .unwrap()
                .unwrap();
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let cancellation_id = trust_client
                .submit_transaction_wait_final(
                    XandKeyRing::Trust.into(),
                    XandTransaction::RedeemCancellation(RedeemCancellation {
                        correlation_id: redeem_correlation_id.clone(),
                        reason: RedeemCancellationReason::InvalidData,
                    }),
                )
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(client, &cancellation_id).await;
            let new_balance = client
                .get_balance(&Address::from(XandKeyRing::Xand1).to_string())
                .await
                .unwrap()
                .unwrap();
            (old_balance, new_balance)
        })
        .await;

    const BLOCK_REWARD: u128 = 100;
    let expected_balance = balance_pending_redeem + BLOCK_REWARD + (REDEEM_AMOUNT as u128);

    assert_eq!(balance_after_cancellation, expected_balance);
}

#[tokio::test]
async fn confidential_redeem_fulfillment_updates_completing_transaction() {
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (request, fulfillment_id) = NetworkBuilder::default()
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .add_service(XandKeyRing::Xand1)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            let val_client = network.api_services.get(&XandKeyRing::Xand1).unwrap();

            have_issuer_submit_n_creates_to_progress_chain(&network, XandKeyRing::Biggie, 10).await;

            let request_id = val_client
                .submit_transaction_wait_final(XandKeyRing::Xand1.into(), transaction)
                .await
                .unwrap()
                .id;
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let fulfillment_id = trust_client
                .submit_transaction_wait_final(
                    XandKeyRing::Trust.into(),
                    XandTransaction::RedeemFulfillment(RedeemFulfillment {
                        correlation_id: redeem_correlation_id.clone(),
                    }),
                )
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(val_client, &fulfillment_id).await;
            (
                val_client
                    .get_transaction_details(&request_id)
                    .await
                    .unwrap(),
                fulfillment_id,
            )
        })
        .await;
    assert!(matches!(
        request,
        Transaction {
            txn: XandTransaction::RedeemRequest(PendingRedeemRequest {
                completing_transaction,
                ..
            }),
            ..
        } if completing_transaction == Some(RedeemRequestCompletion::Confirmation(fulfillment_id))
    ));
}

#[tokio::test]
async fn confidential_redeem_cancellation_updates_completing_transaction() {
    const REDEEM_AMOUNT: u64 = 10;
    let redeem_correlation_id = CorrelationId::gen_random();
    let redeem = PendingRedeemRequest {
        amount_in_minor_unit: REDEEM_AMOUNT,
        correlation_id: redeem_correlation_id.clone(),
        account: BankAccountInfo::Unencrypted(BankAccountId {
            account_number: "123".into(),
            routing_number: "456".into(),
        }),
        completing_transaction: None,
    };
    let transaction = XandTransaction::RedeemRequest(redeem);
    let (request, cancellation_id) = NetworkBuilder::default()
        .add_service(XandKeyRing::Biggie)
        .add_service(XandKeyRing::Trust)
        .add_service(XandKeyRing::Xand1)
        .set_use_emission_rewards(true)
        .run(|network| async move {
            let val_client = network.api_services.get(&XandKeyRing::Xand1).unwrap();

            have_issuer_submit_n_creates_to_progress_chain(&network, XandKeyRing::Biggie, 10).await;

            let request_id = val_client
                .submit_transaction_wait_final(XandKeyRing::Xand1.into(), transaction)
                .await
                .unwrap()
                .id;
            let trust_client = network.api_services.get(&XandKeyRing::Trust).unwrap();
            wait_until_transaction_cached(trust_client, &request_id).await;
            let cancellation_id = trust_client
                .submit_transaction_wait_final(
                    XandKeyRing::Trust.into(),
                    XandTransaction::RedeemCancellation(RedeemCancellation {
                        correlation_id: redeem_correlation_id.clone(),
                        reason: RedeemCancellationReason::InvalidData,
                    }),
                )
                .await
                .unwrap()
                .id;
            wait_until_transaction_cached(val_client, &cancellation_id).await;
            (
                val_client
                    .get_transaction_details(&request_id)
                    .await
                    .unwrap(),
                cancellation_id,
            )
        })
        .await;
    assert!(matches!(
        request,
        Transaction {
            txn: XandTransaction::RedeemRequest(PendingRedeemRequest {
                completing_transaction,
                ..
            }),
            ..
        } if completing_transaction == Some(RedeemRequestCompletion::Cancellation(cancellation_id))
    ));
}
