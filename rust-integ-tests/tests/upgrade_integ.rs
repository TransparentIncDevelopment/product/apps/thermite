use std::path::Path;

#[test]
fn upgrade_wasm_file_exists() {
    // This must be run after `cargo make build-test-wasm-runtime`. Typically, you won't run it
    // explicitly because it will run by default when running the command
    // `run-non-validator-integ-tests` and `rust-integ-tests`
    assert!(Path::new("../test-target/release/wbuild/xandstrate-runtime/target/wasm32-unknown-unknown/release/xandstrate_runtime.wasm").exists());
}
