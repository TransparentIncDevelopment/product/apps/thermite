// We need to use a single test target to avoid overloading the rust compiler.
// This pattern was borrowed from the diesel_tests structure:
// https://github.com/diesel-rs/diesel/tree/master/diesel_tests
#![forbid(unsafe_code)]

#[macro_use]
extern crate galvanic_assert;
#[macro_use]
extern crate log;

mod client_integ;
mod confidential_integ;
mod multinode_validator_integ;
mod non_confidential_integ;
mod upgrade_integ;
mod validator_emissions_integ;
mod xand_api_integ;
