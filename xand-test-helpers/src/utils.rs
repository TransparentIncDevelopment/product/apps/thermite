// This file is part of Substrate.

// Copyright (C) 2020-2021 Parity Technologies (UK) Ltd.
// SPDX-License-Identifier: GPL-3.0-or-later WITH Classpath-exception-2.0

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

use sc_client_api::execution_extensions::ExecutionStrategies;
use sc_executor::WasmExecutionMethod;
use sc_informant::OutputFormat;
use sc_network::{
    config::{NetworkConfiguration, Role, TransportConfig},
    multiaddr,
};
use sc_service::config::KeystoreConfig;
use sc_service::{
    BasePath, ChainSpec, Configuration, DatabaseConfig, KeepBlocks, TaskExecutor,
    TransactionStorageMode,
};

/// Base db path gotten from env
pub fn base_path() -> BasePath {
    if let Ok(base) = std::env::var("DB_BASE_PATH") {
        BasePath::new(base)
    } else {
        BasePath::new_temp_dir().expect("couldn't create a temp dir")
    }
}

/// Produces a default configuration object, suitable for use with most set ups.
pub fn default_config(
    task_executor: TaskExecutor,
    mut chain_spec: Box<dyn ChainSpec>,
    execution: ExecutionStrategies,
) -> Configuration {
    let base_path = base_path();
    let root_path = base_path
        .path()
        .to_path_buf()
        .join("chains")
        .join(chain_spec.id());

    let storage = chain_spec
        .as_storage_builder()
        .build_storage()
        .expect("could not build storage");

    chain_spec.set_storage(storage);

    let key_seed = "//Xand-1";

    let mut network_config = NetworkConfiguration::new(
        format!("Test Node for: {}", key_seed),
        "network/test/0.1",
        Default::default(),
        None,
    );
    let informant_output_format = OutputFormat {
        enable_color: false,
    };
    network_config.allow_non_globals_in_dht = true;

    network_config
        .listen_addresses
        .push(multiaddr::Protocol::Memory(0).into());

    network_config.transport = TransportConfig::MemoryOnly;

    Configuration {
        impl_name: "test-node".to_string(),
        impl_version: "0.1".to_string(),
        role: Role::Authority {
            sentry_nodes: vec![],
        },
        task_executor,
        transaction_pool: Default::default(),
        network: network_config,
        keystore: KeystoreConfig::Path {
            path: root_path.join("key"),
            password: None,
        },
        keystore_remote: None,
        database: DatabaseConfig::RocksDb {
            path: root_path.join("db"),
            cache_size: 128,
        },
        state_cache_size: 16777216,
        state_cache_child_ratio: None,
        state_pruning: Default::default(),
        keep_blocks: KeepBlocks::All,
        transaction_storage: TransactionStorageMode::BlockBody,
        chain_spec,
        wasm_method: WasmExecutionMethod::Compiled,
        wasm_runtime_overrides: None,
        execution_strategies: execution,
        rpc_http: None,
        rpc_ws: None,
        rpc_ipc: None,
        rpc_ws_max_connections: None,
        rpc_cors: None,
        rpc_methods: Default::default(),
        prometheus_config: None,
        telemetry_endpoints: None,
        telemetry_external_transport: None,
        telemetry_handle: None,
        telemetry_span: None,
        default_heap_pages: None,
        offchain_worker: Default::default(),
        force_authoring: false,
        disable_grandpa: false,
        dev_key_seed: Some(key_seed.to_string()),
        tracing_targets: None,
        disable_log_reloading: false,
        tracing_receiver: Default::default(),
        max_runtime_instances: 8,
        announce_block: true,
        base_path: Some(base_path),
        informant_output_format,
    }
}
