#[cfg(test)]
mod tests {

    use crate::key_ring::XandKeyRing;
    use crate::node::{Node, NodeConfig};

    use xandstrate::chain_spec::{dev_account_key, Alternative};
    use xandstrate_runtime::SystemEvent;

    #[tokio::test]
    async fn test_runner() {
        let config = NodeConfig {
            chain_spec: Box::new(Alternative::Standard(1, false).load()),
            execution_strategies: Default::default(),
            auto_seal: false,
        };
        let node = Node::new(tokio::runtime::Handle::current(), config).unwrap();
        // seals blocks
        node.block_seal().seal(1).await;
        // submit extrinsics
        let _tupac = dev_account_key("2Pac");

        node.extrinsic_submitter()
            .submit_extrinsic(
                xandstrate_runtime::Call::System(frame_system::Call::remark(
                    (b"hello world").to_vec(),
                )),
                XandKeyRing::Tupac,
            )
            .await;

        // produce block containing extrinsic
        node.block_seal().seal(1).await;

        // look ma, I can read state.
        let events = node.with_state(frame_system::Module::<xandstrate_runtime::Runtime>::events);

        assert!(
            events.iter().any(|e| matches!(
                e.event,
                xandstrate_runtime::Event::system(
                    SystemEvent::<xandstrate_runtime::Runtime>::ExtrinsicSuccess(_)
                )
            )),
            "Event 'ExtrinsicSuccess' not found in '{:?}'",
            events
        )
    }
}
