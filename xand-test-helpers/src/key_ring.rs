use once_cell::sync::Lazy;
use sp_core::Pair;
use std::collections::HashMap;
use strum::IntoEnumIterator;
use xand_address::Address;

/// Standard test accounts used in xand development networks
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, strum::Display, strum::EnumIter)]
pub enum XandKeyRing {
    Trust,
    Xand1,
    Tupac,
    Biggie,
    Admin,
}

impl XandKeyRing {
    fn alias(self) -> &'static str {
        match &self {
            XandKeyRing::Trust => "trust",
            XandKeyRing::Xand1 => "xand1",
            XandKeyRing::Tupac => "awesomeco",
            XandKeyRing::Biggie => "coolinc",
            XandKeyRing::Admin => "xand",
        }
    }

    pub(crate) fn seed_phrase(self) -> String {
        PARTICIPANT_INFO[&self].secret.clone()
    }

    pub(crate) fn encryption_secret(self) -> Option<String> {
        PARTICIPANT_INFO[&self].encryption_secret.clone()
    }

    pub(crate) fn signing_pair(self) -> sp_core::sr25519::Pair {
        sp_core::sr25519::Pair::from_string(&self.seed_phrase(), None).unwrap()
    }

    pub fn address(self) -> Address {
        PARTICIPANT_INFO[&self].address.clone()
    }
}

impl From<XandKeyRing> for Address {
    fn from(x: XandKeyRing) -> Address {
        x.address()
    }
}

const DEV_KEYS: &str = include_str!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/../dev-keys/test-keys.toml"
));

struct ParticipantInfo {
    address: Address,
    secret: String,
    encryption_secret: Option<String>,
}

static PARTICIPANT_INFO: Lazy<HashMap<XandKeyRing, ParticipantInfo>> = Lazy::new(|| {
    let config = DEV_KEYS.parse::<toml::Value>().unwrap();
    XandKeyRing::iter()
        .map(|participant| {
            let signing = config.get(participant.alias()).unwrap();
            let encryption = signing.get("encryption");
            let info = ParticipantInfo {
                address: signing
                    .get("address")
                    .unwrap()
                    .as_str()
                    .unwrap()
                    .parse()
                    .unwrap(),
                secret: signing.get("secret").unwrap().as_str().unwrap().to_string(),
                encryption_secret: encryption
                    .map(|enc| enc.get("secret").unwrap().as_str().unwrap().to_string()),
            };
            (participant, info)
        })
        .collect()
});
