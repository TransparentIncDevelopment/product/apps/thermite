use crate::aura_provider::AuraConsensusDataProvider;
use crate::key_ring::XandKeyRing;
use crate::utils::default_config;
use codec::Encode;
use futures::{
    channel::{mpsc, oneshot},
    FutureExt, SinkExt, StreamExt,
};
use jsonrpc_core::MetaIoHandler;
use sc_client_api::{
    backend, backend::Backend, execution_extensions::ExecutionStrategies, ExecutorProvider,
};
use sc_consensus_manual_seal::{
    run_manual_seal, ConsensusDataProvider, EngineCommand, ManualSealParams,
};
use sc_service::{
    build_network, config::KeystoreConfig, new_full_parts, spawn_tasks, BuildNetworkParams,
    ChainSpec, Configuration, KeystoreContainer, SpawnTasksParams, TFullBackend, TFullClient,
    TaskExecutor, TaskManager, TaskType,
};
use sc_transaction_pool::BasicPool;
use sp_api::{OverlayedChanges, StorageTransactionCache};
use sp_blockchain::HeaderBackend;
use sp_consensus_aura::sr25519::AuthorityPair as AuraPair;
use sp_core::{sr25519, ExecutionContext, Pair};
use sp_inherents::InherentDataProviders;
use sp_runtime::{
    codec,
    generic::BlockId,
    generic::Era,
    generic::{SignedPayload, UncheckedExtrinsic},
    traits::Block as BlockT,
    traits::Extrinsic,
    traits::NumberFor,
    MultiSignature,
};
use sp_state_machine::Ext;
use sp_transaction_pool::{TransactionPool, TransactionSource};
use std::{convert::TryInto, sync::Arc};
use xandstrate::{
    chain_spec::{generate_and_save_keys, get_authority_keys_from_phrase},
    rpc::create_full,
};
use xandstrate_runtime::{
    self, confidentiality::runtime_interface::crypto, opaque::Block, AccountId, Address, Call,
    Runtime, RuntimeApi, SignedExtra,
};

pub type FullClient = sc_service::TFullClient<Block, RuntimeApi, Executor>;
type FullBackend = sc_service::TFullBackend<Block>;

sc_executor::native_executor_instance!(
    pub Executor,
    xandstrate_runtime::api::dispatch,
    xandstrate_runtime::native_version,
    (frame_benchmarking::benchmarking::HostFunctions, crypto::HostFunctions),
);

/// Configuration options for the node.
pub struct NodeConfig {
    /// ChainSpec for the runtime
    pub chain_spec: Box<dyn ChainSpec>,

    /// wasm execution strategies.
    pub execution_strategies: ExecutionStrategies,

    /// Whether blocks should be automatically sealed when transactions are added to the pool
    pub auto_seal: bool,
}

type EventRecord<T> = frame_system::EventRecord<
    <T as frame_system::Config>::Event,
    <T as frame_system::Config>::Hash,
>;

/// Manual seal node, useful for in-process instant test single-node networks
pub struct Node {
    /// rpc handler for communicating with the node over rpc.
    rpc_handler: Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>>,
    /// handle to the running node.
    task_manager: Option<TaskManager>,
    /// client instance
    client: Arc<FullClient>,
    /// transaction pool
    pool: TransactionPoolTrait,
    /// channel to communicate with manual seal on.
    manual_seal_command_sink: mpsc::Sender<EngineCommand<<Block as BlockT>::Hash>>,
    /// backend type.
    backend: Arc<TFullBackend<Block>>,
    /// Block number at initialization of this Node.
    initial_block_number: NumberFor<Block>,
}

type TransactionPoolTrait = Arc<
    dyn TransactionPool<
        Block = Block,
        Hash = <Block as BlockT>::Hash,
        Error = sc_transaction_pool::error::Error,
        InPoolTransaction = sc_transaction_graph::base_pool::Transaction<
            <Block as BlockT>::Hash,
            <Block as BlockT>::Extrinsic,
        >,
    >,
>;

impl Node {
    /// Starts a node.
    pub fn new(
        runtime_handle: tokio::runtime::Handle,
        node_config: NodeConfig,
    ) -> Result<Self, sc_service::Error> {
        let task_executor = {
            move |fut, task_type| match task_type {
                TaskType::Async => runtime_handle.spawn(fut).map(drop),
                TaskType::Blocking => runtime_handle
                    .spawn_blocking(move || futures::executor::block_on(fut))
                    .map(drop),
            }
        };
        let config = Self::config(
            task_executor.into(),
            node_config.chain_spec,
            node_config.execution_strategies,
        );

        // add validator to keystore
        if let KeystoreConfig::Path { ref path, .. } = config.keystore {
            generate_and_save_keys("Xand-1", path).unwrap();
        }

        let ClientParts {
            client,
            backend,
            keystore,
            mut task_manager,
            inherent_data_providers,
            consensus_data_provider,
            select_chain,
            block_import,
        } = Self::create_client_parts(&config)?;

        let import_queue = sc_consensus_manual_seal::import_queue(
            Box::new(block_import.clone()),
            &task_manager.spawn_handle(),
            None,
        );

        let transaction_pool = BasicPool::new_full(
            config.transaction_pool.clone(),
            config.role.is_authority().into(),
            config.prometheus_registry(),
            task_manager.spawn_handle(),
            client.clone(),
        );

        let (network, network_status_sinks, system_rpc_tx, network_starter) = {
            let params = BuildNetworkParams {
                config: &config,
                client: client.clone(),
                transaction_pool: transaction_pool.clone(),
                spawn_handle: task_manager.spawn_handle(),
                import_queue,
                on_demand: None,
                block_announce_validator_builder: None,
            };
            build_network(params)?
        };

        sc_service::build_offchain_workers(
            &config,
            backend.clone(),
            task_manager.spawn_handle(),
            client.clone(),
            network.clone(),
        );

        // Proposer object for block authorship.
        let env = sc_basic_authorship::ProposerFactory::new(
            task_manager.spawn_handle(),
            client.clone(),
            transaction_pool.clone(),
            config.prometheus_registry(),
        );

        // Channel for the rpc handler to communicate with the authorship task.
        let (command_sink, commands_stream) = mpsc::channel(10);

        let rpc_extensions_builder = {
            let client = client.clone();
            let pool = transaction_pool.clone();

            Box::new(move |deny_unsafe, _| {
                let deps = xandstrate::rpc::FullDeps {
                    client: client.clone(),
                    pool: pool.clone(),
                    deny_unsafe,
                };
                create_full(deps)
            })
        };

        let (rpc_handlers, _) = {
            let params = SpawnTasksParams {
                config,
                client: client.clone(),
                backend: backend.clone(),
                task_manager: &mut task_manager,
                keystore: keystore.sync_keystore(),
                on_demand: None,
                transaction_pool: transaction_pool.clone(),
                rpc_extensions_builder,
                remote_blockchain: None,
                network,
                network_status_sinks,
                system_rpc_tx,
            };
            spawn_tasks(params)?
        };

        if node_config.auto_seal {
            let auto_commands_stream = transaction_pool
                .pool()
                .validated_pool()
                .import_notification_stream()
                .map(|_| EngineCommand::SealNewBlock {
                    create_empty: true,
                    finalize: true,
                    parent_hash: None,
                    sender: None,
                });
            let commands_stream = futures::stream::select(auto_commands_stream, commands_stream);
            let authorship_future = run_manual_seal(ManualSealParams {
                block_import,
                env,
                client: client.clone(),
                pool: transaction_pool.pool().clone(),
                commands_stream,
                select_chain,
                consensus_data_provider,
                inherent_data_providers,
            });
            task_manager
                .spawn_essential_handle()
                .spawn("instant-seal", authorship_future);
        } else {
            // Background authorship future.
            let authorship_future = run_manual_seal(ManualSealParams {
                block_import,
                env,
                client: client.clone(),
                pool: transaction_pool.pool().clone(),
                commands_stream,
                select_chain,
                consensus_data_provider,
                inherent_data_providers,
            });

            // spawn the authorship task as an essential task.
            task_manager
                .spawn_essential_handle()
                .spawn("manual-seal", authorship_future);
        }

        network_starter.start_network();
        let rpc_handler = rpc_handlers.io_handler();
        let initial_number = client.info().best_number;

        Ok(Self {
            rpc_handler,
            task_manager: Some(task_manager),
            client,
            pool: transaction_pool,
            backend,
            manual_seal_command_sink: command_sink,
            initial_block_number: initial_number,
        })
    }

    /// Returns a reference to the rpc handlers.
    pub fn rpc_handler(
        &self,
    ) -> Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>> {
        self.rpc_handler.clone()
    }

    /// Return a reference to the Client
    pub fn client(&self) -> Arc<FullClient> {
        self.client.clone()
    }

    /// Returns chain backend
    pub fn backend(&self) -> Arc<TFullBackend<Block>> {
        self.backend.clone()
    }

    /// Returns the externality environment in which a closure can be executed
    pub fn externality_env(&self) -> ExternalityEnv {
        ExternalityEnv::new(self.client(), self.backend())
    }

    /// Returns an object allowing to seal blocks
    pub fn block_seal(&self) -> BlockSeal {
        BlockSeal::new(self.manual_seal_command_sink.clone())
    }

    /// Returns an object allowing to submit extrinsics
    pub fn extrinsic_submitter(&self) -> ExtrinsicSubmitter {
        ExtrinsicSubmitter::new(self.externality_env(), self.client(), self.pool.clone())
    }

    /// Executes closure in an externalities provided environment.
    pub fn with_state<R>(&self, closure: impl FnOnce() -> R) -> R
    where
        R: codec::Codec,
    {
        self.externality_env().run(closure)
    }

    /// Get the events of the most recently produced block
    pub fn events(&self) -> Vec<EventRecord<Runtime>> {
        self.with_state(frame_system::Module::<Runtime>::events)
    }

    /// Revert count number of blocks from the chain.
    pub fn revert_blocks(&self, count: NumberFor<Block>) {
        self.backend
            .revert(count, true)
            .expect("Failed to revert blocks: ");
    }

    /// Revert all blocks added since create of the node.
    pub fn clean(&self) {
        // if a db path was specified, revert all blocks we've added
        if std::env::var("DB_BASE_PATH").is_ok() {
            let diff = self.client.info().best_number - self.initial_block_number;
            self.revert_blocks(diff);
        }
    }

    fn config(
        task_executor: TaskExecutor,
        chain_spec: Box<dyn ChainSpec>,
        execution: ExecutionStrategies,
    ) -> Configuration {
        default_config(task_executor, chain_spec, execution)
    }

    fn create_client_parts(config: &Configuration) -> Result<ClientParts, sc_service::Error> {
        let (client, backend, keystore, task_manager) =
            new_full_parts::<Block, RuntimeApi, Executor>(config)?;
        let client = Arc::new(client);

        let inherent_providers = InherentDataProviders::new();
        let select_chain = sc_consensus::LongestChain::new(backend.clone());

        let (grandpa_block_import, ..) = sc_finality_grandpa::block_import(
            client.clone(),
            &(client.clone() as Arc<_>),
            select_chain.clone(),
        )?;

        let aura_block_import = sc_consensus_aura::AuraBlockImport::<_, _, _, AuraPair>::new(
            grandpa_block_import,
            client.clone(),
        );

        let (_, aura_id, _) = get_authority_keys_from_phrase("//Xand-1");

        let consensus_data_provider = AuraConsensusDataProvider::new(
            client.clone(),
            keystore.sync_keystore(),
            &inherent_providers,
            vec![aura_id],
        )
        .expect("failed to create ConsensusDataProvider");

        Ok(ClientParts {
            client,
            backend,
            keystore,
            task_manager,
            inherent_data_providers: inherent_providers,
            consensus_data_provider: Some(Box::new(consensus_data_provider)),
            select_chain,
            block_import: aura_block_import,
        })
    }
}

impl Drop for Node {
    fn drop(&mut self) {
        self.clean();

        if let Some(mut task_manager) = self.task_manager.take() {
            // if this isn't called the node will live forever
            task_manager.terminate()
        }
    }
}

pub type ConsensusDataProviderTrait = Option<
    Box<
        dyn ConsensusDataProvider<
            Block,
            Transaction = sp_api::TransactionFor<TFullClient<Block, RuntimeApi, Executor>, Block>,
        >,
    >,
>;

pub struct ClientParts {
    client: Arc<TFullClient<Block, RuntimeApi, Executor>>,
    backend: Arc<FullBackend>,
    keystore: KeystoreContainer,
    task_manager: TaskManager,
    inherent_data_providers: InherentDataProviders,
    consensus_data_provider: ConsensusDataProviderTrait,
    select_chain: SelectChain,
    block_import: BlockImport,
}

type SelectChain = sc_consensus::LongestChain<FullBackend, Block>;
type BlockImport = sc_consensus_aura::AuraBlockImport<
    Block,
    FullClient,
    sc_finality_grandpa::GrandpaBlockImport<FullBackend, Block, FullClient, SelectChain>,
    AuraPair,
>;

/// Type containing the necessary context to seal blocks
#[derive(Clone)]
pub struct BlockSeal {
    command_sender: mpsc::Sender<EngineCommand<<Block as BlockT>::Hash>>,
}

impl BlockSeal {
    fn new(command_sender: mpsc::Sender<EngineCommand<<Block as BlockT>::Hash>>) -> Self {
        Self { command_sender }
    }

    /// Seal `block_count` blocks
    pub async fn seal(&self, block_count: usize) {
        let mut command_sender = self.command_sender.clone();
        for count in 0..block_count {
            let (sender, future_block) = oneshot::channel();
            let future = command_sender.send(EngineCommand::SealNewBlock {
                create_empty: true,
                finalize: true,
                parent_hash: None,
                sender: Some(sender),
            });

            const ERROR: &str = "manual-seal authorship task is shutting down";
            future.await.expect(ERROR);

            match future_block.await.expect(ERROR) {
                Ok(block) => log::info!(
                    "sealed {} (hash: {}) of {} blocks",
                    count + 1,
                    block.hash,
                    block_count
                ),
                Err(err) => log::error!(
                    "failed to seal block {} of {}, error: {:?}",
                    count + 1,
                    block_count,
                    err
                ),
            }
        }
    }
}

/// Type containing the necessary context to run a function in an externalities provided environment
#[derive(Clone)]
pub struct ExternalityEnv {
    client: Arc<FullClient>,
    backend: Arc<TFullBackend<Block>>,
}

impl ExternalityEnv {
    fn new(client: Arc<FullClient>, backend: Arc<TFullBackend<Block>>) -> Self {
        Self { client, backend }
    }

    /// Executes closure in an externalities provided environment.
    pub fn run<F, R>(&self, closure: F) -> R
    where
        F: FnOnce() -> R,
        R: codec::Codec,
    {
        let id = BlockId::Hash(self.client.info().best_hash);
        let mut overlay = OverlayedChanges::default();
        let changes_trie =
            backend::changes_tries_state_at_block(&id, self.backend.changes_trie_storage())
                .unwrap();
        let mut cache = StorageTransactionCache::<
            Block,
            <TFullBackend<Block> as Backend<Block>>::State,
        >::default();
        let (_, mut extensions) = self
            .client
            .execution_extensions()
            .manager_and_extensions::<Box<dyn std::fmt::Debug>, R>(
                &id,
                ExecutionContext::BlockConstruction,
            );
        let state_backend = self
            .backend
            .state_at(id)
            .unwrap_or_else(|_| panic!("State at block {} not found", id));

        let mut ext = Ext::new(
            &mut overlay,
            &mut cache,
            &state_backend,
            changes_trie.clone(),
            Some(&mut extensions),
        );
        sp_externalities::set_and_run_with_externalities(&mut ext, closure)
    }
}

/// Type containing the necessary context to submit extrinsics
pub struct ExtrinsicSubmitter {
    externality_env: ExternalityEnv,
    client: Arc<FullClient>,
    pool: TransactionPoolTrait,
}

impl ExtrinsicSubmitter {
    fn new(
        externality_env: ExternalityEnv,
        client: Arc<FullClient>,
        pool: TransactionPoolTrait,
    ) -> Self {
        Self {
            externality_env,
            client,
            pool,
        }
    }

    /// submit some extrinsic to the node, providing the sending account.
    pub async fn submit_extrinsic(
        &mut self,
        call: Call,
        from: XandKeyRing,
    ) -> <Block as BlockT>::Hash {
        let issuer = from.signing_pair();
        let account: AccountId = issuer.public().into();
        let address = self.with_state(|| Address::Id(account.clone()));
        let extra = self.with_state(|| Self::signed_extras(account.clone()));

        let (sr_signature, function, extra) = self.with_state(|| {
            let raw_payload = SignedPayload::<
                xandstrate_runtime::Call,
                xandstrate_runtime::SignedExtra,
            >::new(call.clone(), extra.clone())
            .unwrap();
            let signature = raw_payload.using_encoded(|payload| issuer.sign(payload));
            let raw_signature: &[u8] = signature.as_ref();
            let sr_signature: sr25519::Signature = raw_signature
                .try_into()
                .expect("We just created the signature");
            let (function, extra, _) = raw_payload.deconstruct();
            (sr_signature, function, extra)
        });

        let signed_data = Some((address, sr_signature.into(), extra));
        let ext = UncheckedExtrinsic::<Address, Call, MultiSignature, SignedExtra>::new(
            function,
            signed_data,
        )
        .unwrap();
        let at = self.client.info().best_hash;

        self.pool
            .submit_one(&BlockId::Hash(at), TransactionSource::Local, ext.into())
            .await
            .unwrap()
    }

    fn with_state<F, R>(&self, f: F) -> R
    where
        F: FnOnce() -> R,
        R: codec::Codec,
    {
        self.externality_env.run(f)
    }

    fn signed_extras(from: AccountId) -> xandstrate_runtime::SignedExtra {
        (
            frame_system::CheckSpecVersion::<Runtime>::new(),
            frame_system::CheckTxVersion::<Runtime>::new(),
            frame_system::CheckGenesis::<Runtime>::new(),
            frame_system::CheckEra::<Runtime>::from(Era::Immortal),
            frame_system::CheckNonce::<Runtime>::from(
                frame_system::Module::<Runtime>::account_nonce(from),
            ),
            frame_system::CheckWeight::<Runtime>::new(),
        )
    }
}
