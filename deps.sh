#!/bin/bash

set -e

sudo apt update

echo "Installing general development requirements."
sudo apt install -y \
    build-essential \
    cmake \
    docker.io \
    docker-compose \
    clang \
    libclang-dev \
    libssl-dev \
    pkg-config \
    libzmq3-dev \
    default-jre \
    jq

echo "Installing Rust."
if ! [ -x "$(command -v rustup)" ]; then
    sudo curl https://sh.rustup.rs -sSf | sh
    source $HOME/.cargo/env
fi
source .ci/.env
rustup toolchain install nightly-$NIGHTLY_RUST_VERSION
rustup target add --toolchain nightly-$NIGHTLY_RUST_VERSION wasm32-unknown-unknown
cargo install cargo-make
cargo make install-deps

echo "Installing wallet client dependencies from apt."
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt install -y nodejs

echo "Installing validator dependencies."
sudo apt install -y \
    clang \
    libclang-dev \
    pkg-config \
    protobuf-compiler

echo "Installing dependencies for acceptance tests"
sudo apt-get install -y default-jdk
sudo npm install yarn -g
