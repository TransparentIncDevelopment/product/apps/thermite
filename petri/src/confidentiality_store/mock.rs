use crate::confidentiality_store::TxoHash;
use crate::{TransactionOutputRecord, TxoSpentStatus, TxoStore, TxoStoreError};
use std::collections::HashMap;
use xand_address::Address;
use xand_models::CorrelationId;

#[derive(Default)]
pub struct MockTxoStore {
    balances: HashMap<Address, u64>,
}

impl MockTxoStore {
    pub fn insert_member_with_balance(&mut self, member: Address, balance: u64) {
        self.balances.insert(member, balance);
    }
}

impl TxoStore for MockTxoStore {
    fn get_txo(&self, _id: &TxoHash) -> Option<TransactionOutputRecord> {
        unimplemented!()
    }

    fn insert(&mut self, _txo: TransactionOutputRecord) {
        unimplemented!()
    }

    fn get_txos_by_correlation_id(
        &self,
        _correlation_id: &CorrelationId,
    ) -> Vec<TransactionOutputRecord> {
        unimplemented!()
    }

    fn update_txo_status_by_correlation_id(
        &mut self,
        _correlation_id: &CorrelationId,
        _new_status: TxoSpentStatus,
    ) {
        unimplemented!()
    }

    fn update_txo_status_by_hash(&mut self, _txo_hash: TxoHash, _status: TxoSpentStatus) {
        unimplemented!()
    }

    fn remove_txos_by_correlation_id(&mut self, _correlation_id: &CorrelationId) {
        unimplemented!()
    }

    fn balance(&self, address: &Address) -> Result<u64, TxoStoreError> {
        if let Some(balance) = self.balances.get(address) {
            Ok(*balance)
        } else {
            Err(TxoStoreError::BalanceNotFound)
        }
    }
}
