use serde::Serialize;
use snafu::Snafu;
use std::{error::Error, sync::Arc};
use xand_utils::snafu_extensions::debug_serialize;

#[derive(Debug, Snafu, Serialize, Clone)]
#[snafu(visibility(pub))]
pub enum PetriError {
    #[snafu(display("Error decoding substrate events for block: {}", blockhash))]
    ErrorDecodingEvents { blockhash: String },

    #[snafu(display("Error decoding proposal"))]
    ErrorDecodingProposal,

    #[snafu(display("Error in substrate client ({}): {:?}", msg, source))]
    SubstrateClientError {
        msg: String,
        #[snafu(source(from(sp_blockchain::Error, arcify_std_err)))]
        #[serde(serialize_with = "debug_serialize")]
        source: Arc<dyn Error + Send + Sync>,
    },

    #[snafu(display("{}", source_str))]
    Unknown { source_str: String },

    #[snafu(display("Validation Error: {}", source_str))]
    ValidationError { source_str: String },

    #[snafu(display("Caching Error: {}", source_str))]
    CachingError { source_str: String },

    #[snafu(display("Failed to get list of members: {}", source_str))]
    FailedToGetMembers { source_str: String },

    #[snafu(display("Failed to get proposal: {}", source_str))]
    FailedToGetProposal { source_str: String },
}

pub const LOCK_ERROR_TEXT: &str = "Unable to acquire lock; read-write lock is poisoned";

fn arcify_std_err<T>(e: T) -> Arc<dyn std::error::Error + Send + Sync>
where
    T: Error + Send + Sync + 'static,
{
    Arc::new(e)
}

impl From<xand_models::TransactionIdError> for PetriError {
    fn from(source: xand_models::TransactionIdError) -> PetriError {
        match source {
            xand_models::TransactionIdError::Encoding { .. } => PetriError::Unknown {
                source_str: format!("{:?}", source),
            },
            xand_models::TransactionIdError::Parsing { .. } => PetriError::ValidationError {
                source_str: format!("{:?}", source),
            },
        }
    }
}
