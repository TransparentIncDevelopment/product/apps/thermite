use super::*;
use proptest::prelude::RngCore;
use rand::rngs::StdRng;
use rand::{CryptoRng, SeedableRng};
use xand_financial_client::models::EncryptionNonce;
use xand_financial_client::test_helpers::redeems::generate_metadata;
use xand_ledger::{RedeemCancellationReason, RedeemRequestTransaction};

const RNG_SEED: u64 = 5623756;

#[tokio::test]
async fn redeem_id_tag_immediately_usable() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let redeem_request = TestRedeemRequestBuilder::default()
        .build(&mut csprng)
        .redeem_request;
    let expected_id_tag = redeem_request.core_transaction.output_identity;
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![(
            tupac.public(),
            confidential_redeem_extrinsic(redeem_request),
        )])
        .build();
    petri.process_available_blocks().await;
    let id_tag_record = petri
        .state
        .identity_tag_store
        .get_id_tag(&expected_id_tag.to_hash())
        .expect("missing expected id tag");
    // Assert id tag is cached by id
    assert_eq!(id_tag_record.tag, expected_id_tag);
    // Assert id tag status is valid
    assert_eq!(id_tag_record.status, IdTagStatus::Valid);
}

#[tokio::test]
async fn redeem_change_output_spendable() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let redeem_request = TestRedeemRequestBuilder::default()
        .build(&mut csprng)
        .redeem_request;
    let change_output = redeem_request.core_transaction.output.change_output.clone();
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![(
            tupac.public(),
            confidential_redeem_extrinsic(redeem_request),
        )])
        .build();
    petri.process_available_blocks().await;
    let txo_record = petri
        .state
        .utxo_store
        .get_txo(&change_output.txo.to_hash())
        .expect("missing expected transaction output");
    // Assert txo is cached by id
    assert_eq!(txo_record.txo.get_transaction_output(), change_output.txo);
    // Assert txo status is unspent
    assert_eq!(txo_record.status, TxoSpentStatus::UnSpent);
}

#[tokio::test]
async fn redeem_request_redeem_output_pending() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let redeem_request = TestRedeemRequestBuilder::default()
        .build(&mut csprng)
        .redeem_request;
    let redeem_output = redeem_request.core_transaction.output.redeem_output.clone();
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![(
            tupac.public(),
            confidential_redeem_extrinsic(redeem_request),
        )])
        .build();
    petri.process_available_blocks().await;
    let txo_record = petri
        .state
        .utxo_store
        .get_txo(&redeem_output.transaction_output.to_hash())
        .expect("missing expected transaction output");
    // Assert txo is cached by id
    assert_eq!(
        txo_record.txo.get_transaction_output(),
        redeem_output.transaction_output
    );
    // Assert txo status is pending (it's not spendable unless txn is cancelled)
    assert_eq!(txo_record.status, TxoSpentStatus::Pending);
}

#[tokio::test]
async fn redeem_fulfillment_removes_pending_redeem_output() {
    let tupac = dev_account_key("2Pac");
    let trust = dev_account_key("trust");
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let redeem_request = TestRedeemRequestBuilder::default()
        .build(&mut csprng)
        .redeem_request;
    let redeem_output = redeem_request.core_transaction.output.redeem_output.clone();
    let fulfillment = RedeemFulfillmentTransaction {
        correlation_id: redeem_request.core_transaction.correlation_id,
    };
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![
            (
                tupac.public(),
                confidential_redeem_extrinsic(redeem_request),
            ),
            (trust.public(), confidential_redeem_fulfillment(fulfillment)),
        ])
        .build();
    petri.process_available_blocks().await;
    let txo_record = petri
        .state
        .utxo_store
        .get_txo(&redeem_output.transaction_output.to_hash());
    // Assert redeem txo is no longer stored
    assert!(txo_record.is_none());
}

#[tokio::test]
async fn redeem_cancellation_makes_redeem_output_spendable() {
    let tupac = dev_account_key("2Pac");
    let trust = dev_account_key("trust");
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let redeem_request = TestRedeemRequestBuilder::default()
        .build(&mut csprng)
        .redeem_request;
    let redeem_output = redeem_request.core_transaction.output.redeem_output.clone();
    let redeem_cancellation = RedeemCancellationTransaction {
        correlation_id: redeem_request.core_transaction.correlation_id,
        reason: RedeemCancellationReason::InvalidData,
    };
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![
            (
                tupac.public(),
                confidential_redeem_extrinsic(redeem_request),
            ),
            (
                trust.public(),
                confidential_redeem_cancellation(redeem_cancellation),
            ),
        ])
        .build();
    petri.process_available_blocks().await;
    let txo_record = petri
        .state
        .utxo_store
        .get_txo(&redeem_output.transaction_output.to_hash())
        .expect("missing expected transaction output");
    // Assert txo is cached by id
    assert_eq!(
        txo_record.txo.get_transaction_output(),
        redeem_output.transaction_output
    );
    // Assert txo status is unspent
    assert_eq!(txo_record.status, TxoSpentStatus::UnSpent);
}

#[tokio::test]
async fn redeem_decreases_balance_before_fulfillment() {
    const REDEEM_AMOUNT: u64 = 5u64;
    let inputs = vec![5u64, 5u64];
    let expected_balance: u64 = inputs.iter().sum::<u64>() - REDEEM_AMOUNT;
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let trust = Member::new();
    let issuer = Member::new();
    let correlation_id: CorrelationId = csprng.gen();
    let redeem_request = generate_redeem_request(
        inputs,
        correlation_id,
        REDEEM_AMOUNT,
        issuer.private_key,
        issuer.encryption_key,
        trust.encryption_key,
        &mut csprng,
    );

    let mut petri = TestPetriBuilder::default()
        .member_with_encryption_key(issuer.address(), issuer.encryption_key)
        .trust_encryption_key(trust.encryption_key)
        .private_key(issuer.private_key)
        .transactions(vec![(
            issuer.account.public(),
            confidential_redeem_extrinsic(redeem_request),
        )])
        .build();
    petri.process_available_blocks().await;

    // Assert balance is decreased by the redeem amount
    assert_eq!(
        petri.state.utxo_store.balance(&issuer.address()).unwrap(),
        expected_balance,
    );
}

#[tokio::test]
async fn redeem_cancellation_restores_balance() {
    const REDEEM_AMOUNT: u64 = 5u64;
    let inputs = vec![5u64, 5u64];
    let expected_balance: u64 = inputs.iter().sum();
    let mut csprng = StdRng::seed_from_u64(RNG_SEED);
    let trust = Member::new();
    let issuer = Member::new();
    let correlation_id: CorrelationId = csprng.gen();
    let redeem_request = generate_redeem_request(
        inputs,
        correlation_id,
        REDEEM_AMOUNT,
        issuer.private_key,
        issuer.encryption_key,
        trust.encryption_key,
        &mut csprng,
    );

    let redeem_cancellation = RedeemCancellationTransaction {
        correlation_id: redeem_request.core_transaction.correlation_id,
        reason: RedeemCancellationReason::InvalidData,
    };
    let mut petri = TestPetriBuilder::default()
        .member_with_encryption_key(issuer.address(), issuer.encryption_key)
        .trust_encryption_key(trust.encryption_key)
        .private_key(issuer.private_key)
        .transactions(vec![
            (
                issuer.account.public(),
                confidential_redeem_extrinsic(redeem_request),
            ),
            (
                trust.account.public(),
                confidential_redeem_cancellation(redeem_cancellation),
            ),
        ])
        .build();
    petri.process_available_blocks().await;

    // Assert balance is increased by redeem amount
    assert_eq!(
        petri.state.utxo_store.balance(&issuer.address()).unwrap(),
        expected_balance,
    );
}

fn generate_redeem_request<R>(
    inputs: Vec<u64>,
    correlation_id: CorrelationId,
    redeem_amount: u64,
    private_key: PrivateKey,
    issuer_encryption_key: EncryptionKey,
    trust_encryption_key: EncryptionKey,
    mut csprng: R,
) -> RedeemRequestTransaction
where
    R: RngCore + CryptoRng,
{
    TestRedeemRequestBuilder::default()
        .correlation_id(correlation_id)
        .input_values(inputs)
        .with_redeem_amount(redeem_amount)
        .with_private_key(private_key)
        .with_encryption_key(issuer_encryption_key)
        .with_generate_metadata(generate_metadata(
            issuer_encryption_key,
            TestEncryptionProvider::default(),
        ))
        .extra(
            TestEncryptionProvider::default()
                .encrypt(
                    &BankAccount::default(),
                    &issuer_encryption_key,
                    &trust_encryption_key,
                    &EncryptionNonce::from(correlation_id.as_ref()),
                )
                .unwrap()
                .into(),
        )
        .build(&mut csprng)
        .redeem_request
}
