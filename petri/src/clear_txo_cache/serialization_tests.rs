use crate::clear_txo_cache::tests::get_random_clear_transaction_output_record;
use crate::{
    clear_txo_cache::ClearTxoCacheInner,
    confidentiality_store::clear_txo_store::{
        ClearTransactionOutputRecord, ClearTxoHash, ClearTxoStatus,
    },
    test_helpers::{arbitrary_public_key, FakeCsprng},
    txo_cache::tests::arbitrary_correlation_id,
    ClearTxoCache,
};
use proptest::{collection::vec, prelude::*};
use rand::SeedableRng;
use std::sync::{Arc, RwLock};
use xand_address::Address;
use xand_ledger::IUtxo;
use xand_models::CorrelationId;
use xand_public_key::public_key_to_address;

fn txo_pair(rec: &ClearTransactionOutputRecord) -> (ClearTxoHash, ClearTransactionOutputRecord) {
    (rec.to_hash(), rec.clone())
}

fn txo_by_correlation_id_pair(
    rec: &ClearTransactionOutputRecord,
) -> Option<(CorrelationId, Vec<ClearTxoHash>)> {
    rec.correlation_id
        .as_ref()
        .map(|correlation_id| (correlation_id.clone(), vec![rec.to_hash()]))
}

fn own_txo_pair(rec: &ClearTransactionOutputRecord) -> (Address, Vec<ClearTxoHash>) {
    let pubkey = rec.clear_txo.public_key();
    let address = public_key_to_address(&pubkey);
    (address, vec![rec.to_hash()])
}

fn arbitrary_clear_txo_status() -> impl Strategy<Value = ClearTxoStatus> {
    prop_oneof![
        Just(ClearTxoStatus::Redeemable),
        Just(ClearTxoStatus::Pending),
    ]
}

prop_compose! {
    pub fn arbitrary_clear_txo_record()(
        bytes: [u8; 32],
        owner in arbitrary_public_key(),
        id in proptest::option::of(arbitrary_correlation_id()),
        amount: u64,
        status in arbitrary_clear_txo_status(),
    ) -> ClearTransactionOutputRecord {
        let mut rng: FakeCsprng = FakeCsprng::from_seed(bytes);
        get_random_clear_transaction_output_record(owner, id, amount, status, &mut rng)
    }
}

proptest! {
    #[test]
    fn serialize_deserialize_round_trip(
        expected in arbitrary_clear_txo_cache()
    ) {
        let serialized = bincode::serialize(&expected).unwrap();
        let deserialized: ClearTxoCache = bincode::deserialize(&serialized).unwrap();
        assert_eq!(expected, deserialized);
    }
}
