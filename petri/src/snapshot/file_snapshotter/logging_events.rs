#[logging_event]
pub(crate) enum LoggingEvent {
    SnapshotBegin,
    SnapshotReadError(String),
    SavingSnapshotToFile(String),
    SnapshotComplete,
    PruningSnapshotFileError(String),
    DeserializeSnapshotIndexFileError(String),
    ListOfSnapshotFilesError(String),
    AttemptingToReadSnapshotFile(String),
    BadSnapshotChecksum,
    RetrievedSnapshot(String),
}
