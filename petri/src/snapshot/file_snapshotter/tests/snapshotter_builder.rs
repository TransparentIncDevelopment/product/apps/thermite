use std::collections::{HashMap, VecDeque};
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};

use async_trait::async_trait;

use crate::snapshot::file_snapshotter::tests::test_data::SimpleData;
use crate::snapshot::file_snapshotter::{
    error::*, FileManager, FileReader, FileSnapshotter, FileWriter, Scheduler, Snapshot,
};
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering::Relaxed;

#[cfg(test)]
#[derive(Debug)]
pub struct FakeFileReader {
    pub files: FakeFileSystem<PathBuf>,
}

#[cfg(test)]
impl FileReader for FakeFileReader {
    fn read<P: AsRef<Path> + Send + Sync>(&self, path: P) -> Result<Snapshot> {
        self.files
            .lock()
            .unwrap()
            .remove_entry(path.as_ref())
            .unwrap()
            .1
    }
}

pub fn test_data_snapshot() -> Snapshot {
    Snapshot::from_serializable(&SimpleData::default()).unwrap()
}

type FakeFileSystem<T> = Arc<Mutex<HashMap<T, Result<Snapshot>>>>;

#[derive(Debug)]
pub struct FakeFileWriter {
    pub files: FakeFileSystem<PathBuf>,
    pub optional_error: Option<Error>,
}

#[async_trait]
impl FileWriter for FakeFileWriter {
    async fn write<P: AsRef<Path> + Send + Sync>(
        &mut self,
        path: P,
        snapshot: Snapshot,
    ) -> Result<()> {
        if let Some(error) = self.optional_error.take() {
            return Err(error);
        }

        let path_buf = PathBuf::from(path.as_ref());
        self.files.lock().unwrap().insert(path_buf, Ok(snapshot));
        Ok(())
    }

    async fn delete<P: AsRef<Path> + Send + Sync>(&mut self, path: P) -> Result<()> {
        self.files.lock().unwrap().remove(path.as_ref());
        Ok(())
    }
}

#[derive(Debug)]
pub struct FakeFileManager {
    pub new_file_path: PathBuf,
    pub files: FakeFileSystem<PathBuf>,
    pub existing_files: VecDeque<PathBuf>,
}

impl FileManager for FakeFileManager {
    fn new_file_path(&mut self) -> Result<PathBuf> {
        Ok(self.new_file_path.clone())
    }

    fn track_file(&mut self, path: PathBuf) -> Result<()> {
        self.existing_files.push_front(path);
        Ok(())
    }

    fn prune_tracked_files_to_x_and_return_old(&mut self, x: usize) -> Result<Vec<PathBuf>, Error> {
        let mut to_be_pruned = Vec::new();
        if self.existing_files.len() > x {
            to_be_pruned = self.existing_files.split_off(x).into();
        }
        Ok(to_be_pruned)
    }

    fn files_youngest_to_oldest(&self) -> Result<Vec<PathBuf>> {
        let paths = self.existing_files.clone().into_iter().collect();
        Ok(paths)
    }
}

#[derive(Debug)]
pub struct FakeScheduler {
    is_scheduled: bool,
    snapshot_completed: Arc<AtomicBool>,
}

#[async_trait]
impl Scheduler for FakeScheduler {
    async fn should_snapshot(&self) -> bool {
        self.is_scheduled
    }

    async fn snapshot_complete(&mut self) -> Result<(), Error> {
        self.snapshot_completed.store(true, Relaxed);
        Ok(())
    }
}

pub struct FileSnapshotterBuilder {
    files: FakeFileSystem<PathBuf>,
    new_file_path: Option<PathBuf>,
    existing_files: VecDeque<PathBuf>,
    is_scheduled: bool,
    file_writer_error: Option<Error>,
    snapshot_completed_handle: Arc<AtomicBool>,
}

impl FileSnapshotterBuilder {
    pub fn new() -> Self {
        FileSnapshotterBuilder {
            files: Arc::new(Mutex::new(HashMap::new())),
            new_file_path: None,
            existing_files: VecDeque::new(),
            is_scheduled: true,
            file_writer_error: None,
            snapshot_completed_handle: Arc::new(false.into()),
        }
    }

    pub fn with_file_writer_error(mut self, error: Error) -> Self {
        self.file_writer_error = Some(error);
        self
    }

    pub fn with_next_snapshot_result(mut self, path: PathBuf, snapshot: Result<Snapshot>) -> Self {
        self.files.lock().unwrap().insert(path.clone(), snapshot);
        self.existing_files.push_front(path);
        self
    }

    pub fn with_new_file_path(mut self, new_file_path: PathBuf) -> Self {
        self.new_file_path = Some(new_file_path);
        self
    }

    pub fn with_scheduled(mut self, is_scheduled: bool) -> Self {
        self.is_scheduled = is_scheduled;
        self
    }

    pub fn build(
        self,
    ) -> FileSnapshotter<FakeFileWriter, FakeFileReader, FakeFileManager, FakeScheduler> {
        let files = self.files;
        let new_file_path = self.new_file_path.unwrap_or_default();
        let existing_files = self.existing_files;
        let is_scheduled = self.is_scheduled;
        let snapshot_completed = self.snapshot_completed_handle;

        let writer = FakeFileWriter {
            files: files.clone(),
            optional_error: self.file_writer_error,
        };
        let reader = FakeFileReader {
            files: files.clone(),
        };
        let manager = FakeFileManager {
            new_file_path,
            files,
            existing_files,
        };

        let scheduler = FakeScheduler {
            is_scheduled,
            snapshot_completed,
        };

        FileSnapshotter::new(writer, reader, manager, scheduler)
    }

    pub fn build_with_files_handle(
        self,
    ) -> (
        FileSnapshotter<FakeFileWriter, FakeFileReader, FakeFileManager, FakeScheduler>,
        FakeFileSystem<PathBuf>,
    ) {
        let files = self.files.clone();
        (self.build(), files)
    }

    pub fn build_with_snapshot_completed_handle(
        self,
    ) -> (
        FileSnapshotter<FakeFileWriter, FakeFileReader, FakeFileManager, FakeScheduler>,
        Arc<AtomicBool>,
    ) {
        let snapshot_completed_handle = self.snapshot_completed_handle.clone();
        (self.build(), snapshot_completed_handle)
    }
}
