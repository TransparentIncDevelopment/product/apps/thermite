use crate::snapshot::file_snapshotter::error::{Error, Result};
use crate::snapshot::file_snapshotter::{FileReader, Snapshot};
use std::fs::File;
use std::io::Read;
use std::path::Path;

#[derive(Debug)]
pub struct FileReaderImpl {}

impl FileReader for FileReaderImpl {
    fn read<P: AsRef<Path> + Send + Sync>(&self, path: P) -> Result<Snapshot> {
        let mut file = File::open(path)?;
        let mut bytes = Vec::new();
        file.read_to_end(&mut bytes)?;
        bincode::deserialize(&bytes).map_err(|e| Error::Serialization(Box::new(e)))
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use super::*;
    use crate::snapshot::file_snapshotter::tests::test_data::SimpleData;
    use std::fs;
    use std::fs::File;
    use std::io::Write;
    use std::path::PathBuf;
    use tempfile::NamedTempFile;

    fn new_temp_path() -> PathBuf {
        NamedTempFile::new().unwrap().into_temp_path().to_path_buf()
    }

    fn write_snapshot_file(path: &Path, snapshot: &Snapshot) {
        if let Some(parent_dir) = path.parent() {
            fs::create_dir_all(parent_dir).unwrap();
        }
        let mut file = File::create(path).unwrap();
        let bytes = bincode::serialize(&snapshot).unwrap();
        file.write_all(&bytes).unwrap();
    }

    fn write_empty_file(path: &Path) {
        if let Some(parent_dir) = path.parent() {
            fs::create_dir_all(parent_dir).unwrap();
        }
        let mut file = File::create(path).unwrap();
        let bytes = b"";
        file.write_all(bytes).unwrap();
    }

    #[test]
    fn read__can_get_snapshot_from_file() {
        // Given
        let path = new_temp_path();

        let data = SimpleData::default();

        let expected = Snapshot::from_serializable(&data).unwrap();

        write_snapshot_file(&path, &expected);

        let reader = FileReaderImpl {};

        // When
        let actual = reader.read(path).unwrap();

        // Then
        assert_eq!(expected, actual);
    }

    #[test]
    fn read__when_no_file_return_io_error() {
        // Given
        let path = new_temp_path();

        // Never write to file

        let reader = FileReaderImpl {};

        // When
        let result = reader.read(path);

        //Then
        assert!(matches!(result, Err(Error::Io(_))));
    }

    #[test]
    fn read__when_file_empty_return_serialization_error() {
        // Given
        let path = new_temp_path();

        write_empty_file(&path);

        let reader = FileReaderImpl {};

        // When
        let result = reader.read(path);

        // Then
        assert!(matches!(result, Err(Error::Serialization(_))));
    }
}
