#![allow(non_snake_case)]

use super::*;
use crate::confidentiality_tests::Member;
use crate::petri_persisted_state::PetriPersistedState;
use crate::test_helpers::{FakeSnapshotter, TestPetriBuilder};
use crate::XandTransaction::CreatePendingCreate;
use rand::rngs::OsRng;
use rand::Rng;
use serde_scale_wrap::Wrap;
use sp_core::Pair;
use xand_financial_client::models::{BankAccount, EncryptionKey};
use xand_financial_client::test_helpers::managers::TestEncryptionProvider;
use xand_financial_client::EncryptionProvider;
use xand_ledger::{
    CorrelationId, CreateCancellationReason, CreateRequestTransaction, TestCreateRequestBuilder,
};
use xand_models::{dev_account_key, ToAddress};
use xand_models::{CreateCompletion, PendingCreate};
use xandstrate_client::confidential_create_request_extrinsic;
use xandstrate_runtime::extrinsic_builder::{register_member_extrinsic, remove_member_extrinsic};
use xandstrate_runtime::{confidentiality, network_voting};

fn random_test_create_request(
    issuer: &Member,
    trust_encryption_key: &EncryptionKey,
) -> CreateRequestTransaction {
    let mut csprng: OsRng = OsRng::default();
    let correlation_id: CorrelationId = csprng.gen();

    let extra = TestEncryptionProvider
        .encrypt(
            &BankAccount {
                routing_number: "123".to_string(),
                account_number: "456".to_string(),
            },
            &issuer.encryption_key.clone(),
            &trust_encryption_key.clone(),
            &(&correlation_id[..]).into(),
        )
        .unwrap();
    TestCreateRequestBuilder::default()
        .private_key(issuer.private_key)
        .values([1, 2, 3].to_vec())
        .correlation_id(correlation_id)
        .extra(extra.into())
        .build(&mut csprng)
        .create_request
}

#[tokio::test]
async fn txn_failures() {
    let mut csprng: OsRng = OsRng::default();
    let member_1 = Member::new();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let create_request_txn = random_test_create_request(&member_1, &trust_encryption_key);

    let mut petri = TestPetriBuilder::default()
        .trust_encryption_key(trust_encryption_key)
        .member_with_encryption_key(member_1.address(), member_1.encryption_key)
        .private_key(member_1.private_key)
        .transactions_with_outcomes(vec![(
            member_1.account.public(),
            confidential_create_request_extrinsic(create_request_txn),
            false,
        )])
        .build();
    petri.process_available_blocks().await;
    let mut all_txns = petri.state.transaction_cache.all_transactions();
    assert_eq!(all_txns.len(), 1);
    assert!(matches!(
        all_txns.next().unwrap().status,
        TransactionStatus::Invalid(_)
    ));
}

#[tokio::test]
async fn process_available_blocks__continues_from_next_block() {
    let member_1 = Member::new();
    let mut csprng: OsRng = OsRng::default();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let create_request_txn_1 = random_test_create_request(&member_1, &trust_encryption_key);
    let create_request_txn_2 = random_test_create_request(&member_1, &trust_encryption_key);
    let starting_block = 1000;
    let mut petri = TestPetriBuilder::default()
        .trust_encryption_key(trust_encryption_key)
        .with_starting_block(starting_block)
        .member_with_encryption_key(member_1.address(), member_1.encryption_key)
        .transactions(vec![
            (
                member_1.account.public(),
                confidential_create_request_extrinsic(create_request_txn_1),
            ),
            (
                member_1.account.public(),
                confidential_create_request_extrinsic(create_request_txn_2),
            ),
        ])
        .build();
    petri.process_available_blocks().await;
    assert_eq!(petri.state.next_block_to_process, starting_block + 2)
}

#[tokio::test]
async fn original_member_has_their_id_tag_cached() {
    let member = dev_account_key("Biggie");
    let member_address = member.public().to_address();
    let member_public_key = address_to_public_key(&member_address).unwrap();
    let mut petri = TestPetriBuilder::default()
        .member(member_address.clone())
        .build();
    petri.process_available_blocks().await;
    let known_tags = petri
        .state
        .identity_tag_store
        .get_known_tags(&member_public_key);
    let tag_found = known_tags
        .iter()
        .any(|t| t.owner.as_ref().map_or(false, |t| *t == member_public_key));
    assert!(
        tag_found,
        "Expected id tag for {} in {:?}",
        member_address, known_tags,
    );
}

#[tokio::test]
async fn proposal_added_member_has_their_id_tag_cached() {
    let tupac = dev_account_key("2Pac");
    let tupac_address = tupac.public().to_address();
    let tupac_public_key = address_to_public_key(&tupac_address).unwrap();
    let biggie = dev_account_key("Biggie");
    let idx = 100;
    let event = EventRecord {
        phase: Phase::ApplyExtrinsic(123),
        event: xandstrate_runtime::Event::network_voting(
            network_voting::Event::<Runtime>::Accepted(idx),
        ),
        topics: vec![],
    };
    let mut events = HashMap::new();
    events.insert(0, vec![event]);

    let mut proposals = HashMap::new();
    let call = register_member_extrinsic(tupac.public().into(), Default::default());
    proposals.insert(idx, call);

    let member_1 = Member::new();
    let mut csprng: OsRng = OsRng::default();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let create_request_txn = random_test_create_request(&member_1, &trust_encryption_key);

    let mut petri = TestPetriBuilder::default()
        .trust_encryption_key(trust_encryption_key)
        .member(biggie.public().to_address())
        .member_with_encryption_key(member_1.address(), member_1.encryption_key)
        .private_key(member_1.private_key)
        .timestamp_interval(3).proposals(proposals)
        // Must include transactions to move block production forward in tests
        .transactions(vec![(
            member_1.account.public(),
            confidential_create_request_extrinsic(create_request_txn),
        )])
        .events(events)
        .build();
    let before_tags = petri
        .state
        .identity_tag_store
        .get_known_tags(&tupac_public_key);
    assert!(before_tags.is_empty());
    petri.process_available_blocks().await;
    let after_tags = petri
        .state
        .identity_tag_store
        .get_known_tags(&tupac_public_key);
    let tag_found = after_tags
        .iter()
        .any(|t| t.owner.as_ref().map_or(false, |t| *t == tupac_public_key));
    assert!(
        tag_found,
        "Expected id tag for {} in {:?}",
        tupac_address, after_tags,
    );
}

#[tokio::test]
async fn id_tag_of_proposal_removed_member_is_removed_from_cache() {
    let tupac = dev_account_key("2Pac");
    let tupac_address = tupac.public().to_address();
    let tupac_public_key = address_to_public_key(&tupac_address).unwrap();
    let biggie = dev_account_key("Biggie");
    let idx = 100;
    let event = EventRecord {
        phase: Phase::ApplyExtrinsic(123),
        event: xandstrate_runtime::Event::network_voting(
            network_voting::Event::<Runtime>::Accepted(idx),
        ),
        topics: vec![],
    };
    let mut events = HashMap::new();
    events.insert(0, vec![event]);

    let mut proposals = HashMap::new();
    let call = remove_member_extrinsic(tupac.public().into());
    proposals.insert(idx, call);

    let member_1 = Member::new();
    let mut csprng: OsRng = OsRng::default();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let create_request_txn = random_test_create_request(&member_1, &trust_encryption_key);

    let mut petri = TestPetriBuilder::default()
        .trust_encryption_key(trust_encryption_key)
        .member(tupac.public().to_address())
        .member(biggie.public().to_address())
        .member_with_encryption_key(member_1.address(), member_1.encryption_key)
        .private_key(member_1.private_key)
        .timestamp_interval(3).proposals(proposals)
        // Must include transactions to move block production forward in tests
        .transactions(vec![(
            member_1.account.public(),
            confidential_create_request_extrinsic(create_request_txn),
        )])
        .events(events)
        .build();
    let before_tags = petri
        .state
        .identity_tag_store
        .get_known_tags(&tupac_public_key);
    assert!(!before_tags.is_empty());
    petri.process_available_blocks().await;
    let after_tags = petri
        .state
        .identity_tag_store
        .get_known_tags(&tupac_public_key);
    assert!(after_tags.is_empty());
}

#[tokio::test]
async fn confidential_cancelling_create_updates_request_tx() {
    let issuer = Member::new();

    let mut csprng: OsRng = OsRng::default();
    let correlation_id: CorrelationId = csprng.gen();

    let event = EventRecord {
        phase: Phase::ApplyExtrinsic(123),
        event: xandstrate_runtime::Event::confidentiality(confidentiality::Event::CreateCancelled(
            correlation_id,
            Wrap(CreateCancellationReason::Expired),
        )),
        topics: vec![],
    };
    let mut events = HashMap::new();
    events.insert(1, vec![event]);

    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());

    let extra = TestEncryptionProvider
        .encrypt(
            &BankAccount {
                routing_number: "123".into(),
                account_number: "456".into(),
            },
            &issuer.encryption_key,
            &trust_encryption_key,
            &(&correlation_id[..]).into(),
        )
        .unwrap();
    let create_request = TestCreateRequestBuilder::default()
        .private_key(issuer.private_key)
        .values([1, 2, 3].to_vec())
        .correlation_id(correlation_id)
        .extra(extra.into())
        .build(&mut csprng)
        .create_request;

    let correlation_id_dummy: CorrelationId = csprng.gen();
    let extra_dummy = TestEncryptionProvider
        .encrypt(
            &BankAccount {
                routing_number: "123".into(),
                account_number: "456".into(),
            },
            &issuer.encryption_key,
            &trust_encryption_key,
            &(&correlation_id_dummy[..]).into(),
        )
        .unwrap();
    // This is only to advance the chain
    let arbitrary_transaction = TestCreateRequestBuilder::default()
        .private_key(issuer.private_key)
        .values([4, 5, 6].to_vec())
        .correlation_id(correlation_id_dummy)
        .extra(extra_dummy.into())
        .build(&mut csprng)
        .create_request;
    let mut petri = TestPetriBuilder::default()
        .member_with_encryption_key(issuer.address(), issuer.encryption_key)
        .trust_encryption_key(trust_encryption_key)
        .private_key(issuer.private_key)
        .timestamp_interval(3)
        .transactions(vec![(
            issuer.account.public(),
            confidential_create_request_extrinsic(create_request),
        )])
        // Must include transactions to move block production forward in tests
        .transactions(vec![(
            issuer.account.public(),
            confidential_create_request_extrinsic(arbitrary_transaction),
        )])
        .events(events)
        .build();
    petri.process_next_block().await;
    let all_txns = petri.state.transaction_cache.all_transactions();
    let utxos = petri
        .state
        .utxo_store
        .get_txos_by_correlation_id(&correlation_id.into());
    assert!(!utxos.is_empty());
    assert!(matches!(
        all_txns.as_slice()[0],
        Transaction {
            txn: CreatePendingCreate(PendingCreate {
                completing_transaction: None,
                ..
            }),
            ..
        }
    ));
    petri.process_next_block().await;
    let all_txns = petri.state.transaction_cache.all_transactions();
    let utxos = petri
        .state
        .utxo_store
        .get_txos_by_correlation_id(&correlation_id.into());
    assert!(utxos.is_empty());
    assert!(matches!(
        all_txns.as_slice()[2], // Up index to 1 to deal with dummy transaction
        Transaction {
            txn: CreatePendingCreate(PendingCreate {
                completing_transaction: Some(CreateCompletion::Cancellation(..)),
                ..
            }),
            ..
        }
    ));
}

#[tokio::test]
async fn petri__when_snapshotter_provided_then_captures_snapshot() {
    // Arrange
    let shutdown_flag = Arc::new(AtomicBool::new(true));

    let snapshotter = FakeSnapshotter::default();

    let mut petri = TestPetriBuilder::default()
        .with_shutdown_flag(shutdown_flag.clone())
        .with_snapshotter(snapshotter.clone())
        .build();

    // Act
    petri.start().await;
    assert_relevent_data_matches(
        &snapshotter.get_last_snapshot().unwrap(),
        &PetriPersistedState::default(),
    );
}

pub fn assert_relevent_data_matches<R>(
    left: &PetriPersistedState<R>,
    right: &PetriPersistedState<R>,
) {
    assert_eq!(left.next_block_to_process, right.next_block_to_process);
    transaction_cache::serialization_tests::assert_relevant_data_is_equal(
        &left.transaction_cache,
        &right.transaction_cache,
    );
    id_tag_cache::serialization_tests::assert_relevant_data_is_equal(
        &left.identity_tag_store,
        &right.identity_tag_store,
    );
    assert_eq!(&left.utxo_store, &right.utxo_store);
}

#[tokio::test]
async fn petri__attempts_snapshot_for_every_indexed_block() {
    // Arrange
    let shutdown_flag = Arc::new(AtomicBool::new(true));

    let snapshotter = FakeSnapshotter::default();
    let mut csprng: OsRng = OsRng::default();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let member_1 = Member::new();
    let create_request_txn_1 = random_test_create_request(&member_1, &trust_encryption_key);
    let create_request_txn_2 = random_test_create_request(&member_1, &trust_encryption_key);
    let mut petri = TestPetriBuilder::default()
        .trust_encryption_key(trust_encryption_key)
        .with_shutdown_flag(shutdown_flag)
        .with_snapshotter(snapshotter.clone())
        .member_with_encryption_key(member_1.address(), member_1.encryption_key)
        .private_key(member_1.private_key)
        .timestamp_interval(3)
        .transactions(vec![
            (
                member_1.account.public(),
                confidential_create_request_extrinsic(create_request_txn_1),
            ),
            (
                member_1.account.public(),
                confidential_create_request_extrinsic(create_request_txn_2),
            ),
        ])
        .build();

    // Act
    petri.start().await;

    assert_eq!(snapshotter.get_snapshot_count(), 2)
}
