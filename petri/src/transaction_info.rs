use chrono::{DateTime, Utc};
use xand_ledger::PublicKey;
use xand_models::{
    CallDecodeResult, ExtrinsicDecodeResult, Transaction, TransactionId, TransactionStatus,
};
use xand_public_key::public_key_to_address;

#[derive(Debug)]
pub(crate) struct TransactionInfo {
    pub xand_model_transaction: Option<Transaction>,
    pub xand_financial_client_transaction: Option<xand_financial_client::models::Transaction>,
    pub status: TransactionStatus,
}

impl From<(ExtrinsicDecodeResult, TransactionStatus)> for TransactionInfo {
    fn from((edr, status): (ExtrinsicDecodeResult, TransactionStatus)) -> Self {
        let xand_model_transaction;
        let xand_financial_client_transaction;
        match edr {
            ExtrinsicDecodeResult::CouldNotDecode(fin_t) => {
                xand_model_transaction = None;
                xand_financial_client_transaction = fin_t;
            }
            ExtrinsicDecodeResult::AccountBased(mod_t) => {
                xand_model_transaction = Some(mod_t);
                xand_financial_client_transaction = None;
            }
            ExtrinsicDecodeResult::RingSignature(mod_t, fin_t) => {
                xand_model_transaction = Some(mod_t);
                xand_financial_client_transaction = Some(fin_t);
            }
            ExtrinsicDecodeResult::RootTransaction(fin_t) => {
                xand_model_transaction = None;
                xand_financial_client_transaction = Some(fin_t);
            }
        }
        TransactionInfo {
            xand_model_transaction,
            xand_financial_client_transaction,
            status,
        }
    }
}

pub(crate) struct CallInfo {
    pub decoded: CallDecodeResult,
    pub status: TransactionStatus,
    pub timestamp: DateTime<Utc>,
    pub transaction_id: TransactionId,
}

impl From<CallInfo> for TransactionInfo {
    fn from(call_info: CallInfo) -> Self {
        match call_info.decoded {
            CallDecodeResult::CouldNotDecode(fin_t) => TransactionInfo {
                xand_model_transaction: None,
                xand_financial_client_transaction: fin_t,
                status: call_info.status,
            },
            CallDecodeResult::DecodedXandTransaction(tx) => {
                let pubkey = PublicKey::default();
                let signer_address = public_key_to_address(&pubkey);

                let transaction = Transaction {
                    signer_address,
                    transaction_id: call_info.transaction_id,
                    status: call_info.status.clone(),
                    txn: tx,
                    timestamp: call_info.timestamp,
                };
                TransactionInfo {
                    xand_model_transaction: Some(transaction),
                    xand_financial_client_transaction: None,
                    status: call_info.status,
                }
            }
            CallDecodeResult::DecodedFinancialTransaction(signer_address, xand_tx, fin_tx) => {
                let transaction = Transaction {
                    signer_address,
                    transaction_id: call_info.transaction_id,
                    status: call_info.status.clone(),
                    txn: xand_tx,
                    timestamp: call_info.timestamp,
                };
                TransactionInfo {
                    xand_model_transaction: Some(transaction),
                    xand_financial_client_transaction: Some(fin_tx),
                    status: call_info.status,
                }
            }
            CallDecodeResult::RootFinancialTransaction(fin_tx) => TransactionInfo {
                xand_model_transaction: None,
                xand_financial_client_transaction: Some(fin_tx),
                status: call_info.status,
            },
        }
    }
}
