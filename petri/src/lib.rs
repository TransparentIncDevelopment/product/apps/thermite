#![forbid(unsafe_code)]

#[macro_use]
extern crate tpfs_logger_port;

pub(crate) mod errors;

#[cfg(test)]
pub mod test_helpers;

pub mod confidentiality_store;
mod events;
mod historical_store;
mod id_tag_cache;
mod logging_events;
mod metrics;
pub mod petri_persisted_state;
pub mod snapshot;
mod subs_client_wrapper;
mod transaction_cache;
mod transaction_info;
mod txo_cache;

#[cfg(test)]
mod test;

#[cfg(test)]
mod confidentiality_tests;

pub use crate::{
    confidentiality_store::{TxoStore, TxoStoreError},
    historical_store::{HistoricalStore, LastBlockInfo},
    id_tag_cache::IdTagCache,
    transaction_cache::TransactionCache,
    txo_cache::TxoCache,
};

use crate::{
    confidentiality_store::{
        IdTagRecord, IdTagStatus, IdTagStore, IdTagStoreError, TransactionOutputRecord,
        TxoSpentStatus,
    },
    errors::PetriError,
    events::*,
    logging_events::LoggingEvent,
    petri_persisted_state::PetriPersistedState,
    snapshot::Snapshotter,
    subs_client_wrapper::{BlockId, BlockNumber, ConcreteBlock, SubstrateBlockchainClient},
    transaction_info::{CallInfo, TransactionInfo},
};
use chrono::{DateTime, Utc, MIN_DATETIME};
use codec::Decode;
use serde::Serialize;
use sp_core::storage::StorageData;
use sp_runtime::{
    generic::SignedBlock,
    traits::{Block, Header},
    DispatchError,
};
use std::{
    collections::HashMap,
    result::Result as StdResult,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::Duration,
};
use thiserror::Error;
use xand_address::Address;
use xand_financial_client::{
    errors::FinancialClientError,
    models::{FinancialTransaction, PendingRedeem},
};
use xand_financial_client_adapter::address_to_id_tag;
use xand_ledger::CreateCancellationTransaction;
use xand_models::{
    hash_substrate_encodeable, timestamp_from_extrinsics, ExtrinsicDecodeResult, ExtrinsicDecoder,
    ExtrinsicDecoderConfig, ExtrinsicDecoderImpl, Transaction, TransactionId, TransactionStatus,
    TxnConversionError, XandTransaction,
};
use xand_public_key::address_to_public_key;
use xandstrate_client::confidential_create_cancellation_extrinsic;
use xandstrate_runtime::{
    confidentiality, fixup_events, network_voting, network_voting::Event::Accepted,
    opaque::unopaqueify_extrinsic, system_events_storage_key, Event, EventRecord, Phase, Runtime,
    SystemEvent, UncheckedExtrinsic,
};

pub struct Petri<
    E: ExtrinsicDecoderConfig,
    R: Send + Sync + Default,
    S: Snapshotter<PetriPersistedState<R>>,
> {
    client: Arc<dyn SubstrateBlockchainClient>,
    /// Store true to signal to Petri it's time for shutdown
    shutdown_flag: Arc<AtomicBool>,
    extrinsic_decoder: ExtrinsicDecoderImpl<E>,
    snapshotter: Option<S>,
    state: PetriPersistedState<R>,
}

pub(crate) type Result<T, E = PetriError> = StdResult<T, E>;

const MAX_RETRIES: u8 = 11; // Consider backoff growth when setting

#[derive(Debug)]
enum ProcessedBlockState {
    Success,
    NeedsRetry,
    Failed,
}

impl<E, R, S> Petri<E, R, S>
where
    E: ExtrinsicDecoderConfig,
    R: Clone + Send + Sync + Default,
    S: Snapshotter<PetriPersistedState<R>>,
{
    pub fn new(
        client: Arc<dyn SubstrateBlockchainClient>,
        extrinsic_decoder: ExtrinsicDecoderImpl<E>,
        shutdown_flag: Arc<AtomicBool>,
        snapshotter: Option<S>,
        state: PetriPersistedState<R>,
    ) -> Self {
        let mut petri = Self {
            client,
            shutdown_flag,
            extrinsic_decoder,
            snapshotter,
            state,
        };
        petri.cache_member_id_tags();
        petri
    }

    /// Starts petri, will block indefinitely, so it should be run in its own thread
    pub async fn start(&mut self) {
        self.listen_to_blocks().await;
    }

    /// Runs an indefinite loop that listens to the blockchain for new blocks and stores
    /// account history in the given map write handle.
    async fn listen_to_blocks(&mut self) {
        info!(LoggingEvent::Info(
            "Beginning listening to blocks.".to_string()
        ));
        loop {
            self.process_available_blocks().await;

            if self.shutdown_flag.load(Ordering::SeqCst) {
                break;
            }
            tokio::time::sleep(Duration::from_secs(2)).await;
        }
    }

    async fn optional_snapshot(&mut self) {
        if let Some(snapshotter) = &mut self.snapshotter {
            if let Err(error) = snapshotter.snapshot(&self.state).await {
                error!(LoggingEvent::SnapshotError(format!("{:?}", error)));
            }
        }
    }

    /// Attempts to process the next block from the chain (if it's available yet)
    async fn process_available_blocks(&mut self) {
        let cur_info = self.client.chain_info();
        let mut retry = 0;
        let mut backoff = Self::default_backoff();
        while self.state.next_block_to_process <= cur_info.finalized_number {
            self.optional_snapshot().await;
            match self.process_next_block().await {
                ProcessedBlockState::Success => {
                    retry = 0;
                    backoff = Self::default_backoff();
                }
                ProcessedBlockState::NeedsRetry => {
                    if retry >= MAX_RETRIES {
                        error!(LoggingEvent::MaxRetriesForTransientBlockProcessingError {
                            retries: MAX_RETRIES
                        });
                        break;
                    } else {
                        retry += 1;
                        tokio::time::sleep(backoff).await;
                        backoff *= 2;
                    }
                }
                ProcessedBlockState::Failed => {
                    // Breaks for now, but outer loop might try again later
                    break;
                }
            }
        }
    }
    fn default_backoff() -> Duration {
        Duration::from_millis(1)
    }

    async fn process_next_block(&mut self) -> ProcessedBlockState {
        let block_number = self.state.next_block_to_process;
        info!(LoggingEvent::PetriProcessingBlock(format!(
            "Block #{}",
            block_number
        )));
        let block = self.client.block(&BlockId::Number(block_number));
        let block = match block {
            Ok(Some(block)) => block,
            Ok(None) => {
                error!(LoggingEvent::BlockNotFound { block_number });
                return ProcessedBlockState::Failed;
            }
            Err(e) => {
                error!(LoggingEvent::PetriError(e));
                return ProcessedBlockState::Failed;
            }
        };
        if let Err(e) = self.process_block(block.clone()).await {
            match e {
                BlockProcessingError::TransientFailure => {
                    error!(LoggingEvent::TransientBlockProcessingError { block_number });
                    ProcessedBlockState::NeedsRetry
                }
                BlockProcessingError::CallDecodingError(e) => {
                    error!(LoggingEvent::CouldNotDecodeExtrinsic(format!("{:?}", e)));
                    ProcessedBlockState::Failed
                }
            }
        } else {
            self.state.next_block_to_process += 1;
            ProcessedBlockState::Success
        }
    }

    async fn filter_completed_transactions<'a, Extrinsics>(
        &mut self,
        extrinsics: Extrinsics,
        interesting_events: &InterestingEvents<'a>,
        timestamp: DateTime<Utc>,
    ) -> Result<Vec<TransactionInfo>, BlockProcessingError>
    where
        Extrinsics: IntoIterator<Item = &'a (usize, UncheckedExtrinsic)>,
    {
        let mut filtered = Vec::<(ExtrinsicDecodeResult, TransactionStatus)>::new();
        for (i, ext) in extrinsics {
            // The index of the extrinsic in the list of extrinsics matches up with
            // the number inside of the `ApplyExtrinsic` part of each event record.
            // We only care about signed extrinsics.
            let ext_hash = hash_substrate_encodeable(ext);
            if let Some(events) = interesting_events.extrinsic_events.get(&(*i as u32)) {
                // Determine extrinsic status
                let mapped = self
                    .extrinsic_decoder
                    .decode_extrinsic(ext.clone(), timestamp)
                    .await;

                match mapped {
                    Ok(decode_result) => {
                        let status = extract_txn_status_from_events(events.as_slice());
                        // Log the reason if the transaction failed
                        if let TransactionStatus::Invalid(_) = &status {
                            warn!(LoggingEvent::SawFailedTransaction(ext_hash, ext.clone()));
                        }
                        filtered.push((decode_result, status));
                    }
                    Err(TxnConversionError::FinancialClientError { source })
                        if matches!(*source, FinancialClientError::Disconnect) =>
                    {
                        return Err(BlockProcessingError::TransientFailure);
                    }
                    Err(e) => {
                        trace!(LoggingEvent::IrrelevantTransaction(format!(
                            "{}: {:?}",
                            ext_hash, e
                        )))
                    }
                }
            } else {
                warn!(LoggingEvent::NoEventsForExtrinsic(ext_hash.to_string()))
            }
        }

        Ok(filtered.into_iter().map(Into::into).collect())
    }

    async fn record_transaction_info(
        &mut self,
        tx_info: TransactionInfo,
        timestamp: DateTime<Utc>,
    ) -> Result<(), PetriError> {
        let xand_tx = tx_info.xand_model_transaction;
        let financial_tx = tx_info.xand_financial_client_transaction;
        let status = tx_info.status;
        // only update txo and id tag store if transaction is finalized and valid
        if let TransactionStatus::Finalized = status {
            self.record_identity_tag_changes(&financial_tx, &xand_tx)
                .map_err(|source| PetriError::CachingError {
                    source_str: format!("{:?}", source),
                })?;

            self.record_txo_changes(&financial_tx, &xand_tx, timestamp);
        }

        // only apply the transaction to the historical store if it was decodable
        if let Some(mut transaction) = xand_tx {
            // TODO: we should be metering these when they aren't decodable but
            // this depends on xand models.
            metrics::meter_txn(&transaction);
            transaction.status = status;
            self.cache_transaction(transaction);
        }
        Ok(())
    }

    fn record_txo_changes(
        &mut self,
        transaction: &Option<xand_financial_client::models::Transaction>,
        xand_transaction: &Option<Transaction>,
        timestamp: DateTime<Utc>,
    ) {
        // Add new txos
        if let Some(transaction) = transaction {
            self.record_new_txos(transaction, timestamp);
            for txo in &transaction.spent_txos {
                self.state
                    .utxo_store
                    .update_txo_status_by_hash(txo.to_hash(), TxoSpentStatus::Spent);
            }
        }

        // apply updates to pending txos
        // find associated transaction by correlation id
        if let Some(xand_tx) = xand_transaction {
            match &xand_tx.txn {
                XandTransaction::FulfillCreate(c) => {
                    self.state.utxo_store.update_txo_status_by_correlation_id(
                        &c.correlation_id,
                        TxoSpentStatus::UnSpent,
                    )
                }
                XandTransaction::CancelCreate(c) => self
                    .state
                    .utxo_store
                    .remove_txos_by_correlation_id(&c.correlation_id),
                XandTransaction::FulfillRedeem(r) => {
                    if !self
                        .state
                        .utxo_store
                        .get_txos_by_correlation_id(&r.correlation_id)
                        .is_empty()
                    {
                        self.state
                            .utxo_store
                            .remove_txos_by_correlation_id(&r.correlation_id)
                    }
                }
                XandTransaction::CancelRedeem(r) => {
                    if !self
                        .state
                        .utxo_store
                        .get_txos_by_correlation_id(&r.correlation_id)
                        .is_empty()
                    {
                        self.state.utxo_store.update_txo_status_by_correlation_id(
                            &r.correlation_id,
                            TxoSpentStatus::UnSpent,
                        )
                    }
                }
                _ => {}
            }
        }
    }

    fn record_new_txos(
        &mut self,
        transaction: &xand_financial_client::models::Transaction,
        timestamp: DateTime<Utc>,
    ) {
        for txo in &transaction.new_txos {
            let (status, correlation_id) = match &transaction.txn {
                FinancialTransaction::CreateRequest(_) => {
                    (TxoSpentStatus::Pending, transaction.get_correlation_id())
                }
                FinancialTransaction::Redeem(PendingRedeem::Confidential(r)) => {
                    if r.redeem_output == txo.get_transaction_output() {
                        (TxoSpentStatus::Pending, transaction.get_correlation_id())
                    } else {
                        (TxoSpentStatus::UnSpent, None)
                    }
                }
                // all other kinds of new_txos are considered spendable by default
                _ => (TxoSpentStatus::UnSpent, None),
            };

            let record = TransactionOutputRecord {
                txo: txo.clone(),
                status,
                correlation_id: correlation_id.map(Into::into),
                timestamp,
            };
            self.state.utxo_store.insert(record);
        }
    }

    fn record_identity_tag_changes(
        &mut self,
        transaction: &Option<xand_financial_client::models::Transaction>,
        xand_transaction: &Option<Transaction>,
    ) -> Result<(), IdTagStoreError> {
        if let Some(transaction) = transaction {
            self.record_new_id_tag(transaction)?;
        }

        // apply updates to pending txos
        // find associated transaction by correlation id
        if let Some(xand_tx) = xand_transaction {
            match &xand_tx.txn {
                XandTransaction::FulfillCreate(c) => self
                    .state
                    .identity_tag_store
                    .update_id_tag_by_correlation_id(&c.correlation_id, IdTagStatus::Valid),
                XandTransaction::CancelCreate(c) => self
                    .state
                    .identity_tag_store
                    .remove_id_tag_by_correlation_id(&c.correlation_id),
                XandTransaction::RegisterMember(xand_models::RegisterAccountAsMember {
                    address,
                    ..
                }) => {
                    cache_member_id_tag(&mut self.state.identity_tag_store, address);
                }
                XandTransaction::RemoveMember(xand_models::RemoveMember { address }) => {
                    match address_to_id_tag(address) {
                        Some(tag) => self.state.identity_tag_store.remove(&tag),
                        None => error!(LoggingEvent::IdTagStoreError(format!(
                            "ID tag could not be derived from address {}",
                            address
                        ))),
                    }
                }
                XandTransaction::ExitMember(xand_models::ExitMember { address }) => {
                    if let Some(tag) = address_to_id_tag(address) {
                        // There is a very small chance the ID tag could still exist in Exiting state
                        self.state.identity_tag_store.remove(&tag);
                    }
                }
                _ => {}
            }
        }
        Ok(())
    }

    fn record_new_id_tag(
        &mut self,
        transaction: &xand_financial_client::models::Transaction,
    ) -> Result<(), IdTagStoreError> {
        let status = match &transaction.txn {
            FinancialTransaction::CreateRequest(_) => IdTagStatus::Pending,
            _ => IdTagStatus::Valid,
        };

        if let Some(id_tag) = &transaction.new_id_tag {
            let id_tag_record = IdTagRecord {
                tag: *id_tag,
                owner: transaction.signer,
                status,
                correlation_id: transaction.get_correlation_id().map(Into::into),
            };
            self.state.identity_tag_store.insert(id_tag_record)?;
        }
        Ok(())
    }

    fn cache_transaction(&self, transaction: Transaction) {
        self.state.transaction_cache.store(transaction);
        metrics::meter_cache_utilization(
            self.state.transaction_cache.transaction_count(),
            self.state.transaction_cache.transaction_limit(),
        );
    }

    fn get_events_for_block(&mut self, block_number: BlockNumber) -> Result<Vec<EventRecord>> {
        let block_id = BlockId::Number(block_number);
        let events_key = system_events_storage_key();
        if let Some(StorageData(events)) = self.client.storage(&block_id, &events_key)? {
            let mut events: &[u8] = events.as_ref();
            let events = Vec::<EventRecord>::decode(&mut events);
            return if let Ok(mut events) = events {
                // Fix up error messages
                let inner_events = events.iter_mut().map(|er| &mut er.event);
                fixup_events(inner_events);
                Ok(events)
            } else {
                Err(PetriError::ErrorDecodingEvents {
                    blockhash: format!("{}", block_id),
                })
            };
        }
        Ok(vec![])
    }

    //Note: This should be idempotent in case it needs to retry
    async fn process_block(
        &mut self,
        block: SignedBlock<ConcreteBlock>,
    ) -> Result<(), BlockProcessingError> {
        let block_number = *block.block.header.number();
        let (_, extrinsics) = block.block.deconstruct();
        let extracted_events;
        let interesting_events = match self.get_events_for_block(block_number) {
            Ok(events) => {
                extracted_events = events;
                extract_interesting_events(&extracted_events)
            }
            Err(e) => {
                error!(LoggingEvent::PetriError(e));
                InterestingEvents::default()
            }
        };
        debug!(LoggingEvent::Info(format!(
            "Events for block {:?}: {:?}",
            block_number, &interesting_events
        )));
        let unchecked_extrinsics = extrinsics
            .into_iter()
            .enumerate()
            .flat_map(|(index, ext)| {
                unopaqueify_extrinsic(ext)
                    .map(move |ext| (index, ext))
                    .map_err(|e| error!(LoggingEvent::CouldNotDecodeExtrinsic(e.to_string())))
            })
            .collect::<Vec<_>>();

        debug!(LoggingEvent::Info(format!(
            "Extrinsics for block {:?}: {:?}",
            block_number, &unchecked_extrinsics
        )));
        let timestamp = timestamp_from_extrinsics(unchecked_extrinsics.iter().map(|(_, e)| e))
            .unwrap_or_else(|| {
                if block_number > 0 {
                    error!(LoggingEvent::BlockMissingTimestamp { block_number });
                }
                MIN_DATETIME
            });

        let all_transactions = self
            .filter_completed_transactions(&unchecked_extrinsics, &interesting_events, timestamp)
            .await?;

        let event_transactions = self
            .get_transaction_info_from_events(block_number, &interesting_events, timestamp)
            .await?;

        for tx in all_transactions.into_iter().chain(event_transactions) {
            let cache_result = self.record_transaction_info(tx, timestamp).await;
            if let Err(e) = cache_result {
                warn!(LoggingEvent::InvalidExtrinsic(e))
            }
        }

        // Update the last block processed info
        if let Err(e) = self
            .state
            .transaction_cache
            .set_last_processed_block_info(LastBlockInfo {
                number: block_number,
                timestamp,
            })
        {
            error!(LoggingEvent::PetriError(PetriError::CachingError {
                source_str: format!(
                    "Error storing last block ({}) in cache: {:?}",
                    block_number, e
                )
            }))
        };
        Ok(())
    }

    async fn get_transaction_info_from_events(
        &self,
        block_number: BlockNumber,
        interesting_events: &InterestingEvents<'_>,
        timestamp: DateTime<Utc>,
    ) -> Result<Vec<TransactionInfo>, BlockProcessingError> {
        let mut txs = Vec::new();
        let block_id = BlockId::Number(block_number);
        for event in &interesting_events.network_voting_events {
            if let Some(tx) = self
                .network_voting_event_to_tx_info(&block_id, event, timestamp)
                .await?
            {
                txs.push(tx);
            }
        }
        for event in &interesting_events.confidentiality_events {
            if let Some(tx) = self
                .confidentiality_event_to_tx_info(block_number, event, timestamp)
                .await?
            {
                txs.push(tx);
            }
        }
        Ok(txs)
    }

    async fn confidentiality_event_to_tx_info(
        &self,
        block_number: BlockNumber,
        event: &confidentiality::Event<Runtime>,
        timestamp: DateTime<Utc>,
    ) -> Result<Option<TransactionInfo>, BlockProcessingError> {
        match event {
            confidentiality::Event::CreateCancelled(correlation_id, reason) => {
                let inner = CreateCancellationTransaction {
                    correlation_id: *correlation_id,
                    reason: reason.0,
                };
                let call = confidential_create_cancellation_extrinsic(inner);
                let transaction_id =
                    Self::tx_id_from_block_number_and_create_cancellation_correlation_id(
                        block_number,
                        correlation_id,
                    );
                let tx = self.call_to_tx(call, timestamp, transaction_id).await?;
                Ok(Some(tx))
            }
            _ => Ok(None),
        }
    }

    fn tx_id_from_block_number_and_create_cancellation_correlation_id(
        block_number: BlockNumber,
        correlation_id: &[u8; 16],
    ) -> TransactionId {
        hash_substrate_encodeable(
            &"create_cancellation_"
                .as_bytes()
                .iter()
                .copied()
                .chain(block_number.to_be_bytes())
                .chain(*correlation_id)
                .collect::<Vec<_>>(),
        )
        .into()
    }

    async fn network_voting_event_to_tx_info(
        &self,
        block_id: &BlockId,
        event: &network_voting::Event<Runtime>,
        timestamp: DateTime<Utc>,
    ) -> Result<Option<TransactionInfo>, BlockProcessingError> {
        match event {
            Accepted(index) => {
                if let Some(call) = self
                    .client
                    .get_proposal_action(block_id, index)
                    .map_err(|_| BlockProcessingError::TransientFailure)?
                {
                    let transaction_id = Self::tx_id_from_prop_index(*index);
                    let tx = self.call_to_tx(call, timestamp, transaction_id).await?;
                    Ok(Some(tx))
                } else {
                    Ok(None)
                }
            }
            _ => Ok(None),
        }
    }

    fn tx_id_from_prop_index(index: u32) -> TransactionId {
        hash_substrate_encodeable(&format!("proposal_{}", &index)).into()
    }

    async fn call_to_tx(
        &self,
        call: xandstrate_runtime::Call,
        timestamp: DateTime<Utc>,
        transaction_id: TransactionId,
    ) -> Result<TransactionInfo, BlockProcessingError> {
        let decoded = self
            .extrinsic_decoder
            .decode_call(call)
            .await
            .map_err(BlockProcessingError::CallDecodingError)?;
        let tx_info = CallInfo {
            decoded,
            status: TransactionStatus::Finalized,
            timestamp,
            transaction_id,
        }
        .into();
        Ok(tx_info)
    }

    fn cache_member_id_tags(&mut self) {
        let cur_info = self.client.chain_info();
        let members = match self
            .client
            .members(&BlockId::Number(cur_info.finalized_number))
        {
            Ok(members) => members,
            Err(e) => {
                error!(LoggingEvent::PetriError(e));
                return;
            }
        };
        for member in &members {
            cache_member_id_tag(&mut self.state.identity_tag_store, member);
        }
    }
}

fn cache_member_id_tag<C>(cache: &mut C, member: &Address)
where
    C: IdTagStore,
{
    let id_tag = match address_to_id_tag(member) {
        Some(tag) => tag,
        None => {
            error!(LoggingEvent::IdTagStoreError(format!(
                "ID tag could not be derived from address {}",
                member
            )));
            return;
        }
    };
    let record = IdTagRecord {
        tag: id_tag,
        owner: address_to_public_key(member),
        status: IdTagStatus::Valid,
        correlation_id: None,
    };
    if let Err(e) = cache.insert(record) {
        error!(LoggingEvent::IdTagStoreError(e.to_string()));
    }
}

#[derive(Serialize)]
struct KeyValuePair<K, V> {
    key: K,
    value: V,
}

#[derive(Debug, Error)]
enum BlockProcessingError {
    #[error("A temporary unexpected error occurred")]
    TransientFailure,
    #[error("Decoding Substrate Call")]
    CallDecodingError(TxnConversionError),
}
