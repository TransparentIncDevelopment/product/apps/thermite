use crate::test_helpers::FakeSnapshotter;
use crate::{
    confidentiality_store::{
        IdTagStatus, IdTagStore, TransactionOutputRecord, TxoHash, TxoSpentStatus, TxoStore,
        TxoStoreError,
    },
    test_helpers::{to_txo, FakeCsprng, TestExtrinsicDecoderConfig, TestPetriBuilder},
    HistoricalStore, Petri,
};
use chrono::MIN_DATETIME;
use rand::{rngs::OsRng, Rng};
use sp_core::{sr25519::Public, Pair};
use xand_address::Address;
use xand_financial_client::{
    models::{BankAccount, EncryptionKey, TransactionOutputData},
    test_helpers::{
        managers::{
            FakeKeyStore, FakeSendBuilderDeps, FakeTxoRepo, TestEncryptionProvider,
            TestNetworkKeyResolver,
        },
        FakeIdentityTagSelector, FakeMemberContext,
    },
    EncryptionProvider, SendBuilder, SendBuilderImpl, SendTxParams,
};
use xand_financial_client_adapter::krypt_adapter::sp_pair_to_private_key;
use xand_ledger::{
    generate_fake_input_txos, generate_spendable_utxos, CashConfirmationTransaction, CorrelationId,
    CreateCancellationReason, CreateCancellationTransaction, IdentityTag, OpenedTransactionOutput,
    PrivateKey, RedeemCancellationTransaction, RedeemFulfillmentTransaction, SendClaimsTransaction,
    TestCreateRequestBuilder, TestRedeemRequestBuilder, TransactionOutput,
};
use xand_models::{dev_account_key, ToAddress, Transaction, XandTransaction};
use xandstrate_client::{
    confidential_redeem_cancellation, confidential_redeem_extrinsic,
    confidential_redeem_fulfillment,
};
use xandstrate_runtime::{
    extrinsic_builder::{
        confidential_create_cancellation_extrinsic, confidential_create_extrinsic,
        confidential_create_request_extrinsic, confidential_send_extrinsic,
    },
    Call,
};

mod redeem;

// TODO - test that a member's owned unspent private txos can be successfully
// https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6624

#[tokio::test]
async fn block_with_confidential_create_request_updates_txos() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = OsRng::default();
    let create_request = TestCreateRequestBuilder::default()
        .build(&mut csprng)
        .create_request;
    let expected_txos: Vec<TransactionOutput> = create_request
        .core_transaction
        .outputs
        .iter()
        .map(|otxo| otxo.transaction_output)
        .collect();
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![(
            tupac.public(),
            confidential_create_request_extrinsic(create_request.clone()),
        )])
        .build();
    petri.process_available_blocks().await;
    let txo_records = petri
        .state
        .utxo_store
        .get_txos_by_correlation_id(&create_request.core_transaction.correlation_id.into());
    let cached_txos: Vec<TransactionOutput> = txo_records
        .iter()
        .map(|record| to_txo(&record.txo))
        .collect();

    // Assert txos are cached by tx id
    assert_eq!(cached_txos, expected_txos);
    // Assert txo spent status is pending
    assert!(txo_records
        .iter()
        .all(|record| record.status == TxoSpentStatus::Pending));
}

#[tokio::test]
async fn block_with_confidential_create_request_and_create_updates_txos_to_spendable() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = OsRng::default();
    let create_request = TestCreateRequestBuilder::default()
        .build(&mut csprng)
        .create_request;
    let expected_txos: Vec<TxoHash> = create_request
        .core_transaction
        .outputs
        .iter()
        .map(|otxo| otxo.transaction_output.to_hash())
        .collect();
    let cash_confirmation = CashConfirmationTransaction {
        correlation_id: create_request.core_transaction.correlation_id,
    };
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![
            (
                tupac.public(),
                confidential_create_request_extrinsic(create_request),
            ),
            (
                tupac.public(),
                confidential_create_extrinsic(cash_confirmation),
            ),
        ])
        .build();
    petri.process_available_blocks().await;
    let txo_records: Vec<TransactionOutputRecord> = expected_txos
        .iter()
        .filter_map(|txo_id| petri.state.utxo_store.get_txo(txo_id))
        .collect();
    let cached_txo_ids: Vec<TxoHash> = txo_records
        .iter()
        .map(|record| to_txo(&record.txo).to_hash())
        .collect();

    // Assert txos are cached by tx id
    assert_eq!(cached_txo_ids, expected_txos);
    // Assert txo spent status is unspent
    assert!(txo_records
        .iter()
        .all(|record| record.status == TxoSpentStatus::UnSpent));
}

#[tokio::test]
async fn block_with_confidential_create_request_and_cancellation_removes_txos() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = OsRng::default();
    let create_request = TestCreateRequestBuilder::default()
        .build(&mut csprng)
        .create_request;
    let cancellation = CreateCancellationTransaction {
        correlation_id: create_request.core_transaction.correlation_id,
        reason: CreateCancellationReason::InvalidData,
    };
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![
            (
                tupac.public(),
                confidential_create_request_extrinsic(create_request.clone()),
            ),
            (
                tupac.public(),
                confidential_create_cancellation_extrinsic(cancellation),
            ),
        ])
        .build();
    petri.process_available_blocks().await;
    let txo_records = petri
        .state
        .utxo_store
        .get_txos_by_correlation_id(&create_request.core_transaction.correlation_id.into());
    // Assert txos are no longer available after cancellation
    assert_eq!(txo_records.len(), 0);
}

#[tokio::test]
async fn block_with_confidential_create_request_inserts_pending_id_tag() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = OsRng::default();
    let create_request = TestCreateRequestBuilder::default()
        .build(&mut csprng)
        .create_request;
    let expected_id_tag = create_request.core_transaction.identity_output;
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![(
            tupac.public(),
            confidential_create_request_extrinsic(create_request),
        )])
        .build();
    petri.process_available_blocks().await;
    let id_tag_record = petri
        .state
        .identity_tag_store
        .get_id_tag(&expected_id_tag.to_hash())
        .expect("missing expected id tag");
    // Assert id tag is cached by id
    assert_eq!(id_tag_record.tag, expected_id_tag);
    // Assert id tag status is pending
    assert_eq!(id_tag_record.status, IdTagStatus::Pending);
}

#[tokio::test]
async fn block_with_confidential_create_request_and_create_updates_id_tag_to_valid() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = OsRng::default();
    let create_request = TestCreateRequestBuilder::default()
        .build(&mut csprng)
        .create_request;
    let expected_id_tag = create_request.core_transaction.identity_output;
    let cash_confirmation = CashConfirmationTransaction {
        correlation_id: create_request.core_transaction.correlation_id,
    };
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![
            (
                tupac.public(),
                confidential_create_request_extrinsic(create_request),
            ),
            (
                tupac.public(),
                confidential_create_extrinsic(cash_confirmation),
            ),
        ])
        .build();
    petri.process_available_blocks().await;
    let id_tag_record = petri
        .state
        .identity_tag_store
        .get_id_tag(&expected_id_tag.to_hash())
        .expect("missing expected id tag");
    // Assert id tag is cached by id
    assert_eq!(id_tag_record.tag, expected_id_tag);
    // Assert id tag status is valid from cash confirmation
    assert_eq!(id_tag_record.status, IdTagStatus::Valid);
}

#[tokio::test]
async fn block_with_confidential_create_request_and_cancellation_removes_id_tag() {
    let tupac = dev_account_key("2Pac");
    let mut csprng = OsRng::default();
    let create_request = TestCreateRequestBuilder::default()
        .build(&mut csprng)
        .create_request;
    let expected_id_tag = create_request.core_transaction.identity_output;
    let cancellation = CreateCancellationTransaction {
        correlation_id: create_request.core_transaction.correlation_id,
        reason: CreateCancellationReason::InvalidData,
    };
    let mut petri = TestPetriBuilder::default()
        .member(tupac.public().to_address())
        .transactions(vec![
            (
                tupac.public(),
                confidential_create_request_extrinsic(create_request),
            ),
            (
                tupac.public(),
                confidential_create_cancellation_extrinsic(cancellation),
            ),
        ])
        .build();
    petri.process_available_blocks().await;
    let id_tag_record = petri
        .state
        .identity_tag_store
        .get_id_tag(&expected_id_tag.to_hash());
    // Assert id tag is removed after cancellation
    assert!(id_tag_record.is_none());
}

#[tokio::test]
async fn issuer_can_see_their_balance() {
    let mut csprng = OsRng::default();
    let issuer = Member::new();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let values = [1, 2, 3];
    let transactions =
        create_request_and_fulfillment(&mut csprng, trust_encryption_key, &issuer, &values);
    let mut petri = TestPetriBuilder::default()
        .member_with_encryption_key(issuer.address(), issuer.encryption_key)
        .trust_encryption_key(trust_encryption_key)
        .private_key(issuer.private_key)
        .transactions(transactions)
        .build();
    petri.process_available_blocks().await;
    let expected_balance = values.iter().copied().sum::<u64>();
    assert_eq!(
        petri.state.utxo_store.balance(&issuer.address()).unwrap(),
        expected_balance,
    );
}

#[tokio::test]
async fn member_cannot_see_balance_of_other_member() {
    let mut csprng = OsRng::default();
    let issuer = Member::new();
    let other_member = Member::new();
    let trust_encryption_key = EncryptionKey::from(csprng.gen::<[u8; 32]>());
    let transactions =
        create_request_and_fulfillment(&mut csprng, trust_encryption_key, &issuer, &[1, 2, 3]);
    let mut petri = TestPetriBuilder::default()
        .member_with_encryption_key(issuer.address(), issuer.encryption_key)
        .member_with_encryption_key(other_member.address(), other_member.encryption_key)
        .trust_encryption_key(trust_encryption_key)
        .private_key(other_member.private_key)
        .transactions(transactions)
        .build();
    petri.process_available_blocks().await;
    assert!(matches!(
        petri.state.utxo_store.balance(&issuer.address()),
        Err(TxoStoreError::BalanceNotFound),
    ));
}

#[tokio::test]
async fn send_updates_status_of_txos_in_cache() {
    let mut ctx = SendContext::new(vec![3, 5]).await;
    ctx.petri.process_available_blocks().await;
    let all_spent = ctx
        .spendable_txos
        .iter()
        .map(|txo| txo.transaction_output.to_hash())
        .all(|txo_hash| {
            ctx.petri
                .state
                .utxo_store
                .get_txo(&txo_hash)
                .map_or(false, |txo| txo.status == TxoSpentStatus::Spent)
        });
    assert!(all_spent);
}

#[tokio::test]
async fn send_appears_in_history() {
    let mut ctx = SendContext::new(vec![3, 5]).await;
    ctx.petri.process_available_blocks().await;
    let transactions = ctx
        .petri
        .state
        .transaction_cache
        .all_transactions()
        .collect::<Vec<_>>();
    assert!(matches!(
        &*transactions,
        [Transaction {
            signer_address,
            txn: XandTransaction::Send(t),
            ..
        }] if *signer_address == ctx.issuer.address()
            && t.destination_account == ctx.recipient.address()
            && t.amount_in_minor_unit == 8
    ));
}

#[tokio::test]
async fn send_adds_identity_tag_to_cache() {
    let mut ctx = SendContext::new(vec![3, 5]).await;
    let id_tag_count = ctx
        .petri
        .state
        .identity_tag_store
        .get_known_tags(&ctx.issuer.private_key.into())
        .len();
    ctx.petri.process_available_blocks().await;
    let new_id_tag_count = ctx
        .petri
        .state
        .identity_tag_store
        .get_known_tags(&ctx.issuer.private_key.into())
        .len();
    assert_eq!(new_id_tag_count, id_tag_count + 1);
}

struct SendContext {
    issuer: Member,
    recipient: Member,
    spendable_txos: Vec<OpenedTransactionOutput>,
    petri: Petri<TestExtrinsicDecoderConfig, FakeCsprng, FakeSnapshotter>,
}

impl SendContext {
    async fn new<V>(values: V) -> Self
    where
        V: IntoIterator<Item = u64>,
    {
        let values = values.into_iter().collect::<Vec<_>>();
        let amount = values.iter().sum();
        let mut rng = OsRng::default();
        let issuer = Member::new();
        let recipient = Member::new();
        let issuer_id_tag =
            IdentityTag::from_key_with_rand_base(issuer.private_key.into(), &mut rng);
        let spendable_txos = generate_spendable_utxos(issuer_id_tag, values, &mut rng);
        let send = make_send(
            &issuer,
            &recipient,
            amount,
            issuer_id_tag,
            spendable_txos.clone(),
            &mut rng,
        )
        .await;
        let call = confidential_send_extrinsic(send);
        let mut petri = TestPetriBuilder::default()
            .member_with_encryption_key(issuer.address(), issuer.encryption_key)
            .member_with_encryption_key(recipient.address(), recipient.encryption_key)
            .private_key(issuer.private_key)
            .transactions(vec![(issuer.account.public(), call)])
            .build();
        for txo in &spendable_txos {
            petri.state.utxo_store.insert(TransactionOutputRecord {
                txo: TransactionOutputData::Public(txo.transaction_output),
                status: TxoSpentStatus::UnSpent,
                correlation_id: None,
                timestamp: MIN_DATETIME,
            });
        }
        Self {
            issuer,
            recipient,
            spendable_txos,
            petri,
        }
    }
}

async fn make_send(
    issuer: &Member,
    recipient: &Member,
    amount: u64,
    issuer_id_tag: IdentityTag,
    spendable_txos: Vec<OpenedTransactionOutput>,
    rng: &mut OsRng,
) -> SendClaimsTransaction {
    let send_builder = SendBuilderImpl::<FakeSendBuilderDeps>::new(
        FakeIdentityTagSelector {
            id_tags_from_address: [
                (issuer.address(), vec![issuer_id_tag]),
                (
                    recipient.address(),
                    vec![IdentityTag::from_key_with_rand_base(
                        recipient.private_key.into(),
                        rng,
                    )],
                ),
            ]
            .iter()
            .cloned()
            .collect(),
        },
        FakeKeyStore {
            encryption_keys: vec![issuer.encryption_key],
            ..FakeKeyStore::new(vec![issuer.private_key])
        },
        FakeTxoRepo {
            decoys: generate_fake_input_txos(4, spendable_txos.len(), rng)
                .into_iter()
                .flat_map(|set| set.txos.components)
                .collect(),
            spendable_txo: spendable_txos,
        },
        TestEncryptionProvider,
        TestNetworkKeyResolver {
            trust_encryption_key: EncryptionKey::random(rng),
            trust_key: PrivateKey::random(rng).into(),
            other_keys: [
                (issuer.address(), issuer.encryption_key),
                (recipient.address(), recipient.encryption_key),
            ]
            .iter()
            .cloned()
            .collect(),
        },
        FakeMemberContext::default(),
        OsRng::default().into(),
    );
    let send_params = SendTxParams {
        issuer: issuer.private_key.into(),
        amount,
        recipient: recipient.private_key.into(),
    };
    send_builder.build_send_tx(send_params).await.unwrap()
}

pub(crate) struct Member {
    pub account: sp_core::sr25519::Pair,
    pub private_key: PrivateKey,
    pub encryption_key: EncryptionKey,
}

impl Member {
    pub fn new() -> Self {
        let account = sp_core::sr25519::Pair::generate().0;
        let private_key = sp_pair_to_private_key(&account).unwrap();
        Self {
            account,
            private_key,
            encryption_key: EncryptionKey::from(rand::thread_rng().gen::<[u8; 32]>()),
        }
    }

    pub fn address(&self) -> Address {
        self.account.public().to_address()
    }
}

fn create_request_and_fulfillment(
    csprng: &mut OsRng,
    trust_encryption_key: EncryptionKey,
    issuer: &Member,
    values: &[u64],
) -> Vec<(Public, Call)> {
    let correlation_id: CorrelationId = csprng.gen();
    let extra = TestEncryptionProvider
        .encrypt(
            &BankAccount {
                routing_number: "123".into(),
                account_number: "456".into(),
            },
            &issuer.encryption_key,
            &trust_encryption_key,
            &(&correlation_id[..]).into(),
        )
        .unwrap();
    let create_request = TestCreateRequestBuilder::default()
        .private_key(issuer.private_key)
        .values(values.to_vec())
        .correlation_id(correlation_id)
        .extra(extra.into())
        .build(csprng)
        .create_request;
    let cash_confirmation = CashConfirmationTransaction {
        correlation_id: create_request.core_transaction.correlation_id,
    };
    let trust = sp_core::sr25519::Pair::generate().0;
    vec![
        (
            issuer.account.public(),
            confidential_create_request_extrinsic(create_request),
        ),
        (
            trust.public(),
            confidential_create_extrinsic(cash_confirmation),
        ),
    ]
}
