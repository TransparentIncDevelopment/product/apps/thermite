<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [xand-runtime-models](#xand-runtime-models)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# xand-runtime-models

This crate is expected to be kept quite small, and provides some very low-level
structs which are used within the runtime, and sometimes above it.

It is **VERY IMPORTANT** that you be careful when changing anything in here,
as it could possibly result in requiring a runtime upgrade. IE: If anything
you changed is used within the runtime, that's potentially a breaking change.
