//! This crate contains models which are used in the runtime, but may also be used outside of the
//! runtime for purposes like creating extrinsics or decoding chain data. It should have the
//! minimum amount of dependencies possible to support this goal. This crate should not be used
//! beyond the xand-api boundary. IE: It has substrate-specific things which should not escape that
//! abstraction, or, alternatively, it should only be used if you talk directly to a validator.
//!
//! **BE AWARE!** -- Because these models are used inside the runtime, changing them (and using
//! the new version) means that the runtime has changed! Take care.
//!
//! For the blockchain-agnostic versions of our models, see the `models` module in the
//! `xand-utils` crate.

#![forbid(unsafe_code)]
#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(feature = "std")]
pub use std_lib::{CidrBlockParseErr, FixedSizeFieldError, PubkeyWrongSizeError};

use core::{
    convert::TryFrom,
    fmt::{self, Debug, Display, Formatter},
    ops::Deref,
    str::FromStr,
};
#[cfg(feature = "parity-scale-codec")]
use parity_scale_codec::{Decode, Encode, EncodeLike, Input, Output};
#[cfg(any(test, feature = "serialization"))]
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};
use x25519_dalek::PublicKey;
use xand_ledger::{CreateCancellationReason, RedeemCancellationReason};

pub mod upgrade;

pub type FiatReqCorrelationId = [u8; 16];

/// The interface accepts any byte sequence as a correlation_id, but the substrate impl expects them to
/// be exactly 16 bytes. This does the conversion, returning an error if the vec is not the right
/// size.
pub fn bytes_as_fiat_correlation_id(
    v: &[u8],
) -> Result<FiatReqCorrelationId, FiatReqCorrelationIdWrongSizeErr> {
    if v.len() != 16 {
        return Err(FiatReqCorrelationIdWrongSizeErr);
    }
    let mut array = [0; 16];
    array.copy_from_slice(v);
    Ok(array)
}

pub fn fiat_correlation_id_from_str(
    s: &str,
) -> Result<FiatReqCorrelationId, FiatReqCorrelationIdWrongSizeErr> {
    let bytes = s.as_bytes();
    bytes_as_fiat_correlation_id(bytes)
}

/// The only error that can be returned by `vec_as_fiat_correlation_id`
pub struct FiatReqCorrelationIdWrongSizeErr;

/// A public key on Curve25519 that can be used for encryption.
///
/// EX: Encrypting bank account metadata for the trust
#[derive(Copy, Clone, Debug)]
pub struct X25519PublicKey {
    inner: PublicKey,
}

impl X25519PublicKey {
    #[inline]
    pub fn as_bytes(&self) -> &[u8; 32] {
        self.inner.as_bytes()
    }
}

impl From<PublicKey> for X25519PublicKey {
    fn from(public_key: PublicKey) -> Self {
        X25519PublicKey { inner: public_key }
    }
}

impl From<[u8; 32]> for X25519PublicKey {
    fn from(bytes: [u8; 32]) -> Self {
        PublicKey::from(bytes).into()
    }
}

impl TryFrom<&[u8]> for X25519PublicKey {
    type Error = X25519PublicKeyParseError;

    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        Ok(<[u8; 32]>::try_from(bytes)
            .map_err(|_| X25519PublicKeyParseError)?
            .into())
    }
}

impl AsRef<[u8]> for X25519PublicKey {
    fn as_ref(&self) -> &[u8] {
        self.inner.as_bytes()
    }
}

#[cfg(any(test, feature = "serialization"))]
impl Serialize for X25519PublicKey {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.to_string().as_str())
    }
}

#[cfg(any(test, feature = "serialization"))]
impl<'de> Deserialize<'de> for X25519PublicKey {
    fn deserialize<D>(deserializer: D) -> Result<X25519PublicKey, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        s.parse()
            .map_err(|err| de::Error::custom(format!("Could not decode encryption key {:?}", err)))
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct X25519PublicKeyParseError;

impl FromStr for X25519PublicKey {
    type Err = X25519PublicKeyParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let point_bytes = bs58::decode(s)
            .into_vec()
            .map_err(|_| X25519PublicKeyParseError)?;
        X25519PublicKey::try_from(point_bytes.as_slice()).map_err(|_| X25519PublicKeyParseError)
    }
}

impl Display for X25519PublicKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let bytes = self.inner.to_bytes();
        let serialized_b58 = bs58::encode(bytes).into_string();
        write!(f, "{}", serialized_b58)
    }
}

impl Display for X25519PublicKeyParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

// We must implement Default to be storable in substrate. You definitely do not ever want an
// all zero pubkey.
impl Default for X25519PublicKey {
    fn default() -> Self {
        Self {
            inner: PublicKey::from([0; 32]),
        }
    }
}

impl PartialEq for X25519PublicKey {
    fn eq(&self, other: &Self) -> bool {
        self.as_bytes() == other.as_bytes()
    }
}

impl Eq for X25519PublicKey {}

#[cfg(feature = "parity-scale-codec")]
mod parity_codec {
    use super::*;
    use parity_scale_codec::Error;

    impl Encode for X25519PublicKey {
        fn encode_to<T: Output + ?Sized>(&self, dest: &mut T) {
            self.as_bytes().encode_to(dest);
        }
    }

    impl Decode for X25519PublicKey {
        fn decode<I: Input>(value: &mut I) -> Result<Self, Error> {
            let mut bytes: [u8; 32] = [0; 32];
            value.read(&mut bytes)?;
            Ok(X25519PublicKey {
                inner: PublicKey::from(bytes),
            })
        }
    }

    impl EncodeLike<X25519PublicKey> for X25519PublicKey {}
}

#[allow(non_camel_case_types)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
#[derive(Clone)]
pub struct U8_128(pub [u8; 128]);

impl core::fmt::Debug for U8_128 {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        let bytes = hex::encode(self.iter());
        write!(f, "{:?}", bytes)
    }
}

#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub enum RuntimeBankAccount {
    /// In this variant, the bank information is stored as a utf8 json string (fixed to 128 bytes),
    /// which is the serialized version of `xand_utils::models::BankAccountId`. Hence, it is not
    /// encrypted.
    JsonEncoded(U8_128),
    Encrypted(U8_128),
}

impl Default for RuntimeBankAccount {
    fn default() -> RuntimeBankAccount {
        RuntimeBankAccount::JsonEncoded(U8_128::default())
    }
}

impl Default for U8_128 {
    fn default() -> Self {
        U8_128([0; 128])
    }
}

impl Deref for U8_128 {
    type Target = [u8; 128];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl PartialEq for U8_128 {
    fn eq(&self, other: &Self) -> bool {
        let same_length = self.len() == other.len();
        if !same_length {
            return false;
        }

        self.iter().zip(other.iter()).all(|(a, b)| a == b)
    }
}

/// IPv4 CIDR Block, with first four indices the ip address and last being the suffix
#[derive(
    derive_more::From,
    derive_more::Into,
    derive_more::Deref,
    Copy,
    Clone,
    Debug,
    Hash,
    Eq,
    PartialEq,
)]
#[cfg_attr(
    feature = "serialization",
    derive(serde::Serialize, serde::Deserialize)
)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub struct CidrBlock(pub [u8; 5]);

pub type CidrBlockArray = [Option<CidrBlock>; 10];

#[derive(Clone, Debug, Eq, PartialEq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub enum CreateCancelReason {
    /// The pending create existed for too long without any fiat transfer
    Expired,
    /// The bank account information could not be acted upon by the trust
    InvalidData,
    /// The bank could not be found
    BankNotFound,
}

impl From<CreateCancellationReason> for CreateCancelReason {
    fn from(r: CreateCancellationReason) -> Self {
        match r {
            CreateCancellationReason::Expired => CreateCancelReason::Expired,
            CreateCancellationReason::InvalidData => CreateCancelReason::InvalidData,
            CreateCancellationReason::BankNotFound => CreateCancelReason::BankNotFound,
        }
    }
}

impl From<CreateCancelReason> for CreateCancellationReason {
    fn from(r: CreateCancelReason) -> Self {
        match r {
            CreateCancelReason::Expired => CreateCancellationReason::Expired,
            CreateCancelReason::InvalidData => CreateCancellationReason::InvalidData,
            CreateCancelReason::BankNotFound => CreateCancellationReason::BankNotFound,
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub enum RedeemCancelReason {
    /// The bank account information could not be acted upon by the trust
    InvalidData,
    /// The bank account is not allowed by the trustee
    AccountNotAllowed,
}

impl From<RedeemCancellationReason> for RedeemCancelReason {
    fn from(r: RedeemCancellationReason) -> Self {
        match r {
            RedeemCancellationReason::InvalidData => RedeemCancelReason::InvalidData,
            RedeemCancellationReason::AccountNotAllowed => RedeemCancelReason::AccountNotAllowed,
        }
    }
}

impl From<RedeemCancelReason> for RedeemCancellationReason {
    fn from(r: RedeemCancelReason) -> RedeemCancellationReason {
        match r {
            RedeemCancelReason::InvalidData => RedeemCancellationReason::InvalidData,
            RedeemCancelReason::AccountNotAllowed => RedeemCancellationReason::AccountNotAllowed,
        }
    }
}

/// A pending redeem request
#[derive(Default, Clone, Debug, PartialEq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub struct PendingRedeem<Balance, Account> {
    pub amount: Balance,
    /// The encrypted account information for the member's fiat funds to be transferred into.
    pub account_data: RuntimeBankAccount,
    /// The account coins will be redeemed out of
    pub take_funds_from: Account,
}

/// A pending create request
#[derive(Default, Clone, PartialEq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub struct PendingCreate<Balance, Account, BlockNumber> {
    pub amount: Balance,
    /// The account data for the transaction. Used by the member to
    /// validate activity and by the Trustee to transfer money
    /// from the correct account.
    pub account_data: RuntimeBankAccount,
    /// The account coins will be created into
    pub destination_account: Account,
    /// The block number that this request will expire at
    pub expires_at: BlockNumber,
}

/// A pending validator redeem request
#[derive(Default, Clone, Debug, PartialEq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
pub struct PendingValidatorRedeem<Balance, Account> {
    pub amount: Balance,
    /// The encrypted account information for the member's fiat funds to be transferred into.
    pub account_data: RuntimeBankAccount,
    /// The account coins will be redeemed from
    pub take_funds_from: Account,
}

impl<B: Display, A: Debug, C: Debug> Debug for PendingCreate<B, A, C> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "PendingCreate(amount: {} account_data: {:?} destination_account: {:?})",
            self.amount, self.account_data, self.destination_account
        )
    }
}

/// Represents the stage of a proposal. Proposals can only ever change stage once, from `Proposed`
/// to another stage. All other stages are final, and any subsequent changes should be considered
/// a bug.
#[derive(Clone, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
#[cfg_attr(
    feature = "serialization",
    derive(serde::Deserialize, serde::Serialize)
)]
pub enum ProposalStage {
    Proposed,
    Accepted,
    Rejected,
    Invalid,
}

impl core::default::Default for ProposalStage {
    fn default() -> Self {
        ProposalStage::Proposed
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "parity-scale-codec", derive(Encode, Decode))]
#[cfg_attr(
    feature = "serialization",
    derive(serde::Deserialize, serde::Serialize)
)]
pub enum BannedState<BlockNumber> {
    Exiting(BlockNumber),
    Exited,
}

impl<BlockNumber> From<BannedState<BlockNumber>> for xand_ledger::BannedState<BlockNumber> {
    fn from(state: BannedState<BlockNumber>) -> Self {
        match state {
            BannedState::Exiting(num) => xand_ledger::BannedState::Exiting(num),
            BannedState::Exited => xand_ledger::BannedState::Exited,
        }
    }
}

impl<T: core::default::Default> core::default::Default for BannedState<T> {
    fn default() -> Self {
        Self::Exiting(T::default())
    }
}

#[cfg(feature = "std")]
mod std_lib {
    use super::*;
    use std::hash::{Hash, Hasher};
    use std::{
        convert::{TryFrom, TryInto},
        fmt::{Debug, Display, Formatter},
        str::FromStr,
    };

    #[cfg_attr(
        feature = "serialization",
        derive(serde::Serialize, serde::Deserialize)
    )]
    #[derive(Clone, Debug)]
    pub struct PubkeyWrongSizeError;

    // TODO - genericize conversion - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6083
    impl TryFrom<Vec<u8>> for X25519PublicKey {
        type Error = PubkeyWrongSizeError;

        fn try_from(value: Vec<u8>) -> Result<Self, Self::Error> {
            if value.len() != 32 {
                return Err(PubkeyWrongSizeError);
            }
            let fixed_size: [u8; 32] = value
                .as_slice()
                .try_into()
                .expect("Already checked size is 32");
            Ok(Self {
                inner: PublicKey::from(fixed_size),
            })
        }
    }

    impl Hash for X25519PublicKey {
        fn hash<H: Hasher>(&self, state: &mut H) {
            self.inner.as_bytes().hash(state);
        }
    }

    #[cfg_attr(
        feature = "serialization",
        derive(serde::Serialize, serde::Deserialize, snafu::Snafu)
    )]
    #[derive(Clone)]
    pub enum FixedSizeFieldError {
        DataTooLargeForField { size: usize },
    }

    impl Debug for FixedSizeFieldError {
        fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
            match self {
                FixedSizeFieldError::DataTooLargeForField { size } => {
                    write!(f, "ParserError::DataTooLargeForField({})", size)
                }
            }
        }
    }

    impl Display for U8_128 {
        /// The underlying byte array is fixed width so will be right-padded
        /// with null terminators. This method extracts the non-null bytes
        /// and builds a string from that.
        fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
            let input = self.iter().copied().take_while(|b| b != &0).collect();
            write!(f, "{}", String::from_utf8(input).unwrap())
        }
    }

    impl TryFrom<Vec<u8>> for U8_128 {
        type Error = FixedSizeFieldError;

        fn try_from(input: Vec<u8>) -> Result<Self, Self::Error> {
            if input.len() > 128 {
                return Err(FixedSizeFieldError::DataTooLargeForField { size: 128 });
            }

            let mut array = [0; 128];
            let mut sizable = input;
            sizable.resize(128, 0);
            array.copy_from_slice(&sizable);
            Ok(U8_128(array))
        }
    }

    impl FromStr for U8_128 {
        type Err = FixedSizeFieldError;
        fn from_str(input: &str) -> Result<Self, Self::Err> {
            let result: U8_128 = input.to_string().into_bytes().try_into()?;
            Ok(result)
        }
    }

    impl U8_128 {
        /// Parses a U8_128 from a given string.
        /// [`FixedSizeFieldError`] results if string input is too large.
        pub fn parse(input: String) -> Result<U8_128, FixedSizeFieldError> {
            let result: U8_128 = input.into_bytes().try_into()?;
            Ok(result)
        }

        /// Converts a string to a `[u8; 128]` in one step.
        pub fn from_str_to_array(input: String) -> Result<[u8; 128], FixedSizeFieldError> {
            let converted = U8_128::parse(input)?;
            Ok(converted.0)
        }
    }

    #[derive(Debug, Clone)]
    pub struct CidrBlockParseErr;

    impl FromStr for CidrBlock {
        type Err = CidrBlockParseErr;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let ip_and_suffix: Vec<&str> = s.split('/').collect();
            if ip_and_suffix.len() != 2 {
                return Err(CidrBlockParseErr);
            }
            let mut cidr_block_strings: Vec<&str> = ip_and_suffix[0].split('.').collect();
            if cidr_block_strings.len() != 4 {
                return Err(CidrBlockParseErr);
            }
            cidr_block_strings.push(ip_and_suffix[1]);
            let mut array = [0_u8; 5];
            for i in 0..array.len() {
                array[i] = match cidr_block_strings[i].parse().ok() {
                    Some(x) => x,
                    None => return Err(CidrBlockParseErr),
                }
            }
            Ok(array.into())
        }
    }

    impl Display for CidrBlock {
        fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
            write!(
                f,
                "{}.{}.{}.{}/{}",
                self[0], self[1], self[2], self[3], self[4]
            )
        }
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    pub fn u8_128_parse() {
        let value = "Some random string".to_string();
        let result = U8_128::parse(value.clone());

        assert!(result.is_ok());

        assert_eq!(value, result.unwrap().to_string());
    }

    #[test]
    pub fn u8_128_parse_128_characters() {
        let mut value = "1234567890".to_string();
        while value.len() < 128 {
            value.push('z');
        }
        let result = U8_128::parse(value.clone());

        assert!(result.is_ok());

        assert_eq!(value, result.unwrap().to_string());
    }

    #[test]
    pub fn u8_128_parse_129_characters() {
        let mut value = "1234567890".to_string();
        while value.len() < 129 {
            value.push('z');
        }

        match U8_128::parse(value) {
            Err(error) => match error {
                FixedSizeFieldError::DataTooLargeForField { size } => {
                    assert_eq!(128, size);
                }
            },
            _ => {
                panic!("The result was expected to be an error.");
            }
        }
    }

    #[test]
    fn test_convert_cidr_block_to_string() {
        let block: CidrBlock = [1, 2, 3, 4, 5].into();
        let converted = block.to_string();
        assert_eq!(converted, "1.2.3.4/5");
    }

    #[test]
    fn parse_x25519_public_key() {
        let input = "JD2nn6aqCENiAasdKU9cxXaeUySAUfxNbdsoFNj3JYgr";
        let mut expected = [0xABu8; 32];
        expected[0] = 0xFFu8;

        assert_eq!(
            input.parse::<X25519PublicKey>(),
            Ok(X25519PublicKey::from(expected))
        );
    }

    #[test]
    fn display_x25519_public_key() {
        let mut key = [0x0A; 32];
        key[0] = 0xFFu8;
        let expected = "JAZqpMRmsqGBUNxmzcWaFnKdqyXriPpKebE7uaTp4SCZ";

        assert_eq!(X25519PublicKey::from(key).to_string(), expected);
    }

    #[test]
    fn x25519_serde_roundtrip() {
        let key_bytes = [0x0A; 32];
        let key = X25519PublicKey::from(key_bytes);
        let encoded = serde_json::to_string(&key).unwrap();
        let decoded: X25519PublicKey = serde_json::from_str(encoded.as_str()).unwrap();
        assert_eq!(key, decoded);
    }
}
