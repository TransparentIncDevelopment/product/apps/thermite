<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Building docker images for Xand Components](#building-docker-images-for-xand-components)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Building docker images for Xand Components

Each directory here in `/deployment/docker/` maps to a docker image that we ship for customer use. 

All the components are built together using the `Dockerfile-rustbuilder` image, into an image called `rust-built-binaries`.
Then using multistage builds, we copy the the binary of choice from `rust-built-binaries` into a component-specific docker image.

So, each component's Dockerfile follows the pattern
```Dockerfile
# Stage 1
FROM rust-built-binaries as bins

# Stage 2
FROM rust-runtime
COPY --from=bins <PATH/TO/BINARY> .
```

The `.ci/.env` file has the versioning for our docker components. You can see in the `./gitlab-ci.yml`
file that the .env file is passed into the `--env-file` arg for `cargo make`. 

If building the docker images locally, you can pass the arg in as well with:
```shell
cargo make --env-file=.ci/.env docker-build-publish-flow
```  
