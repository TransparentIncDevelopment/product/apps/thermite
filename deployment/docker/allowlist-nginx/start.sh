#!/bin/bash

envsubst '${PROXY_TO_LOCATION}' < /nginx_proxy.conf.template > /etc/nginx/conf.d/default.conf
nginx -t
nginx

while true
do
  nginx -t
  nginx -s reload
  inotifywait --exclude .swp -e create -e modify -e delete -e move /etc/nginx/allowlist/
  echo "Reloading Nginx Configuration"
done
