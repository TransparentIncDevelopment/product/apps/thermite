use crate::errors::FinancialClientError;
use num_traits::{
    bounds::Bounded, identities::Zero, ops::checked::CheckedAdd, ops::checked::CheckedSub,
    sign::Unsigned,
};
use rand::prelude::SliceRandom;
use rand::thread_rng;
use std::fmt::Debug;
use xand_ledger::{ClearTransactionOutput, IUtxo, OpenedTransactionOutput};

pub const MAX_CLEAR_TXOS_PER_TRANSACTION: usize = 100;
pub const MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION: usize = 100;

pub trait ClaimValue<T>
where
    T: Zero + CheckedAdd,
{
    /// Monetary value of the [`ClaimValue`].
    fn amount(&self) -> T;

    /// Sum of the monetary values of the [`ClaimValue`]s.
    ///
    /// Returns [`None`] in case the sum would overflow.
    fn total_amount<I>(iter: I) -> Option<T>
    where
        I: Iterator<Item = Self>,
        Self: Sized,
    {
        let mut total = T::zero();
        for item in iter {
            total = total.checked_add(&item.amount())?;
        }
        Some(total)
    }
}

impl ClaimValue<u64> for &OpenedTransactionOutput {
    fn amount(&self) -> u64 {
        self.value
    }
}

impl ClaimValue<u64> for &ClearTransactionOutput {
    fn amount(&self) -> u64 {
        self.value().value()
    }
}

/// [Random-Improve algorithm](https://iohk.io/en/blog/posts/2018/07/03/self-organisation-in-coin-selection/)
/// to select a set of spendable TxOs while minimizing dust accretion
///
/// # Errors
///
/// Fails with [`FinancialClientError::InsufficientClaims`] when the randomly selected TxOs do not sum
/// to at least the requested amount.
///
/// As the number of selected TxOs approaches [`MAX_TXOS_PER_TRANSACTION`] there is a greater likelihood
/// of failing with [`FinancialClientError::NondeterministicTxoSelectionFailed`]. Callers may wish to retry
/// or use a smaller amount.
///
/// Under certain conditions random-improve can revert to a deterministic largest-first selection strategy.
/// As the payment/redeem amount approaches [`T::max_value()`] there's a greater chance of failing
/// the [`FinancialClientError::NondeterministicTxoSelectionFailed`] error as well. In practice, when using
/// [`u64`] to represent USD, including 2 decimals for minor units, this is highly unlikely.
///
/// In rare cases when a lot of dust has accumulated (precisely what this algorithm tries to prevent), a
/// [`FinancialClientError::NondeterministicTxoSelectionFailed`] error may occur regardless of number of
/// retries. The only fix in such cases is to spend a smaller amount.
pub fn random_improve<'a, T, Txo>(
    amount: T,
    mut spendable_txos: Vec<&'a Txo>,
    maximum_txos_per_transaction: usize,
) -> Result<Vec<&'a Txo>, FinancialClientError>
where
    T: Ord + CheckedAdd + CheckedSub + Unsigned + Bounded + Zero + std::marker::Copy + Debug,
    &'a Txo: ClaimValue<T> + Clone + Debug,
{
    // Randomize all spendable txos
    spendable_txos.shuffle(&mut thread_rng());

    // Build up set of selected txos
    let mut running_total = T::zero();
    let mut selected_txos: Vec<&'a Txo> = vec![];
    for txo in spendable_txos.iter() {
        let txo_meets_improvement_criteria =
            should_select_next_txo(running_total, amount, txo.amount());
        match txo_meets_improvement_criteria {
            true => {
                // If adding txo would exceed set's `maximum_txos_per_transaction`, fall back to largest-first selection
                if selected_txos.len() + 1 > maximum_txos_per_transaction {
                    return select_largest_first_txo(
                        amount,
                        spendable_txos,
                        maximum_txos_per_transaction,
                    );
                }
                // If adding txo value to total value would overflow, abort with error
                match running_total.checked_add(&txo.amount()) {
                    Some(value) => running_total = value,
                    None => return Err(FinancialClientError::NondeterministicTxoSelectionFailed),
                }
                // Add txo to set
                selected_txos.push(txo);
            }
            false => break, // don't add txo to set and exit loop
        }
    }

    // if the resulting set does not fulfill request, abort
    if running_total < amount {
        return Err(FinancialClientError::InsufficientClaims);
    }

    Ok(selected_txos)
}

/// Naive TxO input selection strategy which incrementally selects the largest of
/// the available `spendable_txos` until the running total is at least the requested
/// `amount`.
fn select_largest_first_txo<'a, T, Txo>(
    amount: T,
    spendable_txos: Vec<&'a Txo>,
    maximum_txos_per_transaction: usize,
) -> Result<Vec<&'a Txo>, FinancialClientError>
where
    T: Ord + CheckedAdd + CheckedSub + Bounded + Zero,
    &'a Txo: ClaimValue<T>,
{
    // sort `spendable_txos` in descending order
    let mut spendable_txos = spendable_txos;
    spendable_txos.sort_by_key(ClaimValue::amount);
    spendable_txos.reverse();

    // find shortest slice at the front of `spendable_txos` that satisifies requested `amount`
    let mut running_total = T::zero();
    for (i, txo) in spendable_txos.iter().enumerate() {
        if i >= maximum_txos_per_transaction {
            return Err(FinancialClientError::NondeterministicTxoSelectionFailed);
        }
        running_total = running_total
            .checked_add(&ClaimValue::amount(txo))
            .ok_or(FinancialClientError::NondeterministicTxoSelectionFailed)?;
        if running_total >= amount {
            return Ok(spendable_txos[..=i].to_vec());
        }
    }

    Err(FinancialClientError::InsufficientClaims)
}

fn should_select_next_txo<T>(running_total: T, payment_amount: T, candidate_txo_value: T) -> bool
where
    T: Ord
        + Copy
        + num_traits::CheckedSub
        + num_traits::CheckedAdd
        + std::fmt::Debug
        + num_traits::sign::Unsigned
        + Bounded,
{
    if running_total < payment_amount {
        return true;
    }

    // Instead of bubbling up overflow scenarios to caller, use the maximum numeric value
    // as the target amount. This shrinks the range of valid total amounts returned, but
    // otherwise should not affect the caller.
    let target_amount = payment_amount
        .checked_add(&payment_amount)
        .unwrap_or_else(T::max_value);

    let old_delta = abs_sub(target_amount, running_total);
    let new_delta = running_total
        .checked_add(&candidate_txo_value)
        .map(|new_total| abs_sub(target_amount, new_total));
    match new_delta {
        Some(delta) => delta < old_delta,
        None => false, // If overflow, we don't want to add the next txo
    }
}

/// Absolute value difference between two unsigned numbers.
fn abs_sub<T: CheckedSub + Unsigned + Bounded>(lhs: T, rhs: T) -> T {
    match lhs.checked_sub(&rhs) {
        Some(val) => val,
        None => match rhs.checked_sub(&lhs) {
            Some(val) => val,
            None => unreachable!("unsigned subtraction cannot underflow after flipping operands"),
        },
    }
}

#[cfg(test)]
pub mod test {
    use super::{
        abs_sub, random_improve, select_largest_first_txo, should_select_next_txo, ClaimValue,
    };
    use crate::errors::FinancialClientError;
    use crate::txo_selection::MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION;
    use proptest::{collection::vec, prelude::*};
    use std::collections::HashSet;

    impl ClaimValue<u64> for &u64 {
        fn amount(&self) -> u64 {
            **self
        }
    }

    impl ClaimValue<u8> for &u8 {
        fn amount(&self) -> u8 {
            **self
        }
    }

    #[test]
    fn random_improve_succeeds_if_enough_txo_value_exists() {
        // Given
        let payment_amount = 1000u64;
        let spendable_txo_amounts = vec![&100u64, &200u64, &500u64, &750u64];
        debug_assert!(
            ClaimValue::total_amount(spendable_txo_amounts.iter().cloned()).unwrap()
                >= payment_amount,
            "insufficient spendable txo amounts available"
        );

        // When
        let result = random_improve(
            payment_amount,
            spendable_txo_amounts,
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        );

        // Then
        assert!(result.is_ok())
    }

    prop_compose! {
        fn arbitrary_random_improve_inputs()(
            spendable_txo_amounts in vec(any::<u8>(), 0..=MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION),
        )(
            payment_amount in (0u8..=ClaimValue::total_amount(spendable_txo_amounts.iter()).unwrap_or(u8::MAX)),
            spendable_txo_amounts in Just(spendable_txo_amounts)
        ) -> (u8, Vec<u8>) {
            (payment_amount, spendable_txo_amounts)
        }
    }

    proptest! {
        #[test]
        fn random_improve_succeeds_or_nondeterminstically_fails(
            (payment_amount, spendable_txo_amounts) in arbitrary_random_improve_inputs()
        ) {
            // Given
            let spendable_txo_amounts = spendable_txo_amounts.iter().collect();

            // When
            let result = random_improve(payment_amount, spendable_txo_amounts, MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION);

            // Then
            assert!(matches!(result, Ok(_) | Err(FinancialClientError::NondeterministicTxoSelectionFailed)));
        }
    }

    #[test]
    fn random_improve_succeed_implies_total_amount_of_returned_set_is_at_least_the_requested_amount(
    ) {
        // Given
        let payment_amount = 1000u64;
        let spendable_txo_amounts = vec![&100u64, &200u64, &500u64, &30_000u64];
        debug_assert!(
            ClaimValue::total_amount(spendable_txo_amounts.iter().cloned()).unwrap()
                >= payment_amount,
            "insufficient spendable txo amounts available"
        );

        // When
        let selected_txos = random_improve(
            payment_amount,
            spendable_txo_amounts,
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap();

        // Then
        assert!(ClaimValue::total_amount(selected_txos.into_iter()).unwrap() >= payment_amount);
    }

    #[test]
    fn random_improve_fails_if_not_enough_txo_value_exists() {
        let payment_amount = 1000u64;
        let spendable_txo_amounts = vec![&100u64, &200u64, &300u64];
        debug_assert!(
            ClaimValue::total_amount(spendable_txo_amounts.iter().cloned()).unwrap()
                < payment_amount,
            "insufficient payment amount"
        );
        // When
        let result = random_improve(
            payment_amount,
            spendable_txo_amounts,
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        );

        // Then
        assert!(matches!(
            result,
            Err(FinancialClientError::InsufficientClaims)
        ))
    }

    #[test]
    fn random_improve_returns_valid_subset_when_total_txo_amounts_would_overflow() {
        // Given
        let payment_amount = 255u8;
        let spendable_txo_amounts = vec![&255u8, &255u8, &255u8];
        debug_assert!(
            ClaimValue::total_amount(spendable_txo_amounts.iter().cloned()).is_none(),
            "does not overflow"
        );
        // When
        let selected_txos = random_improve(
            payment_amount,
            spendable_txo_amounts,
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap();
        let sut = ClaimValue::total_amount(selected_txos.iter().cloned());

        // Then
        assert!(sut.is_some()); // must not overflow
    }

    #[test]
    fn random_improve_successfully_selects_all_txos_when_total_available_txo_value_less_than_equal_to_target_amount(
    ) {
        // Given
        let payment_amount = 1000u64;
        let target_amount = payment_amount + payment_amount;
        let spendable_txo_amounts = vec![&100u64, &200u64, &1000u64];
        debug_assert!(
            ClaimValue::total_amount(spendable_txo_amounts.iter().cloned()).unwrap()
                <= target_amount,
            "too much spendable txo value"
        );
        debug_assert!(
            ClaimValue::total_amount(spendable_txo_amounts.iter().cloned()).unwrap()
                >= payment_amount,
            "not enough spendable txo value"
        );

        // When
        let selected_txos = random_improve(
            payment_amount,
            spendable_txo_amounts.clone(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap();

        // Then
        assert_eq!(
            selected_txos.iter().collect::<HashSet<_>>(),
            spendable_txo_amounts.iter().collect()
        );
    }

    #[test]
    fn random_improve_falls_back_to_largest_first_if_max_txos_exceeded() {
        // Given
        let payment_amount = MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION as u64 * 10u64;

        let dust = [1u64; MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION];
        debug_assert!(ClaimValue::total_amount(dust.iter()).unwrap() < payment_amount);

        let big_txo = MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION as u64 * 10u64;
        debug_assert!(big_txo >= payment_amount);

        let spendable_txo_amounts: Vec<&u64> =
            dust.iter().chain(std::iter::once(&big_txo)).collect();
        debug_assert!(
            spendable_txo_amounts.len() > MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
            "too few txos in set"
        );

        // When
        let random_improve_selection = random_improve(
            payment_amount,
            spendable_txo_amounts.clone(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap();
        let largest_first_selection = select_largest_first_txo(
            payment_amount,
            spendable_txo_amounts.clone(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap();

        // Then
        assert_eq!(largest_first_selection, random_improve_selection);
    }

    #[test]
    fn largest_first_returns_the_txo_with_largest_value_when_it_covers_amount() {
        // Given
        let payment_amount = 1001u64;

        let dust = [1u64; MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION];
        debug_assert!(ClaimValue::total_amount(dust.iter()).unwrap() < payment_amount);

        let big_txo = 1001u64;
        debug_assert!(big_txo >= payment_amount);

        let spendable_txo_amounts: Vec<&u64> =
            dust.iter().chain(std::iter::once(&big_txo)).collect();
        debug_assert!(
            spendable_txo_amounts.len() > MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
            "too few txos in set"
        );

        // When
        let selected_txos = select_largest_first_txo(
            payment_amount,
            spendable_txo_amounts.clone(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap();

        // Then
        assert!(selected_txos.len() <= MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION);
        assert!(ClaimValue::total_amount(selected_txos.iter().cloned()).unwrap() >= payment_amount);
        assert_eq!(selected_txos, vec![&1001u64]);
    }

    #[test]
    fn random_improve_falls_back_to_largest_first_if_max_txos_exceeded_and_then_fails_when_overwhelmed_by_dust(
    ) {
        // Given
        const PAYMENT_AMOUNT: usize = MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION + 1;
        let dust = [1u64; PAYMENT_AMOUNT];

        let spendable_txo_amounts: Vec<&u64> = dust.iter().collect();
        debug_assert!(
            spendable_txo_amounts.len() > MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
            "too few txos in set"
        );

        // When
        let random_improve_selection = random_improve(
            PAYMENT_AMOUNT as u64,
            spendable_txo_amounts.clone(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap_err();
        let largest_first_selection = select_largest_first_txo(
            PAYMENT_AMOUNT as u64,
            spendable_txo_amounts.clone(),
            MAX_CONFIDENTIAL_TXOS_PER_TRANSACTION,
        )
        .unwrap_err();

        // Then
        assert!(matches!(
            random_improve_selection,
            FinancialClientError::NondeterministicTxoSelectionFailed
        ));
        assert!(matches!(
            largest_first_selection,
            FinancialClientError::NondeterministicTxoSelectionFailed
        ));
    }

    // Conditions for `should_select_next_txo()`
    // A. running_total < payment_amount -> "needs more value"
    // B. new delta < old delta -> "improved delta"
    // Delta is the difference between the running total and the target amount (payment amount * 2)
    // Truth Table
    // Condition is A || B
    // |       | A    | not A |
    // |-------|------|-------|
    // | B     | true | true  |
    // | not B | true | false |

    #[test] // A and B
    fn should_select_next_txo_returns_true_when_running_total_less_than_payment_amount_and_new_delta_less_than_old_delta(
    ) {
        // Given
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 498i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 100i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(running_total < payment_amount, "running total too big");
        debug_assert!(new_delta < old_delta, "new delta too big");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(result);
    }

    #[test] // not A and B
    fn should_select_next_txo_returns_true_when_running_total_greater_than_payment_amount_and_new_delta_less_than_old_delta(
    ) {
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 502i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 100i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(running_total > payment_amount, "running total too small");
        debug_assert!(new_delta < old_delta, "new delta too big");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(result);
    }

    #[test] // not A and B - boundary condition
    fn should_select_next_txo_returns_true_when_running_total_equals_payment_amount_and_new_delta_less_than_old_delta(
    ) {
        // Given
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 500i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 100i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(
            running_total == payment_amount,
            "running total and payment amount must equal"
        );
        debug_assert!(new_delta < old_delta, "new delta too big");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(result);
    }

    #[test] // A and not B
    fn should_select_next_txo_returns_true_when_running_total_less_than_payment_amount_and_new_delta_greater_than_old_delta(
    ) {
        // Given
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 498i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 1500i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(running_total < payment_amount, "running total too big");
        debug_assert!(new_delta > old_delta, "new delta too small");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(result);
    }

    #[test] // A and not B - boundary condition
    fn should_select_next_txo_returns_true_when_running_total_less_than_payment_amount_and_new_delta_equal_to_old_delta(
    ) {
        // Given
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 450i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 1100i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(running_total < payment_amount, "running total too big");
        debug_assert!(new_delta == old_delta, "new delta and old delta must equal");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(result);
    }

    #[test] // not A and not B
    fn should_select_next_txo_returns_false_when_running_total_greater_than_payment_amount_and_new_delta_greater_than_old_delta(
    ) {
        // Given
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 550i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 1500i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(running_total > payment_amount, "running total too small");
        debug_assert!(new_delta > old_delta, "new delta too small");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(!result);
    }

    #[test] // not A and not B - boundary condition
    fn should_select_next_txo_returns_false_when_running_total_equal_to_payment_amount_and_new_delta_equal_to_old_delta(
    ) {
        // Given
        let payment_amount = 500i64;
        let target_amount = payment_amount * 2i64;
        let running_total = 500i64;
        let old_delta = (target_amount - running_total).unsigned_abs();
        let next_txo = 1000i64;
        let potential_result = running_total + next_txo;
        let new_delta = (target_amount - potential_result).unsigned_abs();
        debug_assert!(
            running_total == payment_amount,
            "running total must equal payment amount"
        );
        debug_assert!(new_delta == old_delta, "new delta must equal old delta");
        // When
        let result =
            should_select_next_txo(running_total as u64, payment_amount as u64, next_txo as u64);
        // Then
        assert!(!result);
    }

    proptest! {
        #[test]
        fn abs_sub_does_not_panic(x: u8, y: u8) {
            let _ = abs_sub(x, y);
        }
    }

    #[test]
    fn abs_sub_does_not_panic_when_subtracting_max_from_min() {
        let result = abs_sub(usize::MIN, usize::MAX);
        assert_eq!(result, usize::MAX);
    }
}
