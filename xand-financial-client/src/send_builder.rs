use async_trait::async_trait;

use crate::{
    errors::FinancialClientError, EncryptionProvider, IdentityTagSelector, KeyStore, MemberContext,
    NetworkKeyResolver, TxoRepository,
};
use futures::lock::Mutex;
use rand::{CryptoRng, Rng};
use xand_ledger::{
    Encrypted, EncryptionKey, IdentityTag, OpenedTransactionOutput, PrivateInputs, PrivateKey,
    PublicInputSet, PublicKey, SendClaimsTransaction, SpendingOutput, TransactionBuilderErrors,
};

#[async_trait]
pub trait SendBuilder {
    async fn build_send_tx(
        &self,
        params: SendTxParams,
    ) -> Result<SendClaimsTransaction, FinancialClientError>;
}

#[derive(Clone)]
pub struct SendTxParams {
    pub issuer: PublicKey,
    pub amount: u64,
    pub recipient: PublicKey,
}

pub trait SendBuilderDeps {
    type KeyStore: KeyStore;
    type TxoRepo: TxoRepository + Send + Sync;
    type Rng: Rng + CryptoRng + Send;
    type NetworkKeyResolver: NetworkKeyResolver + Send + Sync;
    type EncryptionProvider: EncryptionProvider + Send + Sync;
    type IdentityTagSelector: IdentityTagSelector + Send + Sync;
    type MemberContext: MemberContext + Send + Sync;
}

pub struct SendBuilderImpl<D: SendBuilderDeps> {
    pub id_tag_selector: D::IdentityTagSelector,
    pub key_store: D::KeyStore,
    pub txo_repo: D::TxoRepo,
    pub encryption_provider: D::EncryptionProvider,
    pub encryption_key_resolver: D::NetworkKeyResolver,
    pub member_context: D::MemberContext,
    pub rng: Mutex<D::Rng>,
}

#[async_trait]
impl<D: SendBuilderDeps> SendBuilder for SendBuilderImpl<D> {
    async fn build_send_tx(
        &self,
        params: SendTxParams,
    ) -> Result<SendClaimsTransaction, FinancialClientError> {
        let private_inputs = self
            .private_inputs(params.issuer, params.amount, params.recipient)
            .await?;
        let masking_inputs = self
            .masking_inputs(&private_inputs, &private_inputs.identity_tag())
            .await?;
        let private_key = self.private_key(params.issuer).await?;
        let banned_members = self.member_context.get_banned_members().await?;
        let mut rng = self.rng.lock().await;
        let sender_encryption_key = self.get_encryption_key(params.issuer).await?;
        xand_ledger::construct_send_claims_transaction(
            private_inputs,
            masking_inputs,
            private_key,
            banned_members,
            &mut *rng,
            // TODO: Look into refactoring this https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6641
            |opened_txo, receiver_key| {
                self.generate_metadata(opened_txo, receiver_key, sender_encryption_key)
            },
        )
        .map_err(|e| FinancialClientError::BuildCreateTransactionFailure { error: e })
    }
}

impl<D: SendBuilderDeps> SendBuilderImpl<D> {
    pub fn new(
        id_tag_selector: D::IdentityTagSelector,
        key_store: D::KeyStore,
        txo_repo: D::TxoRepo,
        encryption_provider: D::EncryptionProvider,
        encryption_key_resolver: D::NetworkKeyResolver,
        member_context: D::MemberContext,
        rng: Mutex<D::Rng>,
    ) -> Self {
        SendBuilderImpl {
            id_tag_selector,
            key_store,
            txo_repo,
            encryption_provider,
            encryption_key_resolver,
            member_context,
            rng,
        }
    }

    pub async fn private_inputs(
        &self,
        issuer: PublicKey,
        amount: u64,
        recipient: PublicKey,
    ) -> Result<PrivateInputs, FinancialClientError> {
        let spends = self.txo_repo.get_spendable_txos(issuer, amount).await?;
        let outputs = self
            .calculate_outputs_with_change(&spends, issuer, amount, recipient)
            .await?;
        let identity_input = self.id_tag_selector.get_owned_id_tag(&issuer).await?;
        Ok(PrivateInputs::new(spends, outputs, identity_input))
    }

    pub async fn calculate_outputs_with_change(
        &self,
        spends: &[OpenedTransactionOutput],
        issuer: PublicKey,
        amount: u64,
        recipient: PublicKey,
    ) -> Result<Vec<SpendingOutput>, FinancialClientError> {
        let issuer_encryption_key = self.get_encryption_key(issuer).await?;
        let recipient_encryption_key = self.get_encryption_key(recipient).await?;

        let total_value = spends.iter().fold(0, |acc, txo| acc + txo.value);
        let change = total_value - amount;
        Ok(vec![
            SpendingOutput::new(amount, recipient, recipient_encryption_key),
            SpendingOutput::new(change, issuer, issuer_encryption_key),
        ])
    }

    pub async fn masking_inputs(
        &self,
        private_inputs: &PrivateInputs,
        true_tag: &IdentityTag,
    ) -> Result<Vec<PublicInputSet>, FinancialClientError> {
        crate::transactions::masking_inputs(
            &self.id_tag_selector,
            &self.txo_repo,
            private_inputs.spends().len(),
            true_tag,
        )
        .await
    }

    async fn private_key(&self, issuer: PublicKey) -> Result<PrivateKey, FinancialClientError> {
        self.key_store
            .get(&issuer)
            .await?
            .ok_or(FinancialClientError::CannotSignForIssuer { issuer })
    }

    async fn get_encryption_key(
        &self,
        public_key: PublicKey,
    ) -> Result<EncryptionKey, FinancialClientError> {
        self.encryption_key_resolver
            .get_encryption_key(&public_key)
            .await
    }

    pub fn generate_metadata(
        &self,
        opened_txo: &OpenedTransactionOutput,
        receiver_encryption_key: &EncryptionKey,
        sender_encryption_key: EncryptionKey,
    ) -> Result<Encrypted, TransactionBuilderErrors> {
        crate::transactions::generate_metadata(
            &self.encryption_provider,
            opened_txo,
            receiver_encryption_key,
            sender_encryption_key,
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        errors::FinancialClientError,
        models::{EncryptionKey, SecretCommitmentInputs},
        send_builder::{SendBuilder, SendBuilderDeps, SendBuilderImpl, SendTxParams},
        test_helpers::{
            managers::{FakeKeyStore, FakeTxoRepo, TestEncryptionProvider, TestNetworkKeyResolver},
            private_key_to_address, FakeIdentityTagSelector, FakeMemberContext,
        },
        EncryptionProvider,
    };
    use rand::{rngs::OsRng, Rng};
    use std::convert::TryInto;
    use xand_ledger::{
        check_txo_ownership, generate_fake_input_txos, generate_spendable_utxos, IdentityTag,
        OpenedTransactionOutput, PrivateKey, PublicKey, SendClaimsTransaction, SpendingOutput,
        TransactionOutput,
    };
    use xand_public_key::address_to_public_key;

    pub const TRUST_ADDRESS: &str = "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7";

    struct TestDeps;

    impl SendBuilderDeps for TestDeps {
        type KeyStore = FakeKeyStore;
        type TxoRepo = FakeTxoRepo;
        type Rng = OsRng;
        type NetworkKeyResolver = TestNetworkKeyResolver;
        type EncryptionProvider = TestEncryptionProvider;
        type IdentityTagSelector = FakeIdentityTagSelector;
        type MemberContext = FakeMemberContext;
    }

    struct TestContext {
        pub issuer: PrivateKey,
        pub recipient: PrivateKey,
        pub issuer_encryption_key: EncryptionKey,
        pub recipient_encryption_key: EncryptionKey,
        pub spendable_txos: Vec<u64>,
        pub decoy_sets: usize,
        pub send_amount: u64,
    }

    impl Default for TestContext {
        fn default() -> Self {
            let mut csprng = OsRng::default();
            Self {
                issuer: PrivateKey::random(&mut csprng),
                recipient: PrivateKey::random(&mut csprng),
                issuer_encryption_key: EncryptionKey::random(&mut csprng),
                recipient_encryption_key: EncryptionKey::random(&mut csprng),
                spendable_txos: Vec::new(),
                decoy_sets: 4,
                send_amount: 0,
            }
        }
    }

    impl TestContext {
        fn set_issuer(self, issuer: PrivateKey) -> Self {
            TestContext { issuer, ..self }
        }

        fn set_recipient(self, recipient: PrivateKey) -> Self {
            TestContext { recipient, ..self }
        }

        fn set_issuer_encryption_key(self, issuer_encryption_key: EncryptionKey) -> Self {
            Self {
                issuer_encryption_key,
                ..self
            }
        }

        fn set_recipient_encryption_key(self, recipient_encryption_key: EncryptionKey) -> Self {
            Self {
                recipient_encryption_key,
                ..self
            }
        }

        fn set_spendable_txos(self, spendable_txos: Vec<u64>) -> Self {
            TestContext {
                spendable_txos,
                ..self
            }
        }

        fn set_decoy_txos(self, decoy_sets: usize) -> Self {
            TestContext { decoy_sets, ..self }
        }

        fn set_send_amount(self, send_amount: u64) -> Self {
            TestContext {
                send_amount,
                ..self
            }
        }

        fn send_builder(&self) -> TestSendBuilderContext {
            let mut rng = OsRng::default();
            let private_key = self.issuer;
            let issuer = private_key_to_address(&private_key);
            let owned_id_tag = IdentityTag::from_key_with_rand_base(private_key.into(), &mut rng);
            let other_private_key = self.recipient;
            let other_participant = private_key_to_address(&other_private_key);
            let decoy_id_tags = std::iter::repeat_with(|| {
                IdentityTag::from_key_with_rand_base(other_private_key.into(), &mut rng)
            })
            .take(4)
            .collect::<Vec<_>>();
            let id_tag_selector = FakeIdentityTagSelector {
                id_tags_from_address: vec![
                    (issuer.clone(), vec![owned_id_tag]),
                    (other_participant.clone(), decoy_id_tags),
                ]
                .into_iter()
                .collect(),
            };
            let key_store = FakeKeyStore::new(vec![private_key]);

            let txo_repo = FakeTxoRepo {
                spendable_txo: generate_spendable_utxos(
                    owned_id_tag,
                    self.spendable_txos.clone(),
                    &mut rng,
                ),
                decoys: generate_fake_input_txos(
                    self.decoy_sets,
                    self.spendable_txos.len(),
                    &mut rng,
                )
                .into_iter()
                .flat_map(|set| set.txos.components)
                .collect(),
            };

            let encryption_provider = TestEncryptionProvider::default();
            let encryption_key_resolver = TestNetworkKeyResolver {
                trust_encryption_key: EncryptionKey::from(rng.gen::<[u8; 32]>()),
                trust_key: address_to_public_key(&TRUST_ADDRESS.to_string().try_into().unwrap())
                    .unwrap(),
                other_keys: vec![
                    (issuer, self.issuer_encryption_key),
                    (other_participant, self.recipient_encryption_key),
                ]
                .into_iter()
                .collect(),
            };
            let member_context = FakeMemberContext::default();

            TestSendBuilderContext {
                builder: SendBuilderImpl::<TestDeps>::new(
                    id_tag_selector,
                    key_store,
                    txo_repo,
                    encryption_provider.clone(),
                    encryption_key_resolver,
                    member_context,
                    rng.into(),
                ),
                encryption_provider,
            }
        }

        async fn get_tx(&self) -> Result<SendClaimsTransaction, FinancialClientError> {
            let builder = self.send_builder();
            let input_params = SendTxParams {
                issuer: self.issuer.into(),
                amount: self.send_amount,
                recipient: self.recipient.into(),
            };

            let send = builder.builder.build_send_tx(input_params).await?;

            Ok(send)
        }
    }

    struct TestSendBuilderContext {
        builder: SendBuilderImpl<TestDeps>,
        encryption_provider: TestEncryptionProvider,
    }

    #[tokio::test]
    async fn happy_path_build() {
        TestContext::default().get_tx().await.unwrap();
    }

    #[tokio::test]
    async fn get_change_back() {
        let mut rng = OsRng::default();
        let issuer_private = PrivateKey::random(&mut rng);
        let issuer: PublicKey = issuer_private.into();
        let issuer_encryption_key = EncryptionKey::random(&mut rng);
        let recipient_private = PrivateKey::random(&mut rng);
        let recipient: PublicKey = recipient_private.into();
        let recipient_encryption_key = EncryptionKey::random(&mut rng);

        let txo_amounts = vec![10, 20, 33];
        let send_amount = 6;
        let inputs = dummy_opened_txos(&txo_amounts);

        let context = TestContext::default()
            .set_issuer(issuer_private)
            .set_recipient(recipient_private)
            .set_issuer_encryption_key(issuer_encryption_key)
            .set_recipient_encryption_key(recipient_encryption_key)
            .set_spendable_txos(txo_amounts.clone())
            .set_decoy_txos(4)
            .set_send_amount(send_amount);
        let test_tx = context.send_builder();
        let outputs = test_tx
            .builder
            .calculate_outputs_with_change(&inputs, issuer, send_amount, recipient)
            .await
            .unwrap();

        let total_input_value: u64 = txo_amounts.iter().sum();
        let expected_change = total_input_value - send_amount;

        assert_eq!(outputs.len(), 2);
        assert!(outputs.contains(&SpendingOutput::new(
            send_amount,
            recipient,
            recipient_encryption_key,
        )));
        assert!(outputs.contains(&SpendingOutput::new(
            expected_change,
            issuer,
            issuer_encryption_key,
        )));
    }

    fn dummy_opened_txos(amounts: &[u64]) -> Vec<OpenedTransactionOutput> {
        amounts.iter().copied().map(dummy_opened_txo).collect()
    }

    fn dummy_opened_txo(amount: u64) -> OpenedTransactionOutput {
        OpenedTransactionOutput {
            transaction_output: TransactionOutput::random(&mut OsRng::default()),
            blinding_factor: Default::default(),
            value: amount,
        }
    }

    #[tokio::test]
    async fn can_roundtrip_encrypted_txo_metadata() {
        let ctx = TestContext::default()
            .set_spendable_txos(vec![3u64])
            .set_send_amount(2);
        let tx = ctx.get_tx().await.unwrap();
        let builder = ctx.send_builder();

        for send_output in tx.core_transaction.output {
            let receiver_key = if check_txo_ownership(send_output.txo, ctx.issuer) {
                ctx.issuer_encryption_key
            } else {
                ctx.recipient_encryption_key
            };

            // assert these metadata items match the spending outputs
            let data: SecretCommitmentInputs = builder
                .encryption_provider
                .decrypt_as_receiver(send_output.encrypted_metadata, receiver_key)
                .unwrap();

            if receiver_key == ctx.recipient_encryption_key {
                assert_eq!(data.value, 2)
            } else {
                assert_eq!(data.value, 1)
            }
        }
    }
}
