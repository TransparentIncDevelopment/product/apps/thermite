use derive_more::Constructor;
use serde::{Deserialize, Serialize};

use xand_ledger::curve25519_dalek::scalar::Scalar;
use xand_ledger::{
    CashConfirmationTransaction, ClearRedeemRequestTransaction, ClearTransactionOutput,
    CorrelationId, CreateCancellationTransaction, IUtxo, IdentityTag, OpenedTransactionOutput,
    PublicKey, RedeemCancellationTransaction, RedeemFulfillmentTransaction, RewardTransaction,
    TransactionOutput,
};
pub use xand_ledger::{Encrypted, EncryptionKey};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SecretCommitmentInputs {
    pub blinding_factor: Scalar,
    pub value: u64,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize, Default)]
pub struct EncryptionNonce(Vec<u8>);

impl EncryptionNonce {
    pub fn as_slice(&self) -> &[u8] {
        self.0.as_slice()
    }
}

impl From<&[u8]> for EncryptionNonce {
    fn from(bytes: &[u8]) -> Self {
        EncryptionNonce(bytes.to_vec())
    }
}

impl<const N: usize> From<[u8; N]> for EncryptionNonce {
    fn from(bytes: [u8; N]) -> Self {
        EncryptionNonce(bytes.to_vec())
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum ConfidentialPayload<T> {
    Decrypted(T),
    Encrypted(Encrypted),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PendingCreate {
    /// Amount of the pending create, in USD cents
    pub amount_in_minor_unit: u64,
    /// A correlation_id uniquely identifying this pending create
    pub correlation_id: CorrelationId,
    /// The bank account details for this request which may be encrypted.
    pub account: ConfidentialPayload<BankAccount>,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
#[allow(clippy::large_enum_variant)]
pub enum PendingRedeem {
    Confidential(PendingConfidentialRedeem),
    Clear(PendingClearRedeem),
}

impl PendingRedeem {
    pub fn correlation_id(&self) -> CorrelationId {
        match self {
            PendingRedeem::Confidential(r) => r.correlation_id,
            PendingRedeem::Clear(r) => r.correlation_id,
        }
    }
    pub fn account(&self) -> ConfidentialPayload<BankAccount> {
        match self {
            PendingRedeem::Confidential(r) => r.account.clone(),
            PendingRedeem::Clear(r) => r.account.clone(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PendingConfidentialRedeem {
    /// Amount of the pending create, in USD cents
    pub amount_in_minor_unit: u64,
    /// A correlation_id uniquely identifying this pending create
    pub correlation_id: CorrelationId,
    /// The bank account details for this request which may be encrypted.
    pub account: ConfidentialPayload<BankAccount>,
    /// Redeem Output
    pub redeem_output: TransactionOutput,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PendingClearRedeem {
    /// Amount of the pending create, in USD cents
    pub amount_in_minor_unit: u64,
    /// A correlation_id uniquely identifying this pending redeem
    pub correlation_id: CorrelationId,
    /// The bank account details for this request which may be encrypted.
    pub account: ConfidentialPayload<BankAccount>,
    /// Redeem Output
    pub redeem_output: ClearTransactionOutput,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct BankAccount {
    pub routing_number: String,
    pub account_number: String,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum TransactionOutputData {
    Private(OpenedTransactionOutput, PublicKey),
    Public(TransactionOutput),
}

impl TransactionOutputData {
    pub fn get_transaction_output(&self) -> TransactionOutput {
        match self {
            TransactionOutputData::Private(opened_txo, _) => opened_txo.transaction_output,
            TransactionOutputData::Public(txo) => *txo,
        }
    }
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct Transaction {
    pub signer: Option<PublicKey>,
    pub txn: FinancialTransaction,
    pub new_txos: Vec<TransactionOutputData>,
    pub new_id_tag: Option<IdentityTag>,
    pub spent_txos: Vec<TransactionOutput>,
    pub new_clear_txos: Vec<ClearTransactionOutput>,
    pub redeemed_clear_txos: Vec<ClearTransactionOutput>,
}

impl Transaction {
    pub fn get_correlation_id(&self) -> Option<CorrelationId> {
        match &self.txn {
            FinancialTransaction::CreateRequest(c) => Some(c.correlation_id),
            FinancialTransaction::Create(c) => Some(c.correlation_id),
            FinancialTransaction::CreateCancellation(c) => Some(c.correlation_id),
            FinancialTransaction::Send(_) => None,
            FinancialTransaction::Redeem(r) => Some(r.correlation_id()),
            FinancialTransaction::RedeemFulfillment(r) => Some(r.correlation_id),
            FinancialTransaction::RedeemCancellation(r) => Some(r.correlation_id),
            FinancialTransaction::RewardValidator(_) => None,
        }
    }
}

impl From<ClearRedeemRequestTransaction<ClearTransactionOutput>> for Transaction {
    fn from(clear_redeem: ClearRedeemRequestTransaction<ClearTransactionOutput>) -> Self {
        let redeem_output = clear_redeem.output().redeem_output;
        let signer = Some(redeem_output.public_key());
        let new_clear_txos = if let Some(change_output) = clear_redeem.output().change_output {
            vec![change_output, redeem_output]
        } else {
            vec![redeem_output]
        };
        let redeemed_clear_txos = clear_redeem.input();
        Self {
            signer,
            txn: FinancialTransaction::Redeem(PendingRedeem::Clear(clear_redeem.into())),
            new_txos: vec![],
            new_id_tag: None,
            spent_txos: vec![],
            new_clear_txos,
            redeemed_clear_txos,
        }
    }
}

impl From<ClearRedeemRequestTransaction<ClearTransactionOutput>> for PendingClearRedeem {
    fn from(clear_redeem: ClearRedeemRequestTransaction<ClearTransactionOutput>) -> Self {
        Self {
            amount_in_minor_unit: clear_redeem.output().redeem_output.value().value(),
            correlation_id: clear_redeem.correlation_id(),
            account: ConfidentialPayload::Encrypted(clear_redeem.extra_data().into()),
            redeem_output: clear_redeem.output().redeem_output,
        }
    }
}

impl From<RewardTransaction> for Transaction {
    fn from(reward: RewardTransaction) -> Self {
        let new_clear_txos = vec![reward.utxo()];
        Self {
            signer: None,
            txn: FinancialTransaction::RewardValidator(reward),
            new_txos: vec![],
            new_id_tag: None,
            spent_txos: vec![],
            new_clear_txos,
            redeemed_clear_txos: vec![],
        }
    }
}

#[derive(
    Clone,
    Debug,
    Eq,
    PartialEq,
    Serialize,
    Deserialize,
    strum_macros::Display,
    strum_macros::EnumDiscriminants,
)]
#[serde(tag = "serde_operation")]
#[strum_discriminants(name(TransactionType))]
#[strum_discriminants(derive(Deserialize, Serialize))]
#[allow(clippy::large_enum_variant)]
pub enum FinancialTransaction {
    #[strum(serialize = "create_request")]
    CreateRequest(PendingCreate),
    #[strum(serialize = "create")]
    Create(CashConfirmationTransaction),
    #[strum(serialize = "create_cancellation")]
    CreateCancellation(CreateCancellationTransaction),
    #[strum(serialize = "send")]
    Send(Option<SendModel>),
    #[strum(serialize = "redeem")]
    Redeem(PendingRedeem),
    #[strum(serialize = "redeem_fulfillment")]
    RedeemFulfillment(RedeemFulfillmentTransaction),
    #[strum(serialize = "redeem_cancellation")]
    RedeemCancellation(RedeemCancellationTransaction),
    #[strum(serialize = "reward_validator")]
    RewardValidator(RewardTransaction),
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct CreateRequestParams {
    /// Issuer
    pub issuer: PublicKey,
    /// Amount of the pending create, in USD cents
    pub amount_in_minor_unit: u64,
    /// An identifier uniquely identifying this pending create
    pub correlation_id: CorrelationId,
    /// The info about the bank account which will/did provide funds for this request
    pub account: BankAccount,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RedeemRequestParams {
    /// Issuer
    pub issuer: PublicKey,
    /// Amount of the redeem request, in USD cents
    pub amount_in_minor_unit: u64,
    /// An identifier uniquely identifying this redeem request
    pub correlation_id: CorrelationId,
    /// The info about the bank account which will receive funds for this request
    pub account: BankAccount,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize)]
pub struct SendModel {
    pub recipient: PublicKey,
    pub amount_in_minor_unit: u64,
}
