use crate::errors::FinancialClientError;
use crate::models::{
    BankAccount, ConfidentialPayload, Encrypted, EncryptionNonce, FinancialTransaction,
    PendingClearRedeem, PendingConfidentialRedeem, PendingRedeem, SecretCommitmentInputs,
    Transaction, TransactionOutputData,
};
use crate::{EncryptionProvider, KeyStore, NetworkKeyResolver};
use async_trait::async_trait;
use derive_more::Constructor;
use xand_ledger::{
    check_txo_ownership, find_secret_input_index, ConfidentialRedeemRequestRecord, CorrelationId,
    DecryptSigner, IUtxo, OpenedTransactionOutput, PrivateKey, PublicKey, RedeemRequestRecord,
    RedeemRequestTransaction, RedeemSigner, SendClaimsOutput, TransactionOutput,
};

#[async_trait]
pub trait RedeemManager {
    async fn reveal_redeem(
        &self,
        txn: RedeemRequestTransaction,
    ) -> Result<Transaction, FinancialClientError>;

    async fn reveal_redeem_record(
        &self,
        correlation_id: CorrelationId,
        record: RedeemRequestRecord,
    ) -> Result<(Option<PublicKey>, PendingRedeem), FinancialClientError>;
}

pub trait RedeemManagerDeps {
    type KeyStore: KeyStore + Send + Sync + Clone;
    type EncryptionKeyResolver: NetworkKeyResolver + Send + Sync + Clone;
    type EncryptionProvider: EncryptionProvider + Send + Sync + Clone;
}

#[derive(Debug, Constructor)]
pub struct RedeemManagerImpl<T: RedeemManagerDeps> {
    pub key_store: T::KeyStore,
    pub network_key_lookup: T::EncryptionKeyResolver,
    pub encryption_provider: T::EncryptionProvider,
}

impl<T: RedeemManagerDeps> Clone for RedeemManagerImpl<T> {
    fn clone(&self) -> Self {
        Self {
            key_store: self.key_store.clone(),
            network_key_lookup: self.network_key_lookup.clone(),
            encryption_provider: self.encryption_provider.clone(),
        }
    }
}

#[async_trait]
impl<T: RedeemManagerDeps> RedeemManager for RedeemManagerImpl<T> {
    async fn reveal_redeem(
        &self,
        txn: RedeemRequestTransaction,
    ) -> Result<Transaction, FinancialClientError> {
        let mut new_txos: Vec<TransactionOutputData> = vec![];
        let record = RedeemRequestRecord::Confidential(Box::new(
            ConfidentialRedeemRequestRecord::from(txn.clone()),
        ));
        if let (signer, PendingRedeem::Confidential(pending_redeem)) = self
            .reveal_redeem_record(txn.core_transaction.correlation_id, record)
            .await?
        {
            let mut spent_txos = vec![];
            // add redeem output info
            new_txos.push(signer.map_or(
                TransactionOutputData::Public(
                    txn.core_transaction.output.redeem_output.transaction_output,
                ),
                |key| {
                    TransactionOutputData::Private(
                        txn.core_transaction.output.redeem_output.clone(),
                        key,
                    )
                },
            ));

            let private_key = if let Some(pub_key) = signer.as_ref() {
                self.key_store.get(pub_key).await?
            } else {
                None
            };

            // add change output info
            if let (Some(pub_key), Some(private_key)) = (signer, private_key) {
                // use private key to detect which inputs were spent
                spent_txos = self.get_spent_txos(&txn, private_key)?;

                // decrypt redeem change output for later spending
                let opened_txo = self
                    .decrypt_change(txn.core_transaction.output.change_output, pub_key)
                    .await?;
                new_txos.push(TransactionOutputData::Private(opened_txo, pub_key));
            } else {
                new_txos.push(TransactionOutputData::Public(
                    txn.core_transaction.output.change_output.txo,
                ));
            }

            Ok(Transaction {
                signer,
                txn: FinancialTransaction::Redeem(PendingRedeem::Confidential(pending_redeem)),
                new_txos,
                new_id_tag: Some(txn.core_transaction.output_identity),
                spent_txos,
                new_clear_txos: Vec::new(),
                redeemed_clear_txos: Vec::new(),
            })
        } else {
            Err(FinancialClientError::NotFound)
        }
    }

    async fn reveal_redeem_record(
        &self,
        correlation_id: [u8; 16],
        record: RedeemRequestRecord,
    ) -> Result<(Option<PublicKey>, PendingRedeem), FinancialClientError> {
        match record {
            RedeemRequestRecord::Confidential(r) => {
                let keys = self.key_store.get_all().await?;
                let trust = self.network_key_lookup.get_trust_key().await?;
                let sender = keys.iter().find_map(|key| {
                    if key.0 == trust {
                        match &r.signer {
                            RedeemSigner::EncryptedSigner(encrypted_signer) => {
                                let pubkey = encrypted_signer.decrypt(0, key.1);
                                Some(pubkey)
                            }
                            RedeemSigner::PublicKey(pubkey) => Some(*pubkey),
                        }
                    } else if check_txo_ownership(r.redeem_output.transaction_output, key.1) {
                        Some(key.0)
                    } else {
                        None
                    }
                });
                let bank_data = match &sender {
                    Some(key) => ConfidentialPayload::Decrypted(
                        self.decrypt_extra_data(key, r.account_data.into(), &correlation_id)
                            .await?,
                    ),
                    None => ConfidentialPayload::Encrypted(r.account_data.into()),
                };

                Ok((
                    sender,
                    PendingRedeem::Confidential(PendingConfidentialRedeem {
                        amount_in_minor_unit: r.amount,
                        correlation_id,
                        account: bank_data,
                        redeem_output: r.redeem_output.transaction_output,
                    }),
                ))
            }
            RedeemRequestRecord::Clear(r) => {
                let sender = r.redeem_output.public_key();
                let bank_data = ConfidentialPayload::Decrypted(
                    self.decrypt_extra_data(&sender, r.account_data.into(), &correlation_id)
                        .await?,
                );

                Ok((
                    Some(sender),
                    PendingRedeem::Clear(PendingClearRedeem {
                        amount_in_minor_unit: r.amount.value(),
                        correlation_id,
                        account: bank_data,
                        redeem_output: r.redeem_output,
                    }),
                ))
            }
        }
    }
}

impl<T: RedeemManagerDeps> RedeemManagerImpl<T> {
    fn get_spent_txos(
        &self,
        txn: &RedeemRequestTransaction,
        private_key: PrivateKey,
    ) -> Result<Vec<TransactionOutput>, FinancialClientError> {
        let input_index = find_secret_input_index(
            &txn.core_transaction.input,
            &txn.core_transaction.key_images,
            private_key,
        )
        .ok_or(FinancialClientError::SenderUnableToFindSpentTxos)?;

        Ok(txn
            .core_transaction
            .input
            .iter()
            .skip(input_index)
            .map(|input| input.txos.components.clone())
            .next()
            .unwrap_or_default())
    }

    async fn decrypt_change(
        &self,
        change: SendClaimsOutput,
        public_key: PublicKey,
    ) -> Result<OpenedTransactionOutput, FinancialClientError> {
        let enc_key = self
            .network_key_lookup
            .get_encryption_key(&public_key)
            .await?;
        let SecretCommitmentInputs {
            value,
            blinding_factor,
        } = self
            .encryption_provider
            .decrypt_as_receiver::<SecretCommitmentInputs>(change.encrypted_metadata, enc_key)?;
        Ok(OpenedTransactionOutput {
            transaction_output: change.txo,
            blinding_factor,
            value,
        })
    }

    async fn decrypt_extra_data(
        &self,
        sender: &PublicKey,
        encrypted: Encrypted,
        correlation_id: &CorrelationId,
    ) -> Result<BankAccount, FinancialClientError> {
        // Get the trust encryption key
        let trust_encryption_key = self.network_key_lookup.get_trust_encryption_key().await?;

        // if trust encryption key is available, attempt receiver decryption
        if self
            .key_store
            .has_encryption_key(&trust_encryption_key)
            .await?
        {
            self.encryption_provider
                .decrypt_as_receiver(encrypted, trust_encryption_key)
        } else {
            // Perform sender decryption since the txos are owned
            let my_encryption_key = self.network_key_lookup.get_encryption_key(sender).await?;
            // Setup the nonce
            let nonce = EncryptionNonce::from(correlation_id.as_ref());
            // Perform sender decryption
            self.encryption_provider.decrypt_as_sender(
                &encrypted,
                &my_encryption_key,
                &trust_encryption_key,
                &nonce,
            )
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::models::PendingRedeem;
    use crate::{
        models::{ConfidentialPayload, FinancialTransaction, PendingConfidentialRedeem},
        test_helpers::{
            managers::{generate_test_member_redeem_manager, generate_test_trust_redeem_manager},
            redeems::{construct_redeem_record, construct_redeem_request},
        },
        RedeemManager,
    };
    use rand::prelude::StdRng;
    use rand::SeedableRng;
    use xand_ledger::{PrivateKey, RedeemRequestRecord};

    #[tokio::test]
    async fn attempting_to_reveal_owned_redeem_record_reveals_address_and_account() {
        let mut csprng = StdRng::seed_from_u64(42);
        let private_key = PrivateKey::random(&mut csprng);
        let redeem_manager = generate_test_member_redeem_manager(private_key, &Default::default());
        let record = construct_redeem_record(&mut csprng, private_key, &redeem_manager);
        let revealed_record = redeem_manager
            .reveal_redeem_record(
                record.0,
                RedeemRequestRecord::Confidential(Box::new(record.1)),
            )
            .await
            .expect("No failure expected");
        // Ensure that the signer is revealed to the sender
        let actual_signer = revealed_record.0;
        assert_eq!(actual_signer, Some(private_key.into()));
        assert!(matches!(
            revealed_record.1.account(),
            ConfidentialPayload::Decrypted(_)
        ));
    }

    #[tokio::test]
    async fn attempting_to_reveal_owned_transaction_reveals_identity_and_bank_data() {
        let mut csprng = StdRng::seed_from_u64(42);
        let private_key = PrivateKey::random(&mut csprng);
        let redeem_manager = generate_test_member_redeem_manager(private_key, &Default::default());
        let txn = construct_redeem_request(&mut csprng, private_key, &redeem_manager);
        let revealed_tx = redeem_manager
            .reveal_redeem(txn)
            .await
            .expect("No failure expected");
        // Ensure that the signer is revealed to the sender
        assert_eq!(revealed_tx.signer, Some(private_key.into()));
        assert!(matches!(
            revealed_tx.txn,
            FinancialTransaction::Redeem(PendingRedeem::Confidential(PendingConfidentialRedeem {
                account: ConfidentialPayload::Decrypted(_),
                ..
            }))
        ));
    }

    #[tokio::test]
    async fn attempting_to_reveal_unowned_redeem_record_does_not_reveal() {
        let mut csprng = StdRng::seed_from_u64(42);
        let private_key = PrivateKey::random(&mut csprng);
        let redeem_manager = generate_test_member_redeem_manager(private_key, &Default::default());

        let other_private_key = PrivateKey::random(&mut csprng);
        let other_redeem_manager =
            generate_test_member_redeem_manager(other_private_key, &Default::default());

        let record = construct_redeem_record(&mut csprng, private_key, &redeem_manager);
        let revealed_record = other_redeem_manager
            .reveal_redeem_record(
                record.0,
                RedeemRequestRecord::Confidential(Box::new(record.1)),
            )
            .await
            .expect("No failure expected");
        // Ensure that the signer is revealed to the sender
        let actual_signer = revealed_record.0;
        assert_eq!(actual_signer, None);
        assert!(matches!(
            revealed_record.1.account(),
            ConfidentialPayload::Encrypted(_)
        ));
    }

    #[tokio::test]
    async fn trust_can_reveal_redeem_record_bank_data() {
        let mut csprng = StdRng::seed_from_u64(42);
        let private_key = PrivateKey::random(&mut csprng);
        let member_redeem_manager =
            generate_test_member_redeem_manager(private_key, &Default::default());
        let record = construct_redeem_record(&mut csprng, private_key, &member_redeem_manager);

        let trust_redeeem_manager = generate_test_trust_redeem_manager(Default::default());
        let (_, pending_create) = trust_redeeem_manager
            .reveal_redeem_record(
                record.0,
                RedeemRequestRecord::Confidential(Box::new(record.1)),
            )
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        let actual_account = pending_create.account();
        assert!(matches!(actual_account, ConfidentialPayload::Decrypted(_)));
    }

    #[tokio::test]
    async fn trust_can_reveal_redeem_transaction_issuer_identity() {
        let mut csprng = StdRng::seed_from_u64(42);
        let member_private_key = PrivateKey::random(&mut csprng);
        let trust_private_key = PrivateKey::random(&mut csprng);
        let member_redeem_manager =
            generate_test_member_redeem_manager(member_private_key, &trust_private_key.into());
        let txn = construct_redeem_request(&mut csprng, member_private_key, &member_redeem_manager);

        let trust_redeem_manager = generate_test_trust_redeem_manager(trust_private_key);
        let revealed_tx = trust_redeem_manager
            .reveal_redeem(txn)
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        assert_eq!(revealed_tx.signer.unwrap(), member_private_key.into())
    }

    #[tokio::test]
    async fn trust_can_reveal_redeem_record_issuer_identity() {
        let mut csprng = StdRng::seed_from_u64(42);
        let member_private_key = PrivateKey::random(&mut csprng);
        let trust_private_key = PrivateKey::random(&mut csprng);
        let member_redeem_manager =
            generate_test_member_redeem_manager(member_private_key, &trust_private_key.into());
        let record =
            construct_redeem_record(&mut csprng, member_private_key, &member_redeem_manager);

        let trust_redeem_manager = generate_test_trust_redeem_manager(trust_private_key);
        let (maybe_issuer, _) = trust_redeem_manager
            .reveal_redeem_record(
                record.0,
                RedeemRequestRecord::Confidential(Box::new(record.1)),
            )
            .await
            .expect("No failure expected");
        // Ensure that the transaction is revealed to the trust
        let issuer = maybe_issuer.unwrap();
        assert_eq!(issuer, member_private_key.into())
    }
}
