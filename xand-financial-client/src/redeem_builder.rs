use crate::{
    EncryptionProvider, FinancialClientError, IdentityTagSelector, KeyStore, MemberContext,
    NetworkKeyResolver, RedeemRequestParams, TxoRepository,
};
use async_trait::async_trait;
use futures::lock::Mutex;
use rand::{CryptoRng, Rng};
use xand_ledger::{
    Encrypted, EncryptionKey, OpenedTransactionOutput, PrivateRedeemRequestInputs, PublicKey,
    RedeemRequestTransaction, TransactionBuilderErrors,
};

#[async_trait]
pub trait RedeemBuilder {
    async fn build_redeem_request(
        &self,
        params: RedeemRequestParams,
    ) -> Result<RedeemRequestTransaction, FinancialClientError>;
}

pub trait RedeemBuilderDeps {
    type TxoRepo: TxoRepository;
    type IdentityTagSelector: IdentityTagSelector;
    type KeyStore: KeyStore;
    type MemberContext: MemberContext;
    type EncryptionProvider: EncryptionProvider;
    type NetworkKeyResolver: NetworkKeyResolver;
    type Rng: Rng + CryptoRng + Send;
}

pub struct RedeemBuilderImpl<D: RedeemBuilderDeps> {
    pub txo_repo: D::TxoRepo,
    pub id_tag_selector: D::IdentityTagSelector,
    pub key_store: D::KeyStore,
    pub member_context: D::MemberContext,
    pub encryption_provider: D::EncryptionProvider,
    pub network_key_resolver: D::NetworkKeyResolver,
    pub rng: Mutex<D::Rng>,
}

impl<D: RedeemBuilderDeps> RedeemBuilderImpl<D> {
    async fn private_inputs(
        &self,
        issuer: PublicKey,
        amount: u64,
    ) -> Result<PrivateRedeemRequestInputs, FinancialClientError> {
        let spends = self.txo_repo.get_spendable_txos(issuer, amount).await?;
        let identity_input = self.id_tag_selector.get_owned_id_tag(&issuer).await?;
        let private_key = self
            .key_store
            .get(&issuer)
            .await?
            .ok_or(FinancialClientError::CannotSignForIssuer { issuer })?;
        Ok(PrivateRedeemRequestInputs {
            identity_input,
            spends,
            private_key,
        })
    }

    fn generate_metadata(
        &self,
        opened_txo: &OpenedTransactionOutput,
        receiver_encryption_key: &EncryptionKey,
        sender_encryption_key: EncryptionKey,
    ) -> Result<Encrypted, TransactionBuilderErrors> {
        crate::transactions::generate_metadata(
            &self.encryption_provider,
            opened_txo,
            receiver_encryption_key,
            sender_encryption_key,
        )
    }
}

#[async_trait]
impl<D: RedeemBuilderDeps> RedeemBuilder for RedeemBuilderImpl<D> {
    async fn build_redeem_request(
        &self,
        params: RedeemRequestParams,
    ) -> Result<RedeemRequestTransaction, FinancialClientError> {
        let private_inputs = self
            .private_inputs(params.issuer, params.amount_in_minor_unit)
            .await?;
        let masking_inputs = crate::transactions::masking_inputs(
            &self.id_tag_selector,
            &self.txo_repo,
            private_inputs.spends.len(),
            &private_inputs.identity_input,
        )
        .await?;
        let banned_members = self.member_context.get_banned_members().await?;
        let issuer_encryption_key = self
            .network_key_resolver
            .get_encryption_key(&params.issuer)
            .await?;
        let trust_encryption_key = self.network_key_resolver.get_trust_encryption_key().await?;
        let trust_pub_key = self.network_key_resolver.get_trust_key().await?;
        let extra = self.encryption_provider.encrypt(
            &params.account,
            &issuer_encryption_key,
            &trust_encryption_key,
            &params.correlation_id[..].into(),
        )?;
        let transaction = xand_ledger::construct_redeem_request_transaction(
            private_inputs,
            &issuer_encryption_key,
            params.amount_in_minor_unit,
            trust_pub_key,
            params.correlation_id,
            masking_inputs,
            extra.into(),
            &banned_members,
            &mut *self.rng.lock().await,
            |opened_txo, receiver_key| {
                self.generate_metadata(opened_txo, receiver_key, issuer_encryption_key)
            },
        )
        .map_err(|error| FinancialClientError::BuildRedeemTransactionFailure { error })?;
        Ok(transaction)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        models::{BankAccount, EncryptionKey},
        test_helpers::{
            managers::{FakeKeyStore, FakeTxoRepo, TestEncryptionProvider, TestNetworkKeyResolver},
            private_key_to_address, FakeIdentityTagSelector, FakeMemberContext,
        },
        EncryptionProvider, RedeemBuilder, RedeemBuilderDeps, RedeemBuilderImpl,
        RedeemRequestParams,
    };
    use rand::{rngs::OsRng, CryptoRng, Rng};
    use xand_ledger::{
        generate_fake_input_txos, generate_spendable_utxos, Encrypted, IdentityTag, PrivateKey,
    };

    struct TestDeps;

    impl RedeemBuilderDeps for TestDeps {
        type TxoRepo = FakeTxoRepo;
        type IdentityTagSelector = FakeIdentityTagSelector;
        type KeyStore = FakeKeyStore;
        type MemberContext = FakeMemberContext;
        type EncryptionProvider = TestEncryptionProvider;
        type NetworkKeyResolver = TestNetworkKeyResolver;
        type Rng = OsRng;
    }

    struct TestContext {
        builder: RedeemBuilderImpl<TestDeps>,
        issuer_priv_key: PrivateKey,
    }

    impl TestContext {
        fn new<R>(rng: &mut R) -> Self
        where
            R: Rng + CryptoRng,
        {
            let issuer_priv_key = PrivateKey::random(rng);
            let issuer = private_key_to_address(&issuer_priv_key);
            let owned_id_tag = IdentityTag::from_key_with_rand_base(issuer_priv_key.into(), rng);
            const SPENDABLE_TXO_COUNT: usize = 10_000;
            let spendable_txos = vec![10; SPENDABLE_TXO_COUNT];
            let builder = RedeemBuilderImpl {
                txo_repo: FakeTxoRepo {
                    spendable_txo: generate_spendable_utxos(owned_id_tag, spendable_txos, rng),
                    decoys: generate_fake_input_txos(4, SPENDABLE_TXO_COUNT, rng)
                        .into_iter()
                        .flat_map(|set| set.txos.components)
                        .collect(),
                },
                id_tag_selector: FakeIdentityTagSelector {
                    id_tags_from_address: IntoIterator::into_iter([(
                        issuer.clone(),
                        vec![owned_id_tag],
                    )])
                    .collect(),
                },
                key_store: FakeKeyStore::new(vec![issuer_priv_key]),
                member_context: FakeMemberContext::default(),
                encryption_provider: TestEncryptionProvider::default(),
                network_key_resolver: TestNetworkKeyResolver {
                    trust_encryption_key: EncryptionKey::from(rng.gen::<[u8; 32]>()),
                    trust_key: PrivateKey::random(rng).into(),
                    other_keys: IntoIterator::into_iter([(
                        issuer,
                        EncryptionKey::from(rng.gen::<[u8; 32]>()),
                    )])
                    .collect(),
                },
                rng: OsRng::default().into(),
            };
            Self {
                builder,
                issuer_priv_key,
            }
        }

        fn make_redeem_request_params<R>(&self, rng: &mut R) -> RedeemRequestParams
        where
            R: Rng + CryptoRng,
        {
            RedeemRequestParams {
                issuer: self.issuer_priv_key.into(),
                amount_in_minor_unit: 9,
                correlation_id: rng.gen(),
                account: BankAccount {
                    routing_number: "123".into(),
                    account_number: "456".into(),
                },
            }
        }
    }

    #[tokio::test]
    async fn redeem_has_requested_correlation_id() {
        let rng = &mut OsRng::default();
        let context = TestContext::new(rng);
        let redeem_params = context.make_redeem_request_params(rng);
        let expected_id = redeem_params.correlation_id;
        let request = context
            .builder
            .build_redeem_request(redeem_params)
            .await
            .unwrap();
        let actual_id = request.core_transaction.correlation_id;
        assert_eq!(actual_id, expected_id);
    }

    #[tokio::test]
    async fn redeem_has_requested_amount() {
        let rng = &mut OsRng::default();
        let context = TestContext::new(rng);
        let redeem_params = context.make_redeem_request_params(rng);
        let expected_amount = redeem_params.amount_in_minor_unit;
        let request = context
            .builder
            .build_redeem_request(redeem_params)
            .await
            .unwrap();
        assert_eq!(
            request.core_transaction.output.redeem_output.value,
            expected_amount
        );
    }

    #[tokio::test]
    async fn redeem_has_requested_encrypted_bank_account() {
        let rng = &mut OsRng::default();
        let context = TestContext::new(rng);
        let redeem_params = context.make_redeem_request_params(rng);
        let expected_account = redeem_params.account.clone();
        let correlation_id = redeem_params.correlation_id;
        let request = context
            .builder
            .build_redeem_request(redeem_params)
            .await
            .unwrap();
        let actual_account = context
            .builder
            .encryption_provider
            .decrypt_as_sender::<BankAccount>(
                &Encrypted::from(request.core_transaction.extra),
                &context.builder.network_key_resolver.other_keys
                    [&private_key_to_address(&context.issuer_priv_key)],
                &context.builder.network_key_resolver.trust_encryption_key,
                &correlation_id[..].into(),
            )
            .unwrap();
        assert_eq!(actual_account, expected_account);
    }
}
