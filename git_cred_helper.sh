#!/usr/bin/env bash

git config --global credential.helper store
echo "https://$1:$2@gitlab.com" > ~/.git-credentials
chmod 600 ~/.git-credentials
