//! Substrate Node Template CLI library.
#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]

#[macro_use]
extern crate tpfs_logger_port;

mod error;
mod rpc;
#[macro_use]
mod service;
pub mod chain_spec;
mod cli;

use error::Error;
use std::panic;

fn main() -> Result<(), Error> {
    if let Err(Error::TaskPanicked(panic)) = cli::run() {
        panic::resume_unwind(panic)
    }
    Ok(())
}
