use crate::{
    chain_spec::{generate_and_save_keys, SHORT_EPOCH_ENV_FLAG},
    CliConfig, SUBSTRATE_STARTING_HTTP_RPC_PORT, SUBSTRATE_STARTING_TCP_PORT,
    SUBSTRATE_STARTING_WS_RPC_PORT, SUBSTRATE_STARTING_XAND_API_PORT,
};
use derive_more::Constructor;
use glob::glob;
use snafu::{ResultExt, Snafu};
use sp_core::{ed25519, Pair};
use std::{
    collections::HashMap,
    fs::{create_dir_all, File},
    io::Write,
    panic,
    path::{Path, PathBuf},
    process::{Command, Stdio},
    time::Duration,
};
use url::Url;
use xand_utils::procutils::KillChildOnDrop;

pub const STARTUP_CHECK_DELAY: Duration = Duration::from_secs(1);
/// By default, most tests only need one substrate node operational. Tests which are concerned with
/// consensus related activities should use at least 3 nodes.
pub const DEFAULT_NUM_TEST_NODES: u32 = 1;

pub struct MultinodeHandle {
    pub children: Vec<KillChildOnDrop>,
}

#[derive(Debug, Snafu)]
#[snafu(visibility(pub))]
pub enum MultinodeError {
    #[snafu(display("Xandstrate executable not found at {}", path))]
    ExecutableNotBuilt { path: String },
    #[snafu(display("Underlying IO failure ({}): {}", message, source))]
    IoFailure {
        message: String,
        source: std::io::Error,
    },
    #[snafu(display("Logging initialization errror: {}", source))]
    LogInitFailure { source: tpfs_logger_port::LogError },
    #[snafu(display("Substrate keystore error: {}", source))]
    SubstrateKeystoreError { source: sc_keystore::Error },
    #[snafu(display("Path is not unicode: {}", path.display()))]
    NonUnicodePath { path: PathBuf },
    #[snafu(display("Multinode panic"))]
    MultinodeHandlePanic,
}

pub fn kill_zombies() {
    Command::new("pkill")
        .args(["-15", "xandstrate"])
        .output()
        .expect("failed to kill any xandstrate procs");
}

pub fn cleanup_temp_dir() {
    rm_dir_if_exists("/tmp/xandstrate-dev")
        .context(IoFailure {
            message: "Purging substrate data before start",
        })
        .unwrap();
}

// Annoying but effective workaround for https://github.com/JelteF/derive_more/issues/109
pub use _bugfix::*;
use tokio::runtime::Runtime;

mod _bugfix {
    #![allow(clippy::too_many_arguments)]

    use super::*;

    #[derive(Constructor, Clone)]
    pub struct MultinodeParams {
        pub base_name: String,
        pub starting_tcp_port: u32,
        pub starting_http_rpc_port: u32,
        pub starting_ws_rpc_port: u32,
        pub starting_xand_api_port: u32,
        pub purge_before_start: bool,
        pub quiet: bool,
        pub release: bool,
        pub env_overrides: HashMap<String, String>,
        pub jwt_secret: Option<PathBuf>,
        pub dry_run: bool,
        pub snapshot_override: Option<SnapshotOverride>,
    }
}

impl Default for MultinodeParams {
    fn default() -> Self {
        Self::new(
            "Xand".to_string(),
            SUBSTRATE_STARTING_TCP_PORT,
            SUBSTRATE_STARTING_HTTP_RPC_PORT,
            SUBSTRATE_STARTING_WS_RPC_PORT,
            SUBSTRATE_STARTING_XAND_API_PORT,
            true,
            false,
            true,
            HashMap::default(),
            None,
            false,
            None,
        )
    }
}

/// Starts a multinode network and returns a `MultinodeHandle` to manage this network instance.
/// Processes running nodes will be killed if `MultinodeHandle` instance is dropped.
pub fn start_multinode(
    num_nodes: u32,
    params: MultinodeParams,
) -> Result<MultinodeHandle, MultinodeError> {
    let mut children = vec![];

    for i in 0..num_nodes {
        if let Some(child) = start_one_multinode_node(num_nodes, params.clone(), i)? {
            children.push(child);
        }
    }

    Ok(MultinodeHandle { children })
}

/// Start one node in a multinode array. This can be called separately by tests which may need
/// to stagger start up, or start new nodes after the network has begun.
///
/// You must still provide the `num_nodes` parameter so that the new node can start with the right
/// chainspec (IE: num_nodes here must be the same as any previous calls to `start_multinode`).
///
/// The `node_index` parameter is zero indexed. EX: If you are adding a third node to a previously
/// two-node network, `node_index` should be 2.
pub fn start_one_multinode_node(
    num_nodes: u32,
    params: MultinodeParams,
    node_index: u32,
) -> Result<Option<KillChildOnDrop>, MultinodeError> {
    let node_key_index = (node_index + 1).to_string();
    let dev_path = "/tmp/xandstrate-dev";

    // Settings unique per node
    let base_path = format!("{}/{}-{}", &dev_path, params.base_name, node_key_index);
    let key_management_path = format!("{}/keys/", &base_path);
    if params.purge_before_start {
        warn!(LoggingEvent::Msg(format!(
            "MULTINODE: Purging substrate data at {}.",
            base_path
        )));
        rm_dir_if_exists(base_path.as_str()).context(IoFailure {
            message: "Purging substrate data before start",
        })?;
    } else {
        warn!(LoggingEvent::Msg(format!(
            "MULTINODE: starting substrate and preserving data at {}",
            base_path
        )));
        // Even if we don't purge, we may need to clean up a rocksdb lockfile if the previous
        // run didn't end cleanly for some reason.
        let lockfile = glob(&format!("{}/**/LOCK", base_path))
            .expect("Static pattern OK")
            .filter_map(Result::ok);
        for found_lockfile in lockfile {
            std::fs::remove_file(found_lockfile).expect("Must be able to remove DB lockfile");
        }
    }

    create_dir_all(&base_path).context(IoFailure {
        message: "Creating directories for multinode",
    })?;
    let node_name = format!("{}{}", params.base_name, node_key_index);
    let key_seed_str = format!("//{}", node_name);
    let keypair = get_p2p_keypair_for_node(&params, node_key_index);
    let node_key_file = base_path.clone() + "/libp2p-keyfile";
    {
        let priv_seed = keypair.seed();
        write_bytes_to_file(&node_key_file, priv_seed);
    }
    println!("Libp2p key path: {}", &node_key_file);
    let tcp_port = params.starting_tcp_port + node_index;
    let http_rpc_port = params.starting_http_rpc_port + node_index;
    let ws_rpc_port = params.starting_ws_rpc_port + node_index;
    let xand_api_port = params.starting_xand_api_port + node_index;
    let other_nodes = 0..num_nodes;
    let bootnodes: Vec<_> = other_nodes
        // Filter out ourselves
        .filter(|ix| *ix != node_index)
        // Generate appropriate bootnode string for all other nodes by generating the same
        // libp2p key they would use.
        .map(|ix| {
            let keypair = get_p2p_keypair_for_node(&params, (ix + 1).to_string());
            let p2p_pubkey = keypair.public();
            let peer_id =
                libp2p_core::identity::PublicKey::Ed25519(
                    libp2p_core::identity::ed25519::PublicKey::decode(p2p_pubkey.as_ref())
                        .expect("Static key is decodeable"))
                    .into_peer_id().to_base58();
            format!("/ip4/127.0.0.1/tcp/{}/p2p/{}", params.starting_tcp_port + ix, peer_id)
        }).collect();
    // Common settings across all nodes
    let node_key_type = "ed25519".to_string();
    let chain = format!("confidential-{}", num_nodes);
    let telemetry_url = std::env::var("SUBSTRATE_TELEMETRY_URL").unwrap_or_default();

    let snapshot_config = params.snapshot_override.unwrap_or_else(|| {
        let mut snapshot_dir = PathBuf::from(&base_path);
        snapshot_dir.push("snapshots");

        let happens_every_block = 0;
        SnapshotOverride {
            enable_snapshotter: true,
            snapshot_dir: Some(snapshot_dir),
            snapshot_cool_down_secs: Some(happens_every_block),
        }
    });

    let config: CliConfig = CliConfig {
        node_name,
        base_path,
        node_key_type,
        node_key_file,
        key_seed_str,
        tcp_port,
        http_rpc_port,
        ws_rpc_port,
        xand_api_port,
        key_management_path,
        chain,
        telemetry_url,
        quiet: params.quiet,
        release: params.release,
        bootnodes,
        env_overrides: params.env_overrides,
        jwt_secret: params.jwt_secret,
        enable_snapshotter: snapshot_config.enable_snapshotter,
        snapshot_dir: snapshot_config.snapshot_dir,
        snapshot_frequency_secs: snapshot_config.snapshot_cool_down_secs,
    };
    if !params.dry_run {
        println!(
            "Starting node {} -- Config: {:?}\n===========================",
            node_index, config
        );
    }
    let child = start_node_in_subprocess(config, params.dry_run)?;
    Ok(child)
}

fn get_p2p_keypair_for_node(params: &MultinodeParams, node_key_index: String) -> ed25519::Pair {
    let node_name = format!("{}{}", params.base_name, node_key_index);
    let key_seed_str = format!("//{}", node_name);
    ed25519::Pair::from_string(&key_seed_str, None).expect("static keygen works")
}

/// This will blindly write to the file, overwriting any existing content. Panics if given an invalid
/// file path to write to, or if unable to write to destination.
fn write_bytes_to_file(filepath: &str, bytes: &[u8]) {
    let mut file = File::create(filepath).expect("Must be valid filepath");
    file.write_all(bytes)
        .expect("Must be able to write to file");
}

/// Returns bool for whether directory existed or not
fn rm_dir_if_exists(path_str: &str) -> Result<bool, std::io::Error> {
    let path = Path::new(path_str);
    if !path.exists() {
        return Ok(false);
    }
    std::fs::remove_dir_all(path)?;
    Ok(true)
}

fn get_initial_keys_path() -> String {
    PathBuf::from(env!("CARGO_MANIFEST_DIR"))
        .join("../dev-keys")
        .into_os_string()
        .into_string()
        .unwrap()
}

pub fn start_node_in_subprocess(
    config: CliConfig,
    dry_run: bool,
) -> Result<Option<KillChildOnDrop>, MultinodeError> {
    // TODO: Once we are using a xand-logger, set up logs to go to file and (conditionally) console.
    let quiet = config.quiet;
    let stdout_setting: Stdio = if quiet {
        Stdio::null()
    } else {
        Stdio::inherit()
    };
    let stderr_setting: Stdio = if quiet {
        Stdio::null()
    } else {
        Stdio::inherit()
    };

    let tcp_port_string = config.tcp_port.to_string();
    let http_rpc_port_string = config.http_rpc_port.to_string();
    let ws_rpc_port_string = config.ws_rpc_port.to_string();
    let xand_api_port_string = config.xand_api_port.to_string();
    let initial_keys_path = get_initial_keys_path();
    let keystore_path = format!("{}/keystore", &config.base_path);
    let jwt_secret = config
        .jwt_secret
        .as_ref()
        .map(|p| {
            p.to_str()
                .map(String::from)
                .ok_or_else(|| MultinodeError::NonUnicodePath { path: p.to_owned() })
        })
        .transpose()?;

    generate_and_save_keys(&config.node_name, &keystore_path).context(SubstrateKeystoreError)?;

    let freq_secs = config.snapshot_frequency_secs.map(|x| format!("{}", x));

    let vargs = {
        let mut vargs = vec![
            "--base-path",
            &config.base_path,
            "--keystore-path",
            &keystore_path,
            "--node-key-type",
            "ed25519",
            "--node-key-file",
            &config.node_key_file,
            "--name",
            &config.node_name,
            "--port",
            &tcp_port_string,
            "--rpc-port",
            &http_rpc_port_string,
            "--ws-port",
            &ws_rpc_port_string,
            "--xand-api-grpc-port",
            &xand_api_port_string,
            "--initial-key-path",
            &initial_keys_path,
            "--key-management-path",
            &config.key_management_path,
            "--chain",
            &config.chain,
            "--validator",
            "--no-mdns",
            "--execution",
            "Native",
        ];
        if !config.telemetry_url.is_empty() {
            vargs.extend(vec!["--telemetry-url", &config.telemetry_url]);
        }
        if !config.bootnodes.is_empty() {
            vargs.push("--bootnodes");
            vargs.extend(config.bootnodes.iter().map(|x| x.as_str()));
        }
        if let Some(secret) = &jwt_secret {
            vargs.extend(["--jwt-secret-path", secret]);
        }
        if config.enable_snapshotter {
            vargs.extend(["--enable_snapshotter"]);
        }
        if let Some(snapshot_dir) = config.snapshot_dir.as_ref() {
            vargs.extend(["--snapshot_dir", snapshot_dir.to_str().unwrap_or(".")]);
        }
        if let Some(snapshot_frequency_secs) = freq_secs.as_ref() {
            vargs.extend(["--snapshot_frequency_secs", snapshot_frequency_secs]);
        }
        vargs
    };
    let mut xandstrate_exe = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    if config.release {
        xandstrate_exe.push("../target/release/xandstrate");
    } else {
        xandstrate_exe.push("../target/debug/xandstrate");
    };
    if !xandstrate_exe.exists() {
        return Err(MultinodeError::ExecutableNotBuilt {
            path: xandstrate_exe
                .to_str()
                .map(Into::into)
                .unwrap_or_else(|| "Path not string representable".to_string()),
        });
    }
    let mut cmd = Command::new(xandstrate_exe);
    cmd.args(vargs.clone())
        .envs(&config.env_overrides)
        .stdout(stdout_setting)
        .stderr(stderr_setting);

    // If dry-run is enabled, just print the start commands for each node
    if dry_run {
        println!(
            "cargo run --package xandstrate --bin xandstrate -- {}",
            vargs.join(" ")
        );
        Ok(None)
    } else {
        let mut child = KillChildOnDrop::new(cmd);
        child.spawn().context(IoFailure {
            message: "Spawning children",
        })?;
        Ok(Some(child))
    }
}

#[derive(Clone, Debug)]
pub struct SnapshotOverride {
    pub enable_snapshotter: bool,
    pub snapshot_dir: Option<PathBuf>,
    pub snapshot_cool_down_secs: Option<u32>,
}

#[derive(Clone)]
pub struct NetworkParams {
    pub num_nodes: u32,
    pub start_fresh: bool,
    pub use_short_epochs: bool,
    pub snapshot_override: Option<SnapshotOverride>,
}

impl Default for NetworkParams {
    fn default() -> Self {
        Self {
            num_nodes: DEFAULT_NUM_TEST_NODES,
            start_fresh: true,
            use_short_epochs: false,
            snapshot_override: None,
        }
    }
}

/// Starts a test network with a clean chain, waiting until the nodes are responsive.
/// Returns the [MultinodeHandle] for the network instance.
pub async fn start_and_wait_test_network(params: NetworkParams) -> MultinodeHandle {
    kill_zombies();

    let env_overrides = if params.use_short_epochs {
        let mut hm = HashMap::new();
        hm.insert(SHORT_EPOCH_ENV_FLAG.to_string(), "true".to_string());
        hm
    } else {
        HashMap::default()
    };

    let children = start_multinode(
        params.num_nodes,
        MultinodeParams {
            purge_before_start: params.start_fresh,
            env_overrides,
            snapshot_override: params.snapshot_override,
            ..Default::default()
        },
    )
    .expect("Error in starting default test network");

    for i in 0..params.num_nodes {
        let node_http_port = SUBSTRATE_STARTING_HTTP_RPC_PORT + i;
        let healthcheck_url = Url::parse(&format!("http://127.0.0.1:{}/health", node_http_port))
            .expect("Invalid url");

        // If the node does not respond in time, stop the network and panic
        if let Err(err) = loop_until_node_is_ready(healthcheck_url).await {
            kill_zombies();
            cleanup_temp_dir();
            panic!("Nodes' healthcheck failed with: {}", err);
        }
    }
    info!(LoggingEvent::Msg("Network started!".to_string()));

    children
}

/// Runs a network for the duration of the provided function.
pub fn run_network_for_test<T>(params: NetworkParams, test: T)
where
    T: FnOnce() + panic::UnwindSafe,
{
    let rt = Runtime::new().unwrap();
    let _drop_me_to_kill_subprocs = rt.block_on(start_and_wait_test_network(params));
    // let result = panic::catch_unwind(test);
    test();
}

#[logging_event]
pub enum LoggingEvent {
    Msg(String),
}

/// Runs a (fresh) network for the duration of the provided function. Uses the default amount of
/// nodes defined at [DEFAULT_NUM_TEST_NODES]. See [NetworkParams] for other defaults.
pub fn run_with_network<T>(test: T)
where
    T: FnOnce() + panic::UnwindSafe,
{
    info!(LoggingEvent::Msg(
        "Starting an instance of the network".to_string()
    ));
    run_network_for_test(NetworkParams::default(), test)
}

#[derive(Debug, Snafu)]
enum HealthcheckError {
    #[snafu(display(
        "RetriesExhausted - NumRetries: {} Interval: {} Url: {}",
        num_retries,
        interval,
        url
    ))]
    RetriesExhausted {
        num_retries: u64,
        interval: u64,
        url: String,
    },
}

/// Queries the node's healthcheck endpoint until 200 is returned.
/// Returns an error if number of retries are exhausted.
async fn loop_until_node_is_ready(healthcheck_url: Url) -> Result<(), HealthcheckError> {
    let num_retries = 10;
    let mut i = 0;
    loop {
        println!("Healthcheck for {}\tAttempt: {}", healthcheck_url, i);
        match reqwest::get(healthcheck_url.to_string()).await {
            Ok(_resp) => {
                println!("healtcheck request returned Ok()");
                break;
            }
            Err(err) => {
                println!("healthcheck returned err: {}", err);
                i += 1;
                if i >= num_retries {
                    return Err(HealthcheckError::RetriesExhausted {
                        num_retries,
                        interval: STARTUP_CHECK_DELAY.as_secs(),
                        url: healthcheck_url.to_string(),
                    });
                }
                tokio::time::sleep(STARTUP_CHECK_DELAY).await;
            }
        }
    }
    Ok(())
}
