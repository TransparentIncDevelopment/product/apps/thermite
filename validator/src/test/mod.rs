use crate::chain_spec::{Alternative, ChainSpec};
use sc_service::ChainType;

#[test]
#[ignore = "Generated WASM in `.system.code` is inconsistent in local dev vs CI."]
fn testnet_genesis_snapshot() {
    let mut props = serde_json::map::Map::new();
    // Make minor units show up correctly (100 minor units = 1 major unit)
    props.insert("tokenDecimals".to_string(), 2.into());
    // "X" symbol for our Xand units
    props.insert("tokenSymbol".to_string(), "🅧".into());
    insta::assert_snapshot!(ChainSpec::from_genesis(
            "Xand Net",
            "template",
            ChainType::Development,
            Alternative::testnet_confidentiality_genesis_with_nodes,
            vec![],
            None,
            None,
            Some(props),
            None,
        )
        .as_json(false) // This mirrors our initialization
        // TODO We should investigate building a Raw chainspec which enables nodes to sync after runtime update, in a way that considers non-Raw existing nodes - https://substrate.dev/docs/en/knowledgebase/integrate/chain-spec#raw-chain-specs
        .unwrap());
}
