//! Handles command line parsing and startup of the node.
//!
//! See https://github.com/paritytech/substrate/tree/master/bin/node-template/node/src for reference

use crate::service::{start_xand_services, KeyConfig, SnapshotConfig, XandServicesConfig};
use crate::{chain_spec, error::Error, service};
use sc_cli::{CliConfiguration, Role, RunCmd, RuntimeVersion, SubstrateCli};
use sc_service::{ChainSpec, PartialComponents};
use std::time::Duration;
use std::{env, path::PathBuf};
use structopt::StructOpt;
use tracing_subscriber::{
    filter::{EnvFilter, LevelFilter},
    layer::SubscriberExt,
    util::SubscriberInitExt,
};
use url::Url;
use xand_api::XandApiConfig;
use xandstrate_runtime::{Block, VERSION};

/// An overarching CLI command definition.
#[derive(Debug, StructOpt)]
pub struct Cli {
    /// Possible subcommand with parameters.
    #[structopt(subcommand)]
    pub subcommand: Option<Subcommand>,
    #[structopt(flatten)]
    pub run: RunCmd,
    #[structopt(long = "xand-api-grpc-port", default_value = "10044")]
    /// The port the xand-apig grpc server will listen on
    pub xand_api_grpc_port: u16,
    #[structopt(long = "initial-key-path", default_value = "/etc/xand-keys")]
    /// The path to where the initial keys (keys created via configuration) will be loaded from.
    pub initial_key_path: PathBuf,
    #[structopt(long = "key-management-path", default_value = "/xand/keys/")]
    /// The location where key management will read and write to. (This will
    /// copy the ones from configuration.
    pub key_management_path: PathBuf,
    /// The maximum number of transactions stored in the cache.
    #[structopt(long = "transaction-limit")]
    pub transaction_limit: Option<usize>,
    /// Path to JWT secret (if set, auth enabled)
    #[structopt(long = "jwt-secret-path")]
    pub jwt_secret_path: Option<PathBuf>,
    #[structopt(long = "enable_snapshotter")]
    pub enable_snapshotter: bool,
    #[structopt(long = "snapshot_dir", default_value = ".")]
    pub snapshot_dir: PathBuf,
    #[structopt(long = "snapshot_frequency_secs", default_value = "3600")] // 1 hour
    pub snapshot_frequency_secs: u64,
}

#[derive(Debug, StructOpt)]
pub enum Subcommand {
    /// Build a chain specification.
    BuildSpec(sc_cli::BuildSpecCmd),

    /// Validate blocks.
    CheckBlock(sc_cli::CheckBlockCmd),

    /// Export blocks.
    ExportBlocks(sc_cli::ExportBlocksCmd),

    /// Export the state of a given block into a chain spec.
    ExportState(sc_cli::ExportStateCmd),

    /// Import blocks.
    ImportBlocks(sc_cli::ImportBlocksCmd),

    /// Remove the whole chain.
    PurgeChain(sc_cli::PurgeChainCmd),

    /// Revert the chain to a previous state.
    Revert(sc_cli::RevertCmd),

    /// The custom benchmark subcommmand benchmarking runtime pallets.
    #[structopt(name = "benchmark", about = "Benchmark runtime pallets.")]
    Benchmark(frame_benchmarking_cli::BenchmarkCmd),
}

impl SubstrateCli for Cli {
    fn impl_name() -> String {
        "Xandstrate Node".into()
    }

    fn impl_version() -> String {
        env!("CARGO_PKG_VERSION").into()
    }

    fn executable_name() -> String {
        env!("CARGO_PKG_NAME").into()
    }

    fn description() -> String {
        env!("CARGO_PKG_DESCRIPTION").into()
    }

    fn author() -> String {
        env!("CARGO_PKG_AUTHORS").into()
    }

    fn support_url() -> String {
        "support@transparent.us".into()
    }

    fn copyright_start_year() -> i32 {
        2019
    }

    fn load_spec(&self, id: &str) -> Result<Box<dyn ChainSpec>, String> {
        load_spec(id)
    }

    fn native_runtime_version(_: &Box<dyn ChainSpec>) -> &'static RuntimeVersion {
        &VERSION
    }
}

/// Parse command line arguments into service configuration.
pub fn run() -> Result<(), Error> {
    let cli: Cli = sc_cli::SubstrateCli::from_args();

    match &cli.subcommand {
        None => {
            let runner = cli.create_runner(&CustomCliConfig(&cli.run))?;
            let websocket_config = runner
                .config()
                .rpc_ws
                .expect("Missing websocket port configuration");
            let grpc_port = cli.xand_api_grpc_port;
            let initial_key_path = Some(cli.initial_key_path);
            let key_management_path = cli.key_management_path;
            let jwt_secret_path = cli.jwt_secret_path;

            let validator_ws_url =
                Url::parse(&format!("ws://127.0.0.1:{}", websocket_config.port()))
                    .expect("static value for validator url works");

            let xand_api_config = XandApiConfig {
                port: grpc_port,
                jwt_secret_path,
            };
            let transaction_limit = cli.transaction_limit;

            let key_config = KeyConfig {
                initial_key_path,
                key_management_path,
            };

            let snapshot_config = if cli.enable_snapshotter {
                let snapshot_dir = cli.snapshot_dir;
                let snapshot_frequency = Duration::from_secs(cli.snapshot_frequency_secs);
                Some(SnapshotConfig {
                    snapshot_dir,
                    snapshot_frequency,
                })
            } else {
                None
            };

            let service_full_with_petri = |nc| {
                let srvc = crate::service::new_full(nc);
                srvc.map(|srvc| {
                    let chain_client = srvc.1;

                    let xand_services_cfg = XandServicesConfig {
                        key_config,
                        validator_ws_url,
                        transaction_limit,
                        xand_api_config,
                        chain_client,
                        snapshot_config,
                    };

                    std::thread::Builder::new()
                        .name("xand_svc_start".into())
                        .spawn(move || {
                            if let Err(e) = start_xand_services(xand_services_cfg) {
                                log::error!("Service aborted: {:?}", e);
                            }
                        })
                        .expect("Must be able to start thread");
                    srvc.0
                })
            };
            Ok(runner.run_node_until_exit(|config| async move {
                match config.role {
                    Role::Light => crate::service::new_light(config),
                    _ => service_full_with_petri(config),
                }
            })?)
        }
        Some(Subcommand::BuildSpec(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.sync_run(|config| cmd.run(config.chain_spec, config.network))?)
        }
        Some(Subcommand::CheckBlock(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.async_run(|config| {
                let PartialComponents {
                    client,
                    task_manager,
                    import_queue,
                    ..
                } = service::new_partial(&config)?;
                Ok((cmd.run(client, import_queue), task_manager))
            })?)
        }
        Some(Subcommand::ExportBlocks(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.async_run(|config| {
                let PartialComponents {
                    client,
                    task_manager,
                    ..
                } = service::new_partial(&config)?;
                Ok((cmd.run(client, config.database), task_manager))
            })?)
        }
        Some(Subcommand::ExportState(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.async_run(|config| {
                let PartialComponents {
                    client,
                    task_manager,
                    ..
                } = service::new_partial(&config)?;
                Ok((cmd.run(client, config.chain_spec), task_manager))
            })?)
        }
        Some(Subcommand::ImportBlocks(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.async_run(|config| {
                let PartialComponents {
                    client,
                    task_manager,
                    import_queue,
                    ..
                } = service::new_partial(&config)?;
                Ok((cmd.run(client, import_queue), task_manager))
            })?)
        }
        Some(Subcommand::PurgeChain(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.sync_run(|config| cmd.run(config.database))?)
        }
        Some(Subcommand::Revert(cmd)) => {
            let runner = cli.create_runner(cmd)?;
            Ok(runner.async_run(|config| {
                let PartialComponents {
                    client,
                    task_manager,
                    backend,
                    ..
                } = service::new_partial(&config)?;
                Ok((cmd.run(client, backend), task_manager))
            })?)
        }
        Some(Subcommand::Benchmark(cmd)) => {
            if cfg!(feature = "runtime-benchmarks") {
                let runner = cli.create_runner(cmd)?;

                Ok(runner.sync_run(|config| cmd.run::<Block, service::Executor>(config))?)
            } else {
                Err(Error::CompiledWithoutBenchmarking)
            }
        }
    }
}

fn load_spec(id: &str) -> Result<Box<dyn ChainSpec>, String> {
    Ok(match id.parse::<chain_spec::Alternative>() {
        Ok(spec) => Box::new(spec.load()),
        Err(_) => Box::new(chain_spec::ChainSpec::from_json_file(
            std::path::PathBuf::from(id),
        )?),
    })
}

struct CustomCliConfig<'a>(&'a RunCmd);

impl CliConfiguration for CustomCliConfig<'_> {
    fn shared_params(&self) -> &sc_cli::SharedParams {
        self.0.shared_params()
    }

    fn import_params(&self) -> Option<&sc_cli::ImportParams> {
        self.0.import_params()
    }

    fn pruning_params(&self) -> Option<&sc_cli::PruningParams> {
        self.0.pruning_params()
    }

    fn keystore_params(&self) -> Option<&sc_cli::KeystoreParams> {
        self.0.keystore_params()
    }

    fn network_params(&self) -> Option<&sc_cli::NetworkParams> {
        self.0.network_params()
    }

    fn offchain_worker_params(&self) -> Option<&sc_cli::OffchainWorkerParams> {
        self.0.offchain_worker_params()
    }

    fn node_key_params(&self) -> Option<&sc_cli::NodeKeyParams> {
        self.0.node_key_params()
    }

    fn database_params(&self) -> Option<&sc_cli::DatabaseParams> {
        self.0.database_params()
    }

    fn base_path(&self) -> sc_cli::Result<Option<sc_service::BasePath>> {
        self.0.base_path()
    }

    fn is_dev(&self) -> sc_cli::Result<bool> {
        self.0.is_dev()
    }

    fn role(&self, is_dev: bool) -> sc_cli::Result<Role> {
        self.0.role(is_dev)
    }

    fn transaction_pool(&self) -> sc_cli::Result<sc_service::TransactionPoolOptions> {
        self.0.transaction_pool()
    }

    fn network_config(
        &self,
        chain_spec: &Box<dyn ChainSpec>,
        is_dev: bool,
        net_config_dir: PathBuf,
        client_id: &str,
        node_name: &str,
        node_key: sc_network::config::NodeKeyConfig,
        default_listen_port: u16,
    ) -> sc_cli::Result<sc_network::config::NetworkConfiguration> {
        self.0.network_config(
            chain_spec,
            is_dev,
            net_config_dir,
            client_id,
            node_name,
            node_key,
            default_listen_port,
        )
    }

    fn keystore_config(
        &self,
        config_dir: &PathBuf,
    ) -> sc_cli::Result<(Option<String>, sc_service::config::KeystoreConfig)> {
        self.0.keystore_config(config_dir)
    }

    fn database_cache_size(&self) -> sc_cli::Result<Option<usize>> {
        self.0.database_cache_size()
    }

    fn database_transaction_storage(&self) -> sc_cli::Result<sc_client_db::TransactionStorageMode> {
        self.0.database_transaction_storage()
    }

    fn database(&self) -> sc_cli::Result<Option<sc_cli::Database>> {
        self.0.database()
    }

    fn database_config(
        &self,
        base_path: &PathBuf,
        cache_size: usize,
        database: sc_cli::Database,
    ) -> sc_cli::Result<sc_client_db::DatabaseSettingsSrc> {
        self.0.database_config(base_path, cache_size, database)
    }

    fn state_cache_size(&self) -> sc_cli::Result<usize> {
        self.0.state_cache_size()
    }

    fn state_cache_child_ratio(&self) -> sc_cli::Result<Option<usize>> {
        self.0.state_cache_child_ratio()
    }

    fn state_pruning(
        &self,
        unsafe_pruning: bool,
        role: &Role,
    ) -> sc_cli::Result<sc_client_db::PruningMode> {
        self.0.state_pruning(unsafe_pruning, role)
    }

    fn keep_blocks(&self) -> sc_cli::Result<sc_client_db::KeepBlocks> {
        self.0.keep_blocks()
    }

    fn chain_id(&self, is_dev: bool) -> sc_cli::Result<String> {
        self.0.chain_id(is_dev)
    }

    fn node_name(&self) -> sc_cli::Result<String> {
        self.0.node_name()
    }

    fn wasm_method(&self) -> sc_cli::Result<sc_executor::WasmExecutionMethod> {
        self.0.wasm_method()
    }

    fn wasm_runtime_overrides(&self) -> Option<PathBuf> {
        self.0.wasm_runtime_overrides()
    }

    fn execution_strategies(
        &self,
        is_dev: bool,
        is_validator: bool,
    ) -> sc_cli::Result<sc_client_api::execution_extensions::ExecutionStrategies> {
        self.0.execution_strategies(is_dev, is_validator)
    }

    fn rpc_http(&self, default_listen_port: u16) -> sc_cli::Result<Option<std::net::SocketAddr>> {
        self.0.rpc_http(default_listen_port)
    }

    fn rpc_ipc(&self) -> sc_cli::Result<Option<String>> {
        self.0.rpc_ipc()
    }

    fn rpc_ws(&self, default_listen_port: u16) -> sc_cli::Result<Option<std::net::SocketAddr>> {
        self.0.rpc_ws(default_listen_port)
    }

    fn rpc_methods(&self) -> sc_cli::Result<sc_service::RpcMethods> {
        self.0.rpc_methods()
    }

    fn rpc_ws_max_connections(&self) -> sc_cli::Result<Option<usize>> {
        self.0.rpc_ws_max_connections()
    }

    fn rpc_cors(&self, is_dev: bool) -> sc_cli::Result<Option<Vec<String>>> {
        self.0.rpc_cors(is_dev)
    }

    fn prometheus_config(
        &self,
        default_listen_port: u16,
    ) -> sc_cli::Result<Option<sc_service::config::PrometheusConfig>> {
        self.0.prometheus_config(default_listen_port)
    }

    fn telemetry_endpoints(
        &self,
        chain_spec: &Box<dyn ChainSpec>,
    ) -> sc_cli::Result<Option<sc_service::config::TelemetryEndpoints>> {
        self.0.telemetry_endpoints(chain_spec)
    }

    fn telemetry_external_transport(
        &self,
    ) -> sc_cli::Result<Option<sc_network::config::ExtTransport>> {
        self.0.telemetry_external_transport()
    }

    fn default_heap_pages(&self) -> sc_cli::Result<Option<u64>> {
        self.0.default_heap_pages()
    }

    fn offchain_worker(
        &self,
        role: &Role,
    ) -> sc_cli::Result<sc_service::config::OffchainWorkerConfig> {
        self.0.offchain_worker(role)
    }

    fn force_authoring(&self) -> sc_cli::Result<bool> {
        self.0.force_authoring()
    }

    fn disable_grandpa(&self) -> sc_cli::Result<bool> {
        self.0.disable_grandpa()
    }

    fn dev_key_seed(&self, is_dev: bool) -> sc_cli::Result<Option<String>> {
        self.0.dev_key_seed(is_dev)
    }

    fn tracing_targets(&self) -> sc_cli::Result<Option<String>> {
        self.0.tracing_targets()
    }

    fn tracing_receiver(&self) -> sc_cli::Result<sc_service::TracingReceiver> {
        self.0.tracing_receiver()
    }

    fn node_key(
        &self,
        net_config_dir: &PathBuf,
    ) -> sc_cli::Result<sc_network::config::NodeKeyConfig> {
        self.0.node_key(net_config_dir)
    }

    fn max_runtime_instances(&self) -> sc_cli::Result<Option<usize>> {
        self.0.max_runtime_instances()
    }

    fn announce_block(&self) -> sc_cli::Result<bool> {
        self.0.announce_block()
    }

    fn create_configuration<C: SubstrateCli>(
        &self,
        cli: &C,
        task_executor: sc_service::TaskExecutor,
        telemetry_handle: Option<sc_telemetry::TelemetryHandle>,
    ) -> sc_cli::Result<sc_service::Configuration> {
        self.0
            .create_configuration(cli, task_executor, telemetry_handle)
    }

    fn log_filters(&self) -> sc_cli::Result<String> {
        self.0.log_filters()
    }

    fn is_log_filter_reloading_disabled(&self) -> sc_cli::Result<bool> {
        self.0.is_log_filter_reloading_disabled()
    }

    fn disable_log_color(&self) -> sc_cli::Result<bool> {
        self.0.disable_log_color()
    }

    fn init<C: SubstrateCli>(&self) -> sc_cli::Result<sc_telemetry::TelemetryWorker> {
        sp_panic_handler::set(&C::support_url(), &C::impl_version());
        let (telemetry_layer, telemetry_worker) =
            sc_telemetry::TelemetryLayer::new(None, self.telemetry_external_transport()?)?;
        let log_filters = self.log_filters()?;
        let env_filter = (!log_filters.is_empty())
            .then(|| log_filters.split(','))
            .into_iter()
            .flatten()
            .chain(Some("telemetry-logger=warn"))
            .try_fold(
                EnvFilter::from_default_env().add_directive(LevelFilter::INFO.into()),
                |filter, s| Ok::<_, sc_tracing::logging::Error>(filter.add_directive(s.parse()?)),
            )?;
        let subscriber_builder = tracing_subscriber::fmt()
            .with_env_filter(env_filter)
            .event_format(tracing_subscriber::fmt::format().with_timer(
                tracing_subscriber::fmt::time::ChronoLocal::with_format(
                    "%Y-%m-%d %H:%M:%S%.3f".to_string(),
                ),
            ));
        if let Ok("true") = env::var("XAND_HUMAN_LOGGING").as_deref() {
            subscriber_builder.finish().with(telemetry_layer).init();
        } else {
            subscriber_builder
                .with_ansi(false)
                .json()
                .finish()
                .with(telemetry_layer)
                .init();
        }
        Ok(telemetry_worker)
    }
}
