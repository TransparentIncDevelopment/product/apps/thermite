//! A collection of node-specific RPC methods.
//!
//! Since `substrate` core functionality makes no assumptions
//! about the modules used inside the runtime, so do
//! RPC methods defined in `substrate-rpc` crate.
//! It means that `core/rpc` can't have any methods that
//! need some strong assumptions about the particular runtime.
//!
//! The RPCs available in this crate however can make some assumptions
//! about how the runtime is constructed and what substrate modules
//! are part of it. Therefore all node-runtime-specific RPCs can
//! be placed here or imported from corresponding substrate module RPC definitions.

use jsonrpc_core::{Error, ErrorCode, Result};
pub use sc_rpc_api::DenyUnsafe;
use sp_api::ProvideRuntimeApi;
use sp_block_builder::BlockBuilder;
use sp_blockchain::{Error as BlockChainError, HeaderBackend, HeaderMetadata};
use sp_runtime::generic::BlockId;
use sp_runtime::traits::Block as BlockT;
use sp_transaction_pool::TransactionPool;
use std::{fmt, sync::Arc};
use xand_address::Address;
use xand_models::{EncryptionKey, ToAddress};
use xand_runtime_models::CidrBlockArray;
use xandstrate_client::confidentiality_client::{ConfidentialityRpcApi, ConfidentialityRpcHandler};
use xandstrate_runtime::{
    confidentiality::api::ConfidentialPendingApi,
    opaque::Block,
    validator_emissions::models::{
        validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    xandstrate::{
        xandstrate_api::convert_data,
        xandstrate_api::{
            AllowlistApi, AuthorityKeyApi, EncryptionApi, LimitedAgentApi, MemberInfoApi,
            ProposalData, ProposalsApi, TotalIssuanceApi, TrusteeApi, ValidatorEmissionsApi,
        },
    },
    AccountId, BlockNumber, Hash, Index,
};

pub use xandstrate_client::XandstrateRpcApi;

pub struct XandSystem<B, C> {
    client: Arc<C>,
    _marker: std::marker::PhantomData<B>,
}

impl<B, C> XandSystem<B, C> {
    pub fn new(client: Arc<C>) -> Self {
        XandSystem {
            client,
            _marker: Default::default(),
        }
    }
}

/// Light client extra dependencies.
pub struct LightDeps<C, F, P> {
    /// The client instance to use.
    pub client: Arc<C>,
    /// Transaction pool instance.
    pub pool: Arc<P>,
    /// Remote access to the blockchain (async).
    pub remote_blockchain: Arc<dyn sc_client_api::light::RemoteBlockchain<Block>>,
    /// Fetcher instance.
    pub fetcher: Arc<F>,
}

/// Full client dependencies.
pub struct FullDeps<C, P> {
    /// The client instance to use.
    pub client: Arc<C>,
    /// Transaction pool instance.
    pub pool: Arc<P>,
    /// Whether to deny unsafe calls
    pub deny_unsafe: DenyUnsafe,
}

impl<Block, C> XandstrateRpcApi for XandSystem<Block, C>
where
    C: ProvideRuntimeApi<Block>,
    C: HeaderBackend<Block>,
    C: Send + Sync + 'static,
    C::Api: AllowlistApi<Block, AccountId>,
    C::Api: TotalIssuanceApi<Block>,
    C::Api: ProposalsApi<Block, AccountId, BlockNumber>,
    C::Api: TrusteeApi<Block, AccountId>,
    C::Api: LimitedAgentApi<Block, AccountId>,
    C::Api: MemberInfoApi<Block, AccountId>,
    C::Api: AuthorityKeyApi<Block, AccountId>,
    C::Api: EncryptionApi<Block, AccountId>,
    C::Api: ConfidentialPendingApi<Block>,
    C::Api: ValidatorEmissionsApi<Block, AccountId>,
    Block: BlockT + 'static,
{
    fn get_validator_emission_rate(&self) -> Result<ValidatorEmissionRate> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let validator_emission_rate = api.get_validator_emission_rate(&at).map_err(|e| Error {
            code: ErrorCode::ServerError(1),
            message: "Unable to query validator emission rate".to_string(),
            data: Some(format!("{:?}", e).into()),
        })?;
        Ok(validator_emission_rate)
    }

    fn get_validator_emission_progress(
        &self,
        acct: AccountId,
    ) -> Result<Option<ValidatorEmissionProgress>> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let validator_emission_progress: Option<ValidatorEmissionProgress> = api
            .get_validator_emission_progress(&at, acct)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query validator emission progress".to_string(),
                data: Some(format!("{:?}", e).into()),
            })?
            .map(Into::into);
        Ok(validator_emission_progress)
    }

    fn allowlist(&self) -> Result<Vec<(Address, CidrBlockArray)>> {
        let api = self.client.runtime_api();
        let best = self.client.info().best_hash;
        let at = BlockId::hash(best);
        let allowlist = api.get_allowlist(&at).map_err(|e| Error {
            code: ErrorCode::ServerError(1),
            message: "Unable to query correlation id.".into(),
            data: Some(format!("{:?}", e).into()),
        })?;
        Ok(allowlist
            .into_iter()
            .map(|(addr, cidrs)| (addr.to_address(), cidrs))
            .collect())
    }

    fn get_proposal(&self, id: u32) -> Result<Option<ProposalData<Address, u32>>> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let maybe_proposal = api
            .get_proposal(&at, id)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: format!("Unable to query proposal {:?}", id),
                data: Some(format!("{:?}", e).into()),
            })?
            .map(|p| convert_data(p, |a| a.to_address(), |b| b));
        Ok(maybe_proposal)
    }

    fn get_all_proposals(&self) -> Result<Vec<ProposalData<Address, u32>>> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let proposals = api
            .get_all_proposals(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query proposals".to_string(),
                data: Some(format!("{:?}", e).into()),
            })?
            .into_iter()
            .map(|p| convert_data(p, |a| a.to_address(), |b| b))
            .collect();
        Ok(proposals)
    }

    fn get_trustee(&self) -> Result<Address> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let trust_id = api
            .get_trustee(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query trust id".to_string(),
                data: Some(format!("{:?}", e).into()),
            })?
            .to_address();
        Ok(trust_id)
    }

    fn get_limited_agent(&self) -> Result<Option<Address>> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let limited_agent_id_option = api.get_limited_agent(&at).map_err(|e| Error {
            code: ErrorCode::ServerError(1),
            message: "Unable to query limited agent id".to_string(),
            data: Some(format!("{:?}", e).into()),
        })?;
        Ok(limited_agent_id_option.map(|addr| addr.to_address()))
    }

    fn get_members(&self) -> Result<Vec<Address>> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        let members = api
            .get_members(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query list of members".to_string(),
                data: Some(format!("{:?}", e).into()),
            })?
            .into_iter()
            .map(|a| a.to_address())
            .collect();
        Ok(members)
    }

    fn get_banned_members(&self) -> Result<Vec<Address>> {
        let at = BlockId::hash(self.client.info().best_hash);
        let banned_members = self
            .client
            .runtime_api()
            .get_banned_members(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query list of banned members.".into(),
                data: Some(format!("{:?}", e).into()),
            })?
            .into_iter()
            .map(|a| a.to_address())
            .collect();
        Ok(banned_members)
    }

    fn get_authority_keys(&self) -> Result<Vec<Address>> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        let keys = api
            .get_authority_keys(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query list of authority keys".to_string(),
                data: Some(format!("{:?}", e).into()),
            })?
            .into_iter()
            .map(|a| a.to_address())
            .collect();
        Ok(keys)
    }

    fn get_encryption_key(&self, acct: AccountId) -> Result<Option<EncryptionKey>> {
        let api = self.client.runtime_api();
        let finalized = self.client.info().finalized_hash;
        let at = BlockId::hash(finalized);
        let encryption_key: Option<EncryptionKey> = api
            .get_encryption_key(&at, acct)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query encryption key".to_string(),
                data: Some(format!("{:?}", e).into()),
            })?
            .map(Into::into);
        Ok(encryption_key)
    }

    fn get_total_issuance(&self) -> Result<u64> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        api.get_total_issuance(&at).map_err(|e| Error {
            code: ErrorCode::ServerError(1),
            message: "Unable to get total issuance".to_string(),
            data: Some(format!("{:?}", e).into()),
        })
    }
}

/// Instantiate all Full RPC extensions.
pub fn create_full<C, P>(deps: FullDeps<C, P>) -> jsonrpc_core::IoHandler<sc_rpc_api::Metadata>
where
    C: ProvideRuntimeApi<Block>,
    C: HeaderBackend<Block> + HeaderMetadata<Block, Error = BlockChainError> + 'static,
    C: Send + Sync + 'static,
    C::Api: substrate_frame_rpc_system::AccountNonceApi<Block, AccountId, Index>,
    C::Api: AllowlistApi<Block, AccountId>,
    C::Api: TotalIssuanceApi<Block>,
    C::Api: ProposalsApi<Block, AccountId, BlockNumber>,
    C::Api: TrusteeApi<Block, AccountId>,
    C::Api: LimitedAgentApi<Block, AccountId>,
    C::Api: MemberInfoApi<Block, AccountId>,
    C::Api: AuthorityKeyApi<Block, AccountId>,
    C::Api: EncryptionApi<Block, AccountId>,
    C::Api: ConfidentialPendingApi<Block>,
    C::Api: ValidatorEmissionsApi<Block, AccountId>,
    <C::Api as sp_api::ApiErrorExt>::Error: fmt::Debug,
    C::Api: BlockBuilder<Block>,
    P: TransactionPool + 'static,
{
    use substrate_frame_rpc_system::{FullSystem, SystemApi};

    let mut io = jsonrpc_core::IoHandler::default();
    let FullDeps {
        client,
        pool,
        deny_unsafe,
    } = deps;

    io.extend_with(SystemApi::to_delegate(FullSystem::new(
        client.clone(),
        pool,
        deny_unsafe,
    )));
    io.extend_with(XandstrateRpcApi::to_delegate(XandSystem::new(
        client.clone(),
    )));
    io.extend_with(ConfidentialityRpcApi::to_delegate(
        ConfidentialityRpcHandler::new(client),
    ));
    io
}

/// Instantiate all Light RPC extensions.
pub fn create_light<C, P, M, F>(deps: LightDeps<C, F, P>) -> jsonrpc_core::IoHandler<M>
where
    C: HeaderBackend<Block>,
    C: Send + Sync + 'static,
    F: sc_client_api::light::Fetcher<Block> + 'static,
    P: TransactionPool + 'static,
    M: jsonrpc_core::Metadata + Default,
{
    use substrate_frame_rpc_system::{LightSystem, SystemApi};

    let LightDeps {
        client,
        pool,
        remote_blockchain,
        fetcher,
    } = deps;
    let mut io = jsonrpc_core::IoHandler::default();
    io.extend_with(SystemApi::<Hash, AccountId, Index>::to_delegate(
        LightSystem::new(client, remote_blockchain, fetcher, pool),
    ));
    // TODO: Doesn't meet bounds. Should we have this in light clients? Ideally yes... How to meet?
    // io.extend_with(XandstrateRpcApi::to_delegate(XandSystem::new(client)));

    io
}
