#![forbid(unsafe_code)]

#[macro_use]
extern crate tpfs_logger_port;

mod error;
pub mod rpc;
#[macro_use]
pub mod service;
pub mod chain_spec;
pub mod cli;
pub mod multinode_util;

#[cfg(test)]
mod test;

use std::{collections::HashMap, path::PathBuf};

#[derive(Debug)]
pub struct CliConfig {
    pub node_name: String,
    pub base_path: String,
    pub node_key_type: String,
    pub node_key_file: String,
    pub key_seed_str: String,
    pub tcp_port: u32,
    pub http_rpc_port: u32,
    pub ws_rpc_port: u32,
    pub xand_api_port: u32,
    pub key_management_path: String,
    pub chain: String,
    pub telemetry_url: String,
    pub quiet: bool,
    pub release: bool,
    pub bootnodes: Vec<String>,
    pub env_overrides: HashMap<String, String>,
    pub jwt_secret: Option<PathBuf>,
    pub enable_snapshotter: bool,
    pub snapshot_dir: Option<PathBuf>,
    pub snapshot_frequency_secs: Option<u32>,
}

// Note: Substrate uses 9933 as the default port for the http_rpc server.
// Using 8933 to give a healthy buffer until ports would start overlapping with the default ws_rpc port
// If a port is already taken, Substrate tries to use a random port instead.
pub const SUBSTRATE_STARTING_HTTP_RPC_PORT: u32 = 8933;
pub const SUBSTRATE_STARTING_WS_RPC_PORT: u32 = 9944;
pub const SUBSTRATE_STARTING_XAND_API_PORT: u32 = 10044;
pub const SUBSTRATE_STARTING_TCP_PORT: u32 = 30333;
