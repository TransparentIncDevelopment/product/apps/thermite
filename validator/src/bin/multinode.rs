#![forbid(unsafe_code)]

use clap::{App, Arg};
use signal_hook::{iterator::Signals, SIGINT, SIGTERM};
use snafu::ResultExt;
use std::{collections::HashMap, path::PathBuf, process, thread};
use xandstrate::multinode_util::{IoFailure, LogInitFailure, MultinodeError, MultinodeParams};
use xandstrate::{
    multinode_util::start_multinode, SUBSTRATE_STARTING_HTTP_RPC_PORT, SUBSTRATE_STARTING_TCP_PORT,
    SUBSTRATE_STARTING_WS_RPC_PORT, SUBSTRATE_STARTING_XAND_API_PORT,
};

fn main() -> Result<(), MultinodeError> {
    let matches = App::new("XandStrate Multinode CLI")
        .version("0.1")
        .author("Transparent Systems <engineering@tpfs.io>")
        .about("Test helper to start and stop a multinode network")
        .arg(
            Arg::with_name("base_name")
                .long("base-name")
                .help("Base name for the 0th validator's keyName, nodeName, and path to a temp directory")
                .default_value("Xand")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("num_nodes")
                .long("num-nodes")
                .short("n")
                .help("Number of nodes to start. Must be >= 1")
                .default_value("1")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("new_chain")
                .long("new")
                .help("If set, network starts on a new chain at block 0")
        )
        .arg(
            Arg::with_name("quiet")
                .long("quiet")
                .short("q")
                .help("Hide each nodes' console output")
        )
        .arg(
            Arg::with_name("release")
                .long("release")
                .help("Run with release profile")
        )
        .arg(
            Arg::with_name("jwt-secret")
                .long("jwt-secret")
                .help("Path to the JWT secret")
                .takes_value(true)
        ).arg(Arg::with_name("confidential")
        .long("confidential")
        .help("Run node in confidential mode"))
        .arg(Arg::with_name("dry-run")
            .long("dry-run")
            .help("Perform multinode setup and then print out start commands for each node. Useful for debugging."))
        .get_matches();

    let base_name = matches.value_of("base_name").unwrap();
    let num_nodes: u32 = matches.value_of("num_nodes").unwrap().parse().unwrap();
    let new_chain: bool = matches.is_present("new_chain");
    let quiet: bool = matches.is_present("quiet");
    let release: bool = matches.is_present("release");
    let jwt_secret = matches.value_of("jwt-secret").map(PathBuf::from);
    let dry_run: bool = matches.is_present("dry-run");

    println!(
        "Parsed args: base_name: {} num_nodes: {} quiet: {} release: {} jwt-secret: {} dry-run: {}",
        base_name,
        num_nodes,
        quiet,
        release,
        jwt_secret
            .as_ref()
            .map(|p| p.to_string_lossy())
            .unwrap_or_default(),
        dry_run
    );

    tpfs_logger_tracing::init_with_default_config().context(LogInitFailure)?;

    let multinode_handle = start_multinode(
        num_nodes,
        MultinodeParams::new(
            base_name.to_string(),
            SUBSTRATE_STARTING_TCP_PORT,
            SUBSTRATE_STARTING_HTTP_RPC_PORT,
            SUBSTRATE_STARTING_WS_RPC_PORT,
            SUBSTRATE_STARTING_XAND_API_PORT,
            new_chain,
            quiet,
            release,
            HashMap::default(),
            jwt_secret,
            dry_run,
            None,
        ),
    )?;

    if !dry_run {
        // Catch signals to drop child processes and exit.
        let signals = Signals::new([SIGINT, SIGTERM]).context(IoFailure {
            message: "Registering signal handlers",
        })?;
        thread::Builder::new()
            .name("os_signal".into())
            .spawn(move || {
                for _sig in signals.forever() {
                    drop(multinode_handle);
                    process::exit(0);
                }
            })
            .expect("Must be able to start signal monitoring thread")
            .join()
            .map_err(|_| MultinodeError::MultinodeHandlePanic)
    } else {
        Ok(())
    }
}
