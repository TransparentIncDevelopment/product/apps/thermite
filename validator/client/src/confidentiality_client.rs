use jsonrpc_core::{Error, ErrorCode, Result};
use jsonrpc_derive::rpc;
use serde_scale_wrap::Wrap;
use sp_api::{BlockId, ProvideRuntimeApi};
use sp_blockchain::HeaderBackend;
use sp_runtime::traits::Block as BlockT;
use std::sync::Arc;
use xand_ledger::{
    ClearTransactionOutput, CorrelationId, CreateRequestRecord, RedeemRequestRecord,
};
use xandstrate_runtime::confidentiality::api::ConfidentialPendingApi;

pub use gen_client::Client as ConfidentialityClient;

/// RPC API for the confidentiality module
#[rpc]
pub trait ConfidentialityRpcApi {
    /// Get the list of confidential pending creates
    #[rpc(name = "confidentiality_getPendingCreates")]
    fn get_confidential_pending_creates(
        &self,
    ) -> Result<Vec<(CorrelationId, Wrap<CreateRequestRecord>)>>;

    #[rpc(name = "confidentiality_getPendingRedeems")]
    fn get_confidential_pending_redeems(
        &self,
    ) -> Result<Vec<(CorrelationId, Wrap<RedeemRequestRecord>)>>;
    #[rpc(name = "confidentiality_getClearUTxOs")]
    fn get_clear_utxos(&self) -> Result<Vec<Wrap<ClearTransactionOutput>>>;
    #[rpc(name = "confidentiality_getCreateExpireTime")]
    fn get_create_expire_time(&self) -> Result<u64>;
}

pub struct ConfidentialityRpcHandler<B, C> {
    client: Arc<C>,
    _marker: std::marker::PhantomData<B>,
}

impl<B, C> ConfidentialityRpcHandler<B, C> {
    pub fn new(client: Arc<C>) -> Self {
        ConfidentialityRpcHandler {
            client,
            _marker: Default::default(),
        }
    }
}

impl<Block, C> ConfidentialityRpcApi for ConfidentialityRpcHandler<Block, C>
where
    C: ProvideRuntimeApi<Block>,
    C: HeaderBackend<Block>,
    C: Send + Sync + 'static,
    C::Api: ConfidentialPendingApi<Block>,
    Block: BlockT + 'static,
{
    fn get_confidential_pending_creates(
        &self,
    ) -> Result<Vec<(CorrelationId, Wrap<CreateRequestRecord>)>> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        api.get_confidential_pending_creates(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query pending creates.".to_string(),
                data: Some(format!("{:?}", e).into()),
            })
    }

    fn get_confidential_pending_redeems(
        &self,
    ) -> Result<Vec<(CorrelationId, Wrap<RedeemRequestRecord>)>> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        api.get_confidential_pending_redeems(&at)
            .map_err(|e| Error {
                code: ErrorCode::ServerError(1),
                message: "Unable to query pending redeems.".to_string(),
                data: Some(format!("{:?}", e).into()),
            })
    }
    fn get_clear_utxos(&self) -> Result<Vec<Wrap<ClearTransactionOutput>>> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        api.get_clear_utxos(&at).map_err(|e| Error {
            code: ErrorCode::ServerError(1),
            message: "Unable to query clear UTxOs".to_string(),
            data: Some(format!("{:?}", e).into()),
        })
    }

    fn get_create_expire_time(&self) -> Result<u64> {
        let api = self.client.runtime_api();
        let at = BlockId::hash(self.client.info().finalized_hash);
        api.get_create_expire_time(&at).map_err(|e| Error {
            code: ErrorCode::ServerError(1),
            message: "Unable to query create expire time".to_string(),
            data: Some(format!("{:?}", e).into()),
        })
    }
}
