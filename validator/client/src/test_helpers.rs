use crate::KeyMgrSigner;
use std::sync::Arc;
use tpfs_krypt::{
    config::{KeyManagerConfig, KryptConfig},
    from_config, InMemoryKeyManagerConfig, KeyManagement, KeyType,
};

pub fn create_key_manager(initial_keys: Vec<(KeyType, String)>) -> Box<dyn KeyManagement> {
    let config = KryptConfig {
        key_manager_config: KeyManagerConfig::InMemoryKeyManager(InMemoryKeyManagerConfig {
            initial_keys,
        }),
    };
    from_config(config).unwrap()
}

pub fn create_key_signer() -> KeyMgrSigner {
    let key_manager = create_key_manager(get_initial_keys());
    KeyMgrSigner(Arc::from(key_manager))
}

pub fn get_initial_keys() -> Vec<(KeyType, String)> {
    let paths = vec!["Biggie", "2Pac", "Xand", "Trust"];

    let mut keys: Vec<(KeyType, String)> = paths
        .iter()
        .map(|path| {
            (
                KeyType::SubstrateSr25519,
                format!("{}//{}", super::DEV_PHRASE, path),
            )
        })
        .collect();

    keys.push((KeyType::SubstrateSr25519, String::from(super::DEV_PHRASE)));
    keys
}
