use crate::BlockHash;

#[logging_event]
pub enum LoggingEvent {
    UsingConfidentialityMode(bool),
    SubmittingExtrinsic(String),
    Info(String),
    FinalityTimedOut(BlockHash),
}
