//! Provides a mock version of the substrate client. Ideally, we could export some "test crate"
//! but that's not a thing in rust, so it may make sense to extract this to it's own crate, but
//! that feels pretty heavy handed just to avoid a dep on pseudo.
//!
//! Pseudo itself is also very boilerplate-y and hasn't been updated in years, but unfortunately
//! neither `mockall` nor `simulacrum` work well at all with async stuff. Simulacrum looked
//! promising at its "mid-level" control, but I ran into problems using the macro.

use crate::{
    extrinsic_states::TransactionStatus, sr25519::Public, Call, ConfidentialClientInterface,
    Result, SubmittedExtrinsic, SubstrateClientErrors, SubstrateClientInterface,
    UncheckedExtrinsic, UnsignedExtrinsic,
};
use async_trait::async_trait;
use futures::{stream::BoxStream, StreamExt};
use pseudo::Mock;
use xand_address::Address;
use xand_ledger::{
    ClearTransactionOutput, CorrelationId, CreateRequestRecord, RedeemRequestRecord,
};
use xand_models::{hash_substrate_encodeable, EncryptionKey};
use xand_runtime_models::CidrBlock;
use xandstrate_runtime::{
    opaque::Header,
    validator_emissions::models::{
        validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    xandstrate::xandstrate_api::ProposalData,
    AccountId, Hash, SignedBlock,
};

pub struct MockSubstrateClient {
    pub mock_get_cidr_blocks_per_address: Mock<(), Result<Vec<(Address, CidrBlock)>>>,
    pub mock_get_confidential_pending_creates:
        Mock<(), Result<Vec<(CorrelationId, CreateRequestRecord)>>>,
    pub mock_get_confidential_pending_redeems:
        Mock<(), Result<Vec<(CorrelationId, RedeemRequestRecord)>>>,
    pub mock_clear_utxos: Mock<(), Result<Vec<ClearTransactionOutput>>>,
    pub mock_get_proposal: Mock<u32, Result<ProposalData<Address, u32>>>,
    pub mock_get_all_proposals: Mock<(), Result<Vec<ProposalData<Address, u32>>>>,
    pub mock_get_trustee: Mock<(), Result<Address>>,
    pub mock_get_limited_agent: Mock<(), Result<Option<Address>>>,
    pub mock_make_signable: Mock<(Public, Call), Result<UnsignedExtrinsic>>,
    pub mock_get_total_issuance: Mock<(), Result<u64>>,
    // This one deviates from the trait signature since it would be very onerous to construct
    // a real stream when setting expectations
    pub mock_submit_and_watch_extrinsic:
        Mock<UncheckedExtrinsic, Vec<Result<TransactionStatus, SubstrateClientErrors>>>,
    pub mock_next_acct_correlation_id: Mock<AccountId, Result<u32, SubstrateClientErrors>>,
    pub mock_get_members: Mock<(), Result<Vec<Address>>>,
    pub mock_get_banned_members: Mock<(), Result<Vec<Address>>>,
    pub mock_get_authority_keys: Mock<(), Result<Vec<Address>>>,
    pub mock_get_encryption_key: Mock<Public, Result<EncryptionKey>>,
    pub mock_finalized_heads: Mock<(), Result<Vec<Result<Header>>>>,
    pub mock_get_block: Mock<Hash, Result<SignedBlock>>,
    pub mock_get_validator_emission_rate: Mock<(), Result<ValidatorEmissionRate>>,
    pub mock_get_validator_emission_progress: Mock<Public, Result<ValidatorEmissionProgress>>,
    pub mock_get_pending_create_expire_time: Mock<(), Result<u64>>,
}

impl Default for MockSubstrateClient {
    fn default() -> Self {
        MockSubstrateClient {
            mock_get_cidr_blocks_per_address: Mock::new(Ok(vec![])),
            mock_get_confidential_pending_creates: Mock::new(Ok(vec![])),
            mock_get_confidential_pending_redeems: Mock::new(Ok(vec![])),
            mock_clear_utxos: Mock::new(Ok(Vec::new())),
            mock_get_proposal: Mock::new(Err(SubstrateClientErrors::ProposalNotFound)),
            mock_get_all_proposals: Mock::new(Ok(Vec::new())),
            mock_get_trustee: Mock::new(Err(SubstrateClientErrors::NoResponse)),
            mock_get_limited_agent: Mock::new(Ok(None)),
            mock_make_signable: Mock::new(Err(SubstrateClientErrors::NoExtrinsic)),
            mock_get_total_issuance: Mock::new(Ok(0)),
            mock_submit_and_watch_extrinsic: Mock::new(vec![]),
            mock_next_acct_correlation_id: Mock::new(Ok(0)),
            mock_get_members: Mock::new(Err(SubstrateClientErrors::NoResponse)),
            mock_get_banned_members: Mock::new(Err(SubstrateClientErrors::NoResponse)),
            mock_get_authority_keys: Mock::new(Ok(Vec::new())),
            mock_get_encryption_key: Mock::new(Err(SubstrateClientErrors::NoResponse)),
            mock_finalized_heads: Mock::new(Ok(vec![])),
            mock_get_block: Mock::new(Err(SubstrateClientErrors::NoResponse)),
            mock_get_validator_emission_rate: Mock::new(Ok(ValidatorEmissionRate::default())),
            mock_get_validator_emission_progress: Mock::new(Ok(
                ValidatorEmissionProgress::default(),
            )),
            mock_get_pending_create_expire_time: Mock::new(Ok(0)),
        }
    }
}

#[async_trait]
impl SubstrateClientInterface for MockSubstrateClient {
    async fn get_allowlisted_cidr_blocks_per_address(&self) -> Result<Vec<(Address, CidrBlock)>> {
        self.mock_get_cidr_blocks_per_address.call(())
    }

    async fn get_proposal(
        &self,
        id: u32,
    ) -> Result<ProposalData<Address, u32>, SubstrateClientErrors> {
        self.mock_get_proposal.call(id)
    }

    async fn get_all_proposals(
        &self,
    ) -> Result<Vec<ProposalData<Address, u32>>, SubstrateClientErrors> {
        self.mock_get_all_proposals.call(())
    }

    async fn get_trustee(&self) -> Result<Address, SubstrateClientErrors> {
        self.mock_get_trustee.call(())
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, SubstrateClientErrors> {
        self.mock_get_limited_agent.call(())
    }

    async fn make_signable(&self, signer: &Public, call: Call) -> Result<UnsignedExtrinsic> {
        self.mock_make_signable.call((*signer, call))
    }

    async fn make_unsignable(
        &self,
        _call: Call,
    ) -> Result<UncheckedExtrinsic, SubstrateClientErrors> {
        unimplemented!()
    }

    async fn get_total_issuance(&self) -> Result<u64> {
        self.mock_get_total_issuance.call(())
    }

    async fn submit_and_watch_extrinsic(
        &self,
        extrinsic: &UncheckedExtrinsic,
    ) -> Result<SubmittedExtrinsic> {
        let ext_hash = hash_substrate_encodeable(extrinsic);
        let res = self.mock_submit_and_watch_extrinsic.call(extrinsic.clone());
        // Turn vec into stream
        let stream = futures::stream::iter(res.into_iter());
        Ok(SubmittedExtrinsic::new(ext_hash, stream.boxed()))
    }

    async fn query_next_account_nonce(
        &self,
        account_id: &AccountId,
    ) -> Result<u32, SubstrateClientErrors> {
        self.mock_next_acct_correlation_id.call(account_id.clone())
    }

    async fn get_members(&self) -> Result<Vec<Address>> {
        self.mock_get_members.call(())
    }

    async fn get_banned_members(&self) -> Result<Vec<Address>> {
        self.mock_get_banned_members.call(())
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>> {
        self.mock_get_authority_keys.call(())
    }

    async fn get_encryption_key(
        &self,
        addr: &Public,
    ) -> Result<EncryptionKey, SubstrateClientErrors> {
        self.mock_get_encryption_key.call(*addr)
    }

    async fn subscribe_finalized_heads(&self) -> Result<BoxStream<'static, Result<Header>>> {
        let replies = self.mock_finalized_heads.call(());
        Ok(futures::stream::iter(replies?.into_iter()).boxed())
    }

    async fn get_block(&self, block_hash: &Hash) -> Result<SignedBlock> {
        self.mock_get_block.call(*block_hash)
    }

    async fn get_validator_emission_rate(&self) -> Result<ValidatorEmissionRate> {
        self.mock_get_validator_emission_rate.call(())
    }

    async fn get_validator_emission_progress(
        &self,
        addr: &Public,
    ) -> Result<ValidatorEmissionProgress, SubstrateClientErrors> {
        self.mock_get_validator_emission_progress.call(*addr)
    }
}

#[async_trait]
impl ConfidentialClientInterface for MockSubstrateClient {
    async fn get_confidential_pending_creates(
        &self,
    ) -> Result<Vec<(CorrelationId, CreateRequestRecord)>, SubstrateClientErrors> {
        self.mock_get_confidential_pending_creates.call(())
    }

    async fn get_confidential_pending_redeems(
        &self,
    ) -> Result<Vec<([u8; 16], RedeemRequestRecord)>, SubstrateClientErrors> {
        self.mock_get_confidential_pending_redeems.call(())
    }

    async fn get_clear_utxos(&self) -> Result<Vec<ClearTransactionOutput>> {
        self.mock_clear_utxos.call(())
    }

    async fn get_pending_create_expire_time(&self) -> Result<u64> {
        self.mock_get_pending_create_expire_time.call(())
    }
}
