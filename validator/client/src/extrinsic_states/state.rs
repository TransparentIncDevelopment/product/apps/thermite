use crate::{logging_events::LoggingEvent, BlockHash, ExtrinsicHash, SubstrateClientErrors};
use derive_more::Constructor;
use futures::{
    future::{err, ok},
    stream::BoxStream,
    FutureExt, Stream, StreamExt, TryFutureExt, TryStreamExt,
};
use serde::Deserialize;
use std::{
    fmt::{Debug, Formatter},
    pin::Pin,
    task::{Context, Poll},
};
use xandstrate_runtime::{Runtime, SystemEvent};

pub type TransactionStatus = sp_transaction_pool::TransactionStatus<ExtrinsicHash, BlockHash>;

/// Groups a TX ID (its hash) and the corresponding stream of updates after submission
pub struct SubmittedExtrinsic {
    /// The hash of the extrinsic, used to uniquely identify a TX
    pub hash: ExtrinsicHash,
    /// The stream of updates for this TX after submission
    updates_stream: BoxStream<'static, Result<TransactionStatus, SubstrateClientErrors>>,
}

impl Debug for SubmittedExtrinsic {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "SubmittedExtrinsic(hash: {:?})", self.hash,)
    }
}

impl Stream for SubmittedExtrinsic {
    type Item = Result<TransactionStatus, SubstrateClientErrors>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        self.updates_stream.as_mut().poll_next(cx)
    }
}

impl SubmittedExtrinsic {
    pub fn new(
        hash: ExtrinsicHash,
        updates_stream: BoxStream<'static, Result<TransactionStatus, SubstrateClientErrors>>,
    ) -> Self {
        SubmittedExtrinsic {
            hash,
            updates_stream,
        }
    }

    /// Consume this struct and get the raw underlying stream
    pub fn get_stream(
        self,
    ) -> BoxStream<'static, Result<TransactionStatus, SubstrateClientErrors>> {
        self.updates_stream
    }

    /// Watch stream from transaction until it is committed. The future will be
    /// resolved with the block hash the extrinsic was committed in.
    pub async fn wait_until_committed(self) -> Result<CommittedExtrinsic, SubstrateClientErrors> {
        let ext_hash = self.hash;

        self.get_stream()
            .try_filter_map(|status| match status {
                TransactionStatus::Future
                | TransactionStatus::Ready
                | TransactionStatus::Broadcast(_)
                | TransactionStatus::Retracted(_)
                | TransactionStatus::Usurped(_)
                | TransactionStatus::Dropped => ok(None),
                TransactionStatus::FinalityTimeout(block) => {
                    warn!(LoggingEvent::FinalityTimedOut(block));
                    err(SubstrateClientErrors::FinalityTimedOut { block })
                }
                TransactionStatus::Finalized(block) | TransactionStatus::InBlock(block) => {
                    ok(Some(block))
                }
                TransactionStatus::Invalid => {
                    warn!(LoggingEvent::Info("Invalid extrinsic".into()));
                    err(SubstrateClientErrors::ExtrinsicInvalid)
                }
            })
            .into_future()
            .map(|(res, _)| res.unwrap_or(Err(SubstrateClientErrors::NoExtrinsic)))
            .map_ok(move |block_hash| CommittedExtrinsic::new(ext_hash, block_hash))
            .await
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Constructor, Serialize, Deserialize)]
pub struct CommittedExtrinsic {
    /// The has of the extrinsic, used to uniquely identify a TX
    pub(super) hash: ExtrinsicHash,
    /// The block hash this was finalized in
    pub(super) committed_in_block: BlockHash,
}

impl CommittedExtrinsic {
    /// Getter for extrinsic hash
    pub fn get_extrinsic_hash(&self) -> &ExtrinsicHash {
        &self.hash
    }

    /// Getter for finalized block hash
    pub fn get_finalized_block_hash(&self) -> &BlockHash {
        &self.committed_in_block
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ErroredExtrinsic {
    pub hash: ExtrinsicHash,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct InvalidExtrinsic {
    pub hash: ExtrinsicHash,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CommittedWithSystemEvent {
    pub(super) ext: CommittedExtrinsic,
    pub(super) sys_event: SystemEvent<Runtime>,
}

impl CommittedWithSystemEvent {
    /// Returns if extrinsic application was successful based on its SystemEvent
    pub fn was_success(&self) -> bool {
        matches!(self.sys_event, SystemEvent::<Runtime>::ExtrinsicSuccess(_))
    }
}
