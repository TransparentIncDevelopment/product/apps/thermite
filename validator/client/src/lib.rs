#![forbid(unsafe_code)]

#[macro_use]
extern crate tpfs_logger_port;

mod block_view;
mod events;
mod logging_events;
mod signing;
mod xandstrate_rpc;

pub mod confidentiality_client;
pub mod extrinsic_states;
pub mod mock;
pub mod test_helpers;

pub use crate::{
    events::SubstrateEventProvider,
    extrinsic_states::{CommittedExtrinsic, SubmittedExtrinsic},
    logging_events::LoggingEvent,
    signing::KeyMgrSigner,
    xandstrate_rpc::XandstrateRpcApi,
};
pub use extrinsic_builder::*;
pub use parity_codec;
pub use sp_core::{
    crypto::{PublicError, Ss58Codec, DEV_PHRASE as OTHER_DEV_PHRASE},
    ed25519, sr25519,
    storage::StorageKey,
    twox_128, Bytes, Pair,
};
pub use xand_models;
pub use xandstrate_runtime::{
    extrinsic_builder,
    network_voting::ProposalStage,
    opaque::{get_pubkey_from_ext_addr, get_signer_pubkey_from_extrinsic, Header, SessionKeys},
    AccountId, Block, Call, Hash, ProposalData, SignedBlock, UncheckedExtrinsic, XandCall,
    XandValidatorsCall,
};

use crate::{
    block_view::BlockView, confidentiality_client::ConfidentialityClient,
    xandstrate_rpc::XandstrateClient,
};
use async_trait::async_trait;
use frame_support::inherent::Extrinsic;
use futures::{
    compat::Stream01CompatExt,
    future::{err, ok, ready},
    stream::BoxStream,
    FutureExt, SinkExt, Stream, StreamExt, TryFutureExt, TryStreamExt,
};
use futures_01::{Future as _, Stream as _};
use jsonrpc_client_transports::{RpcChannel, RpcError as JsonRpcError};
use jsonrpc_core::MetaIoHandler;
use jsonrpc_core_client::transports::{local::LocalRpc, ws as parity_ws};
use once_cell::sync::OnceCell;
use parity_codec::{Decode, Encode};
use sc_rpc_api::{author::AuthorClient, chain::ChainClient, state::StateClient};
use snafu::Snafu;
use sp_rpc::list::ListOrValue;
use sp_runtime::traits::NumberFor;
use std::{fmt::Debug, io, sync::Arc, time::Duration};
use substrate_frame_rpc_system::SystemClient as FrameSystemClient;
use tokio::sync::RwLock;
use tokio_compat::runtime::{Runtime as CompatRuntime, TaskExecutor};
use tpfs_krypt::errors::KeyManagementError;
use url::Url;
use xand_address::Address;
use xand_ledger::{
    ClearTransactionOutput, CorrelationId, CreateRequestRecord, RedeemRequestRecord,
};
use xand_models::{hash_substrate_encodeable, EncryptionKey, ToAddress, TransactionId};
use xand_runtime_models::CidrBlock;
use xandstrate_runtime::validator_emissions::models::{
    validator_emission_progress::ValidatorEmissionProgress,
    validator_emission_rate::ValidatorEmissionRate,
};
use xandstrate_runtime::*;

pub type Result<T, E = SubstrateClientErrors> = std::result::Result<T, E>;

#[async_trait]
pub trait SubstrateClientInterface: Send + Sync {
    /// Get a list of the CIDR blocks allowlisted per address for communication on the network
    async fn get_allowlisted_cidr_blocks_per_address(&self) -> Result<Vec<(Address, CidrBlock)>>;
    /// Get data on specific Proposal by id
    async fn get_proposal(&self, id: u32) -> Result<ProposalData<Address, u32>>;
    /// Get data on specific Proposal by id
    async fn get_all_proposals(&self) -> Result<Vec<ProposalData<Address, u32>>>;
    /// Get Trustee Public Address
    async fn get_trustee(&self) -> Result<Address>;
    /// Get the Limited Agent Public Address if it exists
    async fn get_limited_agent(&self) -> Result<Option<Address>>;
    // TODO: Provide combined make_signable&submit function
    /// Given a `Call` (the inner part of an extrinsic), make it submittable by fetching the current
    /// account index, and attaching the genesis hash. It still needs to be signed before
    /// submission!
    async fn make_signable(
        &self,
        submitter: &sr25519::Public,
        call: Call,
    ) -> Result<UnsignedExtrinsic, SubstrateClientErrors>;
    /// Make an `UncheckedExtrinsic` for a `Call` which is not meant to be signed in the same way
    /// as those produced by `make_signable`. The only intended use case for submitting unsigned
    /// extrinsics is for confidential financial transactions which use their own internal
    /// signing scheme.
    async fn make_unsignable(
        &self,
        call: Call,
    ) -> Result<UncheckedExtrinsic, SubstrateClientErrors>;

    /// Fetch the total issued amount of claims
    async fn get_total_issuance(&self) -> Result<u64>;

    /// Submit an extrinsic and watch it, returning a stream that contains all updates about
    /// the submission.
    async fn submit_and_watch_extrinsic(
        &self,
        extrinsic: &UncheckedExtrinsic,
    ) -> Result<SubmittedExtrinsic>;

    /// Submits JSON RPC request to get next account Nonce for a given AccountId. Nodes
    /// return the appropriate account index given their submitted blocks, and transactions
    /// currently in the pool. Note that calling this while submitting transactions at the same
    /// time can result in a race where you might get a stale index.
    async fn query_next_account_nonce(
        &self,
        account_id: &AccountId,
    ) -> Result<Index, SubstrateClientErrors>;

    /// Get list of members
    async fn get_members(&self) -> Result<Vec<Address>>;

    /// Get list of banned member as of latest committed block
    async fn get_banned_members(&self) -> Result<Vec<Address>>;

    /// Get list of authority keys (validators)
    async fn get_authority_keys(&self) -> Result<Vec<Address>>;

    /// Get the encryption key for an account
    async fn get_encryption_key(&self, addr: &sr25519::Public) -> Result<EncryptionKey>;

    /// Get a list of the CIDR blocks allowlisted for communication on the network
    async fn get_allowlisted_cidr_blocks(&self) -> Result<Vec<CidrBlock>> {
        let blocks = self
            .get_allowlisted_cidr_blocks_per_address()
            .await?
            .into_iter()
            .map(|(_, block)| block)
            .collect();
        Ok(blocks)
    }

    /// Subscribe to finalized block headers.
    ///
    /// The return type is a result, which may fail if the initial connection fails or there is
    /// some other problem subscribing to the stream. After that point, errors are internal to the
    /// stream.
    async fn subscribe_finalized_heads(
        &self,
    ) -> Result<BoxStream<'static, Result<Header, SubstrateClientErrors>>, SubstrateClientErrors>;

    /// Fetch a full block from the chain given a block hash
    async fn get_block(&self, block_hash: &Hash) -> Result<SignedBlock, SubstrateClientErrors>;

    /// Get the current validator emission rate from storage
    async fn get_validator_emission_rate(
        &self,
    ) -> Result<ValidatorEmissionRate, SubstrateClientErrors>;

    /// Get the current validator emission progress from storage
    async fn get_validator_emission_progress(
        &self,
        addr: &sr25519::Public,
    ) -> Result<ValidatorEmissionProgress, SubstrateClientErrors>;
}

#[async_trait]
pub trait ConfidentialClientInterface {
    async fn get_confidential_pending_creates(
        &self,
    ) -> Result<Vec<(CorrelationId, CreateRequestRecord)>, SubstrateClientErrors>;

    async fn get_confidential_pending_redeems(
        &self,
    ) -> Result<Vec<(CorrelationId, RedeemRequestRecord)>, SubstrateClientErrors>;
    async fn get_clear_utxos(&self) -> Result<Vec<ClearTransactionOutput>>;
    async fn get_pending_create_expire_time(&self) -> Result<u64>;
}

pub struct SubstrateClient {
    /// URL of substrate websocket server (if using a websocket)
    url: Option<Url>,

    /// Cached genesis hash - the genesis hash can't change for the lifetime of a given connection.
    /// (Really, the lifetime of a given network).
    genesis_hash: OnceCell<Hash>,

    /// RPC clients
    clients: RwLock<InternalClients>,

    /// tokio_compat runtime
    compat_runtime: Option<CompatRuntime>,
}

impl Drop for SubstrateClient {
    fn drop(&mut self) {
        // The compat runtime panics if dropped in an `async` context, so move it to a separate
        // thread to die.
        let rt = self.compat_runtime.take();
        std::thread::spawn(|| drop(rt)).join().unwrap();
    }
}

struct InternalClients {
    /// Author RPC client
    author_client: AuthorClient<Hash, BlockHash>,

    /// Generated Block RPC Client
    chain_client: ChainClient<NumberFor<Block>, Hash, Header, SignedBlock>,

    /// State RPC client
    state_client: StateClient<Hash>,

    /// Frame system RPC client
    frame_system_client: FrameSystemClient<BlockHash, AccountId, Index>,

    /// Confidentiality RPC client
    confidentiality_client: ConfidentialityClient,

    /// Xandstrate RPC client
    xandstrate_client: XandstrateClient,
}

impl InternalClients {
    async fn connect(executor: TaskExecutor, url: &Url) -> Result<Self, SubstrateClientErrors> {
        let channel: RpcChannel = executor
            .spawn_handle(parity_ws::connect(url))
            .await?
            .map_err(|_| SubstrateClientErrors::WebsocketDisconnected)?;

        Ok(Self::with_rpc_channel(channel))
    }

    async fn connect_with_retry(
        executor: TaskExecutor,
        url: &Url,
    ) -> Result<Self, SubstrateClientErrors> {
        const TIMEOUT: Duration = Duration::from_secs(120);
        const RETRY_INTERVAL: Duration = Duration::from_secs(1);
        let deadline = tokio::time::Instant::now() + TIMEOUT;
        let mut last_error = None;
        loop {
            let clients = {
                let executor = executor.clone();
                let url = url.clone();
                async move { tokio::time::timeout_at(deadline, Self::connect(executor, &url)).await }
            };
            match executor.spawn_handle_std(clients).await? {
                Ok(Ok(clients)) => break Ok(clients),
                Ok(Err(e)) => {
                    last_error = Some(e);
                }
                Err(_) => {
                    break Err(last_error.unwrap_or(SubstrateClientErrors::WebsocketDisconnected));
                }
            }
            executor
                .spawn_handle_std(async move {
                    tokio::time::delay_until(
                        deadline.min(tokio::time::Instant::now() + RETRY_INTERVAL),
                    )
                    .await
                })
                .await?;
        }
    }

    pub async fn connect_local(
        executor: TaskExecutor,
        handler: Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>>,
    ) -> Result<Self, SubstrateClientErrors> {
        let (channel, client_task) = get_rpc_client(handler);
        executor.spawn(client_task.map_err(|e| {
            log::error!("Error running substrate RPC client: {}", e);
        }));
        Ok(Self::with_rpc_channel(channel))
    }

    fn with_rpc_channel(channel: RpcChannel) -> Self {
        Self {
            author_client: AuthorClient::new(channel.clone()),
            chain_client: ChainClient::new(channel.clone()),
            state_client: StateClient::new(channel.clone()),
            frame_system_client: FrameSystemClient::new(channel.clone()),
            confidentiality_client: ConfidentialityClient::new(channel.clone()),
            xandstrate_client: XandstrateClient::new(channel),
        }
    }
}

#[async_trait]
impl ConfidentialClientInterface for SubstrateClient {
    async fn get_confidential_pending_creates(
        &self,
    ) -> Result<Vec<(CorrelationId, CreateRequestRecord)>, SubstrateClientErrors> {
        Ok(self
            .execute_legacy_future(|clients| {
                clients
                    .confidentiality_client
                    .get_confidential_pending_creates()
            })
            .await?
            .into_iter()
            .map(|(id, record)| (id, record.0))
            .collect())
    }

    async fn get_confidential_pending_redeems(
        &self,
    ) -> Result<Vec<(CorrelationId, RedeemRequestRecord)>, SubstrateClientErrors> {
        Ok(self
            .execute_legacy_future(|clients| {
                clients
                    .confidentiality_client
                    .get_confidential_pending_redeems()
            })
            .await?
            .into_iter()
            .map(|(id, record)| (id, record.0))
            .collect())
    }

    async fn get_clear_utxos(&self) -> Result<Vec<ClearTransactionOutput>> {
        Ok(self
            .execute_legacy_future(|clients| clients.confidentiality_client.get_clear_utxos())
            .await?
            .into_iter()
            .map(|clear_utxo| clear_utxo.0)
            .collect())
    }

    async fn get_pending_create_expire_time(&self) -> Result<u64> {
        Ok(self
            .execute_legacy_future(|clients| {
                clients.confidentiality_client.get_create_expire_time()
            })
            .await?)
    }
}

#[async_trait]
impl SubstrateClientInterface for SubstrateClient {
    async fn get_allowlisted_cidr_blocks_per_address(
        &self,
    ) -> Result<Vec<(Address, CidrBlock)>, SubstrateClientErrors> {
        Ok(self
            .execute_legacy_future(|clients| clients.xandstrate_client.allowlist())
            .await?
            .into_iter()
            .flat_map(|(address, blocks)| {
                blocks
                    .iter()
                    .filter_map(move |&block| Some((address.clone(), block?)))
                    .collect::<Vec<_>>()
            })
            .collect())
    }

    async fn get_proposal(&self, id: u32) -> Result<ProposalData<Address, u32>> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_proposal(id))
            .await?
            .ok_or(SubstrateClientErrors::ProposalNotFound)
    }

    async fn get_all_proposals(&self) -> Result<Vec<ProposalData<Address, u32>>> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_all_proposals())
            .await
    }

    async fn get_trustee(&self) -> Result<Address, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_trustee())
            .await
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_limited_agent())
            .await
    }

    async fn make_signable(
        &self,
        signer: &sr25519::Public,
        call: Call,
    ) -> Result<UnsignedExtrinsic, SubstrateClientErrors> {
        let (genesis_hash, next_account_ix) = self
            .get_genesis_hash_and_account_nonce(signer.0.into())
            .await?;
        Ok(UnsignedExtrinsic::new(
            call,
            *signer,
            next_account_ix,
            genesis_hash,
        ))
    }

    async fn make_unsignable(
        &self,
        call: Call,
    ) -> Result<UncheckedExtrinsic, SubstrateClientErrors> {
        Ok(UncheckedExtrinsic {
            signature: None,
            function: call,
        })
    }

    async fn get_total_issuance(&self) -> Result<u64> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_total_issuance())
            .await
    }

    async fn submit_and_watch_extrinsic(
        &self,
        extrinsic: &UncheckedExtrinsic,
    ) -> Result<SubmittedExtrinsic> {
        let hash = hash_substrate_encodeable(extrinsic);
        let encoded_extrinsic = extrinsic.encode();
        let transaction_id: TransactionId = hash.as_fixed_bytes().into();
        info!(LoggingEvent::SubmittingExtrinsic(format!(
            "id={:?}",
            transaction_id.to_string()
        )));
        let updates = self
            .execute_legacy_future(|clients| {
                clients
                    .author_client
                    .watch_extrinsic(encoded_extrinsic.clone().into())
            })
            .await?;
        let updates = self.wrap_legacy_stream(updates).map_err(Into::into).boxed();
        Ok(SubmittedExtrinsic::new(hash, updates))
    }

    async fn query_next_account_nonce(
        &self,
        account_id: &AccountId,
    ) -> Result<Index, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.frame_system_client.nonce(account_id.clone()))
            .await
    }

    async fn get_members(&self) -> Result<Vec<Address>, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_members())
            .await
    }

    async fn get_banned_members(&self) -> Result<Vec<Address>, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_banned_members())
            .await
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.xandstrate_client.get_authority_keys())
            .await
    }

    async fn get_encryption_key(
        &self,
        addr: &sr25519::Public,
    ) -> Result<EncryptionKey, SubstrateClientErrors> {
        let account_id: AccountId = addr.0.into();
        let address = account_id.to_address();
        self.execute_legacy_future(|clients| {
            clients
                .xandstrate_client
                .get_encryption_key(account_id.clone())
        })
        .await?
        .ok_or(SubstrateClientErrors::EncryptionKeyNotFound { address })
    }

    async fn subscribe_finalized_heads(
        &self,
    ) -> Result<BoxStream<'static, Result<Header, SubstrateClientErrors>>, SubstrateClientErrors>
    {
        Ok(self.subscribe_finalized_heads().await?.boxed())
    }

    async fn get_block(&self, block_hash: &Hash) -> Result<SignedBlock, SubstrateClientErrors> {
        self.get_block(block_hash).await
    }

    async fn get_validator_emission_rate(
        &self,
    ) -> Result<ValidatorEmissionRate, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| {
            clients.xandstrate_client.get_validator_emission_rate()
        })
        .await
    }

    async fn get_validator_emission_progress(
        &self,
        addr: &sr25519::Public,
    ) -> Result<ValidatorEmissionProgress, SubstrateClientErrors> {
        let account_id: AccountId = addr.0.into();
        self.execute_legacy_future(|clients| {
            clients
                .xandstrate_client
                .get_validator_emission_progress(account_id.clone())
        })
        .await?
        .ok_or_else(
            || SubstrateClientErrors::ValidatorEmissionProgressNotFound {
                address: account_id.to_address(),
            },
        )
    }
}

type ExtrinsicHash = Hash;
pub type BlockHash = Hash;

impl SubstrateClient {
    /// Opens a connection to the given url (which should be a websocket endpoint).
    /// Returns an error if the connection fails.
    pub async fn connect(url: Url) -> Result<Self, SubstrateClientErrors> {
        let compat_runtime = CompatRuntime::new().unwrap();
        Ok(SubstrateClient {
            url: Some(url.clone()),
            genesis_hash: OnceCell::new(),
            clients: RwLock::new(
                InternalClients::connect_with_retry(compat_runtime.executor(), &url).await?,
            ),
            compat_runtime: Some(compat_runtime),
        })
    }

    pub async fn connect_local(
        handler: Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>>,
    ) -> Result<Self, SubstrateClientErrors> {
        let compat_runtime = CompatRuntime::new().unwrap();
        Ok(SubstrateClient {
            url: None,
            genesis_hash: OnceCell::new(),
            clients: RwLock::new(
                InternalClients::connect_local(compat_runtime.executor(), handler).await?,
            ),
            compat_runtime: Some(compat_runtime),
        })
    }

    /// Queries the chain for the hash of the genesis block (block number 0)
    pub async fn get_genesis_hash(&self) -> Result<Hash, SubstrateClientErrors> {
        if let Some(gh) = self.genesis_hash.get() {
            return Ok(*gh);
        }
        let hash = match self
            .execute_legacy_future(|clients| {
                let zero = Some(ListOrValue::Value(0u32.into()));
                clients.chain_client.block_hash(zero)
            })
            .await?
        {
            ListOrValue::Value(Some(h)) => Ok(h),
            _ => Err(SubstrateClientErrors::GenesisHashNotFound),
        }?;
        let _ = self.genesis_hash.set(hash);
        Ok(hash)
    }

    /// Submit and extrinsic and immediately return the hash that identifies it, which can be used
    /// to check its status later
    pub async fn submit_extrinsic<E: Extrinsic + Encode + Debug>(
        &self,
        extrinsic: &E,
    ) -> Result<Hash, SubstrateClientErrors> {
        let encoded_extrinsic = extrinsic.encode();
        self.execute_legacy_future(|clients| {
            clients
                .author_client
                .submit_extrinsic(encoded_extrinsic.clone().into())
        })
        .await
    }

    /// Given a storage key, fetch the value and decode it with the provided decoder
    pub async fn get_storage_value<Decoder, T>(
        &self,
        storage_key: StorageKey,
        decoder: Decoder,
        block: Option<BlockHash>,
    ) -> Result<Option<T>, SubstrateClientErrors>
    where
        Decoder: FnOnce(Vec<u8>) -> Result<T, SubstrateClientErrors>,
    {
        self.execute_legacy_future(|clients| {
            clients.state_client.storage(storage_key.clone(), block)
        })
        .await?
        .map(|data| decoder(data.0))
        .transpose()
    }

    /// Fetch system events in storage at given block hash. This will error if the block is too old,
    /// and the validator node is no longer holding onto the events generated during the building of
    /// that specific block
    pub async fn get_events_at(
        &self,
        block_hash: &BlockHash,
    ) -> Result<Vec<EventRecord>, SubstrateClientErrors> {
        let events_key = system_events_storage_key();
        self.get_storage_value(events_key, Self::decode_events, Some(*block_hash))
            .map_ok(|events| events.unwrap_or_default())
            .await
    }

    /// Subscribe to finalized block headers
    pub async fn subscribe_finalized_heads(
        &self,
    ) -> Result<impl Stream<Item = Result<Header, SubstrateClientErrors>>, SubstrateClientErrors>
    {
        let heads = self
            .execute_legacy_future(|clients| clients.chain_client.subscribe_finalized_heads())
            .await?;
        let heads = self.wrap_legacy_stream(heads);
        Ok(heads.map_err(Into::into))
    }

    async fn execute_legacy_future<F, R>(&self, mut f: F) -> Result<R::Item, SubstrateClientErrors>
    where
        F: FnMut(&InternalClients) -> R,
        R: futures_01::Future + Send + 'static,
        R::Item: Send + 'static,
        R::Error: Send + 'static,
        SubstrateClientErrors: From<R::Error>,
    {
        let executor = self.compat_runtime.as_ref().unwrap().executor();
        loop {
            let x = executor.spawn_handle({
                let clients = self.clients.read().await;
                f(&clients)
            });
            match (x.await?, &self.url) {
                (Ok(x), _) => break Ok(x),
                (Err(e), Some(url)) => {
                    let mut clients = self.clients.write().await;
                    *clients = InternalClients::connect_with_retry(executor.clone(), url)
                        .await
                        .map_err(|_| e)?;
                }
                (Err(e), None) => break Err(e.into()),
            }
        }
    }

    /// Wraps a `futures 0.1` stream into a `futures 0.3` stream
    ///
    /// In theory, this could just be `stream.compat()`. In practice, the json-rpc client appends
    /// all responses and subscription notifications to the same queue and if the first item in the
    /// queue cannot be delivered, it does not try to deliver other items in the queue. This can
    /// cause stalls when the processing of notifications relies on an extra RPC request whose
    /// response is not delivered because there are pending notifications that are not delivered
    /// because their processing is blocked on waiting for the response. That's why this function
    /// buffers items to prevent jitter in the notification ordering/frequency from filling up the
    /// json-rpc-client internal queue and this chicken-and-egg delivery issue from happening.
    ///
    /// See [6705](https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6705) for
    /// the issue that prompted this fix.
    fn wrap_legacy_stream<S>(&self, stream: S) -> impl Stream<Item = Result<S::Item, S::Error>>
    where
        S: futures_01::Stream + Send + 'static,
        S::Item: Send + 'static,
        S::Error: Send + 'static,
    {
        let stream = stream.compat().map(Ok);
        let (sender, receiver) = futures::channel::mpsc::channel(100);
        let sender = sender.sink_map_err(|_| ());
        let forward = stream.forward(sender).map(|_| ());
        self.compat_runtime.as_ref().unwrap().spawn_std(forward);
        receiver
    }

    /// Returns a stream of finalized heads that terminates once the specified block has been marked
    /// final
    pub async fn subscribe_heads_until(
        &'_ self,
        bnum: BlockNumber,
    ) -> Result<impl Stream<Item = Result<Header, SubstrateClientErrors>> + '_, SubstrateClientErrors>
    {
        Ok(self
            .subscribe_finalized_heads()
            .await?
            .take_while(move |h| ready(h.as_ref().map(|h| h.number < bnum).unwrap_or(true))))
    }

    /// Fetch a full block from the chain given a block hash
    pub async fn get_block(&self, block_hash: &Hash) -> Result<SignedBlock, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.chain_client.block(Some(*block_hash)))
            .await?
            .ok_or(SubstrateClientErrors::BlockNotFound { block: *block_hash })
    }

    /// Fetch the current finalized block hash from the chain
    pub async fn get_finalized_head(&self) -> Result<BlockHash, SubstrateClientErrors> {
        self.execute_legacy_future(|clients| clients.chain_client.finalized_head())
            .await
    }

    /// Submit an extrinsic, and when committed, query for block events to see if extrinsic apply
    /// succeeded or failed. If the extrinsic is not committed, this will not resolve. If the
    /// ApplyExtrinsic phase succeeds, this will resolve with the block hash it was committed in,
    /// and if the ApplyExtrinsic fails, this will wrap the committed extrinsic in a
    /// [ExtrinsicCommittedButFailed(CommittedExtrinsic)] error.
    pub async fn sub_ext_and_watch_until_success<'a, 'b: 'a>(
        &'b self,
        extrinsic: &UncheckedExtrinsic,
    ) -> Result<BlockHash, SubstrateClientErrors> {
        self.submit_and_watch_extrinsic(extrinsic)
            .await?
            .wait_until_committed()
            .and_then(move |finalized_ext| {
                let block_hash = *finalized_ext.get_finalized_block_hash();
                self.is_extrinsic_success(finalized_ext)
                    .and_then(move |is_success| {
                        if is_success {
                            ok(block_hash)
                        } else {
                            err(SubstrateClientErrors::ExtrinsicCommittedButFailed {
                                ext: finalized_ext,
                            })
                        }
                    })
            })
            .await
    }

    fn decode_events<D: AsRef<[u8]>>(data: D) -> Result<Vec<EventRecord>, SubstrateClientErrors> {
        Vec::<EventRecord>::decode(&mut data.as_ref()).map_err(|_| {
            SubstrateClientErrors::DecodeError {
                struct_type: "Event Record Vec".into(),
            }
        })
    }

    /// Wrapper method to get both the genesis_hash and account_nonce
    async fn get_genesis_hash_and_account_nonce(
        &self,
        account_id: AccountId,
    ) -> Result<(Hash, Index), SubstrateClientErrors> {
        let genesis_hash_fut = self.get_genesis_hash();
        let account_nonce_fut = self.query_next_account_nonce(&account_id);
        let (ghres, anres) = futures::future::join(genesis_hash_fut, account_nonce_fut).await;
        Ok((ghres?, anres?))
    }
}

fn get_rpc_client(
    handler: Arc<MetaIoHandler<sc_rpc::Metadata, sc_rpc_server::RpcMiddleware>>,
) -> (
    RpcChannel,
    impl futures_01::Future<Item = (), Error = JsonRpcError>,
) {
    let (tx, rx) = futures_01::sync::mpsc::channel(10);
    let meta = sc_rpc::Metadata::new(tx);
    let (sink, stream) = LocalRpc::with_metadata(handler, meta).split();
    let stream = stream.select(rx.map_err(|_| {
        JsonRpcError::Other(
            io::Error::new(io::ErrorKind::Other, "RPC channel returned an error").into(),
        )
    }));
    let (rpc_client, sender) = jsonrpc_core_client::transports::duplex(sink, stream);
    (sender, rpc_client)
}

#[async_trait::async_trait]
impl SubstrateEventProvider for SubstrateClient {
    async fn get_events_for(
        &self,
        committed_ext: CommittedExtrinsic,
    ) -> Result<Vec<Event>, SubstrateClientErrors> {
        let ext_hash: ExtrinsicHash = *committed_ext.get_extrinsic_hash();
        let block_hash: BlockHash = *committed_ext.get_finalized_block_hash();

        // First query for block.
        let block = self.get_block(&block_hash).await?;
        let event_vec = self.get_events_at(&block_hash).await?;

        let block_view = BlockView::load(block, event_vec);
        block_view.get_events_for_ext(&ext_hash)
    }

    async fn get_events_for_block(
        &self,
        block_hash: &BlockHash,
    ) -> Result<Vec<Event>, SubstrateClientErrors> {
        let block = self.get_block(block_hash).await?;
        let event_vec = self.get_events_at(block_hash).await?;

        let block_view = BlockView::load(block, event_vec);
        Ok(block_view.all_events().cloned().collect())
    }

    async fn is_extrinsic_success(
        &self,
        committed_ext: CommittedExtrinsic,
    ) -> Result<bool, SubstrateClientErrors> {
        let events = self.get_events_for(committed_ext).await?;
        for e in events {
            // If the extrinsic outright failed, or if the sudo transaction wrapping it says
            // it failed, then it failed.
            match e {
                Event::system(SystemEvent::<Runtime>::ExtrinsicFailed(_, _)) => return Ok(false),
                Event::pallet_sudo(SudoEvent::Sudid(dispatch_res)) => {
                    if dispatch_res.is_err() {
                        return Ok(false);
                    }
                }
                _ => (),
            }
        }
        Ok(true)
    }
}

/// The `BlockSpecifier` type is a qualifier that describes which block to query state at.
pub enum BlockSpecifier {
    /// The latest committed state available on the current validator
    Head,
    /// Resolve the latest finalized head that a validator is aware of and query against that
    LatestFinalized,
    /// The state as of a specific point in time (the block hash), must be contained
    /// within the state pruning window.
    SpecificBlock(BlockHash),
}

#[derive(Debug, Snafu, Clone, Serialize)]
pub enum SubstrateClientErrors {
    #[snafu(display("No meaningful response to RPC call"))]
    NoResponse,
    #[snafu(display("RPC error: {:?}", error))]
    RpcError { error: String },
    #[snafu(display("Error decoding jsonrpc response as a {}!", struct_type))]
    DecodeError { struct_type: String },
    #[snafu(display("No account balance was found, account likely has not been initialized"))]
    BalanceNotFound,
    #[snafu(display("No proposal was found with given id"))]
    ProposalNotFound,
    #[snafu(display("No encryption key was found for the given account: {}", address))]
    EncryptionKeyNotFound { address: Address },
    #[snafu(display("Extrinsic was explicitly rejected as invalid"))]
    ExtrinsicInvalid,
    #[snafu(display("No extrinsic found"))]
    NoExtrinsic,
    #[snafu(display("Extrinsic not found in block. Ext Hash: {:?}", hash))]
    ExtrinsicNotInBlock { hash: ExtrinsicHash },
    #[snafu(display("No system event found for extrinsic index: {}", index))]
    SystemEventNotFoundForExtrinsicIndex { index: u32 },
    #[snafu(display(
        "Extrinsic finalized, but event shows that applying the ext failed: {:?}",
        ext
    ))]
    ExtrinsicCommittedButFailed { ext: CommittedExtrinsic },
    #[snafu(display("Substrate websocket has been disconnected."))]
    WebsocketDisconnected,
    #[snafu(display("{}: {:?}", msg, source))]
    KeyMgmtErr {
        msg: String,
        source: KeyManagementError,
    },
    #[snafu(display("Task spawned on tokio executor failed: {}", reason))]
    SpawnedTaskFailed { reason: String },
    #[snafu(display("Genesis hash not found"))]
    GenesisHashNotFound,
    #[snafu(display("Block with hash {:?} not found", block))]
    BlockNotFound { block: BlockHash },
    #[snafu(display("Finality timed out for block {:?}", block))]
    FinalityTimedOut { block: BlockHash },
    #[snafu(display(
        "No validator emission progress was found for the given account: {}",
        address
    ))]
    ValidatorEmissionProgressNotFound { address: Address },
}

impl From<tokio::task::JoinError> for SubstrateClientErrors {
    fn from(e: tokio::task::JoinError) -> Self {
        SubstrateClientErrors::SpawnedTaskFailed {
            reason: e.to_string(),
        }
    }
}

impl From<jsonrpc_core_client::RpcError> for SubstrateClientErrors {
    fn from(e: jsonrpc_core_client::RpcError) -> Self {
        SubstrateClientErrors::RpcError {
            error: e.to_string(),
        }
    }
}

pub const DEV_PHRASE: &str = OTHER_DEV_PHRASE;
