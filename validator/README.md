<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Xandstrate](#xandstrate)
  - [Running](#running)
  - [Developer tools](#developer-tools)
    - [Polkadot UI](#polkadot-ui)
    - [Substrate Telemetry server](#substrate-telemetry-server)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Xandstrate

Xandstrate is the software that runs our blockchain. It is based on Parity's 
[substrate](https://substrate.dev/) framework. Substrate provides a very
flexible, generic framework for implementing our own "runtime" logic and state
storage. For more information, please check out the site and their tutorials.
This document will focus on some non-obvious things that may not pop out from
their docs.

The structure, and some significant portions of code in this directory are 
based on or taken from either:
https://github.com/paritytech/substrate/tree/master/node-template
or
https://github.com/paritytech/substrate/tree/master/node

You will likely want to refer to the official Rust [reference documentation](https://crates.parity.io/)
once you get into the code.

The substrate "recipes" are also quite valuable: https://substrate.dev/recipes/introduction.html

See this [Confluence page](https://transparentinc.atlassian.net/wiki/spaces/PD/pages/179240998/Parity+Substrate)
 for a smattering of other interesting links.
 
Note that you should build from the root with `cargo make build` for the majority of your building
needs.

To understand more about the runtime specifically, see it's [readme](runtime/README.md).

## Running

If you just want a (3) validator network, run `cargo make multinode` from the root.

For more specific options around running a single validator, see the help output:
```bash
cargo run --bin xandstrate -- --help
```

Multinode also has a handful of options:
```bash
cargo run --bin multinode -- --help
```

Each node has its own directory for persistence located under `/tmp/xandstrate-dev/<node-name>`.
Purge a node's chain by deleting the corresponding directory.
 
## Developer tools

Also see the [Substrate Developer Tools](https://transparentinc.atlassian.net/wiki/spaces/PD/pages/210337793/Substrate+Developer+Tools) page
 and please update it when you find helpful tools.

### Polkadot UI

Substrate provides a chain explorer (mostly) compatible with substrate based chains.
It works with ours generally speaking, but does break sometimes parsing some of our
custom structures. You can use their hosted version [here](https://polkadot.js.org/apps/#/settings),
that link will take you to the settings section, where you can change the endpoint
to point locally or at a hosted instance of our chain.

The chain state section is particularily useful for inspecting the values
of different things in state. The extrinsics section can be used to construct
and submit extrinsics in a user friendly way. If you plan to submit them, you
will need to load some accounts into the UI - use the ones in the `dev-keys`
folder.

Note that you will also want to paste the contents of the `validator/runtime/expected_types.json`
file into the `Developer` section of settings. This file may need to be manually updated
to match any changes in the runtime types.

Opening the browser debug console can be helpful if the UI is misbehaving.
We will eventually need to create our own UI or maintain a fork to avoid issues.

Though we have manually confirmed that the hosted UI does not "phone home"
to Parity, that could change at any time. It is not recommended to point the
Polkadot UI at production instances.

### Substrate Telemetry server

This app gives a live view of each nodes' status per network.
Clone the app with:
```
git clone https://github.com/paritytech/substrate-telemetry.git
cd substrate-telemetry && yarn install
```
Start the backend with:
```bash
yarn start:backend
```
And, in another terminal, start the frontend
```bash
yarn start:frontend
```

Configure a node to forward its telemetry to this app by adding the `--telemetry-url` flag.
```bash
cargo run --bin xandstrate -- --key //Xand --name Xand --validator --chain dev --telemetry-url ws://localhost:1024
```

