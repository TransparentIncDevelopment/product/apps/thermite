extern crate proc_macro;

use proc_macro2::TokenStream;
use quote::{quote, ToTokens};
use std::fmt::{self, Display};
use syn::DeriveInput;

#[proc_macro_derive(ErrorHelper)]
pub fn derive_error_helper(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    derive(input)
        .unwrap_or_else(|e| e.to_compile_error())
        .into()
}

fn derive(input: proc_macro::TokenStream) -> Result<TokenStream, syn::Error> {
    let ast = syn::parse::<DeriveInput>(input)?;
    let variants = match &ast.data {
        syn::Data::Enum(e) => Ok(&e.variants),
        _ => Err(Error::ExpectedEnum.with_tokens(&ast)),
    }?;
    let arms = variants
        .iter()
        .enumerate()
        .map(|(i, v)| {
            let id = v.ident.to_string();
            quote! { #i => #id, }
        })
        .collect::<Vec<_>>();
    let output = quote! {
        pub fn err_ix_to_str(i: u8) -> &'static str {
            match usize::from(i) {
                #(#arms)*
                _ => "Unknown",
            }
        }
    };
    Ok(output)
}

#[derive(Debug)]
enum Error {
    ExpectedEnum,
}

impl Error {
    fn with_tokens<T: ToTokens>(self, tokens: T) -> syn::Error {
        syn::Error::new_spanned(tokens, self)
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::ExpectedEnum => f.write_str("ErrorHelper can only be derived for enum types"),
        }
    }
}
