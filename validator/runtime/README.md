<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Xandstrate Runtime](#xandstrate-runtime)
  - [Runtime structure](#runtime-structure)
    - [Understanding how state is stored](#understanding-how-state-is-stored)
  - [Testing](#testing)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Xandstrate Runtime

The xandstrate runtime code defines the core logic of our blockchain

## Runtime structure

The following link is heavily recommended reading and should answer any
questions you may have about the architecture of substrate runtimes:

https://substrate.dev/docs/en/knowledgebase/runtime/

### Understanding how state is stored

Runtime modules, as you have just read in substrate docs (you read them, right?),
can define their own storage. What's that? You didn't read it? Check out
[this](https://substrate.dev/docs/en/knowledgebase/runtime/storage) part again.

There are a few very important takeaways that are worth restating:

* Substrate can store any value that can be encoded with the [Parity SCALE codec](https://substrate.dev/docs/en/knowledgebase/advanced/codec).
* Everything is stored in a [RocksDB](https://rocksdb.org/) key-value store
  * Importantly, this means every value is accessible via some key.
  The methodolgy for determining this key for various storage values is
  described [here](https://substrate.dev/docs/en/knowledgebase/advanced/storage#storage-value-keys).
* When you fetch a value (directly from the database or storage RPC), what
  you'll get back is some binary data which is encoded accoring to the SCALE codec.
  In order to decode it, you will need to know the type of the data you are
  trying to decode, which you should already know since you're querying for
  something specific. By far the easiest way to do this is to use our actual
  Rust structs (or those from substrate) which define the value. Such structs
  will necessarily have `#[derive(Encode, Decode)]` annotations, and hence
  a `decode` method (from the SCALE library) which you can use to decode the
  binary data. This is the way our RPC client works. Decoding such payloads
  manually is an enormous pain - so you should generally try doing it from
  our code first.

## Testing

Normal unit tests can be run with `cargo make test` from the root.
