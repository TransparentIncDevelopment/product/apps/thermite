use crate::network_voting::{Config, PropIndex, VoteStatus};
use crate::DispatchError;
use frame_support::dispatch::DispatchResult;

pub trait VotingPermissions<T: Config, P> {
    type Error: Into<DispatchError>;

    /// Return `true` if the `proposer` is allowed to make proposals, `false` if not
    fn can_propose(
        proposer: &<T as frame_system::Config>::AccountId,
        proposal: &P,
    ) -> Result<bool, Self::Error>;

    /// Return `true` if the `voter` is allowed to vote, `false` if not
    fn can_vote(
        voter: &<T as frame_system::Config>::AccountId,
        proposal: &P,
    ) -> Result<bool, Self::Error>;
}

pub trait ProposalStatusCheck<T: Config> {
    type Error: Into<DispatchError>;

    /// Return the state of the voting as `VoteStatus` enum for proposal with id `proposal_id`
    ///  `Open` if result isn't decided
    ///  `Failed` if voting came back against proposal
    ///  `Succeeded` if voting is in favor of proposal
    /// Implementors determine the qualifications for each
    ///
    /// ## IMPORTANT:
    /// A non-`Open` vote status may actually change in the future. For example, a proposal to add
    /// a member or validator, when passed, may now return `Open` for it's vote status, even though
    /// the proposal is finished! Vote status should be considered an _internal detail_ of your
    /// voting rules implementation. It's job is to indicate when a proposal should be accepted or
    /// rejected, and after that should be considered useless information.
    fn vote_status(proposal_id: PropIndex) -> Result<VoteStatus, Self::Error>;
}

pub trait VoteSubmissionLifecycle<T: Config> {
    /// Defines additional behavior that is called before a vote is submitted. This code can error,
    /// thus cancelling the vote.
    fn before_vote_submitted(
        _voter: &<T as frame_system::Config>::AccountId,
        _prop_id: PropIndex,
        _vote: bool,
    ) -> DispatchResult {
        Ok(())
    }

    /// Defines additional behavior that is called after a vote is submitted. This code can not error.
    fn after_vote_submitted(
        _voter: &<T as frame_system::Config>::AccountId,
        _prop_id: PropIndex,
        _vote: bool,
    ) {
    }
}
