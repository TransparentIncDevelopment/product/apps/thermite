// Annoying, should be scoped to the decl_storage! macro, but that does not work.
#![allow(clippy::string_lit_as_bytes)]
// For decl_module
#![allow(clippy::boxed_local)]
#![allow(clippy::unused_unit)]

pub mod adapters;
#[cfg(test)]
mod tests;

use frame_system::{self, ensure_signed};
use sp_runtime::traits::Dispatchable;
use sp_std::prelude::Box;

pub use xand_runtime_models::ProposalStage;

use crate::{
    network_voting::adapters::{ProposalStatusCheck, VoteSubmissionLifecycle, VotingPermissions},
    DEFAULT_WEIGHT,
};

pub use pallet::*;

#[frame_support::pallet]
pub mod pallet {
    use super::*;
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;
    use xandstrate_error_derive::ErrorHelper;

    #[pallet::config]
    pub trait Config: frame_system::Config {
        type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
        type Proposal: Parameter + Dispatchable<Origin = Self::Origin> + Sized;
        type VotingPermissions: VotingPermissions<Self, Self::Proposal>;
        type ProposalStatusCheck: ProposalStatusCheck<Self>;
        type Lifecycle: VoteSubmissionLifecycle<Self>;
    }

    #[pallet::pallet]
    #[pallet::generate_store(pub trait Store)]
    pub struct Pallet<T>(PhantomData<T>);

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {
        fn on_initialize(n: T::BlockNumber) -> Weight {
            Pallet::<T>::remove_expired_proposals(n);
            0
        }
    }

    #[pallet::call]
    impl<T: Config> Pallet<T> {
        /// Submit a proposal for administrative action
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn submit_proposal(
            origin: OriginFor<T>,
            proposal: Box<T::Proposal>,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::submit_proposal_tx(origin, *proposal)
        }

        /// Vote yes (true) or no (false) on specific proposal `idx`
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn vote_proposal(
            origin: OriginFor<T>,
            idx: PropIndex,
            vote: bool,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::vote_proposal_tx(origin, idx, vote)
        }
    }

    #[pallet::event]
    #[pallet::generate_deposit(pub (super) fn deposit_event)]
    pub enum Event<T: Config> {
        Proposed(PropIndex),
        VoteCast(T::AccountId),
        Accepted(PropIndex),
        Invalid(PropIndex),
        Rejected(PropIndex),
        Expired(PropIndex),
    }

    #[pallet::error]
    #[derive(ErrorHelper)]
    pub enum Error<T> {
        AccountNotPermittedToMakeProposal,
        AccountNotPermittedToVoteOnProposal,
        ProposalNotFound,
        ProposalNoLongerOpenForVoting,
        AccountRoleNotDefined,
        CallNotValidProposal,
    }

    #[pallet::storage]
    #[pallet::getter(fn proposals)]
    pub type Proposals<T: Config> =
        StorageMap<_, Blake2_128Concat, PropIndex, (T::Proposal, T::AccountId, T::BlockNumber)>;

    #[pallet::storage]
    #[pallet::getter(fn proposal_state)]
    pub type ProposalState<T: Config> =
        StorageMap<_, Blake2_128Concat, PropIndex, ProposalStage, ValueQuery>;

    #[pallet::type_value]
    pub fn DefaultForProposalCount() -> PropIndex {
        0
    }

    #[pallet::storage]
    #[pallet::getter(fn proposal_count)]
    pub type ProposalCount<T: Config> =
        StorageValue<_, PropIndex, ValueQuery, DefaultForProposalCount>;

    #[pallet::type_value]
    pub fn DefaultForVotingPeriod<T: Config>() -> T::BlockNumber {
        T::BlockNumber::from(432_000u32)
    }

    #[pallet::storage]
    #[pallet::getter(fn voting_period)]
    pub type VotingPeriod<T: Config> =
        StorageValue<_, T::BlockNumber, ValueQuery, DefaultForVotingPeriod<T>>;

    #[pallet::storage]
    #[pallet::getter(fn votes)]
    pub type Votes<T: Config> = StorageDoubleMap<
        _,
        Blake2_128Concat,
        PropIndex,
        Blake2_128Concat,
        T::AccountId,
        bool,
        ValueQuery,
    >;

    #[pallet::genesis_config]
    pub struct GenesisConfig<T: Config> {
        pub voting_period: T::BlockNumber,
    }

    #[cfg(feature = "std")]
    impl<T: Config> Default for GenesisConfig<T> {
        fn default() -> Self {
            Self {
                voting_period: T::BlockNumber::from(432_000u32),
            }
        }
    }

    #[pallet::genesis_build]
    impl<T: Config> GenesisBuild<T> for GenesisConfig<T> {
        fn build(&self) {
            {
                let data = &self.voting_period;
                let v: &T::BlockNumber = data;
                <VotingPeriod<T> as frame_support::storage::StorageValue<T::BlockNumber>>::put::<
                    &T::BlockNumber,
                >(v);
            }
        }
    }
}

pub type PropIndex = u32;

/// Represents a _just calculated_ status of votes on a proposal. This value should be considered
/// meaningless for proposals that are in any stage other than `Proposed`. See
/// [VotingRules::vote_status] for more.
#[derive(Debug, PartialEq, Eq)]
pub enum VoteStatus {
    Open,
    Rejected,
    Accepted,
}

impl<T: Config> Pallet<T> {
    fn remove_expired_proposals(current_block: T::BlockNumber) {
        fn expire_proposal<T: Config>(prop_id: PropIndex) {
            Proposals::<T>::remove(prop_id);
            ProposalState::<T>::remove(prop_id);
        }

        for maybe_prop in Proposals::<T>::iter() {
            let (prop_id, (_, _, expires)) = maybe_prop;
            if current_block >= expires {
                expire_proposal::<T>(prop_id);
                Self::deposit_event(Event::Expired(prop_id));
            }
        }
    }

    fn submit_proposal_tx(
        origin: T::Origin,
        proposal: T::Proposal,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        fn create_proposal<T: Config>(
            prop_idx: PropIndex,
            proposal: <T as Config>::Proposal,
            sender: <T as frame_system::Config>::AccountId,
        ) {
            let expires: T::BlockNumber = Pallet::<T>::expiration_block_from_now();
            // add proposal to mapping of propidx => proposal
            Proposals::<T>::insert(prop_idx, (proposal, sender, expires));
            ProposalState::<T>::insert(prop_idx, ProposalStage::Proposed);
            Pallet::<T>::deposit_event(Event::Proposed(prop_idx));
        }

        let sender = ensure_signed(origin)?;
        Self::ensure_can_propose(&sender, &proposal)?;
        let prop_idx = Self::new_proposal_id();
        create_proposal::<T>(prop_idx, proposal.clone(), sender.clone());
        // cast initial vote
        if <T as Config>::VotingPermissions::can_vote(&sender, &proposal).map_err(|e| e.into())? {
            Self::inner_vote_proposal(sender, prop_idx, true)?;
        };
        Ok(().into())
    }

    fn vote_proposal_tx(
        origin: T::Origin,
        idx: PropIndex,
        vote: bool,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let sender = ensure_signed(origin)?;
        Self::ensure_can_vote_from_idx(&sender, idx)?;
        Self::ensure_proposal_exists(idx)?;
        Self::ensure_proposal_open(idx)?;
        Self::inner_vote_proposal(sender, idx, vote)
    }

    fn inner_vote_proposal(
        sender: T::AccountId,
        prop_id: PropIndex,
        vote: bool,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        fn reject_proposal<T: Config>(prop_id: PropIndex) {
            ProposalState::<T>::insert(prop_id, ProposalStage::Rejected);
        }

        if let Some((proposal, _, _)) = Proposals::<T>::get(prop_id) {
            // Set vote if voting still open
            <T as Config>::Lifecycle::before_vote_submitted(&sender, prop_id, vote)?;
            Votes::<T>::insert(prop_id, sender.clone(), vote);
            <T as Config>::Lifecycle::after_vote_submitted(&sender, prop_id, vote);

            let vote_status =
                <T as Config>::ProposalStatusCheck::vote_status(prop_id).map_err(|e| e.into())?;
            match vote_status {
                VoteStatus::Open => {}
                VoteStatus::Rejected => {
                    reject_proposal::<T>(prop_id);
                    Self::deposit_event(Event::Rejected(prop_id));
                }
                VoteStatus::Accepted => Self::execute_proposal(proposal, prop_id),
            }
        }
        Ok(().into())
    }

    fn execute_proposal(proposal: T::Proposal, idx: PropIndex) {
        fn accept_proposal<T: Config>(idx: PropIndex) {
            ProposalState::<T>::insert(idx, ProposalStage::Accepted);
        }

        fn fail_proposal<T: Config>(idx: PropIndex) {
            ProposalState::<T>::insert(idx, ProposalStage::Invalid);
        }

        let result = proposal.dispatch(frame_system::RawOrigin::Root.into());
        if result.is_ok() {
            accept_proposal::<T>(idx);
            Self::deposit_event(Event::Accepted(idx));
        } else {
            fail_proposal::<T>(idx);
            Self::deposit_event(Event::Invalid(idx));
        }
    }

    fn ensure_can_propose(
        sender: &T::AccountId,
        proposal: &T::Proposal,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        if <T as Config>::VotingPermissions::can_propose(sender, proposal).map_err(|e| e.into())? {
            Ok(().into())
        } else {
            Err(Error::<T>::AccountNotPermittedToMakeProposal.into())
        }
    }

    fn ensure_can_vote(
        sender: &T::AccountId,
        proposal: &T::Proposal,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        if <T as Config>::VotingPermissions::can_vote(sender, proposal).map_err(|e| e.into())? {
            Ok(().into())
        } else {
            Err(Error::<T>::AccountNotPermittedToVoteOnProposal.into())
        }
    }

    fn ensure_can_vote_from_idx(
        sender: &T::AccountId,
        idx: PropIndex,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let proposal = Proposals::<T>::get(idx);
        match proposal {
            Some((proposal, _, _)) => Self::ensure_can_vote(sender, &proposal),
            None => Err(Error::<T>::ProposalNotFound.into()),
        }
    }

    fn ensure_proposal_exists(
        idx: PropIndex,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        if Proposals::<T>::contains_key(idx) {
            Ok(().into())
        } else {
            Err(Error::<T>::ProposalNotFound.into())
        }
    }

    fn ensure_proposal_open(idx: PropIndex) -> frame_support::dispatch::DispatchResultWithPostInfo {
        if ProposalState::<T>::get(idx) == ProposalStage::Proposed {
            Ok(().into())
        } else {
            Err(Error::<T>::ProposalNoLongerOpenForVoting.into())
        }
    }

    fn new_proposal_id() -> PropIndex {
        let prop_idx = ProposalCount::<T>::get();
        // Increment proposal count
        ProposalCount::<T>::put(prop_idx + 1);
        prop_idx
    }

    fn expiration_block_from_now() -> T::BlockNumber {
        let now = <frame_system::Module<T>>::block_number();
        now + Self::voting_period()
    }
}
