use frame_support::{
    parameter_types,
    traits::{OnFinalize, OnInitialize},
};
use sp_core::H256;
use sp_runtime::{testing::Header, traits::IdentityLookup, Perbill};

use crate::DispatchError;

use super::*;

mod dummy_module;

#[macro_use]
mod new_environment_for_rules;

pub type AccountId = u64;
pub type BlockNumber = u64;
pub type BoolResult = Result<bool, DispatchError>;
pub type VoteStatusResult = Result<VoteStatus, DispatchError>;

const ACC_1: AccountId = 10;
const ACC_2: AccountId = 20;
const NOT_ACC: AccountId = 40;

new_environment_for_rules!(can_propose_env, CanProposeRules);

pub struct CanProposeRules;

impl VotingPermissions<can_propose_env::Runtime, can_propose_env::Call> for CanProposeRules {
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<can_propose_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_propose_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<can_propose_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_propose_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}
impl VoteSubmissionLifecycle<can_propose_env::Runtime> for CanProposeRules {}
impl ProposalStatusCheck<can_propose_env::Runtime> for CanProposeRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(can_not_propose_env, CanNotProposeRules);

pub struct CanNotProposeRules;

impl VotingPermissions<can_not_propose_env::Runtime, can_not_propose_env::Call>
    for CanNotProposeRules
{
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<can_not_propose_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_not_propose_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(false)
    }

    fn can_vote(
        _voter: &<can_not_propose_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_not_propose_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}

impl VoteSubmissionLifecycle<can_not_propose_env::Runtime> for CanNotProposeRules {}
impl ProposalStatusCheck<can_not_propose_env::Runtime> for CanNotProposeRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(can_vote_env, CanVoteRules);

pub struct CanVoteRules;

impl VotingPermissions<can_vote_env::Runtime, can_vote_env::Call> for CanVoteRules {
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<can_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<can_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}

impl VoteSubmissionLifecycle<can_vote_env::Runtime> for CanVoteRules {}
impl ProposalStatusCheck<can_vote_env::Runtime> for CanVoteRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(can_not_vote_env, CanNotVoteRules);

pub struct CanNotVoteRules;

impl VotingPermissions<can_not_vote_env::Runtime, can_not_vote_env::Call> for CanNotVoteRules {
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<can_not_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_not_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<can_not_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_not_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(false)
    }
}

impl VoteSubmissionLifecycle<can_not_vote_env::Runtime> for CanNotVoteRules {}
impl ProposalStatusCheck<can_not_vote_env::Runtime> for CanNotVoteRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(open_vote_env, OpenVoteRules);

pub struct OpenVoteRules;

impl VotingPermissions<open_vote_env::Runtime, open_vote_env::Call> for OpenVoteRules {
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<open_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<open_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<open_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<open_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}

impl VoteSubmissionLifecycle<open_vote_env::Runtime> for OpenVoteRules {}
impl ProposalStatusCheck<open_vote_env::Runtime> for OpenVoteRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(failed_vote_env, FailedVoteRules);

pub struct FailedVoteRules;

impl VotingPermissions<failed_vote_env::Runtime, failed_vote_env::Call> for FailedVoteRules {
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<failed_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<failed_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<failed_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<failed_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}

impl VoteSubmissionLifecycle<failed_vote_env::Runtime> for FailedVoteRules {}
impl ProposalStatusCheck<failed_vote_env::Runtime> for FailedVoteRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Rejected)
    }
}

new_environment_for_rules!(succeeded_vote_env, SucceededVoteRules);

pub struct SucceededVoteRules;

impl VotingPermissions<succeeded_vote_env::Runtime, succeeded_vote_env::Call>
    for SucceededVoteRules
{
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<succeeded_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<succeeded_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<succeeded_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<succeeded_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}

impl VoteSubmissionLifecycle<succeeded_vote_env::Runtime> for SucceededVoteRules {}
impl ProposalStatusCheck<succeeded_vote_env::Runtime> for SucceededVoteRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Accepted)
    }
}

new_environment_for_rules!(can_vote_error_env, CanVoteErrorRules);

pub struct CanVoteErrorRules;
const CAN_VOTE_ERROR_RULES_ERROR_RETURN: DispatchError =
    DispatchError::Other("Error during can_vote");

impl VotingPermissions<can_vote_error_env::Runtime, can_vote_error_env::Call>
    for CanVoteErrorRules
{
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<can_vote_error_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_vote_error_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<can_vote_error_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_vote_error_env::Runtime as Config>::Proposal,
    ) -> Result<bool, Self::Error> {
        Err(CAN_VOTE_ERROR_RULES_ERROR_RETURN)
    }
}
impl VoteSubmissionLifecycle<can_vote_error_env::Runtime> for CanVoteErrorRules {}
impl ProposalStatusCheck<can_vote_error_env::Runtime> for CanVoteErrorRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(can_propose_error_env, CanProposeErrorRules);

pub struct CanProposeErrorRules;
const CAN_PROPOSE_ERROR_RULES_ERROR_RETURN: DispatchError =
    DispatchError::Other("Error during can_propose");

impl VotingPermissions<can_propose_error_env::Runtime, can_propose_error_env::Call>
    for CanProposeErrorRules
{
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<can_propose_error_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_propose_error_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Err(CAN_PROPOSE_ERROR_RULES_ERROR_RETURN)
    }

    fn can_vote(
        _voter: &<can_propose_error_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<can_propose_error_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}
impl VoteSubmissionLifecycle<can_propose_error_env::Runtime> for CanProposeErrorRules {}
impl ProposalStatusCheck<can_propose_error_env::Runtime> for CanProposeErrorRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Ok(VoteStatus::Open)
    }
}

new_environment_for_rules!(error_vote_env, ErrorVoteRules);

pub struct ErrorVoteRules;
const ERROR_VOTE_RULES_ERROR_RETURN: DispatchError =
    DispatchError::Other("Error during vote_status");

impl VotingPermissions<error_vote_env::Runtime, error_vote_env::Call> for ErrorVoteRules {
    type Error = DispatchError;

    fn can_propose(
        _proposer: &<error_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<error_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }

    fn can_vote(
        _voter: &<error_vote_env::Runtime as frame_system::Config>::AccountId,
        _proposal: &<error_vote_env::Runtime as Config>::Proposal,
    ) -> BoolResult {
        Ok(true)
    }
}

impl VoteSubmissionLifecycle<error_vote_env::Runtime> for ErrorVoteRules {}
impl ProposalStatusCheck<error_vote_env::Runtime> for ErrorVoteRules {
    type Error = DispatchError;

    fn vote_status(_prop_id: u32) -> VoteStatusResult {
        Err(ERROR_VOTE_RULES_ERROR_RETURN)
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod cv_tests {
    use super::*;

    #[test]
    fn submit_proposal__can_submit_proposal() {
        use can_propose_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            Pallet::submit_proposal(Origin::signed(ACC_1), proposal).unwrap();
            assert_eq!(ProposalCount::<Runtime>::get(), 1);
            assert!(Proposals::<Runtime>::contains_key(0));
            assert!(ProposalState::<Runtime>::get(0) == ProposalStage::Proposed)
        })
    }

    #[test]
    fn submit_proposal__unqualified_can_not_submit_proposal() {
        use can_not_propose_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            let res = Pallet::submit_proposal(Origin::signed(NOT_ACC), proposal);
            assert!(res.is_err());
        })
    }

    #[test]
    fn submit_proposal__error_in_can_propose_returns_expected_dispatch_error() {
        use can_propose_error_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            let err = Pallet::submit_proposal(Origin::signed(NOT_ACC), proposal)
                .unwrap_err()
                .error;
            assert_eq!(err, CAN_PROPOSE_ERROR_RULES_ERROR_RETURN);
        })
    }

    #[test]
    fn vote_proposal__can_vote_on_proposal() {
        use can_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            Pallet::submit_proposal(Origin::signed(ACC_1), proposal).unwrap();
            let res = Pallet::vote_proposal(Origin::signed(ACC_2), 0, false);
            assert!(res.is_ok());
        })
    }

    #[test]
    fn submit_proposal__error_in_can_vote_returns_expected_dispatch_error() {
        use can_vote_error_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            let err = Pallet::submit_proposal(Origin::signed(ACC_1), proposal)
                .unwrap_err()
                .error;
            assert_eq!(err, CAN_VOTE_ERROR_RULES_ERROR_RETURN)
        })
    }

    #[test]
    fn vote_proposal__unqualified_can_not_vote_on_proposal() {
        use can_not_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            Pallet::submit_proposal(Origin::signed(ACC_1), proposal).unwrap();
            let res = Pallet::vote_proposal(Origin::signed(NOT_ACC), 0, false);
            assert!(res.is_err());
        })
    }

    #[test]
    fn submit_proposal__error_in_vote_status_returns_expected_dispatch_error() {
        use error_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            let err = Pallet::submit_proposal(Origin::signed(ACC_1), proposal)
                .unwrap_err()
                .error;
            assert_eq!(err, ERROR_VOTE_RULES_ERROR_RETURN);
        })
    }

    #[test]
    fn submit_proposal__error_in_can_propose_does_not_add_proposal() {
        use can_propose_error_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            let err = Pallet::submit_proposal(Origin::signed(NOT_ACC), proposal)
                .unwrap_err()
                .error;
            assert_eq!(err, CAN_PROPOSE_ERROR_RULES_ERROR_RETURN);
            assert_eq!(ProposalCount::<Runtime>::get(), 0);
        })
    }

    /// This test mirrors the restrictions on the LimitedAgent.
    /// If an entity can propose but not vote, their proposal does not include
    /// the auto vote on behalf of the proposer
    #[test]
    fn submit_proposal__can_propose_but_cannot_vote_doesnt_include_initial_vote() {
        // Given adapters of { can_propose => true, can_vote => false }
        use can_not_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            // When
            let submit_proposal_res = Pallet::submit_proposal(Origin::signed(ACC_1), proposal);

            // Then proposal submission is Ok and no associated votes
            submit_proposal_res.unwrap();
            assert!(!Votes::<Runtime>::contains_key(0, ACC_1));
        })
    }

    #[test]
    fn submit_proposal__proposal_state_set_to_accepted_when_accepted() {
        use succeeded_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let value = 100;
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(value));
            let proposal = Box::new(inner_ext_fun);
            Pallet::submit_proposal(Origin::signed(ACC_1), proposal).unwrap();
            assert_eq!(dummy_module::Something::<Runtime>::get(), value);
            assert!(ProposalState::<Runtime>::get(0) == ProposalStage::Accepted)
        })
    }

    #[test]
    fn submit_proposal__proposal_state_set_to_invalid_when_invalid() {
        use succeeded_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::failing_txn());
            let proposal = Box::new(inner_ext_fun);
            Pallet::submit_proposal(Origin::signed(ACC_1), proposal).unwrap();
            assert!(ProposalState::<Runtime>::get(0) == ProposalStage::Invalid)
        })
    }

    #[test]
    fn submit_proposal__proposal_state_set_to_rejected_when_rejected() {
        use failed_vote_env::{Call as EnvCall, *};
        base_test_ext(1000).execute_with(|| {
            let value = 100;
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(value));
            let proposal = Box::new(inner_ext_fun);
            Pallet::submit_proposal(Origin::signed(ACC_1), proposal).unwrap();
            // The tx doesn't execute (doesn't equal value in `inner_ext_fun`)
            assert_ne!(dummy_module::Something::<Runtime>::get(), value);
            assert!(ProposalState::<Runtime>::get(0) == ProposalStage::Rejected)
        })
    }

    #[test]
    fn submit_proposal__proposal_removed_on_expiration() {
        use open_vote_env::{Call as EnvCall, *};
        let expiration = 0;

        fn run_to_block(n: u64) {
            while <frame_system::Module<Runtime>>::block_number() <= n {
                Pallet::on_finalize(<frame_system::Module<Runtime>>::block_number());
                <frame_system::Module<Runtime>>::set_block_number(
                    <frame_system::Module<Runtime>>::block_number() + 1,
                );
                Pallet::on_initialize(<frame_system::Module<Runtime>>::block_number());
            }
        }

        base_test_ext(expiration).execute_with(|| {
            let inner_ext_fun = EnvCall::Dummy(dummy_module::Call::root_change_something(100));
            let proposal = Box::new(inner_ext_fun);
            let _ = Pallet::submit_proposal(Origin::signed(ACC_1), proposal);
            run_to_block(expiration + 1);
            assert!(!Proposals::<Runtime>::contains_key(0));
        })
    }
}
