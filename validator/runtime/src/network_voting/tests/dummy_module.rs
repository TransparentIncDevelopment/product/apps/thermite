//! Use this dummy module and its set of transactions to test proposals. Proposals must
//! be able to call transactions from external modules. This module allows tests to be written
//! for that functionality without depending on actual external code.

use crate::DEFAULT_WEIGHT;
use frame_support::{dispatch::DispatchResultWithPostInfo, fail};
use frame_system::{self as system, ensure_root};
use parity_scale_codec::{Decode, Encode};

pub use pallet::*;

#[allow(dead_code)] // `call`, `deposit_event`, and `something` are never called
#[frame_support::pallet]
pub mod pallet {
    use super::*;
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;

    #[pallet::pallet]
    #[pallet::generate_store(pub trait Store)]
    pub struct Pallet<T>(PhantomData<T>);

    #[pallet::config]
    pub trait Config: frame_system::Config {
        type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
    }

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {}

    #[pallet::call]
    impl<T: Config> Pallet<T> {
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn root_change_something(
            origin: OriginFor<T>,
            value: u32,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::root_change_something_tx(origin, value)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn failing_txn(_origin: OriginFor<T>) -> DispatchResultWithPostInfo {
            fail!("This should fail!")
        }
    }

    #[pallet::event]
    #[pallet::generate_deposit(pub(super) fn deposit_event)]
    pub enum Event<T> {}

    #[pallet::type_value]
    pub fn DefaultForSomething() -> u32 {
        0
    }

    #[pallet::storage]
    #[pallet::getter(fn something)]
    pub type Something<T: Config> = StorageValue<_, u32, ValueQuery, DefaultForSomething>;
}

impl<T: Config> Pallet<T> {
    fn root_change_something_tx(origin: T::Origin, value: u32) -> DispatchResultWithPostInfo {
        ensure_root(origin)?;
        Something::<T>::put(value);
        Ok(().into())
    }
}

#[derive(Encode, Decode, Copy, Clone, PartialEq, Eq, Debug)]
pub enum DummyTransactionFailure {
    MustBeRoot,
}

mod tests {
    use super::*;
    use crate::network_voting::tests::dummy_module as dummy_pallet;
    use frame_support::{
        assert_ok, parameter_types,
        weights::constants::{RocksDbWeight, WEIGHT_PER_SECOND},
    };
    use sp_runtime::{
        testing::{Header, H256},
        traits::IdentityLookup,
        Perbill,
    };

    type UncheckedExtrinsic = frame_system::mocking::MockUncheckedExtrinsic<TestRuntime>;
    type Block = frame_system::mocking::MockBlock<TestRuntime>;
    type TestPallet = Pallet<TestRuntime>;

    // NOTE: If you add any new traits that the module uses, they must be "implemented" here
    frame_support::construct_runtime!(
        pub enum TestRuntime where
            Block = Block,
            NodeBlock = Block,
            UncheckedExtrinsic = UncheckedExtrinsic,
        {
            System: frame_system::{Module, Call, Config, Storage, Event<T>},
            Dummy: dummy_pallet::{Module, Call, Storage},
        }
    );

    pub type AccountId = u64;
    pub type BlockNumber = u64;

    const NORMAL_DISPATCH_RATIO: Perbill = Perbill::from_percent(75);

    parameter_types! {
        pub const BlockHashCount: u64 = 250;
        pub BlockWeights: frame_system::limits::BlockWeights = frame_system::limits::BlockWeights
            ::with_sensible_defaults(2 * WEIGHT_PER_SECOND, NORMAL_DISPATCH_RATIO);
        pub BlockLength: frame_system::limits::BlockLength = frame_system::limits::BlockLength
            ::max_with_normal_ratio(5 * 1024 * 1024, NORMAL_DISPATCH_RATIO);
        pub const SS58Prefix: u8 = 42;
    }

    impl frame_system::Config for TestRuntime {
        type BaseCallFilter = ();
        type BlockWeights = BlockWeights;
        type BlockLength = BlockLength;
        type Origin = Origin;
        type Call = Call;
        type Index = u64;
        type BlockNumber = BlockNumber;
        type Hash = H256;
        type Hashing = sp_runtime::traits::BlakeTwo256;
        type AccountId = AccountId;
        type Lookup = IdentityLookup<Self::AccountId>;
        type Header = Header;
        type Event = ();
        type BlockHashCount = BlockHashCount;
        type DbWeight = RocksDbWeight;
        type Version = ();
        type PalletInfo = PalletInfo;
        type AccountData = ();
        type OnNewAccount = ();
        type OnKilledAccount = ();
        type SystemWeightInfo = ();
        type SS58Prefix = SS58Prefix;
    }

    impl crate::network_voting::tests::dummy_module::Config for TestRuntime {
        type Event = ();
    }

    // A genesis with nothing set
    fn new_test_ext() -> sp_io::TestExternalities {
        system::GenesisConfig::default()
            .build_storage::<TestRuntime>()
            .unwrap()
            .into()
    }

    #[test]
    fn non_root_can_not_set_something() {
        new_test_ext().execute_with(|| {
            let signer = 1;
            let value = 100;
            let res = TestPallet::root_change_something(Origin::signed(signer), value);
            assert!(res.is_err());
        })
    }

    #[test]
    fn root_can_set_something() {
        new_test_ext().execute_with(|| {
            let value = 100;
            let res = TestPallet::root_change_something(Origin::root(), value);
            assert_ok!(res);
            assert_eq!(Something::<TestRuntime>::get(), value);
        })
    }

    #[test]
    fn failing_txn_fails_for_root() {
        new_test_ext().execute_with(|| {
            let res = TestPallet::failing_txn(Origin::root());
            assert!(res.is_err());
        })
    }
}
