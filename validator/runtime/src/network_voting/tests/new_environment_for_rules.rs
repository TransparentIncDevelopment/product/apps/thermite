/// This Macro is used to generate a Runtime for a specific VotingRules implementation.
/// It is necessary if you want to test with different voting rules for different tests.
/// Theoretically, you could make the runtime generic for `Runtime<V: VotingRules>`, but
/// the substrate macro magic makes it difficult to create a generic runtime like that.
/// Instead, behind the scenes, this macro will generate a new module with a Runtime that uses
/// your specified `impl VotingRules`.
/// If you instantiate your new environment as `my_test_env`, you can either import it at the
/// top level, or inside the scope of a function or test:
/// ```
/// use xandstrate_runtime::network_voting::{GovernanceVotingRulesAdapter, VoteStatus};
///
/// new_environment_for_rules(my_test_env, MyRules);
///
/// pub struct MyRules;
///
/// impl GovernanceVotingRulesAdapter<can_propose_env::Runtime> for MyRules {
///     fn can_propose(_proposer: &<can_propose_env::Runtime as frame_system::Config>::AccountId) -> bool {
///         true
///     }
///
///     fn can_vote(_voter: &<can_propose_env::Runtime as frame_system::Config>::AccountId) -> bool {
///         true
///     }
///
///     fn vote_status(_prop_id: u32) -> VoteStatus {
///         VoteStatus::Open
///     }
/// }
///
/// #[test]
/// fn this_could_be_your_test() {
///     use my_test_env::{*, Call as EnvCall};
///     my_test_env::base_test_ext.execute_with(|| {});
/// }
/// ```
macro_rules! new_environment_for_rules {
    ($mod_name:ident, $rules:ident) => {
        mod $mod_name {
            use crate::network_voting;
            use super::{Pallet as NetworkVotingPallet, *};
            use frame_support::{traits::GenesisBuild, weights::constants::{WEIGHT_PER_SECOND, RocksDbWeight}};

            pub type Pallet = NetworkVotingPallet<Runtime>;

            type UncheckedExtrinsic = frame_system::mocking::MockUncheckedExtrinsic<Runtime>;
            type Block = frame_system::mocking::MockBlock<Runtime>;

            frame_support::construct_runtime! {
                pub enum Runtime where
                    Block = Block,
                    NodeBlock = Block,
                    UncheckedExtrinsic = UncheckedExtrinsic,
                {
                    System: frame_system::{Module, Call, Config, Storage, Event<T>},
                    Dummy: dummy_module::{Module, Call, Storage},
                    NetworkVoting: network_voting::{Module, Call, Storage, Event<T>, Config<T>},
                }
            }

            const NORMAL_DISPATCH_RATIO: Perbill = Perbill::from_percent(75);
            parameter_types! {
                pub const BlockHashCount: u64 = 250;
                pub BlockWeights: frame_system::limits::BlockWeights = frame_system::limits::BlockWeights
                    ::with_sensible_defaults(2 * WEIGHT_PER_SECOND, NORMAL_DISPATCH_RATIO);
                pub BlockLength: frame_system::limits::BlockLength = frame_system::limits::BlockLength
                    ::max_with_normal_ratio(5 * 1024 * 1024, NORMAL_DISPATCH_RATIO);
                pub const SS58Prefix: u8 = 42;
            }
            impl frame_system::Config for Runtime {
                type BaseCallFilter = ();
                type BlockWeights = BlockWeights;
                type BlockLength = BlockLength;
                type Origin = Origin;
                type Call = Call;
                type Index = u64;
                type BlockNumber = BlockNumber;
                type Hash = H256;
                type Hashing = sp_runtime::traits::BlakeTwo256;
                type AccountId = AccountId;
                type Lookup = IdentityLookup<Self::AccountId>;
                type Header = Header;
                type Event = ();
                type BlockHashCount = BlockHashCount;
                type DbWeight = RocksDbWeight;
                type Version = ();
                type PalletInfo = PalletInfo;
                type AccountData = ();
                type OnNewAccount = ();
                type OnKilledAccount = ();
                type SystemWeightInfo = ();
                type SS58Prefix = SS58Prefix;
            }

            impl crate::network_voting::Config for Runtime {
                type Event = ();
                type Proposal = Call;
                type VotingPermissions = $rules;
                type ProposalStatusCheck = $rules;
                type Lifecycle = $rules;
            }

            impl dummy_module::Config for Runtime {
                type Event = ();
            }

            // A genesis with a defined `voting_period`
            pub(crate) fn base_test_ext(voting_period: BlockNumber) -> sp_io::TestExternalities {
                let mut gen_config = frame_system::GenesisConfig::default()
                    .build_storage::<Runtime>()
                    .unwrap();
                crate::network_voting::GenesisConfig::<Runtime> { voting_period }
                    .assimilate_storage(&mut gen_config)
                    .unwrap();
                gen_config.into()
            }
        }
    };
}
