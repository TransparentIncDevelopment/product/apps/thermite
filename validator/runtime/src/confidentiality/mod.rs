#![allow(clippy::unused_unit)]

pub mod api;
#[cfg(any(feature = "runtime-benchmarks", test))]
pub mod benchmarking;
pub(crate) mod ledger_adapters;
pub mod reward;

pub mod runtime_interface;
#[cfg(any(feature = "runtime-benchmarks", test))]
pub mod test_constants;
#[cfg(any(feature = "runtime-benchmarks", test))]
pub(crate) mod testing_and_benchmarking_shared;
#[cfg(test)]
pub(crate) mod tests;

use self::ledger_adapters::total_issuance_repo::TotalIssuanceRepo;

use crate::confidentiality::ledger_adapters::correlation_id_repo::CorrelationIdRepo;
use crate::confidentiality::ledger_adapters::redeem_repo::RedeemRepo;
use crate::confidentiality::ledger_adapters::NewPublicKey;
use crate::confidentiality::runtime_interface::CryptoImpl;
use crate::{
    confidentiality::ledger_adapters::{
        clear_utxo_repo::ClearUtxoRepo,
        create_repo::CreateRequestRepo,
        identity_tag_repo::IdentityTagRepo,
        key_image_repo::KeyImageRepo,
        member_repo::{NewIdentityTag, XandstrateMemberRepo},
        transaction_output_repo::TransactionOutputRepo,
        trust_metadata_repo::TrustMetadataRepo,
        validator_repo::XandstrateValidatorRepo,
    },
    DEFAULT_WEIGHT,
};
use crate::{fixup_err_msg, TransactionSource, TransactionValidity};
use core::cmp::PartialEq;
use frame_support::dispatch::DispatchError;
use frame_support::{debug, ensure, traits::UnfilteredDispatchable, weights::Weight};
use frame_system::{ensure_root, ensure_signed, RawOrigin};
use serde_scale_wrap::Wrap;
use sp_core::sp_std::convert::{TryFrom, TryInto};
use sp_std::{marker::PhantomData, vec::Vec};
use xand_ledger::{
    CashConfirmationHandler, CashConfirmationTransaction, ClearRedeemRequestHandler,
    ClearRedeemRequestTransaction, ClearTransactionOutput, CreateCancellationHandler,
    CreateCancellationTransaction, CreateRequestHandler, CreateRequestProcessor,
    CreateRequestRecord, CreateRequestRepository, CreateRequestTransaction, FailureReason,
    IdentityTag, KeyImage, LedgerTransaction, RedeemCancellationHandler,
    RedeemCancellationTransaction, RedeemFulfillmentHandler, RedeemFulfillmentTransaction,
    RedeemRequestHandler, RedeemRequestProcessor, RedeemRequestRecord, RedeemRequestRepository,
    RedeemRequestTransaction, RewardProcessor, RewardTransaction, RewardTransactionHandler,
    SendClaimsHandler, SendClaimsProcessor, SendClaimsTransaction, TransactionOutput,
    TransactionResult, TxoStatus,
};
use xand_runtime_models::{BannedState, FiatReqCorrelationId};

use xand_ledger::verifiable_encryption::EncryptionVerificationError;
use xand_ledger::{TransactionValidationError, ZkPlmtError};

pub use pallet::*;

#[frame_support::pallet]
pub mod pallet {
    use super::*;
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;
    use xandstrate_error_derive::ErrorHelper;

    #[pallet::config]
    pub trait Config: frame_system::Config + pallet_timestamp::Config {
        type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
        type XandstrateState: XandstrateStateTrait<Self>;
    }

    #[pallet::pallet]
    #[pallet::generate_store(pub trait Store)]
    pub struct Pallet<T>(PhantomData<T>);

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T>
    where
        T::BlockNumber: Into<u64>,
    {
        fn on_initialize(bnum: BlockNumberFor<T>) -> Weight {
            Pallet::<T>::on_initialize_impl(bnum)
        }

        fn on_finalize(_bnum: BlockNumberFor<T>) {
            Pallet::<T>::on_finalize_impl()
        }
    }

    #[pallet::call]
    impl<T: Config> Pallet<T>
    where
        T::BlockNumber: Into<u64>,
    {
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn request_create(
            origin: OriginFor<T>,
            input: WrapCreateRequestTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::request_create_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn cancel_pending_create(
            origin: OriginFor<T>,
            input: WrapCreateCancellationTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::cancel_pending_create_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn confirm_create(
            origin: OriginFor<T>,
            input: WrapCashConfirmationTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::confirm_create_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn send(
            origin: OriginFor<T>,
            input: WrapSendTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::send_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn redeem(
            origin: OriginFor<T>,
            input: WrapRedeemTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::redeem_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn redeem_fulfill(
            origin: OriginFor<T>,
            input: WrapRedeemFulfillmentTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::redeem_fulfill_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn cancel_pending_redeem(
            origin: OriginFor<T>,
            input: WrapRedeemCancellationTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::cancel_pending_redeem_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn reward(
            origin: OriginFor<T>,
            input: WrapRewardTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::reward_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn validator_redeem(
            origin: OriginFor<T>,
            input: WrapClearRedeemTransaction,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::validator_redeem_tx(origin, input.0)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn set_pending_create_expire_time(
            origin: OriginFor<T>,
            new_val: T::Moment,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::set_pending_create_expire_time_tx(origin, new_val)
        }
    }

    #[pallet::event]
    #[pallet::generate_deposit(pub (super) fn deposit_event)]
    pub enum Event<T> {
        CreateCancelled(
            FiatReqCorrelationId,
            Wrap<xand_ledger::CreateCancellationReason>,
        ),
    }

    #[pallet::error]
    #[derive(ErrorHelper)]
    pub enum Error<T> {
        CreateAmountMustBeMoreThanZero,
        NoMatchingCreateCorrelationIdToFulfill,
        NoMatchingCreateCorrelationIdToCancel,
        NoMatchingRedeemCorrelationIdToFulfill,
        NoMatchingRedeemCorrelationIdToCancel,
        OnlyTrustMayFulfillCreate,
        KeyImageAlreadyUsed,
        TransactionCouldNotBeVerifiedRangeProofCommitmentMismatch,
        TransactionCouldNotBeVerifiedOpenedCommitmentMismatch,
        TransactionCouldNotBeVerifiedInvalidBulletProof,
        TransactionCouldNotBeVerifiedInvalidAlpha,
        TransactionCouldNotBeVerifiedInvalidSignature,
        TransactionCouldNotBeVerifiedInvalidInputSet,
        TransactionCouldNotBeVerifiedInvalidEncryptionVerificationError,
        TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorTupleSizeMismatch,
        TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorTupleSizeZero,
        TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorEmptyTupleList,
        TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorInvalidProof,
        NonExistentUtxoInput,
        TxoAlreadyExists,
        TxoNotSpendable,
        UnknownIdentity,
        CorrelationIdCannotBeReused,
        PendingCreateIdAlreadyExists,
        OnlyTrustMayCancelCreates,
        OnlyTrustMayConfirmCreates,
        OnlyTrustMayFulfillRedeems,
        OnlyTrustMayCancelRedeems,
        OnlyTrustMayModifyPendingCreateExpireTime,
        InsufficientFunds,
        CreateRequestMustProduceOutputs,
        TransactionDispatchError,
        CanNotSubmitConfidentialTransactionWithSignature,
        InvalidTrustKeyType,
        ConstructedByBannedMember,
        BannedMemberListDifferentLengths,
        TransactionCouldNotBeVerifiedConstructedByBannedMember,
        TransactionCouldNotBeVerifiedBannedListMismatch,
        RedeemIdAlreadyExists,
        NoMatchingRedeemToFulfill,
        NoMatchingRedeemToCancel,
        ExitingRedeemMustBeByExitingMember,
        ExitingRedeemTxONewerThanBan,
        TxoNotUETACompliant,
        ClearTxoNotRedeemable,
        InputLessThanRedeemAmount,
        TxoChangeOutputHasUnexpectedAmount,
        ClearTxoDoesNotBelongToRedeemingValidator,
        OnlyValidatorsMaySubmitValidatorRedeems,
    }

    #[pallet::storage]
    #[pallet::getter(fn total_created)]
    pub type TotalCreated<T: Config> = StorageValue<_, u64, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn total_redeemed)]
    pub type TotalRedeemed<T: Config> = StorageValue<_, u64, ValueQuery>;

    /// How long pending creates are allowed to be outstanding before the trust will decide
    /// to cancel them. In milliseconds.
    #[pallet::storage]
    #[pallet::getter(fn pending_create_expire_time)]
    pub type PendingCreateExpireTime<T: Config> = StorageValue<_, T::Moment, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn used_key_images)]
    pub type UsedKeyImages<T: Config> = StorageMap<_, Twox64Concat, WrapKeyImage, bool, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn txos)]
    pub type TXOs<T: Config> = StorageMap<
        _,
        Twox64Concat,
        WrapTransactionOutput,
        (WrapTxoStatus, T::BlockNumber),
        ValueQuery,
    >;

    #[pallet::storage]
    #[pallet::getter(fn identity_tags)]
    pub type IdentityTags<T: Config> =
        StorageMap<_, Twox64Concat, WrapIdentityTag, bool, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn pending_creates)]
    pub type PendingCreates<T: Config> =
        StorageMap<_, Blake2_128Concat, FiatReqCorrelationId, WrapCreateRequestRecord>;

    #[pallet::storage]
    #[pallet::getter(fn pending_redeems)]
    pub type PendingRedeems<T: Config> =
        StorageMap<_, Blake2_128Concat, FiatReqCorrelationId, WrapRedeemRecord>;

    #[pallet::storage]
    #[pallet::getter(fn clear_utxos)]
    pub type ClearUTXOs<T: Config> =
        StorageMap<_, Twox64Concat, WrapClearTransactionOutput, (), ValueQuery>;

    /// A denylist of fiat correlation ids that have been used recently and so must not be
    /// re-used lest someone claim an old bank send is what fulfilled their "new" create
    /// request re-using some old correlation id. The value is when the correlation id
    /// was first recorded
    #[pallet::storage]
    #[pallet::getter(fn used_correlation_ids)]
    pub type UsedCorrelationIds<T: Config> =
        StorageMap<_, Blake2_128Concat, FiatReqCorrelationId, T::Moment, ValueQuery>;

    /// How long (in milliseconds) should correlation ids be held in reserve before they may be re-used?
    ///
    /// Note that, regardless of what this is set to, if a correlation id is still in one of
    /// the pending request maps, it will not be freed.
    ///
    /// Also note that substrate's ideas of time are poorly specified here. A `Moment` should
    /// not be some kind of duration, but our `Moment` type is just milliseconds, which work
    /// as both ms since epoch, and a duration. There is no duration type we can specify.
    #[pallet::storage]
    #[pallet::getter(fn used_correlation_id_expire_time)]
    pub type UsedCorrelationIdExpireTime<T: Config> = StorageValue<_, T::Moment, ValueQuery>;

    #[pallet::genesis_config]
    pub struct GenesisConfig<T: Config>
    where
        <T as pallet_timestamp::Config>::Moment: serde::Serialize + serde::de::DeserializeOwned,
    {
        pub total_created: u64,
        pub total_redeemed: u64,
        /// How long pending creates are allowed to be outstanding before the trust will decide
        /// to cancel them. In milliseconds.
        pub pending_create_expire_time: T::Moment,
        pub used_key_images: Vec<(WrapKeyImage, bool)>,
        pub txos: Vec<(WrapTransactionOutput, (WrapTxoStatus, T::BlockNumber))>,
        pub identity_tags: Vec<(WrapIdentityTag, bool)>,
        pub pending_creates: Vec<(FiatReqCorrelationId, WrapCreateRequestRecord)>,
        pub pending_redeems: Vec<(FiatReqCorrelationId, WrapRedeemRecord)>,
        pub clear_utxos: Vec<(WrapClearTransactionOutput, ())>,
        /// How long (in milliseconds) should correlation ids be held in reserve before they may be re-used?
        ///
        /// Note that, regardless of what this is set to, if a correlation id is still in one of
        /// the pending request maps, it will not be freed.
        ///
        /// Also note that substrate\'s ideas of time are poorly specified here. A `Moment` should
        /// not be some kind of duration, but our `Moment` type is just milliseconds, which work
        /// as both ms since epoch, and a duration. There is no duration type we can specify.
        pub used_correlation_id_expire_time: T::Moment,
    }

    #[cfg(feature = "std")]
    impl<T: Config> Default for GenesisConfig<T>
    where
        <T as pallet_timestamp::Config>::Moment: serde::Serialize + serde::de::DeserializeOwned,
    {
        fn default() -> Self {
            Self {
                total_created: Default::default(),
                total_redeemed: Default::default(),
                pending_create_expire_time: Default::default(),
                used_key_images: Default::default(),
                txos: Default::default(),
                identity_tags: Default::default(),
                pending_creates: Default::default(),
                pending_redeems: Default::default(),
                clear_utxos: Default::default(),
                used_correlation_id_expire_time: Default::default(),
            }
        }
    }

    #[pallet::genesis_build]
    impl<T: Config> GenesisBuild<T> for GenesisConfig<T>
    where
        <T as pallet_timestamp::Config>::Moment: serde::Serialize + serde::de::DeserializeOwned,
    {
        fn build(&self) {
            {
                let data = &self.total_created;
                let v: &u64 = data;
                <TotalCreated<T> as frame_support::storage::StorageValue<u64>>::put::<&u64>(v);
            }
            {
                let data = &self.total_redeemed;
                let v: &u64 = data;
                <TotalRedeemed<T> as frame_support::storage::StorageValue<u64>>::put::<&u64>(v);
            }
            {
                let data = &self.pending_create_expire_time;
                let v: &T::Moment = data;
                <PendingCreateExpireTime<T, > as frame_support::storage::
                StorageValue<T::Moment>>::put::<&T::Moment>(v);
            }
            {
                let data = &self.used_key_images;
                let data: &frame_support::sp_std::vec::Vec<(WrapKeyImage, bool)> = data;
                data.iter().for_each(|(k, v)| {
                    <UsedKeyImages<T> as frame_support::storage
                    ::StorageMap<WrapKeyImage, bool>>::insert
                        ::<&WrapKeyImage, &bool>(k, v);
                });
            }
            {
                self.txos.iter().for_each(|(k, v)| TXOs::<T>::insert(k, v));
            }
            {
                let data = &self.identity_tags;
                let data: &frame_support::sp_std::vec::Vec<(WrapIdentityTag, bool)> = data;
                data.iter().for_each(|(k, v)| {
                    <IdentityTags<T> as frame_support::storage::StorageMap<
                        WrapIdentityTag,
                        bool,
                    >>::insert::<&WrapIdentityTag, &bool>(k, v);
                });
            }
            {
                let data = &self.pending_creates;
                let data: &frame_support::sp_std::vec::Vec<(
                    FiatReqCorrelationId,
                    WrapCreateRequestRecord,
                )> = data;
                data.iter().for_each(|(k, v)| {
                    <PendingCreates<T> as frame_support::storage::StorageMap<
                        FiatReqCorrelationId,
                        WrapCreateRequestRecord,
                    >>::insert::<&FiatReqCorrelationId, &WrapCreateRequestRecord>(
                        k, v
                    );
                });
            }
            {
                let data = &self.pending_redeems;
                let data: &frame_support::sp_std::vec::Vec<(
                    FiatReqCorrelationId,
                    WrapRedeemRecord,
                )> = data;
                data.iter().for_each(|(k, v)| {
                    <PendingRedeems<T> as frame_support::storage::StorageMap<
                        FiatReqCorrelationId,
                        WrapRedeemRecord,
                    >>::insert::<&FiatReqCorrelationId, &WrapRedeemRecord>(k, v);
                });
            }
            {
                let data = &self.clear_utxos;
                let data: &frame_support::sp_std::vec::Vec<(WrapClearTransactionOutput, ())> = data;
                data.iter().for_each(|(k, v)| {
                    <ClearUTXOs<T> as frame_support::storage::StorageMap<
                        WrapClearTransactionOutput,
                        (),
                    >>::insert::<&WrapClearTransactionOutput, &()>(k, v);
                });
            }
            {
                let data = &self.used_correlation_id_expire_time;
                let v: &T::Moment = data;
                <UsedCorrelationIdExpireTime<T> as frame_support::storage::StorageValue<
                    T::Moment,
                >>::put::<&T::Moment>(v);
            }
        }
    }
}

// Due to scale-info being unable to support generics, we need to type alias our usages of Wrap<T>
// in order to differentiate between wrapped types. This enables decoding of these objects in
// chain explorers or other front-ends.

// Storage Wraps
type WrapClearTransactionOutput = Wrap<ClearTransactionOutput>;
type WrapCreateRequestRecord = Wrap<CreateRequestRecord>;
type WrapIdentityTag = Wrap<IdentityTag>;
type WrapTxoStatus = Wrap<TxoStatus>;
type WrapTransactionOutput = Wrap<TransactionOutput>;
type WrapKeyImage = Wrap<KeyImage>;
type WrapRedeemRecord = Wrap<RedeemRequestRecord>;
// Call Wraps
type WrapCreateRequestTransaction = Wrap<CreateRequestTransaction>;
type WrapCreateCancellationTransaction = Wrap<CreateCancellationTransaction>;
type WrapCashConfirmationTransaction = Wrap<CashConfirmationTransaction>;
type WrapSendTransaction = Wrap<SendClaimsTransaction>;
type WrapRedeemTransaction = Wrap<RedeemRequestTransaction>;
type WrapRedeemFulfillmentTransaction = Wrap<RedeemFulfillmentTransaction>;
type WrapRedeemCancellationTransaction = Wrap<RedeemCancellationTransaction>;
type WrapRewardTransaction = Wrap<RewardTransaction>;
type WrapClearRedeemTransaction = Wrap<ClearRedeemRequestTransaction<ClearTransactionOutput>>;

pub trait XandstrateStateTrait<T: frame_system::Config> {
    type MemberAccountId: TryFrom<NewIdentityTag, Error = ()>;
    type BannedMemberAccountId: PartialEq<<T as frame_system::Config>::AccountId>
        + TryInto<NewPublicKey>;
    type TrustAccountId: PartialEq<<T as frame_system::Config>::AccountId> + TryInto<NewPublicKey>;
    type ValidatorAccountId: TryFrom<NewPublicKey> + From<<T as frame_system::Config>::AccountId>;

    fn is_member(address: &Self::MemberAccountId) -> bool;
    fn get_banned_members() -> Vec<(Self::BannedMemberAccountId, BannedState<T::BlockNumber>)>;
    fn get_trust_id() -> Self::TrustAccountId;
    fn is_validator(address: &Self::ValidatorAccountId) -> bool;
}

impl<T: Config> From<xand_ledger::FailureReason> for Error<T> {
    fn from(e: FailureReason) -> Self {
        use Error::*;

        match e {
            FailureReason::InsufficientFunds => Error::<T>::InsufficientFunds,
            FailureReason::PendingCreateRequestIdAlreadyExists => {
                Error::<T>::PendingCreateIdAlreadyExists
            }
            FailureReason::CorrelationIdCannotBeReused => {
                Error::<T>::CorrelationIdCannotBeReused
            }
            FailureReason::UnknownIdentity => Error::<T>::UnknownIdentity,
            FailureReason::CreateRequestAmountMustBeMoreThanZero => {
                Error::<T>::CreateAmountMustBeMoreThanZero
            }
            FailureReason::TransactionCouldNotBeVerified(inner) => match inner {
                TransactionValidationError::RangeProofCommitmentMismatch => {
                    TransactionCouldNotBeVerifiedRangeProofCommitmentMismatch
                }
                TransactionValidationError::OpenedCommitmentMismatch => {
                    TransactionCouldNotBeVerifiedOpenedCommitmentMismatch
                }
                TransactionValidationError::InvalidBulletProof => {
                    TransactionCouldNotBeVerifiedInvalidBulletProof
                }
                TransactionValidationError::InvalidAlpha => {
                    TransactionCouldNotBeVerifiedInvalidAlpha
                }
                TransactionValidationError::InvalidSignature => {
                    TransactionCouldNotBeVerifiedInvalidSignature
                }
                TransactionValidationError::InvalidInputSet => {
                    TransactionCouldNotBeVerifiedInvalidInputSet
                }
                TransactionValidationError::InvalidEncryption(encryption_error) => {
                    match encryption_error {
                        EncryptionVerificationError::VerificationError => TransactionCouldNotBeVerifiedInvalidEncryptionVerificationError,
                        EncryptionVerificationError::FailedZkplmtError(zkplmt_error) => {
                            match zkplmt_error {
                                ZkPlmtError::TupleSizeMismatch => TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorTupleSizeMismatch,
                                ZkPlmtError::TupleSizeZero => TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorTupleSizeZero,
                                ZkPlmtError::EmptyTupleList => TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorEmptyTupleList,
                                ZkPlmtError::InvalidProof => TransactionCouldNotBeVerifiedInvalidEncryptionFailedZkplmtErrorInvalidProof
                            }
                        }
                    }
                }
                TransactionValidationError::BannedMemberListDifferentLengths => TransactionCouldNotBeVerifiedBannedListMismatch,
                TransactionValidationError::ConstructedByBannedMember => TransactionCouldNotBeVerifiedConstructedByBannedMember,
            },
            FailureReason::NoMatchingCreateRequestToFulfill => {
                Error::<T>::NoMatchingCreateCorrelationIdToFulfill
            }
            FailureReason::NoMatchingCreateRequestToCancel => {
                Error::<T>::NoMatchingCreateCorrelationIdToCancel
            }
            FailureReason::CreateRequestMustProduceOutputs => {
                Error::<T>::CreateRequestMustProduceOutputs
            }
            FailureReason::TransactionDispatchError(_) => {
                // This case should be unreachable, but in the event that assumption is violated, we
                // will just lose the error details instead of crashing.
                Error::<T>::TransactionDispatchError
            }
            FailureReason::InvalidTrustAccountId => {
                Error::<T>::InvalidTrustKeyType
            }
            FailureReason::OutputTxoAlreadyExists => Error::<T>::TxoAlreadyExists,
            FailureReason::TxoNotSpendable(_) => Error::<T>::TxoNotSpendable,
            FailureReason::KeyImageAlreadySpent => Error::<T>::KeyImageAlreadyUsed,
            FailureReason::RedeemRequestIdAlreadyExists => Error::<T>::RedeemIdAlreadyExists,
            FailureReason::NoMatchingRedeemRequestToFulfill => Error::<T>::NoMatchingRedeemCorrelationIdToFulfill,
            FailureReason::NoMatchingRedeemRequestToCancel => Error::<T>::NoMatchingRedeemCorrelationIdToCancel,
            FailureReason::ExitingRedeemRequestMustBeByExitingMember => Error::<T>::ExitingRedeemMustBeByExitingMember,
            FailureReason::ExitingRedeemRequestTxONewerThanBan => Error::<T>::ExitingRedeemTxONewerThanBan,
            FailureReason::TxoNotUETACompliant => Error::<T>::TxoNotUETACompliant,
            FailureReason::ClearTxoNotRedeemable => Error::<T>::ClearTxoNotRedeemable,
            FailureReason::InputLessThanRedeemAmount => Error::<T>::InputLessThanRedeemAmount,
            FailureReason::TxoChangeOutputHasUnexpectedAmount => Error::<T>::TxoChangeOutputHasUnexpectedAmount,
            FailureReason::ClearTxoDoesNotBelongToRedeemingValidator => Error::<T>::ClearTxoDoesNotBelongToRedeemingValidator,
        }
    }
}

fn get_create_request_processor<'a, T>() -> CreateRequestProcessor<
    'a,
    <CreateRequestRepo<T> as CreateRequestRepository>::Iter,
    CryptoImpl,
    T::BlockNumber,
>
where
    T: Config,
{
    CreateRequestProcessor::new(
        &CreateRequestRepo::<T> {
            _module_marker: PhantomData,
        },
        &TotalIssuanceRepo::<T> {
            _module_marker: PhantomData,
        },
        &CorrelationIdRepo::<T> {
            _module_marker: PhantomData,
        },
        &IdentityTagRepo::<T> {
            _module_marker: PhantomData,
        },
        &TransactionOutputRepo::<T> {
            _module_marker: PhantomData,
        },
        &XandstrateMemberRepo::<T> {
            _module_marker: PhantomData,
        },
        &CreateRequestRepo::<T> {
            _module_marker: PhantomData,
        },
        &TrustMetadataRepo::<T> {
            _module_marker: PhantomData,
        },
        PhantomData,
    )
}

type RedeemIter<T> = <RedeemRepo<T> as RedeemRequestRepository>::Iter;

fn get_redeem_processor<'a, T>(
) -> RedeemRequestProcessor<'a, RedeemIter<T>, CryptoImpl, T::BlockNumber, ClearUtxoRepo<T>>
where
    T: Config,
{
    RedeemRequestProcessor::new(
        &ClearUtxoRepo::<T> {
            _module_marker: PhantomData,
        },
        &TransactionOutputRepo::<T> {
            _module_marker: PhantomData,
        },
        &KeyImageRepo::<T> {
            _module_marker: PhantomData,
        },
        &IdentityTagRepo::<T> {
            _module_marker: PhantomData,
        },
        &XandstrateMemberRepo::<T> {
            _module_marker: PhantomData,
        },
        &TrustMetadataRepo::<T> {
            _module_marker: PhantomData,
        },
        &CorrelationIdRepo::<T> {
            _module_marker: PhantomData,
        },
        &RedeemRepo::<T> {
            _module_marker: PhantomData,
        },
        &TotalIssuanceRepo::<T> {
            _module_marker: PhantomData,
        },
        &XandstrateValidatorRepo::<T> {
            _module_marker: PhantomData,
        },
        PhantomData,
    )
}

fn get_send_processor<'a, T>() -> SendClaimsProcessor<'a, CryptoImpl, T::BlockNumber>
where
    T: Config,
{
    SendClaimsProcessor::new(
        &TransactionOutputRepo::<T> {
            _module_marker: PhantomData,
        },
        &KeyImageRepo::<T> {
            _module_marker: PhantomData,
        },
        &IdentityTagRepo::<T> {
            _module_marker: PhantomData,
        },
        &XandstrateMemberRepo::<T> {
            _module_marker: PhantomData,
        },
        PhantomData,
    )
}

fn get_reward_processor<'a, T>() -> RewardProcessor<'a, ClearUtxoRepo<T>>
where
    T: Config,
{
    RewardProcessor::new(
        &ClearUtxoRepo::<T> {
            _module_marker: PhantomData,
        },
        &TotalIssuanceRepo::<T> {
            _module_marker: PhantomData,
        },
    )
}

impl<T> Pallet<T>
where
    T::BlockNumber: core::fmt::Debug,
    T: Config,
    T::BlockNumber: Into<u64>,
{
    fn on_initialize_impl(_bnum: T::BlockNumber) -> Weight {
        // Errors must be swallowed
        let _ = Self::cancel_expired_create_requests();
        Weight::default()
    }

    fn on_finalize_impl() {
        // Check if any denylisted fiat correlation ids have passed the denylist window and should be
        // removed
        Self::remove_old_correlation_ids()
    }

    fn request_create_tx(
        origin: T::Origin,
        create: CreateRequestTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        Self::fail_if_signed(&origin)?;

        let cri = get_create_request_processor::<T>();
        cri.create_request(create).map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn confirm_create_tx(
        origin: T::Origin,
        txn: CashConfirmationTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure signer is the trust
        let sender = ensure_signed(origin)?;
        Self::ensure_sender_is_trust(&sender, Error::<T>::OnlyTrustMayConfirmCreates)?;

        let cri = get_create_request_processor::<T>();
        cri.cash_confirmation(txn).map_err(Error::<T>::from)?;
        Self::remove_old_correlation_ids();
        Ok(().into())
    }

    fn send_tx(
        origin: T::Origin,
        txn: SendClaimsTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        Self::fail_if_signed(&origin)?;
        let send_processor = get_send_processor::<T>();
        send_processor.send_claims(txn).map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn reward_tx(
        origin: T::Origin,
        reward: RewardTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        ensure_root(origin)?;

        let reward_processor = get_reward_processor::<T>();
        reward_processor
            .process_reward_transaction(reward)
            .map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn redeem_tx(
        origin: T::Origin,
        txn: RedeemRequestTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        Self::fail_if_signed(&origin)?;
        let redeem_processor = get_redeem_processor::<T>();
        redeem_processor
            .redeem_request(txn)
            .map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn redeem_fulfill_tx(
        origin: T::Origin,
        txn: RedeemFulfillmentTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let sender = ensure_signed(origin)?;
        Self::ensure_sender_is_trust(&sender, Error::<T>::OnlyTrustMayFulfillRedeems)?;
        let redeem_processor = get_redeem_processor::<T>();
        redeem_processor
            .redeem_fulfillment(txn)
            .map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn cancel_pending_redeem_tx(
        origin: T::Origin,
        txn: RedeemCancellationTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let sender = ensure_signed(origin)?;
        Self::ensure_sender_is_trust(&sender, Error::<T>::OnlyTrustMayCancelRedeems)?;
        let redeem_processor = get_redeem_processor::<T>();
        redeem_processor
            .redeem_cancellation(txn)
            .map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn validator_redeem_tx(
        origin: T::Origin,
        txn: ClearRedeemRequestTransaction<ClearTransactionOutput>,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let sender = ensure_signed(origin)?;
        Self::ensure_sender_is_validator(
            &sender,
            Error::<T>::OnlyValidatorsMaySubmitValidatorRedeems,
        )?;
        let redeem_processor = get_redeem_processor::<T>();
        redeem_processor
            .clear_redeem_request(txn)
            .map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn set_pending_create_expire_time_tx(
        origin: T::Origin,
        new_val: T::Moment,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure signer is the trust
        let sender = ensure_signed(origin)?;
        Self::ensure_sender_is_trust(
            &sender,
            Error::<T>::OnlyTrustMayModifyPendingCreateExpireTime,
        )?;
        PendingCreateExpireTime::<T>::put(new_val);
        Ok(().into())
    }

    fn fail_if_signed(origin: &T::Origin) -> frame_support::dispatch::DispatchResultWithPostInfo {
        if let Ok(raw_origin) = origin.clone().into() {
            ensure!(
                RawOrigin::<T::AccountId>::None == raw_origin,
                Error::<T>::CanNotSubmitConfidentialTransactionWithSignature
            )
        }
        Ok(().into())
    }

    // This is mostly dupe of the non-confidential runtime
    fn remove_old_correlation_ids() {
        let curts = pallet_timestamp::Module::<T>::get();
        let mut remove_me = sp_core::sp_std::vec::Vec::new();
        for (correlation_id, initial_use) in UsedCorrelationIds::<T>::iter() {
            let expired_at = initial_use + UsedCorrelationIdExpireTime::<T>::get();
            if expired_at <= curts {
                // Correlation id has passed expiration window
                if !PendingCreates::<T>::contains_key(correlation_id)
                    && !PendingRedeems::<T>::contains_key(correlation_id)
                {
                    // The correlation id is also not still pending, so we can remove it.
                    remove_me.push(correlation_id);
                } else {
                    debug::info!(
                        "Correlation ID {:?} couldn't be expired due to pending activity",
                        correlation_id
                    );
                }
            }
        }
        for correlation_id in remove_me {
            UsedCorrelationIds::<T>::remove(correlation_id);
        }
    }

    fn cancel_pending_create_tx(
        origin: T::Origin,
        txn: CreateCancellationTransaction,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure signer is the trust, or root (intrinsically cancelled by system)
        if ensure_root(origin.clone()).is_err() {
            let sender = ensure_signed(origin)?;
            Self::ensure_sender_is_trust(&sender, Error::<T>::OnlyTrustMayCancelCreates)?;
        }

        let cri = get_create_request_processor::<T>();
        cri.create_cancellation(txn).map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn cancel_expired_create_requests() -> frame_support::dispatch::DispatchResultWithPostInfo {
        let cri = get_create_request_processor::<T>();
        cri.cancel_expired_create_requests(&StaticDispatcher {
            _marker: PhantomData::<T>,
        })
        .map_err(Error::<T>::from)?;
        Ok(().into())
    }

    fn ensure_sender_is_trust(
        sender: &T::AccountId,
        error: Error<T>,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let trust_id = <T as Config>::XandstrateState::get_trust_id();

        ensure!(trust_id == *sender, error);
        Ok(().into())
    }

    fn ensure_sender_is_validator(
        sender: &T::AccountId,
        error: Error<T>,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let is_validator = <T as Config>::XandstrateState::is_validator(&(sender.clone()).into());

        ensure!(is_validator, error);
        Ok(().into())
    }
}

struct StaticDispatcher<T> {
    _marker: PhantomData<T>,
}

impl<T: Config> xand_ledger::Dispatcher for StaticDispatcher<T>
where
    T::BlockNumber: Into<u64>,
{
    fn dispatch(&self, txn: LedgerTransaction) -> TransactionResult {
        // TODO: This is an odd place to put this for now, but we don't have any event mechanism
        //  in ledger yet. Move this there once we do.
        if let LedgerTransaction::CreateCancellation(cc) = &txn {
            Pallet::<T>::deposit_event(Event::CreateCancelled(cc.correlation_id, Wrap(cc.reason)));
        }

        let converted: Call<T> = txn.into();
        converted
            .dispatch_bypass_filter(RawOrigin::Root.into())
            .map_err(|mut e| {
                fixup_err_msg(&mut e.error);
                let reason = match e.error {
                    DispatchError::Other(s) => s,
                    DispatchError::BadOrigin => "bad origin",
                    DispatchError::CannotLookup => "cannot lookup",
                    DispatchError::Module { message, .. } => message.unwrap_or("no message"),
                    DispatchError::ConsumerRemaining => "consumer remaining",
                    DispatchError::NoProviders => "no providers",
                };
                FailureReason::TransactionDispatchError(reason)
            })?;
        Ok(())
    }
}

impl<T: Config> From<LedgerTransaction> for Call<T>
where
    T::BlockNumber: Into<u64>,
{
    fn from(t: LedgerTransaction) -> Self {
        match t {
            LedgerTransaction::CreateRequest(v) => Call::<T>::request_create(Wrap(v)),
            LedgerTransaction::CashConfirmation(v) => Call::<T>::confirm_create(Wrap(v)),
            LedgerTransaction::CreateCancellation(v) => Call::<T>::cancel_pending_create(Wrap(v)),
            LedgerTransaction::Send(v) => Call::<T>::send(Wrap(v)),
            LedgerTransaction::RedeemRequest(v) => Call::<T>::redeem(Wrap(v)),
            LedgerTransaction::RedeemFulfillment(v) => Call::<T>::redeem_fulfill(Wrap(v)),
            LedgerTransaction::RedeemCancellation(v) => Call::<T>::cancel_pending_redeem(Wrap(v)),
        }
    }
}

impl<T: Config> frame_support::unsigned::ValidateUnsigned for Pallet<T>
where
    T::BlockNumber: Into<u64>,
{
    type Call = Call<T>;
    fn validate_unsigned(_source: TransactionSource, call: &Self::Call) -> TransactionValidity {
        let valid_tx = |provides| {
            sp_runtime::transaction_validity::ValidTransaction::with_tag_prefix("zkplmt")
                .and_provides(provides)
                .propagate(true)
                .build()
        };
        match call {
            Call::request_create(txn) => {
                let provides = txn
                    .0
                    .core_transaction
                    .outputs
                    .iter()
                    .map(|txo| txo.transaction_output.to_hash())
                    .collect::<Vec<_>>();
                valid_tx(provides)
            }
            Call::send(txn) => {
                let provides = txn
                    .0
                    .core_transaction
                    .output
                    .iter()
                    .map(|txo| txo.txo.to_hash())
                    .collect::<Vec<_>>();
                valid_tx(provides)
            }
            Call::redeem(txn) => {
                let provides = alloc::vec![
                    txn.0
                        .core_transaction
                        .output
                        .redeem_output
                        .transaction_output
                        .to_hash(),
                    txn.0.core_transaction.output.change_output.txo.to_hash(),
                ];
                valid_tx(provides)
            }
            Call::confirm_create(_)
            | Call::cancel_pending_create(_)
            | Call::redeem_fulfill(_)
            | Call::cancel_pending_redeem(_)
            | Call::reward(_)
            | Call::validator_redeem(_)
            | Call::set_pending_create_expire_time(_)
            | Call::__Ignore(..) => {
                sp_runtime::transaction_validity::InvalidTransaction::Call.into()
            }
        }
    }
}
