use super::*;
use crate::confidentiality::test_constants::*;
use crate::{confidentiality, Block, Decode, Encode, UncheckedExtrinsic};
use frame_support::{
    parameter_types,
    traits::{GenesisBuild, OnFinalize, OnInitialize},
    weights::constants::WEIGHT_PER_SECOND,
};
use rand_core::{CryptoRng, OsRng};
use sp_core::{crypto::AccountId32, H256};
use sp_runtime::{
    testing::Header,
    traits::{BlakeTwo256, IdentityLookup},
    Perbill,
};
use std::cell::RefCell;
use xand_ledger::{PublicKey, TestClearRedeemRequestBuilder};

mod clear_redeems;
pub(crate) mod creates;
mod redeems;
mod rewards;
mod sends;

#[derive(Debug, Encode, Decode, Clone, Eq, PartialEq)]
pub struct FakeEvent(String);

impl From<()> for FakeEvent {
    fn from(_: ()) -> Self {
        FakeEvent("empty".to_string())
    }
}

impl From<Event> for FakeEvent {
    fn from(e: Event) -> Self {
        FakeEvent(format!("confidentiality: {:?}", e))
    }
}

impl<T: frame_system::Config> From<frame_system::Event<T>> for FakeEvent {
    fn from(e: frame_system::Event<T>) -> Self {
        FakeEvent(format!("confidentiality: {:?}", e))
    }
}

impl From<confidentiality::Event<confidentiality::tests::Test>> for FakeEvent {
    fn from(e: confidentiality::Event<confidentiality::tests::Test>) -> Self {
        FakeEvent(format!("confidentiality: {:?}", e))
    }
}

// Configure a mock runtime to test the pallet.
frame_support::construct_runtime!(
    pub enum Test where
        Block = Block,
        NodeBlock = Block,
        UncheckedExtrinsic = UncheckedExtrinsic,
    {
        System: frame_system::{Module, Call, Config, Storage, Event<T>},
        Confidentiality: confidentiality::{Module, Call, Storage, Event<T>},
        Timestamp: pallet_timestamp::{Module, Call, Storage},
    }
);

/// We allow `Normal` extrinsics to fill up the block up to 75%, the rest can be used
/// by  Operational  extrinsics.
const NORMAL_DISPATCH_RATIO: Perbill = Perbill::from_percent(75);

parameter_types! {
    pub const BlockHashCount: u64 = 250;
    pub BlockWeights: frame_system::limits::BlockWeights = frame_system::limits::BlockWeights
        ::with_sensible_defaults(2 * WEIGHT_PER_SECOND, NORMAL_DISPATCH_RATIO);
    pub BlockLength: frame_system::limits::BlockLength = frame_system::limits::BlockLength
        ::max_with_normal_ratio(MINIMUM_PERIOD * 1024 * 1024, NORMAL_DISPATCH_RATIO);
    pub const SS58Prefix: u8 = 42;
}
impl frame_system::Config for Test {
    type BaseCallFilter = ();
    type BlockWeights = BlockWeights;
    type BlockLength = BlockLength;
    type Origin = Origin;
    type Call = Call;
    type Index = u64;
    type BlockNumber = u64;
    type Hash = H256;
    type Hashing = BlakeTwo256;
    type AccountId = AccountId32;
    type Lookup = IdentityLookup<Self::AccountId>;
    type Header = Header;
    type Event = FakeEvent;
    type BlockHashCount = BlockHashCount;
    type DbWeight = ();
    type Version = ();
    type PalletInfo = PalletInfo;
    type AccountData = ();
    type OnNewAccount = ();
    type OnKilledAccount = ();
    type SystemWeightInfo = ();
    type SS58Prefix = SS58Prefix;
}

parameter_types! {
    pub const MinimumPeriod: u64 = SLOT_DURATION / 2;
}

impl pallet_timestamp::Config for Test {
    type Moment = u64;
    type OnTimestampSet = ();
    type MinimumPeriod = MinimumPeriod;
    type WeightInfo = ();
}

impl Config for Test {
    type Event = FakeEvent;
    type XandstrateState = TestXandstrateStateAdapter;
}

pub struct TestXandstrateStateAdapter;

impl XandstrateStateTrait<Test> for TestXandstrateStateAdapter {
    type MemberAccountId = AccountId32;
    type BannedMemberAccountId = AccountId32;
    type TrustAccountId = AccountId32;
    type ValidatorAccountId = AccountId32;

    fn is_member(address: &Self::MemberAccountId) -> bool {
        XANDSTRATE_STATE.with(|state| state.borrow().members.contains(address))
    }

    fn get_banned_members() -> Vec<(
        Self::BannedMemberAccountId,
        BannedState<<Test as frame_system::Config>::BlockNumber>,
    )> {
        XANDSTRATE_STATE.with(|state| state.borrow().banned_members.clone())
    }

    fn get_trust_id() -> Self::TrustAccountId {
        XANDSTRATE_STATE.with(|state| state.borrow().trust_node_id.clone())
    }

    fn is_validator(address: &Self::ValidatorAccountId) -> bool {
        XANDSTRATE_STATE.with(|state| state.borrow().validators.contains(address))
    }
}

thread_local! {
    pub(crate) static XANDSTRATE_STATE: RefCell<XandstrateStateMock<Test>> = RefCell::new(Default::default());
}

pub(crate) struct XandstrateStateMock<T: Config> {
    pub(crate) members: Vec<T::AccountId>,
    pub(crate) banned_members: Vec<(T::AccountId, BannedState<T::BlockNumber>)>,
    pub(crate) trust_node_id: T::AccountId,
    pub(crate) validators: Vec<T::AccountId>,
}

impl Default for XandstrateStateMock<Test> {
    fn default() -> Self {
        XandstrateStateMock {
            members: vec![],
            banned_members: vec![],
            trust_node_id: TRUST.clone(),
            validators: vec![],
        }
    }
}

pub(crate) fn set_xandstrate_state(state: XandstrateStateMock<Test>) {
    XANDSTRATE_STATE.with(|f| *f.borrow_mut() = state)
}

pub(crate) type ConfidentialityPallet = Pallet<Test>;

pub(crate) type TimestampMod = pallet_timestamp::Module<Test>;

pub(crate) fn new_test_externalities(
    genesis: confidentiality::GenesisConfig<Test>,
    xandstrate_state: XandstrateStateMock<Test>,
) -> sp_io::TestExternalities {
    set_xandstrate_state(xandstrate_state);

    let mut t = frame_system::GenesisConfig::default()
        .build_storage::<Test>()
        .unwrap();
    genesis.assimilate_storage(&mut t).unwrap();
    t.into()
}

const SLOT_DURATION_U32: u32 = 10;
pub(crate) const SLOT_DURATION: u64 = SLOT_DURATION_U32 as u64;
pub(crate) const MINIMUM_PERIOD: u32 = SLOT_DURATION_U32 / 2;

pub(crate) fn rand_txo(mut csprng: &mut OsRng) -> Wrap<TransactionOutput> {
    Wrap(TransactionOutput::random(&mut csprng))
}

#[test]
fn can_bootstrap_genesis() {
    let mut csprng: OsRng = OsRng::default();

    let already_spent_txo = Wrap(KeyImage::random(&mut csprng));

    let available_txo = rand_txo(&mut csprng);
    let off_chain_txo = rand_txo(&mut csprng);

    let genesis = crate::confidentiality::GenesisConfig {
        used_key_images: vec![(already_spent_txo, true)],
        txos: vec![(available_txo, (Wrap(TxoStatus::Confirmed), 0))],
        ..Default::default()
    };
    new_test_externalities(genesis, Default::default()).execute_with(|| {
        // spend the utxo's
        assert!(ConfidentialityPallet::used_key_images(already_spent_txo));
        assert_eq!(
            ConfidentialityPallet::txos(available_txo).0 .0,
            TxoStatus::Confirmed
        );
        assert_eq!(
            ConfidentialityPallet::txos(off_chain_txo).0 .0,
            TxoStatus::Nonexistent
        );
    });
}

pub fn run_to_block(n: u64) {
    while System::block_number() < n {
        ConfidentialityPallet::on_finalize(System::block_number());
        System::set_block_number(System::block_number() + 1);
        // Every block will take the given slot duration
        TimestampMod::set_timestamp(System::block_number() * SLOT_DURATION);
        ConfidentialityPallet::on_initialize(System::block_number());
    }
}

pub struct TestClearRedeemTransaction {
    pub tx: ClearRedeemRequestTransaction<ClearTransactionOutput>,
    pub input_txos: Vec<(Wrap<ClearTransactionOutput>, ())>,
}

pub(crate) fn create_random_validator_redeem_transaction<R>(
    csprng: &mut R,
    validator: PublicKey,
) -> TestClearRedeemTransaction
where
    R: rand::Rng + CryptoRng,
{
    let tx = TestClearRedeemRequestBuilder::default()
        .with_issuer(validator)
        .build(csprng)
        .unwrap();
    let input_txos = tx.input().iter().map(|txo| (Wrap(*txo), ())).collect();
    TestClearRedeemTransaction { tx, input_txos }
}

// TODO  - write tests for redeem_tx(), redeem_fulfill_tx(), and cancel_pending_redeem_tx(), that that verify failure if requested against a non-confidential runtime;
// https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6007
