#![allow(clippy::useless_vec)] // The benchmarks! macro is causing this in its depths

use super::*;
use crate::confidentiality::{
    ledger_adapters::NewPublicKey,
    test_constants::TRUST,
    testing_and_benchmarking_shared::{
        create_random_create_transaction, create_random_redeem_transaction,
        create_random_send_transaction, TestCreateTransaction, TestRedeemTransaction,
        TestSendTransaction,
    },
};
use crate::Runtime;
use frame_benchmarking::benchmarks;
use lazy_static::lazy_static;
use rand::{rngs::StdRng, SeedableRng};
use sp_std::prelude::*;
use xand_ledger::PublicKey;

lazy_static! {
    pub static ref CREATE_TX: TestCreateTransaction = {
        let mut csprng: StdRng = StdRng::seed_from_u64(123);
        create_random_create_transaction(&mut csprng)
    };
    pub static ref SEND_TX: TestSendTransaction = {
        let mut csprng: StdRng = StdRng::seed_from_u64(123);
        create_random_send_transaction(&mut csprng)
    };
    pub static ref REDEEM_TX: TestRedeemTransaction<u64> = {
        let mut csprng: StdRng = StdRng::seed_from_u64(123);
        let trust_pubkey: PublicKey = NewPublicKey::new(TRUST.clone()).unwrap().into();
        create_random_redeem_transaction(&mut csprng, trust_pubkey)
    };
}

benchmarks! {
    where_clause { where T::BlockNumber: Into<u64> }

    request_create {
        let create_tx = CREATE_TX.tx.clone();
        let wrap_create_tx = Wrap(create_tx);
    }: request_create(RawOrigin::None, wrap_create_tx)
    verify {
        let correlation_id = CREATE_TX.tx.core_transaction.correlation_id;
        assert!(PendingCreates::<Runtime>::get(correlation_id).is_some());
    }

    send {
        let send_tx = SEND_TX.send.clone();
        let wrap_send_tx = Wrap(send_tx);
    }: send(RawOrigin::None, wrap_send_tx)

    request_redeem {
        let redeem_tx = REDEEM_TX.tx.clone();
        let wrap_redeem_tx = Wrap(redeem_tx);
    }: redeem(RawOrigin::None, wrap_redeem_tx)
    verify {
        let correlation_id = REDEEM_TX.tx.core_transaction.correlation_id;
        assert!(PendingRedeems::<Runtime>::get(correlation_id).is_some())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::confidentiality::tests::{new_test_externalities, Test, XandstrateStateMock};
    use frame_support::assert_ok;

    #[test]
    fn test_request_create() {
        let genesis = GenesisConfig::<Test> {
            identity_tags: CREATE_TX.identity_set.clone(),
            ..Default::default()
        };
        new_test_externalities(genesis, XandstrateStateMock::default()).execute_with(|| {
            assert_ok!(test_benchmark_request_create::<Test>());
        })
    }

    #[test]
    fn test_send() {
        let genesis = GenesisConfig::<Test> {
            identity_tags: SEND_TX.input_identities.clone(),
            txos: SEND_TX.input_txos.clone(),
            ..Default::default()
        };
        new_test_externalities(genesis, XandstrateStateMock::default()).execute_with(|| {
            assert_ok!(test_benchmark_send::<Test>());
        })
    }
}
