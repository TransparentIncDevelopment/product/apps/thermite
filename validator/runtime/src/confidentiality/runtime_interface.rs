/// This module allows our WASM environment to make use of native code that runs too slowly in WASM.
use parity_scale_codec::{Decode, Encode};
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_scale_wrap::Wrap;
use sp_runtime_interface::pass_by::{Codec, PassBy};
use sp_runtime_interface::runtime_interface;
use sp_std::vec::Vec;
use xand_ledger::crypto::{BulletRangeProof, LedgerCrypto};
use xand_ledger::{
    curve25519_dalek::ristretto::RistrettoPoint, Proof, TransactionOutput, VectorTuple, ZkPlmtError,
};

/// Bridge native functions into the runtime
#[runtime_interface]
pub trait Crypto {
    fn verify_bulletproof(bulletproof: FFIWrapper<BulletRangeProof>) -> bool {
        let range_proof = bulletproof.into_inner();
        xand_ledger::crypto::DefaultCryptoImpl::verify_bulletproof(&range_proof)
    }

    fn verify_zkplmt(
        message: &[u8],
        tuples: FFIWrapper<Vec<VectorTuple>>,
        proof: FFIWrapper<Proof>,
    ) -> FFIWrapper<Result<(), ZkPlmtError>> {
        let tuples = tuples.into_inner();
        let proof = proof.into_inner();
        let result =
            xand_ledger::crypto::DefaultCryptoImpl::verify_zkplmt(message, &tuples, &proof);
        FFIWrapper(Wrap(result))
    }

    fn txn_input_keys_to_hash_curve_points(
        input: FFIWrapper<Vec<TransactionOutput>>,
    ) -> FFIWrapper<Vec<RistrettoPoint>> {
        let input = input.into_inner();
        let result =
            xand_ledger::crypto::DefaultCryptoImpl::txn_input_keys_to_hash_curve_points(&input);
        FFIWrapper(Wrap(result))
    }
}

pub struct CryptoImpl;

impl LedgerCrypto for CryptoImpl {
    #[cfg(not(feature = "std"))]
    fn verify_bulletproof(proof: &BulletRangeProof) -> bool {
        let wrap_proof = FFIWrapper(Wrap(proof.clone()));
        crypto::verify_bulletproof(wrap_proof)
    }

    #[cfg(feature = "std")]
    fn verify_bulletproof(proof: &BulletRangeProof) -> bool {
        xand_ledger::crypto::DefaultCryptoImpl::verify_bulletproof(proof)
    }

    #[cfg(not(feature = "std"))]
    fn verify_zkplmt(
        message: &[u8],
        tuples: &[VectorTuple],
        proof: &Proof,
    ) -> Result<(), ZkPlmtError> {
        let tuples = FFIWrapper(Wrap(tuples.iter().cloned().collect()));
        let proof = FFIWrapper(Wrap(proof.clone()));
        crypto::verify_zkplmt(message, tuples, proof).into_inner()
    }

    #[cfg(feature = "std")]
    fn verify_zkplmt(
        message: &[u8],
        tuples: &[VectorTuple],
        proof: &Proof,
    ) -> Result<(), ZkPlmtError> {
        xand_ledger::crypto::DefaultCryptoImpl::verify_zkplmt(message, tuples, proof)
    }

    #[cfg(not(feature = "std"))]
    fn txn_input_keys_to_hash_curve_points(input: &[TransactionOutput]) -> Vec<RistrettoPoint> {
        let input = FFIWrapper(Wrap(input.iter().cloned().collect()));
        crypto::txn_input_keys_to_hash_curve_points(input).into_inner()
    }

    #[cfg(feature = "std")]
    fn txn_input_keys_to_hash_curve_points(input: &[TransactionOutput]) -> Vec<RistrettoPoint> {
        xand_ledger::crypto::DefaultCryptoImpl::txn_input_keys_to_hash_curve_points(input)
    }
}

#[derive(Encode, Decode)]
pub struct FFIWrapper<T: Serialize + DeserializeOwned>(Wrap<T>);

impl<T: Serialize + DeserializeOwned> FFIWrapper<T> {
    fn into_inner(self) -> T {
        self.0 .0
    }
}

impl<T: Serialize + DeserializeOwned> PassBy for FFIWrapper<T> {
    type PassBy = Codec<Self>;
}
