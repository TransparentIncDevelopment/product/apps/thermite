#![allow(non_snake_case)]

use crate::confidentiality::{
    ledger_adapters::NewPublicKey,
    test_constants::{TRUST, VAL_1},
    tests::{
        create_random_validator_redeem_transaction, new_test_externalities, ConfidentialityPallet,
        Origin, Test, XandstrateStateMock,
    },
    Error, GenesisConfig,
};
use frame_support::{assert_err, assert_ok};
use frame_system::RawOrigin;
use rand::{prelude::StdRng, SeedableRng};
use serde_scale_wrap::Wrap;
use sp_runtime::DispatchError;
use xand_ledger::{
    PublicKey, RedeemCancellationReason, RedeemCancellationTransaction,
    RedeemFulfillmentTransaction,
};

#[test]
fn validator_redeem__happy_path() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let validator_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, validator_pub_key);
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let result = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );

        // Then
        assert_ok!(result);
    });
}

#[test]
fn validator_redeem__fails_if_unsigned() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let result =
            ConfidentialityPallet::validator_redeem(RawOrigin::None.into(), Wrap(redeem_tx.tx));

        // Then
        assert_err!(result, DispatchError::BadOrigin);
    });
}

#[test]
fn validator_redeem__fails_if_signed_by_non_validator() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let result = ConfidentialityPallet::validator_redeem(
            Origin::signed(TRUST.clone()),
            Wrap(redeem_tx.tx),
        );

        // Then
        assert_err!(
            result,
            Error::<Test>::OnlyValidatorsMaySubmitValidatorRedeems
        );
    });
}

#[test]
fn validator_redeem_fulfill__happy_path() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let correlation_id = redeem_tx.tx.correlation_id();
    let fulfill = RedeemFulfillmentTransaction { correlation_id };
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let req = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );
        let result =
            ConfidentialityPallet::redeem_fulfill(Origin::signed(TRUST.clone()), Wrap(fulfill));

        // Then
        assert_ok!(req);
        assert_ok!(result);
    });
}

#[test]
fn validator_redeem_fulfill__fails_if_not_sent_by_trust() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let correlation_id = redeem_tx.tx.correlation_id();
    let fulfill = RedeemFulfillmentTransaction { correlation_id };
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let req = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );
        let result =
            ConfidentialityPallet::redeem_fulfill(Origin::signed(VAL_1.clone()), Wrap(fulfill));

        // Then
        assert_ok!(req);
        assert_err!(result, Error::<Test>::OnlyTrustMayFulfillRedeems);
    });
}

#[test]
fn validator_redeem_fulfill__fails_if_unsigned() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let correlation_id = redeem_tx.tx.correlation_id();
    let fulfill = RedeemFulfillmentTransaction { correlation_id };
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let req = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );
        let result = ConfidentialityPallet::redeem_fulfill(RawOrigin::None.into(), Wrap(fulfill));

        // Then
        assert_ok!(req);
        assert_err!(result, DispatchError::BadOrigin);
    });
}

#[test]
fn cancel_pending_validator_redeem__redeem_happy_path() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let correlation_id = redeem_tx.tx.correlation_id();
    let reason = RedeemCancellationReason::InvalidData;
    let cancel = RedeemCancellationTransaction {
        correlation_id,
        reason,
    };
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let req = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );
        let result = ConfidentialityPallet::cancel_pending_redeem(
            Origin::signed(TRUST.clone()),
            Wrap(cancel),
        );

        // Then
        assert_ok!(req);
        assert_ok!(result);
    });
}

#[test]
fn cancel_pending_validator_redeem__fails_if_not_sent_by_trust() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let correlation_id = redeem_tx.tx.correlation_id();
    let reason = RedeemCancellationReason::InvalidData;
    let cancel = RedeemCancellationTransaction {
        correlation_id,
        reason,
    };
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let req = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );
        let result = ConfidentialityPallet::cancel_pending_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(cancel),
        );

        // Then
        assert_ok!(req);
        assert_err!(result, Error::<Test>::OnlyTrustMayCancelRedeems);
    });
}

#[test]
fn cancel_pending_validator_redeem__fails_if_unsigned() {
    // Given
    let mut csprng = StdRng::seed_from_u64(42);
    let val_pub_key: PublicKey = NewPublicKey::new(VAL_1.clone()).unwrap().into();
    let redeem_tx = create_random_validator_redeem_transaction(&mut csprng, val_pub_key);
    let correlation_id = redeem_tx.tx.correlation_id();
    let reason = RedeemCancellationReason::InvalidData;
    let cancel = RedeemCancellationTransaction {
        correlation_id,
        reason,
    };
    let genesis = GenesisConfig::<Test> {
        clear_utxos: redeem_tx.input_txos.clone(),
        ..Default::default()
    };
    let state = XandstrateStateMock {
        validators: vec![VAL_1.clone()],
        ..Default::default()
    };

    new_test_externalities(genesis, state).execute_with(|| {
        // When
        let req = ConfidentialityPallet::validator_redeem(
            Origin::signed(VAL_1.clone()),
            Wrap(redeem_tx.tx),
        );
        let result =
            ConfidentialityPallet::cancel_pending_redeem(RawOrigin::None.into(), Wrap(cancel));

        // Then
        assert_ok!(req);
        assert_err!(result, DispatchError::BadOrigin);
    });
}
