use sp_core::crypto::AccountId32;
use sp_core::sp_std::convert::TryFrom;
use xand_ledger::curve25519_dalek::ristretto::CompressedRistretto;
use xand_ledger::PublicKey;

pub(crate) mod clear_utxo_repo;
pub(crate) mod correlation_id_repo;
pub(crate) mod create_repo;
pub(crate) mod identity_tag_repo;
pub(crate) mod key_image_repo;
pub(crate) mod member_repo;
pub(crate) mod redeem_repo;
pub(crate) mod total_issuance_repo;
pub(crate) mod transaction_output_repo;
pub(crate) mod trust_metadata_repo;
pub(crate) mod validator_repo;

pub struct NewPublicKey(PublicKey);

impl NewPublicKey {
    pub fn new(account_id: AccountId32) -> Option<Self> {
        let bytes = account_id.as_ref();
        let compressed_ristretto = CompressedRistretto::from_slice(bytes);
        let pub_key = compressed_ristretto.decompress().map(Into::into)?;
        Some(Self(pub_key))
    }
}

impl TryFrom<AccountId32> for NewPublicKey {
    type Error = ();

    fn try_from(value: AccountId32) -> Result<Self, ()> {
        Self::new(value).ok_or(())
    }
}

impl From<NewPublicKey> for PublicKey {
    fn from(wrapper: NewPublicKey) -> Self {
        wrapper.0
    }
}
