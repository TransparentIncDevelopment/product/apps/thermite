use crate::confidentiality;
use serde_scale_wrap::Wrap;
use sp_std::marker::PhantomData;
use xand_ledger::{TransactionDataRepository, TransactionOutput, TxoStatus};

#[derive(Default)]
pub(crate) struct TransactionOutputRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T> TransactionDataRepository<T::BlockNumber> for TransactionOutputRepo<T>
where
    T: confidentiality::Config,
{
    fn lookup_status(&self, output: &TransactionOutput) -> (TxoStatus, Option<T::BlockNumber>) {
        let (status, block) = confidentiality::TXOs::<T>::get(Wrap(*output));
        (status.0, Some(block))
    }

    fn set_output_status(&self, output: TransactionOutput, new_status: TxoStatus) {
        let (_, block) = confidentiality::TXOs::<T>::get(Wrap(output));
        let new_state = (Wrap(new_status), block);
        confidentiality::TXOs::<T>::insert(Wrap(output), new_state)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        confidentiality::ledger_adapters::transaction_output_repo::TransactionOutputRepo,
        confidentiality::tests::{new_test_externalities, Test},
        confidentiality::GenesisConfig,
    };
    use rand::{prelude::StdRng, SeedableRng};
    use serde_scale_wrap::Wrap;
    use sp_std::marker::PhantomData;
    use xand_ledger::{TransactionDataRepository, TransactionOutput, TxoStatus};

    #[test]
    fn transaction_repo_storage() {
        let mut csprng = StdRng::seed_from_u64(42);
        let existing_txo = TransactionOutput::random(&mut csprng);
        let nonexistent_txo = TransactionOutput::random(&mut csprng);
        let genesis = GenesisConfig::<Test> {
            txos: vec![(Wrap(existing_txo), (Wrap(TxoStatus::Confirmed), 0))],
            ..Default::default()
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TransactionOutputRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(repo.lookup_status(&existing_txo).0, TxoStatus::Confirmed);
            assert_eq!(
                repo.lookup_status(&nonexistent_txo).0,
                TxoStatus::Nonexistent
            );
            repo.set_output_status(nonexistent_txo, TxoStatus::Pending);
            assert_eq!(repo.lookup_status(&nonexistent_txo).0, TxoStatus::Pending);
            repo.set_output_status(nonexistent_txo, TxoStatus::Confirmed);
            assert_eq!(repo.lookup_status(&nonexistent_txo).0, TxoStatus::Confirmed);
        });
    }
}
