use crate::confidentiality;
use core::marker::PhantomData;
use xand_ledger::TotalIssuanceRepository;

pub(crate) struct TotalIssuanceRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T: confidentiality::Config> TotalIssuanceRepository for TotalIssuanceRepo<T> {
    fn get_total_cash_confirmations(&self) -> u64 {
        confidentiality::TotalCreated::<T>::get()
    }

    fn add_to_total_cash_confirmations(&self, amount: u64) -> u64 {
        confidentiality::TotalCreated::<T>::mutate(|x| {
            *x = x.saturating_add(amount);
            *x
        })
    }

    fn add_to_total_redeemed(&self, amount: u64) -> u64 {
        confidentiality::TotalRedeemed::<T>::mutate(|x| {
            *x = x.saturating_add(amount);
            *x
        })
    }

    fn subtract_from_total_redeemed(&self, amount: u64) -> u64 {
        confidentiality::TotalRedeemed::<T>::mutate(|x| {
            *x = x.saturating_sub(amount);
            *x
        })
    }

    fn get_total_redeemed(&self) -> u64 {
        confidentiality::TotalRedeemed::<T>::get()
    }
}

#[cfg(test)]
mod unit_tests {
    use crate::confidentiality::{
        ledger_adapters::total_issuance_repo::TotalIssuanceRepo, tests::*, GenesisConfig,
    };
    use std::marker::PhantomData;
    use xand_ledger::TotalIssuanceRepository;

    #[test]
    fn can_get_total_created() {
        const TOTAL_CREATED: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_created: TOTAL_CREATED,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(repo.get_total_cash_confirmations(), TOTAL_CREATED);
        })
    }

    #[test]
    fn can_add_to_total_created() {
        const INITIAL_TOTAL_CREATED: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_created: INITIAL_TOTAL_CREATED,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            repo.add_to_total_cash_confirmations(5);
            assert_eq!(
                repo.get_total_cash_confirmations(),
                INITIAL_TOTAL_CREATED + 5
            );
        })
    }

    #[test]
    fn does_not_overflow_total_created() {
        const INITIAL_TOTAL_CREATED: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_created: INITIAL_TOTAL_CREATED,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            repo.add_to_total_cash_confirmations(u64::MAX);
            assert_eq!(repo.get_total_cash_confirmations(), u64::MAX);
        })
    }

    #[test]
    fn can_add_to_total_redeemed() {
        let total_redeemed: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_redeemed,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            let add_amount = 5;
            repo.add_to_total_redeemed(add_amount);
            assert_eq!(repo.get_total_redeemed(), total_redeemed + add_amount);
        })
    }

    #[test]
    fn add_does_not_overflow_total_redeemed() {
        let total_redeemed: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_redeemed,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            let add_amount = u64::MAX;
            repo.add_to_total_redeemed(add_amount);
            assert_eq!(repo.get_total_redeemed(), u64::MAX);
        })
    }

    #[test]
    fn subtract_does_not_underflow_total_redeemed() {
        let total_redeemed: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_redeemed,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            let subtract_amount = u64::MAX;
            repo.subtract_from_total_redeemed(subtract_amount);
            assert_eq!(repo.get_total_redeemed(), 0)
        })
    }

    #[test]
    fn can_substract_from_total_redeemed() {
        let total_redeemed: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_redeemed,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            let subtract_amount = 5;
            repo.subtract_from_total_redeemed(subtract_amount);
            assert_eq!(repo.get_total_redeemed(), total_redeemed - subtract_amount);
        })
    }

    #[test]
    fn can_get_total_redeemed() {
        let total_redeemed: u64 = 42;
        let genesis = GenesisConfig::<Test> {
            total_redeemed,
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let repo = TotalIssuanceRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(repo.get_total_redeemed(), total_redeemed);
        })
    }
}
