use crate::confidentiality;
use core::marker::PhantomData;
use xand_ledger::{CorrelationId, CorrelationIdRepository, CorrelationIdUsedBy};

pub(crate) struct CorrelationIdRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

impl<T: confidentiality::Config> CorrelationIdRepository for CorrelationIdRepo<T> {
    fn is_used(&self, id: CorrelationId) -> Option<CorrelationIdUsedBy> {
        if confidentiality::PendingCreates::<T>::contains_key(id) {
            return Some(CorrelationIdUsedBy::PendingCreateRequest);
        }
        if confidentiality::PendingRedeems::<T>::contains_key(id) {
            return Some(CorrelationIdUsedBy::PendingRedeemRequest);
        }
        if confidentiality::UsedCorrelationIds::<T>::contains_key(id) {
            return Some(CorrelationIdUsedBy::UsedCorrelationId);
        }
        None
    }

    fn store_id(&self, id: CorrelationId) {
        confidentiality::UsedCorrelationIds::<T>::insert(id, pallet_timestamp::Module::<T>::get());
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::confidentiality::ledger_adapters::redeem_repo::tests::random_confidential_redeem_record;
    use crate::confidentiality::{tests::*, GenesisConfig};
    use rand::{prelude::StdRng, SeedableRng};
    use serde_scale_wrap::Wrap;
    use xand_ledger::{CreateRequestRecord, IdentityTag, VerifiableEncryptionOfSignerKey};

    #[test]
    fn correlation_id_used_by_create() {
        let id = [1; 16];
        let mut csprng = StdRng::seed_from_u64(42);
        let fake_create = Wrap(CreateRequestRecord {
            outputs: vec![],
            identity_output: IdentityTag::random(&mut csprng),
            account_data: vec![],
            expires_at: 0.into(),
            encrypted_sender: VerifiableEncryptionOfSignerKey::random(&mut csprng).into(),
        });

        let genesis = GenesisConfig::<Test> {
            pending_creates: vec![(id, fake_create)],
            ..Default::default()
        };

        let cr = CorrelationIdRepo::<Test> {
            _module_marker: PhantomData,
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            assert_eq!(
                cr.is_used(id),
                Some(CorrelationIdUsedBy::PendingCreateRequest)
            );
        });
    }

    #[test]
    fn correlation_id_used_by_redeem() {
        let id = [1; 16];
        let mut csprng = StdRng::seed_from_u64(42);
        let fake_redeem = Wrap(random_confidential_redeem_record(&mut csprng));

        let genesis = GenesisConfig::<Test> {
            pending_redeems: vec![(id, fake_redeem)],
            ..Default::default()
        };

        let cr = CorrelationIdRepo::<Test> {
            _module_marker: PhantomData,
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            assert_eq!(
                cr.is_used(id),
                Some(CorrelationIdUsedBy::PendingRedeemRequest)
            );
        });
    }

    #[test]
    fn correlation_id_used_generic() {
        let id = [2; 16];
        let genesis = GenesisConfig::<Test>::default();
        let cr = CorrelationIdRepo::<Test> {
            _module_marker: PhantomData,
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            confidentiality::UsedCorrelationIds::<Test>::insert(
                id,
                pallet_timestamp::Module::<Test>::get(),
            );
            assert_eq!(cr.is_used(id), Some(CorrelationIdUsedBy::UsedCorrelationId));
        });
    }

    #[test]
    fn can_store_correlation_id() {
        let id = [2; 16];
        let genesis = GenesisConfig::<Test>::default();
        let cr = CorrelationIdRepo::<Test> {
            _module_marker: PhantomData,
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            cr.store_id(id);
            assert_eq!(cr.is_used(id), Some(CorrelationIdUsedBy::UsedCorrelationId));
        });
    }

    #[test]
    fn no_id_returns_none() {
        let id = [2; 16];
        let genesis = GenesisConfig::<Test>::default();
        let cr = CorrelationIdRepo::<Test> {
            _module_marker: PhantomData,
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            assert_eq!(cr.is_used(id), None);
        });
    }
}
