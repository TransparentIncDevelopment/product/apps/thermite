use crate::confidentiality;
use crate::BlockNumber;
use core::marker::PhantomData;
use frame_support::IterableStorageMap;
use serde_scale_wrap::Wrap;
use sp_runtime::traits::SaturatedConversion;
use xand_ledger::{
    BlockInfoRepository, CorrelationId, CreateRequestRecord, CreateRequestRepository, Milliseconds,
    PendingCreateRequestExpireTime,
};

pub(crate) struct CreateRequestRepo<T> {
    pub(crate) _module_marker: PhantomData<T>,
}

pub(crate) struct PendingCreateMapUnwrapper<T: confidentiality::Config> {
    inner: <confidentiality::PendingCreates<T> as IterableStorageMap<
        CorrelationId,
        Wrap<CreateRequestRecord>,
    >>::Iterator,
}

impl<T: confidentiality::Config> Iterator for PendingCreateMapUnwrapper<T> {
    type Item = (CorrelationId, CreateRequestRecord);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|(a, b)| (a, b.0))
    }
}

impl<T: confidentiality::Config> CreateRequestRepository for CreateRequestRepo<T> {
    type Iter = PendingCreateMapUnwrapper<T>;

    fn get_create_request(&self, correlation_id: CorrelationId) -> Option<CreateRequestRecord> {
        confidentiality::PendingCreates::<T>::get(correlation_id).map(|x| x.0)
    }

    fn store_create_request(&self, correlation_id: CorrelationId, record: CreateRequestRecord) {
        confidentiality::PendingCreates::<T>::insert(correlation_id, Wrap(record))
    }

    fn remove_create_request(&self, correlation_id: CorrelationId) {
        confidentiality::PendingCreates::<T>::remove(correlation_id)
    }

    fn all_pending_create_requests(&self) -> Self::Iter {
        // Unwrap from the serialization wrapper
        PendingCreateMapUnwrapper {
            inner: confidentiality::PendingCreates::<T>::iter(),
        }
    }
}

impl<T: confidentiality::Config> BlockInfoRepository for CreateRequestRepo<T> {
    fn current_block(&self) -> BlockNumber {
        frame_system::Module::<T>::block_number().saturated_into::<u32>()
    }

    fn current_block_timestamp(&self) -> Milliseconds {
        pallet_timestamp::Module::<T>::get()
            .saturated_into::<u64>()
            .into()
    }
}

impl<T: confidentiality::Config> PendingCreateRequestExpireTime for CreateRequestRepo<T> {
    fn get_pending_create_request_expire_time(&self) -> Milliseconds {
        confidentiality::PendingCreateExpireTime::<T>::get()
            .saturated_into::<u64>()
            .into()
    }

    fn calc_expire_time_for_new_create_request(&self) -> Milliseconds {
        let cur_ts: Milliseconds = pallet_timestamp::Module::<T>::get()
            .saturated_into::<u64>()
            .into();
        cur_ts + self.get_pending_create_request_expire_time()
    }
}

#[cfg(test)]
mod unit_tests {
    use crate::confidentiality::{
        ledger_adapters::create_repo::CreateRequestRepo,
        tests::{SLOT_DURATION, *},
        GenesisConfig,
    };
    use rand::{prelude::StdRng, SeedableRng};
    use serde_scale_wrap::Wrap;
    use std::marker::PhantomData;
    use xand_ledger::{
        BlockInfoRepository, CreateRequestRecord, CreateRequestRepository, IdentityTag,
        PendingCreateRequestExpireTime, VerifiableEncryptionOfSignerKey,
    };

    #[test]
    fn cant_fetch_nonexistent_create_req() {
        let genesis = GenesisConfig::<Test>::default();
        let cid = [1; 16];
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.get_create_request(cid), None);
        })
    }

    #[test]
    fn can_fetch_create_req_from_genesis() {
        let mut csprng = StdRng::seed_from_u64(42);
        let cid = [1; 16];
        let fake_create = CreateRequestRecord {
            outputs: vec![],
            identity_output: IdentityTag::random(&mut csprng),
            account_data: vec![],
            expires_at: 0.into(),
            encrypted_sender: VerifiableEncryptionOfSignerKey::random(&mut csprng).into(),
        };

        let genesis = GenesisConfig::<Test> {
            pending_creates: vec![(cid, Wrap(fake_create.clone()))],
            ..Default::default()
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.get_create_request(cid), Some(fake_create));
        })
    }

    #[test]
    fn can_retrieve_store_pending_create_request() {
        let mut csprng = StdRng::seed_from_u64(42);
        let genesis = GenesisConfig::<Test>::default();
        let cid = [1; 16];
        let fake_create = CreateRequestRecord {
            outputs: vec![],
            identity_output: IdentityTag::random(&mut csprng),
            account_data: vec![],
            expires_at: 0.into(),
            encrypted_sender: VerifiableEncryptionOfSignerKey::random(&mut csprng).into(),
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.get_create_request(cid), None);
            // Store the create req
            cr.store_create_request(cid, fake_create.clone());
            // Ensure it can be properly fetched after storage
            assert_eq!(cr.get_create_request(cid), Some(fake_create));
        })
    }

    #[test]
    fn can_iterate_stored_pending_create_request() {
        let mut csprng = StdRng::seed_from_u64(42);
        let genesis = GenesisConfig::<Test>::default();
        let cid = [1; 16];
        let fake_create = CreateRequestRecord {
            outputs: vec![],
            identity_output: IdentityTag::random(&mut csprng),
            account_data: vec![],
            expires_at: 0.into(),
            encrypted_sender: VerifiableEncryptionOfSignerKey::random(&mut csprng).into(),
        };
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            //ensure the repository is empty before storing
            assert!(cr.all_pending_create_requests().next().is_none());
            // Store the create req
            cr.store_create_request(cid, fake_create.clone());
            // Ensure it can be properly fetched after storage
            assert_eq!(cr.all_pending_create_requests().count(), 1);
        })
    }

    #[test]
    fn can_remove_pending_create_request() {
        let mut csprng = StdRng::seed_from_u64(42);
        let cid = [1; 16];
        let fake_create = CreateRequestRecord {
            outputs: vec![],
            identity_output: IdentityTag::random(&mut csprng),
            account_data: vec![],
            expires_at: 0.into(),
            encrypted_sender: VerifiableEncryptionOfSignerKey::random(&mut csprng).into(),
        };
        let genesis = GenesisConfig::<Test> {
            pending_creates: vec![(cid, Wrap(fake_create.clone()))],
            ..Default::default()
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.get_create_request(cid), Some(fake_create.clone()));
            cr.remove_create_request(cid);
            assert_eq!(cr.get_create_request(cid), None);
        })
    }

    #[test]
    fn removing_nonexistent_create_request_doesnt_blow_things_up() {
        let genesis = GenesisConfig::<Test>::default();
        let cid = [1; 16];
        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            cr.remove_create_request(cid);
        })
    }

    #[test]
    fn can_get_all_pending_creates() {
        let mut csprng = StdRng::seed_from_u64(42);
        let genesis_pending_creates = (1..=3)
            .map(|i| {
                (
                    [i; 16],
                    CreateRequestRecord {
                        outputs: vec![],
                        identity_output: IdentityTag::random(&mut csprng),
                        account_data: vec![],
                        expires_at: 0.into(),
                        encrypted_sender: VerifiableEncryptionOfSignerKey::random(&mut csprng)
                            .into(),
                    },
                )
            })
            .collect::<Vec<_>>();
        let genesis = GenesisConfig::<Test> {
            pending_creates: genesis_pending_creates
                .iter()
                .map(|(id, record)| (*id, Wrap(record.clone())))
                .collect(),
            ..Default::default()
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            let mut fetched_pending_creates: Vec<_> = cr.all_pending_create_requests().collect();
            fetched_pending_creates.sort_by_key(|a| a.0);
            assert_eq!(genesis_pending_creates, fetched_pending_creates);
        })
    }

    #[test]
    fn pending_create_exp_time_storage() {
        let genesis = GenesisConfig::<Test> {
            pending_create_expire_time: 10,
            ..Default::default()
        };

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.get_pending_create_request_expire_time(), 10.into());
            // The "current block" is zero here, so it should expire in 10
            assert_eq!(cr.calc_expire_time_for_new_create_request(), 10.into());
        });
    }

    #[test]
    fn can_get_current_block() {
        let genesis = GenesisConfig::<Test>::default();

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            run_to_block(5);
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.current_block(), 5)
        });
    }

    #[test]
    fn can_get_current_block_timestamp() {
        let genesis = GenesisConfig::<Test>::default();

        new_test_externalities(genesis, Default::default()).execute_with(|| {
            run_to_block(5);
            let cr = CreateRequestRepo::<Test> {
                _module_marker: PhantomData,
            };
            assert_eq!(cr.current_block_timestamp(), (5 * SLOT_DURATION).into());
        });
    }
}
