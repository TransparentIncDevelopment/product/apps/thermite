use sp_core::crypto::AccountId32;

// lazy_static allows us to use the non-const fn `create_acct_id()` in a static variable.
// Hardcoding the values to keep it WASM compatible but including tests below to verify values
// (the tests include the stuff that isn't WASM compatible).
lazy_static::lazy_static! {
    pub static ref VAL_1: AccountId32 = [30, 164, 30, 93, 96, 164, 50, 58, 241, 124, 20, 194, 222, 99, 221, 187, 127, 5, 188, 0, 201, 128, 210, 86, 153, 230, 242, 90, 137, 11, 91, 103].into();
    pub static ref MEM_1: AccountId32 = [222, 62, 120, 158, 205, 83, 67, 31, 229, 192, 108, 18, 183, 33, 55, 21, 52, 150, 218, 206, 53, 198, 149, 181, 244, 215, 180, 31, 126, 213, 118, 59].into();
    pub static ref TRUST: AccountId32 = [244, 86, 63, 133, 161, 227, 46, 66, 75, 242, 252, 142, 68, 115, 218, 50, 156, 184, 64, 75, 38, 46, 5, 60, 29, 49, 67, 64, 126, 239, 30, 112].into();
    pub static ref NON_EXISTENT_TRUST: AccountId32 = [0, 65, 92, 198, 194, 43, 247, 59, 80, 34, 147, 192, 95, 119, 254, 130, 5, 212, 26, 124, 84, 2, 124, 150, 247, 41, 143, 10, 230, 0, 107, 122].into();
}

#[cfg(test)]
mod tests {
    use super::*;
    use sp_core::{crypto::AccountId32, sr25519, Pair};

    pub(crate) fn create_acct_id(acct_name: &str) -> AccountId32 {
        // Enables decoding bytes into AccountId
        dev_account_key(acct_name).public().into()
    }

    /// Generates a sr25519 keypair using substrate's default dev passphrase, and the provided string
    /// as a sub-path. Note that this is *very slow* and should not be done in a tight loop in tests.
    ///
    /// # Only use this for testing!
    pub fn dev_account_key(s: &str) -> sr25519::Pair {
        sr25519::Pair::from_string(&format!("//{}", s), None)
            .expect("Any string is a valid subpath for an account key")
    }

    #[test]
    fn check_constants() {
        let val_bytes: [u8; 32] = create_acct_id("42").into();
        let mem_bytes: [u8; 32] = create_acct_id("10").into();
        let trust_bytes: [u8; 32] = create_acct_id("88").into();
        let non_trust_bytes: [u8; 32] = create_acct_id("99").into();
        assert_eq!(<[u8; 32]>::from(VAL_1.clone()), val_bytes);
        assert_eq!(<[u8; 32]>::from(MEM_1.clone()), mem_bytes);
        assert_eq!(<[u8; 32]>::from(TRUST.clone()), trust_bytes);
        assert_eq!(
            <[u8; 32]>::from(NON_EXISTENT_TRUST.clone()),
            non_trust_bytes
        );
    }
}
