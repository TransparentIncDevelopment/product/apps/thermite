pub mod adapters;

use core::convert::TryInto;

use frame_support::{dispatch::DispatchResult, ensure};
use frame_system as system;

use xand_governance::{
    models::{proposal::Proposal, role::Role},
    permissions::XandGovernancePermissionsImpl,
    ports::{rules::XandGovernancePermissions, status::XandProposalStatusCheck},
    proposal_status::XandProposalStatusCheckImpl,
};

use crate::{
    network_voting::{
        self,
        adapters::{ProposalStatusCheck, VoteSubmissionLifecycle, VotingPermissions},
        PropIndex,
    },
    xandstrate::{self, participants::adapters::check_role, Error},
    xandvalidators,
};

use crate::xandstrate::xand_voting_rules::adapters::queries::get_proposal;
use adapters::queries::get_vote_tallies_for_proposal;

pub struct XandVotingRules<T: xandstrate::Config + xandvalidators::Config + network_voting::Config>(
    core::marker::PhantomData<T>,
);

impl VotingPermissions<crate::Runtime, crate::Call> for XandVotingRules<crate::Runtime> {
    type Error = Error<crate::Runtime>;

    fn can_propose(
        proposer: &crate::AccountId,
        proposal: &crate::Call,
    ) -> Result<bool, Self::Error> {
        let proposal: Proposal = proposal.try_into()?;
        let role: Role = check_role::<crate::Runtime>(proposer)?;

        Ok(XandGovernancePermissionsImpl::can_propose(role, proposal))
    }

    fn can_vote(voter: &crate::AccountId, proposal: &crate::Call) -> Result<bool, Self::Error> {
        let proposal: Proposal = proposal.try_into()?;
        let role: Role = check_role::<crate::Runtime>(voter)?;

        Ok(XandGovernancePermissionsImpl::can_vote(role, proposal))
    }
}

impl ProposalStatusCheck<crate::Runtime> for XandVotingRules<crate::Runtime> {
    type Error = Error<crate::Runtime>;

    fn vote_status(prop_id: u32) -> Result<network_voting::VoteStatus, Self::Error> {
        let tallies = get_vote_tallies_for_proposal::<crate::Runtime>(prop_id);
        let proposal_call: Option<crate::Call> = get_proposal::<crate::Runtime>(prop_id);

        if let Some(p) = proposal_call {
            let proposal: Proposal = (&p).try_into()?;
            let status = XandProposalStatusCheckImpl::check_status(&proposal, &tallies);
            Ok(status.into())
        } else {
            Err(Error::ProposalIdNotFound)
        }
    }
}

impl<T> VoteSubmissionLifecycle<T> for XandVotingRules<T>
where
    T: xandstrate::Config,
{
    fn before_vote_submitted(
        voter: &<T as system::Config>::AccountId,
        prop_id: PropIndex,
        _vote: bool,
    ) -> DispatchResult {
        ensure!(
            !network_voting::Votes::<T>::contains_key(prop_id, voter),
            Error::<T>::VoteAlreadySubmittedForAccount,
        );
        Ok(())
    }
}
