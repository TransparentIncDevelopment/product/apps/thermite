#![allow(clippy::too_many_arguments)]
// This is needed because the codegen from decl_runtime_apis! fails newer versions of clippy
#![allow(clippy::unnecessary_mut_passed)]

use crate::{
    network_voting::{PropIndex, ProposalStage},
    validator_emissions::models::{
        validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    xandstrate::{PendingCreate, PendingRedeem},
    EncryptionPubKey,
};
use parity_scale_codec::{Codec, Decode, Encode};
use sp_api::decl_runtime_apis;
use sp_std::vec::Vec;
use xand_runtime_models::{CidrBlockArray, FiatReqCorrelationId};

/// Representation of all data available for a specific proposal
#[derive(Decode, Encode, PartialEq, Eq, Clone, Debug)]
#[cfg_attr(feature = "std", derive(serde::Deserialize, serde::Serialize))]
pub struct ProposalData<AccountId, BlockNumber> {
    pub id: PropIndex,
    pub votes: Vec<(AccountId, bool)>,
    pub proposer: AccountId,
    pub expiration_block_id: BlockNumber,
    pub proposed_action: Vec<u8>,
    pub status: ProposalStage,
}

/// Generically convert one `ProposalData` instance into another, mapping the `AccountId` and
/// `BlockNumber` type parameters.
pub fn convert_data<A1, B1, A2, B2, A, B>(
    data: ProposalData<A1, B1>,
    mut map_account: A,
    mut map_block: B,
) -> ProposalData<A2, B2>
where
    A: FnMut(A1) -> A2,
    B: FnMut(B1) -> B2,
{
    ProposalData {
        id: data.id,
        votes: data
            .votes
            .into_iter()
            .map(|(a, v)| (map_account(a), v))
            .collect(),
        proposer: map_account(data.proposer),
        expiration_block_id: map_block(data.expiration_block_id),
        proposed_action: data.proposed_action,
        status: data.status,
    }
}

decl_runtime_apis! {
    pub trait AllowlistApi<AccountId> where AccountId: Codec {
        fn get_allowlist() -> Vec<(AccountId, CidrBlockArray)>;
    }

    // Technically, being generic over blocknumber is unnecessary since `Block` is secretly
    // passed in by the macro, and block has blocknumber as an associated type deep within it.
    // In practice, getting the bounds to cooperate was enough of a pain in the ass to ignore it.
    // TODO: Remove above comment and fix tech debt

    pub trait ConfidentialModeApi {
        fn get_confidential_mode() -> bool;
    }

    pub trait PendingFiatApi<AccountId, BalanceType, BlockNumber>
        where AccountId: Codec, BalanceType: Codec, BlockNumber: Codec {
        fn get_pending_creates()
            -> Vec<(FiatReqCorrelationId, PendingCreate<BalanceType, AccountId, BlockNumber>)>;
        fn get_pending_redeems()
            -> Vec<(FiatReqCorrelationId, PendingRedeem<BalanceType, AccountId>)>;
    }

    pub trait ProposalsApi<AccountId: Codec, BlockNumber: Codec> {
        fn get_proposal(id: PropIndex) -> Option<ProposalData<AccountId, BlockNumber>>;
        fn get_all_proposals() -> Vec<ProposalData<AccountId, BlockNumber>>;
    }

    pub trait TrusteeApi<AccountId> where AccountId: Decode {
        fn get_trustee() -> AccountId;
    }

    pub trait LimitedAgentApi<AccountId> where AccountId: Decode {
        fn get_limited_agent() -> Option<AccountId>;
    }

    pub trait MemberInfoApi<AccountId> where AccountId: Decode {
        fn get_members() -> Vec<AccountId>;
        fn get_banned_members() -> Vec<AccountId>;
    }

    pub trait AuthorityKeyApi<AccountId> where AccountId: Decode {
        fn get_authority_keys() -> Vec<AccountId>;
    }

    pub trait EncryptionApi<AccountId> where AccountId: Codec {
        fn get_encryption_key(account: AccountId) -> Option<EncryptionPubKey>;
    }

    pub trait TotalIssuanceApi {
        fn get_total_issuance() -> u64;
    }

    pub trait ValidatorEmissionsApi<AccountId> where AccountId: Codec {
        fn get_validator_emission_rate() -> ValidatorEmissionRate;
        fn get_validator_emission_progress(account: AccountId) -> Option<ValidatorEmissionProgress>;
    }

    pub trait PendingCreateExpireTimeApi {
        fn get_pending_create_expire_time() -> u32;
    }
}
