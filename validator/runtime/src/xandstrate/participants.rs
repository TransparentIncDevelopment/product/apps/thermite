pub mod adapters;

use super::{Config, LimitedAgentId, RegisteredMembers, TrustNodeId};
use crate::xandvalidators::queryable_xand_validators::QueryableXandValidators;
use frame_system as system;

pub fn is_member<T: Config>(account: &<T as system::Config>::AccountId) -> bool {
    RegisteredMembers::<T>::get(account)
}

pub fn is_trust<T: Config>(account: &<T as system::Config>::AccountId) -> bool {
    &TrustNodeId::<T>::get() == account
}

pub fn is_limited_agent<T: Config>(account: &<T as system::Config>::AccountId) -> bool {
    match &LimitedAgentId::<T>::get() {
        Some(l_a) => l_a == account,
        None => false,
    }
}

pub fn is_member_or_trust_or_validator<T: Config>(account: &T::AccountId) -> bool {
    is_member::<T>(account)
        || is_trust::<T>(account)
        || <T as QueryableXandValidators>::is_validator(account)
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use crate::xandstrate::participants::{is_limited_agent, is_member, is_trust};
    use crate::xandstrate::xandstrate_test_runtime::constants::*;
    use crate::xandstrate::xandstrate_test_runtime::test_runtime::{base_test_ext, TestRuntime};
    use crate::xandvalidators::queryable_xand_validators::QueryableXandValidators;

    #[test]
    fn is_member__true_for_member() {
        base_test_ext().execute_with(|| {
            assert!(is_member::<TestRuntime>(&MEM_2));
        })
    }

    #[test]
    fn is_validator__false_for_member() {
        base_test_ext().execute_with(|| {
            assert!(!<TestRuntime as QueryableXandValidators>::is_validator(
                &MEM_2
            ));
        })
    }

    #[test]
    fn is_validator__true_for_validator() {
        base_test_ext().execute_with(|| {
            assert!(<TestRuntime as QueryableXandValidators>::is_validator(
                &VAL_1
            ));
        })
    }

    #[test]
    fn is_trust__false_for_validator() {
        base_test_ext().execute_with(|| {
            assert!(!is_trust::<TestRuntime>(&VAL_1));
        })
    }

    #[test]
    fn is_limited_agent__true_for_la() {
        base_test_ext().execute_with(|| {
            assert!(is_limited_agent::<TestRuntime>(&LIMITED_AGENT));
        })
    }

    #[test]
    fn is_trust__true_for_trust() {
        base_test_ext().execute_with(|| {
            assert!(is_trust::<TestRuntime>(&TRUST));
        })
    }
}
