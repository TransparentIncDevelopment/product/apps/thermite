use frame_support::{assert_err, assert_ok};

use xand_runtime_models::{CidrBlock, CidrBlockArray};

use crate::xandstrate::{
    xandstrate_test_runtime::{
        constants::{MEM_1, TRUST, VAL_1},
        test_runtime::{
            base_test_ext, multi_vals_test_ext, Origin, TestModule, TestRuntime, ValidatorMod,
        },
    },
    AllowListedCidrBlocks, Error,
};

#[test]
fn can_not_double_add_cidr_block() {
    let cidr_block = [2; 5].into();
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).unwrap();
        assert!(TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).is_err());
    })
}

#[test]
fn can_not_remove_non_existent_cidr_block() {
    let cidr_block = [3; 5].into();
    base_test_ext().execute_with(|| {
        assert!(
            TestModule::remove_allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).is_err()
        );
    })
}

#[test]
fn can_not_add_cidr_block_when_full() {
    let cidr_block = [4; 5].into();
    let full_array = [Some(CidrBlock([5; 5])); 10];
    base_test_ext().execute_with(|| {
        AllowListedCidrBlocks::<TestRuntime>::insert(MEM_1, full_array);
        assert!(TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).is_err())
    })
}

#[test]
fn non_member_trust_validator_can_not_add_cidr_block() {
    let cidr_block = [5; 5].into();
    let signature = 666;
    base_test_ext().execute_with(|| {
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(
            signature
        ));
        assert_err!(
            TestModule::allowlist_cidr_block(Origin::signed(signature), cidr_block),
            Error::<TestRuntime>::OnlyMembersTrustAndValidatorsCanAddAllowlistCidrBlocks
        );
    });
}

#[test]
fn trust_can_add_cidr_block() {
    let cidr_block: CidrBlock = [0; 5].into();
    base_test_ext().execute_with(|| {
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(TRUST));
        assert_ok!(TestModule::allowlist_cidr_block(
            Origin::signed(TRUST),
            cidr_block
        ));
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(TRUST);
        assert!(blocks.contains(&Some(cidr_block)));
    })
}

#[test]
fn member_can_add_cidr_block() {
    let cidr_block: CidrBlock = [0; 5].into();
    base_test_ext().execute_with(|| {
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(MEM_1));
        assert_ok!(TestModule::allowlist_cidr_block(
            Origin::signed(MEM_1),
            cidr_block
        ));
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(MEM_1);
        assert!(blocks.contains(&Some(cidr_block)));
    })
}

#[test]
fn validator_can_add_cidr_block() {
    let cidr_block: CidrBlock = [0; 5].into();
    base_test_ext().execute_with(|| {
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(VAL_1));
        assert_ok!(TestModule::allowlist_cidr_block(
            Origin::signed(VAL_1),
            cidr_block
        ));
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(VAL_1);
        assert!(blocks.contains(&Some(cidr_block)));
    })
}

#[test]
fn non_member_trust_validator_or_trust_can_not_remove_cidr_block() {
    let cidr_block: CidrBlock = [6; 5].into();
    let mut blocks: CidrBlockArray = [None; 10];
    blocks[0] = Some(cidr_block);
    let signature = 666;
    base_test_ext().execute_with(|| {
        AllowListedCidrBlocks::<TestRuntime>::insert(signature, blocks);
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(signature);
        assert!(blocks.contains(&Some(cidr_block)));
        assert_err!(
            TestModule::remove_allowlist_cidr_block(Origin::signed(signature), cidr_block),
            Error::<TestRuntime>::OnlyMembersTrustAndValidatorsCanRemoveAllowlistCidrBlocks
        );
    })
}

#[test]
fn trust_can_remove_cidr_block() {
    let cidr_block: CidrBlock = [0; 5].into();
    let mut blocks: CidrBlockArray = [None; 10];
    blocks[0] = Some(cidr_block);
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(TRUST), cidr_block).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(TRUST);
        assert!(blocks.contains(&Some(cidr_block)));
        assert_ok!(TestModule::remove_allowlist_cidr_block(
            Origin::signed(TRUST),
            cidr_block
        ));
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(TRUST));
    })
}

#[test]
fn member_can_remove_cidr_block() {
    let cidr_block: CidrBlock = [0; 5].into();
    let mut blocks: CidrBlockArray = [None; 10];
    blocks[0] = Some(cidr_block);
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(MEM_1);
        assert!(blocks.contains(&Some(cidr_block)));
        assert_ok!(TestModule::remove_allowlist_cidr_block(
            Origin::signed(MEM_1),
            cidr_block
        ));
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(MEM_1));
    })
}

#[test]
fn validator_can_remove_cidr_block() {
    let cidr_block: CidrBlock = [0; 5].into();
    let mut blocks: CidrBlockArray = [None; 10];
    blocks[0] = Some(cidr_block);
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(VAL_1), cidr_block).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(VAL_1);
        assert!(blocks.contains(&Some(cidr_block)));
        assert_ok!(TestModule::remove_allowlist_cidr_block(
            Origin::signed(VAL_1),
            cidr_block
        ));
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(VAL_1));
    })
}

#[test]
fn root_can_add_cidr_block_for_account() {
    let cidr_block = [0; 5].into();
    base_test_ext().execute_with(|| {
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(MEM_1));
        assert_ok!(TestModule::root_allowlist_cidr_block(
            Origin::root(),
            MEM_1,
            cidr_block
        ));
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(MEM_1);
        assert!(blocks.contains(&Some(cidr_block)));
    });
}

#[test]
fn root_can_remove_cidr_block() {
    let cidr_block: CidrBlock = [1; 5].into();
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(MEM_1);
        assert!(blocks.contains(&Some(cidr_block)));
        assert_ok!(TestModule::root_remove_allowlist_cidr_block(
            Origin::root(),
            MEM_1,
            cidr_block
        ));
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(MEM_1));
    })
}

#[test]
fn root_can_not_double_add_cidr_block() {
    let cidr_block = [2; 5].into();
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block).unwrap();
        assert_err!(
            TestModule::root_allowlist_cidr_block(Origin::root(), MEM_1, cidr_block),
            Error::<TestRuntime>::CidrBlockAlreadyAllowlisted
        );
    })
}

#[test]
fn root_can_not_remove_non_existent_cidr_block() {
    let cidr_block = [3; 5].into();
    base_test_ext().execute_with(|| {
        assert_err!(
            TestModule::root_remove_allowlist_cidr_block(Origin::root(), MEM_1, cidr_block),
            Error::<TestRuntime>::CidrBlockNotAllowlisted
        );
    })
}

#[test]
fn root_can_not_add_cidr_block_when_full() {
    let cidr_block = [4; 5].into();
    let full_array = [Some(CidrBlock([5; 5])); 10];
    base_test_ext().execute_with(|| {
        AllowListedCidrBlocks::<TestRuntime>::insert(MEM_1, full_array);
        assert_err!(
            TestModule::root_allowlist_cidr_block(Origin::root(), MEM_1, cidr_block),
            Error::<TestRuntime>::CidrBlockAllowlistCapacityExceeded
        )
    })
}

#[test]
fn when_validator_is_removed_associated_cidr_blocks_are_removed() {
    let cidr_block_1: CidrBlock = [1; 5].into();
    let cidr_block_2: CidrBlock = [2; 5].into();
    multi_vals_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(VAL_1), cidr_block_1).unwrap();
        TestModule::allowlist_cidr_block(Origin::signed(VAL_1), cidr_block_2).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(VAL_1);
        assert!(blocks.contains(&Some(cidr_block_1)));
        assert!(blocks.contains(&Some(cidr_block_2)));
        assert_ok!(ValidatorMod::remove_authority_key(Origin::root(), VAL_1));
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(VAL_1))
    })
}

#[test]
fn when_member_is_marked_for_removal_associated_cidr_blocks_persist() {
    let cidr_block_1: CidrBlock = [1; 5].into();
    let cidr_block_2: CidrBlock = [2; 5].into();
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block_1).unwrap();
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block_2).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(MEM_1);
        assert!(blocks.contains(&Some(cidr_block_1)));
        assert!(blocks.contains(&Some(cidr_block_2)));
        assert_ok!(TestModule::remove_member(Origin::root(), MEM_1));
        assert!(AllowListedCidrBlocks::<TestRuntime>::contains_key(MEM_1))
    })
}

#[test]
fn when_member_is_finally_exited_associated_cidr_blocks_are_removed() {
    let cidr_block_1: CidrBlock = [1; 5].into();
    let cidr_block_2: CidrBlock = [2; 5].into();
    base_test_ext().execute_with(|| {
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block_1).unwrap();
        TestModule::allowlist_cidr_block(Origin::signed(MEM_1), cidr_block_2).unwrap();
        let blocks = AllowListedCidrBlocks::<TestRuntime>::get(MEM_1);
        assert!(blocks.contains(&Some(cidr_block_1)));
        assert!(blocks.contains(&Some(cidr_block_2)));
        assert_ok!(TestModule::remove_member(Origin::root(), MEM_1));
        assert_ok!(TestModule::exit_member(Origin::root(), MEM_1));
        assert!(!AllowListedCidrBlocks::<TestRuntime>::contains_key(MEM_1))
    })
}
