#![allow(non_snake_case)]

use frame_support::{assert_err, assert_ok};
use parity_scale_codec::Encode;
use serde_scale_wrap::Wrap;
use sp_runtime::RuntimeString;
use sp_version::RuntimeVersion;
use wasm_hash_verifier::{WasmBlob, WasmHasher, XandHash, XandWasmHasher};
use xand_runtime_models::upgrade::{UpgradeWasmBlob, UpgradeXandHash};

use crate::xandstrate::{
    xandstrate_test_runtime::{
        constants::{LIMITED_AGENT, MEM_1, VAL_1},
        test_runtime::{empty_test_ext, Origin, TestModule, TestRuntime},
    },
    Error,
};

struct CallInWasm(Vec<u8>);

impl sp_core::traits::CallInWasm for CallInWasm {
    fn call_in_wasm(
        &self,
        _: &[u8],
        _: Option<Vec<u8>>,
        _: &str,
        _: &[u8],
        _: &mut dyn sp_externalities::Externalities,
        _: sp_core::traits::MissingHostFunctions,
    ) -> Result<Vec<u8>, String> {
        Ok(self.0.clone())
    }
}

impl CallInWasm {
    pub fn new() -> Self {
        let version = RuntimeVersion {
            spec_name: RuntimeString::from("test"),
            spec_version: 1,
            impl_version: 1,
            ..Default::default()
        };
        Self(version.encode())
    }
}

fn test_blob_and_hash(blob_values: Vec<u8>) -> (UpgradeWasmBlob, UpgradeXandHash) {
    let wasm_blob: WasmBlob<Vec<u8>> = WasmBlob::from(blob_values);
    let hash: XandHash = XandWasmHasher::hash(&wasm_blob);
    (Wrap(wasm_blob), Wrap(hash))
}

// TODO ADO 7578 - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7578 supply valid wasm blob for consistent values
fn arbitrary_constant_blob_and_hash() -> (UpgradeWasmBlob, UpgradeXandHash) {
    let consistent_values = vec![1, 2, 3, 4];
    test_blob_and_hash(consistent_values)
}

fn arbitrary_mismatched_blob_and_hash() -> (UpgradeWasmBlob, UpgradeXandHash) {
    let initial_blob_vals = vec![1, 2, 3, 4];
    let mismatched_blob_vals = vec![4, 3, 2, 1];
    let (_, initial_hash_wrap) = test_blob_and_hash(initial_blob_vals);
    let mismatched_wasm_blob: WasmBlob<Vec<u8>> = WasmBlob::from(mismatched_blob_vals);
    (Wrap(mismatched_wasm_blob), initial_hash_wrap)
}

#[ignore]
// TODO ADO 7578 - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7578 supply valid wasm blob
#[test]
fn upgrade_runtime__root_can_upgrade_runtime() {
    // Given
    let (wasm_blob, hash) = arbitrary_constant_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    //When
    test_ext.execute_with(|| {
        let upgrade_runtime_result = TestModule::upgrade_runtime(Origin::root(), wasm_blob, hash);
        //Then
        assert_ok!(upgrade_runtime_result);
    });
}

#[test]
fn upgrade_runtime__non_root_member_cannot_upgrade_runtime() {
    // Given
    let (wasm_blob, hash) = arbitrary_constant_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    // When
    test_ext.execute_with(|| {
        let upgrade_runtime_result =
            TestModule::upgrade_runtime(Origin::signed(MEM_1), wasm_blob, hash);
        // Then
        assert_err!(
            upgrade_runtime_result,
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
    });
}

#[test]
fn upgrade_runtime__non_root_validator_cannot_upgrade_runtime() {
    // Given
    let (wasm_blob, hash) = arbitrary_constant_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    //When
    test_ext.execute_with(|| {
        let upgrade_runtime_result =
            TestModule::upgrade_runtime(Origin::signed(VAL_1), wasm_blob, hash);
        // Then
        assert_err!(
            upgrade_runtime_result,
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
    });
}

#[test]
fn upgrade_runtime__non_root_limited_agent_cannot_upgrade_runtime() {
    // Given
    let (wasm_blob, hash) = arbitrary_constant_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    //When
    test_ext.execute_with(|| {
        let upgrade_runtime_result =
            TestModule::upgrade_runtime(Origin::signed(LIMITED_AGENT), wasm_blob, hash);
        // Then
        assert_err!(
            upgrade_runtime_result,
            Error::<TestRuntime>::TransactionMustBeDoneByRoot
        );
    });
}

#[ignore]
// TODO ADO 7578 - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7578 supply valid wasm blob
#[test]
fn upgrade_runtime__valid_hash_is_verified() {
    // Given
    let (wasm_blob, hash) = arbitrary_constant_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    //When
    test_ext.execute_with(|| {
        let upgrade_runtime_result = TestModule::upgrade_runtime(Origin::root(), wasm_blob, hash);
        // Then
        assert_ok!(upgrade_runtime_result);
    });
}

#[test]
fn upgrade_runtime__invalid_hash_fails() {
    // Given
    let (wasm_blob, hash) = arbitrary_mismatched_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    //When
    test_ext.execute_with(|| {
        let upgrade_runtime_result = TestModule::upgrade_runtime(Origin::root(), wasm_blob, hash);
        // Then
        assert_err!(
            upgrade_runtime_result,
            Error::<TestRuntime>::UpgradeCodeFailedVerification
        );
    });
}

#[ignore]
// TODO ADO 7578 - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7578 supply valid wasm blob
#[test]
fn upgrade_runtime__can_set_valid_code() {
    todo!("TODO ADO 7578 - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7578");
}

#[test]
fn upgrade_runtime__rejects_invalid_code() {
    // Given
    let (wasm_blob, hash) = arbitrary_constant_blob_and_hash();
    let mut test_ext = empty_test_ext();
    test_ext.register_extension(sp_core::traits::CallInWasmExt::new(CallInWasm::new()));
    //When
    test_ext.execute_with(|| {
        let upgrade_runtime_result = TestModule::upgrade_runtime(Origin::root(), wasm_blob, hash);
        // Then
        assert_err!(
            upgrade_runtime_result,
            Error::<TestRuntime>::UpgradeCodeCannotBeSet
        );
    });
}
