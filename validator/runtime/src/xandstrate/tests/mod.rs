use crate::xandstrate::xandstrate_test_runtime::test_runtime::{TestModule, TestRuntime};
use frame_support::traits::{OnFinalize, OnInitialize};

mod allowlist_tests;
mod participants_tests;
mod upgrade_tests;

fn run_to_block(n: u64) {
    while <frame_system::Module<TestRuntime>>::block_number() <= n {
        TestModule::on_finalize(<frame_system::Module<TestRuntime>>::block_number());
        <frame_system::Module<TestRuntime>>::set_block_number(
            <frame_system::Module<TestRuntime>>::block_number() + 1,
        );
        TestModule::on_initialize(<frame_system::Module<TestRuntime>>::block_number());
    }
}
