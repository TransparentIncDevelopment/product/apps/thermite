use xand_governance::models::vote::{VoteTallies, VoteTally};

use crate::{
    network_voting::{self, PropIndex, Proposals, Votes},
    xandstrate::{self, RegisteredMembers},
    xandvalidators,
};

pub fn get_member_votes<T: xandstrate::Config + network_voting::Config>(
    prop_id: PropIndex,
) -> VoteTally {
    let mut yes = 0;
    let mut no = 0;
    let mut total = 0;
    for (member, _) in RegisteredMembers::<T>::iter() {
        total += 1;
        if Votes::<T>::contains_key(prop_id, &member) {
            if Votes::<T>::get(prop_id, member) {
                yes += 1;
            } else {
                no += 1
            }
        }
    }
    VoteTally {
        yea_votes: yes,
        nay_votes: no,
        total_participant_count: total,
    }
}

pub fn get_validator_votes<T: xandvalidators::Config + network_voting::Config>(
    prop_id: PropIndex,
) -> VoteTally {
    let mut yes = 0;
    let mut no = 0;
    let mut total = 0;
    for validator in xandvalidators::Pallet::<T>::validators().iter() {
        total += 1;
        if Votes::<T>::contains_key(prop_id, validator) {
            if Votes::<T>::get(prop_id, validator) {
                yes += 1;
            } else {
                no += 1;
            }
        }
    }
    VoteTally {
        yea_votes: yes,
        nay_votes: no,
        total_participant_count: total,
    }
}

pub fn get_vote_tallies_for_proposal<
    T: xandstrate::Config + xandvalidators::Config + network_voting::Config,
>(
    prop_id: PropIndex,
) -> VoteTallies {
    let member_tally = get_member_votes::<T>(prop_id);
    let validator_tally = get_validator_votes::<T>(prop_id);
    VoteTallies {
        member_tally,
        validator_tally,
    }
}

pub fn get_proposal<T: network_voting::Config>(prop_id: PropIndex) -> Option<T::Proposal> {
    // Proposals::get returns a tuple of associated types (::Proposal, ::AccountId, ::BlockNumber)
    // map the result to only take the proposal
    <Proposals<T>>::get(prop_id).map(|tup| tup.0)
}
