use core::convert::{TryFrom, TryInto};
use xand_governance::models::proposal::Proposal;

use crate::Error;

impl TryFrom<crate::Call> for Proposal {
    type Error = Error<crate::Runtime>;

    fn try_from(proposal: crate::Call) -> Result<Self, Self::Error> {
        (&proposal).try_into()
    }
}

impl TryFrom<&crate::Call> for Proposal {
    type Error = Error<crate::Runtime>;

    fn try_from(proposal: &crate::Call) -> Result<Self, Self::Error> {
        match proposal {
            crate::Call::XandStrate(xandcall) => match xandcall {
                crate::xandstrate::Call::register_account_as_member(..) => Ok(Proposal::AddMember),
                crate::xandstrate::Call::remove_member(..) => Ok(Proposal::RemoveMember),
                crate::xandstrate::Call::set_trust_node_id(..) => Ok(Proposal::SetTrust),
                crate::xandstrate::Call::set_limited_agent_id(..) => Ok(Proposal::SetLimitedAgent),
                crate::xandstrate::Call::root_allowlist_cidr_block(..) => Ok(Proposal::AddCidrBlock),
                crate::xandstrate::Call::root_remove_allowlist_cidr_block(..) => Ok(Proposal::RemoveCidrBlock),
                crate::xandstrate::Call::upgrade_runtime(..) => Ok(Proposal::RuntimeUpgrade),
                crate::xandstrate::Call::allowlist_cidr_block(..)
                | crate::xandstrate::Call::remove_self(..)
                | crate::xandstrate::Call::remove_allowlist_cidr_block(..)
                | crate::xandstrate::Call::set_member_encryption_key(..)
                | crate::xandstrate::Call::set_trust_encryption_key(..)
                | crate::xandstrate::Call::exit_member(..)
                // This is for sure impossible, but included so we don't have a blanket _
                // arm that might hide errors for new transaction types
                | crate::xandstrate::Call::__Ignore(_, _) => Err(Error::CallRequestedIsNotProposal),
            },
            crate::Call::XandValidators(xandvalscall) => match xandvalscall {
                crate::XandValidatorsCall::add_authority_key(..) => Ok(Proposal::AddValidator),
                crate::XandValidatorsCall::remove_authority_key(..) => {
                    Ok(Proposal::RemoveValidator)
                }
                // This is for sure impossible, but included so we don't have a blanket _
                // arm that might hide errors for new transaction types
                crate::XandValidatorsCall::__Ignore(_, _) => Err(Error::CallRequestedIsNotProposal),
            },
            crate::Call::ValidatorEmissions(val_emissions_call) => match val_emissions_call {
                crate::validator_emissions::Call::set_validator_emission_rate(..) => {
                    Ok(Proposal::SetValidatorEmissionRate)
                }
                // This is for sure impossible, but included so we don't have a blanket _
                // arm that might hide errors for new transaction types
                crate::validator_emissions::Call::__Ignore(_, _) => {
                    Err(Error::CallRequestedIsNotProposal)
                }
            },
            crate::Call::NetworkVoting(..)
            | crate::Call::Sudo(..)
            | crate::Call::Timestamp(..)
            | crate::Call::Session(..)
            | crate::Call::Grandpa(..)
            | crate::Call::RandomnessCollectiveFlip(..)
            | crate::Call::System(..)
            | crate::Call::Authorship(..)
            | crate::Call::Confidentiality(..) => Err(Error::CallRequestedIsNotProposal),
            #[cfg(feature = "test-upgrade-pallet")]
            crate::Call::TestUpgrade(..) => Err(Error::CallRequestedIsNotProposal),
        }
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use core::convert::{TryFrom, TryInto};
    use serde_scale_wrap::Wrap;
    use wasm_hash_verifier::{WasmBlob, WasmHasher, XandWasmHasher};
    use xand_governance::models::proposal::Proposal;

    use xand_runtime_models::CidrBlock;

    use crate::extrinsic_builder::{
        add_authority_key_extrinsic, allowlist_cidr_block, exit_member_extrinsic,
        register_member_extrinsic, remove_allowlist_cidr_block, remove_authority_key_extrinsic,
        remove_member_extrinsic, root_allowlist_cidr_block, root_remove_allowlist_cidr_block,
        set_limited_agent_id_extrinsic, set_member_encryption_key, set_trust_node_id_extrinsic,
        upgrade_runtime,
    };
    use crate::{EncryptionPubKey, Error};

    fn get_test_cidr_block() -> CidrBlock {
        CidrBlock([0, 0, 0, 0, 0])
    }

    #[test]
    fn try_from__register_account_as_member_maps_to_add_member() {
        // Given
        let call = register_member_extrinsic(Default::default(), Default::default());

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::AddMember);
    }

    #[test]
    fn try_from__remove_member_maps_to_remove_member() {
        // Given
        let call = remove_member_extrinsic(Default::default());

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::RemoveMember);
    }

    #[test]
    fn try_from__exit_member_returns_error() {
        // Given
        let call = exit_member_extrinsic(Default::default());

        // When
        let res = Proposal::try_from(call);

        // Then
        assert!(matches!(
            res.unwrap_err(),
            Error::CallRequestedIsNotProposal
        ));
    }

    #[test]
    fn try_from__add_authority_key_extrinsic_maps_to_add_validator() {
        // Given
        let call = add_authority_key_extrinsic(Default::default());

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::AddValidator);
    }

    #[test]
    fn try_from__remove_authority_key_extrinsic_maps_to_remove_validator() {
        // Given
        let call = remove_authority_key_extrinsic(Default::default());

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::RemoveValidator);
    }

    #[test]
    fn try_from__set_trust_node_id_extrinsic_maps_to_set_trust() {
        // Given
        let call = set_trust_node_id_extrinsic(Default::default(), Default::default());

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::SetTrust);
    }

    #[test]
    fn try_from__set_limited_agent_id_extrinsic_maps_to_set_limited_agent() {
        // Given
        let call = set_limited_agent_id_extrinsic(Default::default());

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::SetLimitedAgent);
    }

    #[test]
    fn try_from__allowlist_cidr_block_maps_to_error() {
        // Given
        let cidr_block = get_test_cidr_block();
        let call = allowlist_cidr_block(cidr_block);

        // When
        let res = Proposal::try_from(call).unwrap_err();

        // Then
        if let Error::CallRequestedIsNotProposal = res {
        } else {
            panic!("unexpected error: {:?}", res)
        }
    }

    #[test]
    fn try_from__remove_allowlist_cidr_block_maps_to_error() {
        // Given
        let cidr_block = get_test_cidr_block();
        let call = remove_allowlist_cidr_block(cidr_block);

        // When
        let res = Proposal::try_from(call).unwrap_err();

        // Then
        if let Error::CallRequestedIsNotProposal = res {
        } else {
            panic!("unexpected error: {:?}", res)
        }
    }

    #[test]
    fn try_from__set_member_encryption_key_maps_to_none() {
        // Given
        let call = set_member_encryption_key(EncryptionPubKey::default());

        // When
        let res = Proposal::try_from(call).unwrap_err();

        // Then
        if let Error::CallRequestedIsNotProposal = res {
        } else {
            panic!("unexpected error: {:?}", res)
        }
    }

    #[test]
    fn try_from__root_allowlist_cidr_block_maps_to_add_cidr_block() {
        // Given
        let cidr_block = get_test_cidr_block();
        let call = root_allowlist_cidr_block(Default::default(), cidr_block);

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::AddCidrBlock);
    }

    #[test]
    fn try_from__root_remove_allowlist_cidr_block_maps_to_remove_cidr_block() {
        // Given
        let cidr_block = get_test_cidr_block();
        let call = root_remove_allowlist_cidr_block(Default::default(), cidr_block);

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::RemoveCidrBlock);
    }

    #[test]
    fn try_from__runtime_upgrade_extrinsic_maps_to_runtime_upgrade() {
        // Given
        let blob = Wrap(WasmBlob::from(Vec::new()));
        let hash = Wrap(XandWasmHasher::hash(&blob.0));
        let call = upgrade_runtime(blob, hash);

        // When
        let res: Proposal = call.try_into().unwrap();

        // Then
        assert_eq!(res, Proposal::RuntimeUpgrade);
    }
}
