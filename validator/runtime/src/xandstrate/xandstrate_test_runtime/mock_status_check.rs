use xand_governance::models::vote::VoteTallies;

use crate::{
    network_voting::{adapters::ProposalStatusCheck, PropIndex, VoteStatus},
    xandstrate::{
        xand_voting_rules::adapters::queries::get_vote_tallies_for_proposal,
        xandstrate_test_runtime::test_runtime::TestRuntime,
    },
    DispatchError,
};

pub struct TestRuntimeProposalStatusCheck;

impl TestRuntimeProposalStatusCheck {
    fn check_naive_unanimous_acceptance(tallies: &VoteTallies) -> bool {
        let total_yes = tallies.validator_tally.yea_votes + tallies.member_tally.yea_votes;
        let total_eligible = tallies.validator_tally.total_participant_count
            + tallies.member_tally.total_participant_count;
        if total_eligible == 0 {
            false
        } else {
            total_yes / total_eligible == 1
        }
    }

    fn check_naive_unanimous_rejection(tallies: &VoteTallies) -> bool {
        let total_no = tallies.validator_tally.nay_votes + tallies.member_tally.nay_votes;
        let total_eligible = tallies.validator_tally.total_participant_count
            + tallies.member_tally.total_participant_count;
        if total_eligible == 0 {
            false
        } else {
            total_no / total_eligible == 1
        }
    }
}

impl ProposalStatusCheck<TestRuntime> for TestRuntimeProposalStatusCheck {
    type Error = DispatchError;

    fn vote_status(proposal_id: PropIndex) -> Result<VoteStatus, Self::Error> {
        let tallies = get_vote_tallies_for_proposal::<TestRuntime>(proposal_id);
        if TestRuntimeProposalStatusCheck::check_naive_unanimous_acceptance(&tallies) {
            Ok(VoteStatus::Accepted)
        } else if TestRuntimeProposalStatusCheck::check_naive_unanimous_rejection(&tallies) {
            Ok(VoteStatus::Rejected)
        } else {
            Ok(VoteStatus::Open)
        }
    }
}
