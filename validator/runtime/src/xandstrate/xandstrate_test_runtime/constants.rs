// pub const TEST_CORRELATION_ID: FiatReqCorrelationId =
//     [0, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0];
// pub const TEST_CORRELATION_ID_2: FiatReqCorrelationId =
//     [2, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 2, 0, 0, 0];
pub const MEM_1: u64 = 10;
pub const MEM_2: u64 = 20;
pub const TRUST: u64 = 88;
pub const LIMITED_AGENT: u64 = 44;
pub const VAL_1: u64 = 99;
pub const VAL_2: u64 = 199;
pub const VAL_3: u64 = 299;
pub const ACCOUNT_NO_ROLE: u64 = 0;
