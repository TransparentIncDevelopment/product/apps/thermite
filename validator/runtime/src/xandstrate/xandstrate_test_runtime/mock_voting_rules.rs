use frame_system as system;

use crate::{
    network_voting::adapters::VotingPermissions,
    xandstrate::{
        self,
        xandstrate_test_runtime::test_runtime::{Call as TestCall, TestRuntime},
    },
};

pub struct TestRuntimeVotingPermissions;

impl<T> VotingPermissions<T, TestCall> for TestRuntimeVotingPermissions
where
    T: xandstrate::Config,
{
    type Error = xandstrate::Error<TestRuntime>;

    fn can_propose(
        _proposer: &<T as system::Config>::AccountId,
        _proposal: &TestCall,
    ) -> Result<bool, Self::Error> {
        Ok(true)
    }

    fn can_vote(
        _voter: &<T as system::Config>::AccountId,
        _proposal: &TestCall,
    ) -> Result<bool, Self::Error> {
        Ok(true)
    }
}
