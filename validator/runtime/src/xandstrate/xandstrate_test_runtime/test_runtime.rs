use frame_support::{
    parameter_types,
    traits::{GenesisBuild, KeyOwnerProofSystem},
    weights::constants::{RocksDbWeight, WEIGHT_PER_SECOND},
};
use frame_system as system;
use pallet_grandpa::AuthorityId as GrandpaId;
use pallet_session::TestSessionHandler;
use parity_scale_codec::{Decode, Encode};
use sp_core::{crypto::KeyTypeId, H256};
use sp_runtime::{
    impl_opaque_keys,
    testing::{Header, UintAuthorityId},
    traits::{ConvertInto, IdentityLookup},
    Perbill,
};

use crate::{
    network_voting,
    xandstrate::{
        self,
        xand_voting_rules::XandVotingRules,
        xandstrate_test_runtime::{
            constants::*, mock_status_check::TestRuntimeProposalStatusCheck,
            mock_voting_rules::TestRuntimeVotingPermissions,
        },
    },
    xandvalidators,
};

pub type TestModule = xandstrate::Pallet<TestRuntime>;
pub type ValidatorMod = xandvalidators::Module<TestRuntime>;

impl_opaque_keys! {
    pub struct MockSessionKeys {
        pub dummy: UintAuthorityId,
    }
}

impl From<UintAuthorityId> for MockSessionKeys {
    fn from(dummy: UintAuthorityId) -> Self {
        Self { dummy }
    }
}

type UncheckedExtrinsic = frame_system::mocking::MockUncheckedExtrinsic<TestRuntime>;
type Block = frame_system::mocking::MockBlock<TestRuntime>;

frame_support::construct_runtime! {
    pub enum TestRuntime where
        Block = Block,
        NodeBlock = Block,
        UncheckedExtrinsic = UncheckedExtrinsic,
    {
        System: frame_system::{Module, Call, Config, Storage, Event<T>},
        Grandpa: pallet_grandpa::{Module, Call, Storage, Event},
        XandStrate: xandstrate::{Module, Call, Storage, Event<T>, Config<T>},
        Session: pallet_session::{Module, Call, Storage, Event},
        Aura: pallet_aura::{Module, Config<T>},
        XandValidators: xandvalidators::{Module, Call, Storage, Event<T>, Config<T>},
        NetworkVoting: network_voting::{Module, Call, Storage, Event<T>, Config<T>},
        Timestamp: pallet_timestamp::{Module, Call, Storage, Inherent},
    }
}

pub type AccountId = u64;
pub type BlockNumber = u64;
pub type Balance = u64;

const NORMAL_DISPATCH_RATIO: Perbill = Perbill::from_percent(75);
parameter_types! {
    pub const BlockHashCount: u64 = 250;
    pub BlockWeights: frame_system::limits::BlockWeights = frame_system::limits::BlockWeights
        ::with_sensible_defaults(2 * WEIGHT_PER_SECOND, NORMAL_DISPATCH_RATIO);
    pub BlockLength: frame_system::limits::BlockLength = frame_system::limits::BlockLength
        ::max_with_normal_ratio(5 * 1024 * 1024, NORMAL_DISPATCH_RATIO);
    pub const SS58Prefix: u8 = 42;
}

#[derive(Debug, Encode, Decode, Clone, Eq, PartialEq)]
pub struct FakeEvent(pub String);

impl From<()> for FakeEvent {
    fn from(_: ()) -> Self {
        FakeEvent("empty".to_string())
    }
}

impl From<xandstrate::Event<TestRuntime>> for FakeEvent {
    fn from(e: xandstrate::Event<TestRuntime>) -> Self {
        FakeEvent(format!("xandstrate: {:?}", e))
    }
}

impl From<xandvalidators::Event<TestRuntime>> for FakeEvent {
    fn from(e: xandvalidators::Event<TestRuntime>) -> Self {
        FakeEvent(format!("xandvalidators: {:?}", e))
    }
}

impl From<network_voting::Event<TestRuntime>> for FakeEvent {
    fn from(e: network_voting::Event<TestRuntime>) -> Self {
        FakeEvent(format!("network_voting: {:?}", e))
    }
}

impl frame_system::Config for TestRuntime {
    type BaseCallFilter = ();
    type BlockWeights = ();
    type BlockLength = ();
    type Origin = Origin;
    type Call = Call;
    type Index = u64;
    type BlockNumber = BlockNumber;
    type Hash = H256;
    type Hashing = sp_runtime::traits::BlakeTwo256;
    type AccountId = AccountId;
    type Lookup = IdentityLookup<Self::AccountId>;
    type Header = Header;
    type Event = FakeEvent;
    type BlockHashCount = BlockHashCount;
    type DbWeight = RocksDbWeight;
    type Version = ();
    type PalletInfo = PalletInfo;
    type AccountData = ();
    type OnNewAccount = ();
    type OnKilledAccount = ();
    type SystemWeightInfo = ();
    type SS58Prefix = SS58Prefix;
}

// Expose system events as strings so that we can make sure they're being recorded properly
impl From<frame_system::Event<TestRuntime>> for FakeEvent {
    fn from(e: frame_system::Event<TestRuntime>) -> Self {
        FakeEvent(format!("{:?}", e))
    }
}

parameter_types! {
    pub const TransferFee: Balance = 0;
    pub const CreateFee: Balance = 0;
}

parameter_types! {
    pub const Period: BlockNumber = 1;
    pub const Offset: BlockNumber = 0;
    pub const DisabledValidatorsThreshold: Perbill = Perbill::from_percent(25);
}
impl pallet_session::Config for TestRuntime {
    type Event = ();
    type ValidatorId = AccountId;
    type ValidatorIdOf = ConvertInto;
    type ShouldEndSession = ValidatorMod;
    type NextSessionRotation = ();
    type SessionManager = ValidatorMod;
    type SessionHandler = TestSessionHandler;
    type Keys = MockSessionKeys;
    type DisabledValidatorsThreshold = DisabledValidatorsThreshold;
    type WeightInfo = ();
}

impl pallet_grandpa::Config for TestRuntime {
    type Event = ();
    type Call = Call;

    type KeyOwnerProofSystem = ();

    type KeyOwnerProof =
        <Self::KeyOwnerProofSystem as KeyOwnerProofSystem<(KeyTypeId, GrandpaId)>>::Proof;

    type KeyOwnerIdentification = <Self::KeyOwnerProofSystem as KeyOwnerProofSystem<(
        KeyTypeId,
        GrandpaId,
    )>>::IdentificationTuple;

    type HandleEquivocation = ();
    type WeightInfo = ();
}

impl pallet_aura::Config for TestRuntime {
    type AuthorityId = UintAuthorityId;
}

impl xandvalidators::Config for TestRuntime {
    type Event = FakeEvent;
    type OnRemoval = TestModule;
}

parameter_types! {
    pub const MinimumPeriod: u64 = 5;
}
impl pallet_timestamp::Config for TestRuntime {
    type Moment = u64;
    type OnTimestampSet = ();
    type MinimumPeriod = MinimumPeriod;
    type WeightInfo = ();
}

impl xandstrate::Config for TestRuntime {
    type Event = FakeEvent;
}

impl network_voting::Config for TestRuntime {
    type Event = FakeEvent;
    type Proposal = Call;
    type VotingPermissions = TestRuntimeVotingPermissions;
    type ProposalStatusCheck = TestRuntimeProposalStatusCheck;
    type Lifecycle = XandVotingRules<Self>;
}

/// A test setup with an empty genesis
pub fn empty_test_ext() -> sp_io::TestExternalities {
    system::GenesisConfig::default()
        .build_storage::<TestRuntime>()
        .unwrap()
        .into()
}

/// A test setup with the `base_xandstrate_genesis` and one validator, `VAL_1`
pub fn base_test_ext() -> sp_io::TestExternalities {
    new_test_extrinsics(
        base_xandstrate_genesis(),
        // Configure a single validator, whose account key and signing key are both VAL_1
        vec![(VAL_1, VAL_1, UintAuthorityId(VAL_1))],
    )
}

/// A test setup with the `base_xandstrate_genesis` and three validators `VAL_1 - 3`
pub fn multi_vals_test_ext() -> sp_io::TestExternalities {
    new_test_extrinsics(
        base_xandstrate_genesis(),
        vec![
            (VAL_1, VAL_1, UintAuthorityId(VAL_1)),
            (VAL_2, VAL_2, UintAuthorityId(VAL_2)),
            (VAL_3, VAL_3, UintAuthorityId(VAL_3)),
        ],
    )
}

pub const BLOCKS_UNTIL_AUTO_EXIT: u64 = 1_000;

/// A xandstrate genesis with a default registered trust (id# TRUST) and two Members
/// (id#s MEM_1 and MEM_2) with limited agent (id#s LIMITED_AGENT)
pub fn base_xandstrate_genesis() -> xandstrate::GenesisConfig<TestRuntime> {
    xandstrate::GenesisConfig::<TestRuntime> {
        registered_members: vec![(MEM_1, true), (MEM_2, true)],
        banned_members: vec![],
        blocks_until_auto_exit: BLOCKS_UNTIL_AUTO_EXIT,
        trust_node_id: TRUST,
        limited_agent_id: LIMITED_AGENT,
        allow_listed_cidr_blocks: vec![],
        node_encryption_key: vec![],
    }
}

pub fn new_test_extrinsics(
    xandstrate_genesis: xandstrate::GenesisConfig<TestRuntime>,
    validators: Vec<(u64, u64, UintAuthorityId)>,
) -> sp_io::TestExternalities {
    let mut gen_config = system::GenesisConfig::default()
        .build_storage::<TestRuntime>()
        .unwrap();
    xandstrate_genesis
        .assimilate_storage(&mut gen_config)
        .unwrap();

    xandvalidators::GenesisConfig::<TestRuntime> {
        epoch_duration: 1000,
        validators: validators.iter().map(|(id, _, _)| *id).collect(),
    }
    .assimilate_storage(&mut gen_config)
    .unwrap();
    // Configure a single validator, whose account key and signing key are both VAL_1
    pallet_session::GenesisConfig::<TestRuntime> {
        keys: validators
            .into_iter()
            .map(|(a, b, c)| (a, b, c.into()))
            .collect(),
    }
    .assimilate_storage(&mut gen_config)
    .unwrap();
    pallet_grandpa::GenesisConfig {
        authorities: vec![],
    }
    .assimilate_storage::<TestRuntime>(&mut gen_config)
    .unwrap();
    gen_config.into()
}
