// Annoying, should be scoped to the decl_storage! macro, but that does not work.
#![allow(clippy::string_lit_as_bytes)]
#![allow(clippy::unused_unit)]

// For more guidance on Substrate modules, see the example module
// https://github.com/paritytech/substrate/blob/master/srml/example/src/lib.rs
pub(crate) mod participants;

#[cfg(test)]
mod tests;
pub mod vote_queries;
pub mod xand_voting_rules;
pub mod xandstrate_api;

pub(crate) mod xandstrate_state_adapter;
#[cfg(test)]
pub(crate) mod xandstrate_test_runtime;

use crate::{xandvalidators, DEFAULT_WEIGHT};
use crate::{xandvalidators::OnValidatorRemoved, EncryptionPubKey};
use frame_support::{debug, ensure, fail, weights::Weight};
use frame_system::{self, ensure_root, ensure_signed};
use participants::*;
use xand_runtime_models::{
    upgrade::{verify_upgrade_wasm_blob_and_xand_hash, UpgradeWasmBlob, UpgradeXandHash},
    BannedState, CidrBlock, CidrBlockArray, CreateCancelReason, FiatReqCorrelationId,
    PendingCreate, PendingRedeem,
};

pub use pallet::*;

#[frame_support::pallet]
pub mod pallet {
    use super::*;
    use crate::{network_voting, xandvalidators, EncryptionPubKey};
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;
    use xand_runtime_models::BannedState;
    use xandstrate_error_derive::ErrorHelper;

    #[pallet::config]
    pub trait Config:
        frame_system::Config
        + pallet_session::Config
        + pallet_grandpa::Config
        + pallet_timestamp::Config
        + network_voting::Config
        + xandvalidators::Config
    {
        /// The overarching event type.
        type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
    }

    #[pallet::pallet]
    #[pallet::generate_store(pub trait Store)]
    pub struct Pallet<T>(PhantomData<T>);

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {
        fn on_finalize(_bnum: T::BlockNumber) {
            Pallet::<T>::on_finalize_impl()
        }

        fn on_initialize(bnum: T::BlockNumber) -> Weight {
            Pallet::<T>::on_initialize_impl(bnum)
        }
    }

    #[pallet::call]
    impl<T: Config> Pallet<T> {
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn register_account_as_member(
            origin: OriginFor<T>,
            account: T::AccountId,
            pub_key: EncryptionPubKey,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::register_account_as_member_tx(origin, account, pub_key)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn remove_self(origin: OriginFor<T>) -> DispatchResultWithPostInfo {
            Pallet::<T>::remove_self_tx(origin)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn remove_member(
            origin: OriginFor<T>,
            account: T::AccountId,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::remove_member_tx(origin, account)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn exit_member(
            origin: OriginFor<T>,
            account: T::AccountId,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::exit_member_tx(origin, account)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn set_trust_node_id(
            origin: OriginFor<T>,
            account: T::AccountId,
            pub_key: EncryptionPubKey,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::set_trust_node_id_tx(origin, account, pub_key)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn set_limited_agent_id(
            origin: OriginFor<T>,
            account: Option<T::AccountId>,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::set_limited_agent_id_tx(origin, account)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn set_member_encryption_key(
            origin: OriginFor<T>,
            pub_key: EncryptionPubKey,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::set_member_encryption_key_tx(origin, pub_key)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn set_trust_encryption_key(
            origin: OriginFor<T>,
            pub_key: EncryptionPubKey,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::set_trust_encryption_key_tx(origin, pub_key)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn allowlist_cidr_block(
            origin: OriginFor<T>,
            cidr_block: CidrBlock,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::allowlist_cidr_block_tx(origin, cidr_block)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn remove_allowlist_cidr_block(
            origin: OriginFor<T>,
            cidr_block: CidrBlock,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::remove_allowlist_cidr_block_tx(origin, cidr_block)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn root_allowlist_cidr_block(
            origin: OriginFor<T>,
            account: T::AccountId,
            cidr_block: CidrBlock,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::root_allowlist_cidr_block_tx(origin, account, cidr_block)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn root_remove_allowlist_cidr_block(
            origin: OriginFor<T>,
            account: T::AccountId,
            cidr_block: CidrBlock,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::root_remove_allowlist_cidr_block_tx(origin, account, cidr_block)
        }

        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn upgrade_runtime(
            origin: OriginFor<T>,
            wasm_blob: UpgradeWasmBlob,
            xand_hash: UpgradeXandHash,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::upgrade_runtime_tx(origin, wasm_blob, xand_hash)
        }
    }

    #[pallet::event]
    #[pallet::generate_deposit(pub (super) fn deposit_event)]
    pub enum Event<T: Config> {
        /// If the full amount of the redeem isn't slashed, log this event, as it likely means we
        /// need to take manual action
        FullAmountWasntSlashed(FiatReqCorrelationId),
        CreateCancellation(FiatReqCorrelationId, CreateCancelReason),
        MemberExited(T::AccountId),
    }

    #[pallet::error]
    #[derive(ErrorHelper)]
    pub enum Error<T> {
        Unsigned,
        OnlySuperuserMayRegisterMembers,
        OnlySuperuserMaySetTrustNode,
        OnlyRegisteredMemberCanSetOwnEncryptionKey,
        OnlyTrustMaySetOwnEncryptionKey,
        FiatCorrelationIdCannotBeReused,
        TransactionMustBeDoneByRoot,
        UpgradeCodeFailedVerification,
        UpgradeCodeCannotBeSet,
        TransactionMustBeDoneByRootOrAccount,
        KeyIsAlreadyAnAuthority,
        KeyIsNotAnAuthority,
        CidrBlockAlreadyAllowlisted,
        CidrBlockNotAllowlisted,
        CidrBlockAllowlistCapacityExceeded,
        NoCidrBlocksForAccount,
        OnlyMembersTrustAndValidatorsCanAddAllowlistCidrBlocks,
        OnlyMembersTrustAndValidatorsCanRemoveAllowlistCidrBlocks,
        VoteAlreadySubmittedForAccount,
        OnlyClearTransactionsPermittedInNonConfidentialRuntime,
        AlreadyRegistered,
        CallRequestedIsNotProposal,
        RoleNotFoundForAccount,
        ProposalIdNotFound,
        EntityIsBanned,
        CannotDecrementProviders,
        SignerNotAValidMemberOrValidator,
        MemberNotInExitingState,
    }

    #[pallet::storage]
    #[pallet::getter(fn registered_members)]
    pub type RegisteredMembers<T: Config> =
        StorageMap<_, Blake2_128Concat, T::AccountId, bool, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn banned_members)]
    pub type BannedMembers<T: Config> =
        StorageMap<_, Blake2_128Concat, T::AccountId, BannedState<T::BlockNumber>, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn blocks_until_auto_exit)]
    pub type BlocksUntilAutoExit<T: Config> = StorageValue<_, T::BlockNumber, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn trust_node_id)]
    pub type TrustNodeId<T: Config> = StorageValue<_, T::AccountId, ValueQuery>;

    #[pallet::type_value]
    pub fn DefaultForLimitedAgentId<T: Config>() -> Option<T::AccountId> {
        None
    }

    #[pallet::storage]
    #[pallet::getter(fn limited_agent_id)]
    pub type LimitedAgentId<T: Config> =
        StorageValue<_, Option<T::AccountId>, ValueQuery, DefaultForLimitedAgentId<T>>;

    /// Node's stored encryption key
    #[pallet::storage]
    #[pallet::getter(fn node_encryption_key)]
    pub type NodeEncryptionKey<T: Config> =
        StorageMap<_, Blake2_128Concat, T::AccountId, EncryptionPubKey, ValueQuery>;

    /// A mapping of accounts to an array of their associated allowlisted CIDR Blocks
    #[pallet::storage]
    #[pallet::getter(fn allow_listed_cidr_blocks)]
    pub type AllowListedCidrBlocks<T: Config> =
        StorageMap<_, Blake2_128Concat, T::AccountId, CidrBlockArray, ValueQuery>;

    #[pallet::genesis_config]
    pub struct GenesisConfig<T: Config>
    where
        <T as pallet_timestamp::Config>::Moment: serde::Serialize + serde::de::DeserializeOwned,
    {
        pub banned_members: Vec<(T::AccountId, BannedState<T::BlockNumber>)>,
        pub blocks_until_auto_exit: T::BlockNumber,
        pub trust_node_id: T::AccountId,
        pub limited_agent_id: T::AccountId,
        /// Node's stored encryption key
        pub node_encryption_key: Vec<(T::AccountId, EncryptionPubKey)>,
        /// A mapping of accounts to an array of their associated allowlisted CIDR Blocks
        pub allow_listed_cidr_blocks: Vec<(T::AccountId, CidrBlockArray)>,
        pub registered_members: Vec<(T::AccountId, bool)>,
    }

    #[cfg(feature = "std")]
    impl<T: Config> Default for GenesisConfig<T>
    where
        <T as pallet_timestamp::Config>::Moment: serde::Serialize + serde::de::DeserializeOwned,
    {
        fn default() -> Self {
            Self {
                banned_members: Default::default(),
                blocks_until_auto_exit: Default::default(),
                trust_node_id: Default::default(),
                limited_agent_id: None.unwrap_or_default(),
                node_encryption_key: Default::default(),
                allow_listed_cidr_blocks: Default::default(),
                registered_members: Default::default(),
            }
        }
    }

    #[pallet::genesis_build]
    impl<T: Config> GenesisBuild<T> for GenesisConfig<T>
    where
        <T as pallet_timestamp::Config>::Moment: serde::Serialize + serde::de::DeserializeOwned,
    {
        fn build(&self) {
            {
                let data = &self.banned_members;
                let data: &frame_support::sp_std::vec::Vec<(
                    T::AccountId,
                    BannedState<T::BlockNumber>,
                )> = data;
                data.iter().for_each(|(k, v)| {
                    <BannedMembers<T> as frame_support::storage::StorageMap<
                        T::AccountId,
                        BannedState<T::BlockNumber>,
                    >>::insert::<&T::AccountId, &BannedState<T::BlockNumber>>(
                        k, v
                    );
                });
            }
            {
                let data = &self.blocks_until_auto_exit;
                let v: &T::BlockNumber = data;
                <BlocksUntilAutoExit<T> as frame_support::storage::StorageValue<
                    T::BlockNumber,
                >>::put::<&T::BlockNumber>(v);
            }
            {
                let data = &self.trust_node_id;
                let v: &T::AccountId = data;
                <TrustNodeId<T> as frame_support::storage::StorageValue<T::AccountId>>::put::<
                    &T::AccountId,
                >(v);
            }
            {
                let data = Some(&self.limited_agent_id);
                let v: Option<&T::AccountId> = data;
                <LimitedAgentId<T> as frame_support::storage::StorageValue<Option<T::AccountId>>>::put::<Option<&T::AccountId>>(v);
            }
            {
                let data = &self.node_encryption_key;
                let data: &frame_support::sp_std::vec::Vec<(T::AccountId, EncryptionPubKey)> = data;
                data.iter().for_each(|(k, v)| {
                    <NodeEncryptionKey<T> as frame_support::storage::StorageMap<
                        T::AccountId,
                        EncryptionPubKey,
                    >>::insert::<&T::AccountId, &EncryptionPubKey>(k, v);
                });
            }
            {
                let data = &self.allow_listed_cidr_blocks;
                let data: &frame_support::sp_std::vec::Vec<(T::AccountId, CidrBlockArray)> = data;
                data.iter().for_each(|(k, v)| {
                    <AllowListedCidrBlocks<T> as frame_support::storage::StorageMap<
                        T::AccountId,
                        CidrBlockArray,
                    >>::insert::<&T::AccountId, &CidrBlockArray>(k, v);
                });
            }
            let extra_genesis_builder: fn(&Self) = |config| {
                for (account, registered) in config.registered_members.iter().cloned() {
                    if registered {
                        frame_system::Module::<T>::inc_providers(&account);
                    }
                    RegisteredMembers::<T>::insert(account, registered);
                }
            };
            extra_genesis_builder(self);
        }
    }
}

impl<T: Config> Pallet<T> {
    fn on_initialize_impl(bnum: T::BlockNumber) -> Weight {
        Self::auto_exit_members(bnum);
        Weight::default()
    }

    fn on_finalize_impl() {}

    fn auto_exit_members(current_block: T::BlockNumber) {
        BannedMembers::<T>::iter()
            .filter_map(|(member, state)| match state {
                BannedState::Exiting(block) => Some((member, block)),
                BannedState::Exited => None,
            })
            .filter(|(_, banned_block)| {
                let grace_period = BlocksUntilAutoExit::<T>::get();
                let removal_block = *banned_block + grace_period;
                current_block > removal_block
            })
            .for_each(|(member, _)| {
                Self::inner_exit_member(&member);
                Self::deposit_event(Event::MemberExited(member));
                ()
            });
    }

    fn register_account_as_member_tx(
        origin: T::Origin,
        account: T::AccountId,
        pub_key: EncryptionPubKey,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure tx issuer has permission to register a member
        let root_res = ensure_root(origin);
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot,);
        ensure!(
            !RegisteredMembers::<T>::contains_key(account.clone()),
            Error::<T>::AlreadyRegistered,
        );
        ensure!(
            !BannedMembers::<T>::contains_key(account.clone()),
            Error::<T>::EntityIsBanned,
        );
        frame_system::Module::<T>::inc_providers(&account);
        RegisteredMembers::<T>::insert(account.clone(), true);
        NodeEncryptionKey::<T>::insert(account, pub_key);
        Ok(().into())
    }

    fn remove_self_tx(origin: T::Origin) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let signer = ensure_signed(origin)?;
        if RegisteredMembers::<T>::contains_key(&signer) {
            Self::remove_member_logic(signer)
        } else if let Some(index) = xandvalidators::Pallet::<T>::validator_index(&signer) {
            xandvalidators::Pallet::<T>::remove_validator_logic(index, signer)
        } else {
            fail!(Error::<T>::SignerNotAValidMemberOrValidator)
        }
    }

    fn remove_member_tx(
        origin: T::Origin,
        account: T::AccountId,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let root_res = ensure_root(origin);
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot);
        Self::remove_member_logic(account)
    }

    fn remove_member_logic(
        account: T::AccountId,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        RegisteredMembers::<T>::remove(&account);
        // Not removing CIDR blocks here because they are needed for any `FinalRedeem`
        let banned_block = <frame_system::Pallet<T>>::block_number();
        BannedMembers::<T>::insert(account, BannedState::Exiting(banned_block));
        Ok(().into())
    }

    fn exit_member_tx(
        origin: T::Origin,
        account: T::AccountId,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Root or Self can immediately remove a member
        let is_root = ensure_root(origin.clone()).is_ok();
        let is_self = ensure_signed(origin).map_or_else(|_e| false, |signer| signer == account);

        ensure!(
            is_root || is_self,
            Error::<T>::TransactionMustBeDoneByRootOrAccount
        );
        ensure!(
            BannedMembers::<T>::contains_key(account.clone())
                && matches!(BannedMembers::<T>::get(&account), BannedState::Exiting(..)),
            Error::<T>::MemberNotInExitingState
        ); // We cannot immediately remove a member unless they are on the banned list and marked for removal
        Self::inner_exit_member(&account);
        // Remove member's Substrate network provider entry
        let removed_provider_result = frame_system::Module::<T>::dec_providers(&account);
        ensure!(
            removed_provider_result.is_ok(),
            Error::<T>::CannotDecrementProviders
        );
        Ok(().into())
    }

    fn inner_exit_member(account: &T::AccountId) {
        if RegisteredMembers::<T>::contains_key(account) {
            // Entering this arm is highly unlikely as account should have already been
            // removed from `RegisteredMembers`
            RegisteredMembers::<T>::remove(account);
        };
        Self::remove_cidr_block_account_array_pair(account);
        // Update existing Banned member state
        BannedMembers::<T>::insert(account, BannedState::Exited);
        NodeEncryptionKey::<T>::remove(account);
    }

    fn set_trust_node_id_tx(
        origin: T::Origin,
        account: T::AccountId,
        pub_key: EncryptionPubKey,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure tx issuer has permission to change the trust node
        let root_res = ensure_root(origin);
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot,);

        TrustNodeId::<T>::put(account.clone());
        NodeEncryptionKey::<T>::insert(account, pub_key);
        Ok(().into())
    }

    fn set_limited_agent_id_tx(
        origin: T::Origin,
        account: Option<T::AccountId>,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure tx issuer has permission to change the trust node
        let root_res = ensure_root(origin);
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot,);

        // See docs for `StorageValue::set()`:
        // https://docs.rs/frame-support/3.0.0/frame_support/storage/trait.StorageValue.html
        LimitedAgentId::<T>::set(account);
        Ok(().into())
    }

    fn set_member_encryption_key_tx(
        origin: T::Origin,
        pub_key: EncryptionPubKey,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let member_account_id = ensure_signed(origin)?;
        // Ensure signer is a registered member
        ensure!(
            RegisteredMembers::<T>::get(&member_account_id),
            Error::<T>::OnlyRegisteredMemberCanSetOwnEncryptionKey,
        );
        // Member can only set own key
        NodeEncryptionKey::<T>::insert(member_account_id, pub_key);
        Ok(().into())
    }

    fn set_trust_encryption_key_tx(
        origin: T::Origin,
        pub_key: EncryptionPubKey,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Ensure signer is the trust
        let sender = ensure_signed(origin)?;
        let trust_account_id = TrustNodeId::<T>::get();
        ensure!(
            trust_account_id == sender,
            Error::<T>::OnlyTrustMaySetOwnEncryptionKey,
        );
        NodeEncryptionKey::<T>::insert(trust_account_id, pub_key);
        Ok(().into())
    }

    /// As root, add CIDR block to the allowlist array for an account.
    fn root_allowlist_cidr_block_tx(
        origin: T::Origin,
        account: T::AccountId,
        cidr_block: CidrBlock,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let root_res = ensure_root(origin);
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot,);
        Self::inner_allowlist_cidr_block(account, cidr_block)
    }

    // As a Member, Trust, or Validator, add CIDR block to the sender's allowlist array
    fn allowlist_cidr_block_tx(
        origin: T::Origin,
        cidr_block: CidrBlock,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let sender = ensure_signed(origin)?;
        ensure!(
            is_member_or_trust_or_validator::<T>(&sender),
            Error::<T>::OnlyMembersTrustAndValidatorsCanAddAllowlistCidrBlocks,
        );
        Self::inner_allowlist_cidr_block(sender, cidr_block)
    }

    /// Add a CIDR block to the array of "allowlisted" CIDR blocks associated with the account.
    /// Will error out if the CIDR block is already added or the array is full.
    fn inner_allowlist_cidr_block(
        account: T::AccountId,
        cidr_block: CidrBlock,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Check if CIDR block is already added
        let mut blocks = Self::get_stored_or_default_cidr_blocks(&account);
        ensure!(
            !blocks.contains(&Some(cidr_block)),
            Error::<T>::CidrBlockAlreadyAllowlisted,
        );
        // Add CIDR block
        let inserted = Self::inserted_cidr_block_into_array(&mut blocks, cidr_block, 0);
        ensure!(inserted, Error::<T>::CidrBlockAllowlistCapacityExceeded,);
        AllowListedCidrBlocks::<T>::insert(account, blocks);
        Ok(().into())
    }

    /// Get the array of CIDR blocks associated with an account. If it doesn't exist, get a default
    /// empty array.
    fn get_stored_or_default_cidr_blocks(account: &T::AccountId) -> CidrBlockArray {
        if !AllowListedCidrBlocks::<T>::contains_key(account) {
            [None; 10]
        } else {
            AllowListedCidrBlocks::<T>::get(account)
        }
    }

    /// Attempts to insert CIDR block into array, returns some new array if successful, none if not.
    fn inserted_cidr_block_into_array(
        array: &mut CidrBlockArray,
        cidr_block: CidrBlock,
        index: usize,
    ) -> bool {
        match array[index] {
            None => {
                array[index] = Some(cidr_block);
                true
            }
            Some(_) => {
                if index >= (array.len() - 1) {
                    false
                } else {
                    Self::inserted_cidr_block_into_array(array, cidr_block, index + 1)
                }
            }
        }
    }

    /// As root, add CIDR block to the allowlist array for an account.
    fn root_remove_allowlist_cidr_block_tx(
        origin: T::Origin,
        account: T::AccountId,
        cidr_block: CidrBlock,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let root_res = ensure_root(origin);
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot,);
        Self::inner_remove_allowlist_cidr_block(account, cidr_block)
    }

    pub fn upgrade_runtime_tx(
        origin: T::Origin,
        wasm_blob: UpgradeWasmBlob,
        xand_hash: UpgradeXandHash,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let root_res = ensure_root(origin.clone());
        ensure!(root_res.is_ok(), Error::<T>::TransactionMustBeDoneByRoot);
        ensure!(
            verify_upgrade_wasm_blob_and_xand_hash(&wasm_blob, &xand_hash),
            Error::<T>::UpgradeCodeFailedVerification
        );
        let can_set_code_res = frame_system::Module::<T>::can_set_code(wasm_blob.0.as_ref());
        ensure!(can_set_code_res.is_ok(), Error::<T>::UpgradeCodeCannotBeSet);
        // TODO ADO 7578 - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7578 Make sure this is integ-tested
        frame_system::Pallet::<T>::set_code(origin, wasm_blob.0.as_ref().to_vec())?;
        Ok(().into())
    }

    /// As a Member, Trust, or Validator, add CIDR block to the sender's allowlist array.
    fn remove_allowlist_cidr_block_tx(
        origin: T::Origin,
        cidr_block: CidrBlock,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        let sender = ensure_signed(origin)?;
        ensure!(
            is_member_or_trust_or_validator::<T>(&sender),
            Error::<T>::OnlyMembersTrustAndValidatorsCanRemoveAllowlistCidrBlocks,
        );
        Self::inner_remove_allowlist_cidr_block(sender, cidr_block)
    }

    /// Remove a specific CIDR block from the array of "allowlisted" CIDR blocks associated with the
    /// account. Will error out if the CIDR block isn't in the array or there is no array.
    fn inner_remove_allowlist_cidr_block(
        account: T::AccountId,
        cidr_block: CidrBlock,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        // Check if CIDR block exists
        ensure!(
            AllowListedCidrBlocks::<T>::contains_key(&account),
            Error::<T>::CidrBlockNotAllowlisted,
        );
        // Remove CIDR block
        let mut blocks = AllowListedCidrBlocks::<T>::get(&account);
        let removed = Self::removed_cidr_block_from_array(&mut blocks, cidr_block, 0);
        ensure!(removed, Error::<T>::CidrBlockNotAllowlisted);
        if blocks.iter().all(|o| o.is_none()) {
            AllowListedCidrBlocks::<T>::remove(&account);
        } else {
            AllowListedCidrBlocks::<T>::insert(&account, blocks);
        }
        Ok(().into())
    }

    fn remove_cidr_block_account_array_pair(account: &T::AccountId) {
        // Check if CIDR block exists
        if AllowListedCidrBlocks::<T>::contains_key(account) {
            AllowListedCidrBlocks::<T>::remove(account);
        } else {
            debug::info!(
                "Attempted to remove allowlist for validator {:?}, nothing to remove",
                account
            );
        }
    }

    /// Attempts to insert CIDR block into array, returns some new array if successful, none if not.
    fn removed_cidr_block_from_array(
        array: &mut CidrBlockArray,
        cidr_block: CidrBlock,
        index: usize,
    ) -> bool {
        match array[index] {
            None => false,
            Some(b) => {
                if b == cidr_block {
                    array[index] = None;
                    true
                } else if index >= (array.len() - 1) {
                    false
                } else {
                    Self::removed_cidr_block_from_array(array, cidr_block, index + 1)
                }
            }
        }
    }
}

impl<T: Config> OnValidatorRemoved<T::AccountId> for Pallet<T> {
    fn on_validator_removed(
        account: &T::AccountId,
    ) -> frame_support::dispatch::DispatchResultWithPostInfo {
        Self::remove_cidr_block_account_array_pair(account);
        Ok(().into())
    }
}
