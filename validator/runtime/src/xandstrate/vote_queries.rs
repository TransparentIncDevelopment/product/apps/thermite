use frame_system as system;
use sp_std::prelude::Vec;

use crate::{
    network_voting::{self, PropIndex, Votes},
    xandstrate::{self, RegisteredMembers},
    xandvalidators,
};

pub fn get_vote_pairs<T: xandstrate::Config + xandvalidators::Config + network_voting::Config>(
    prop_id: PropIndex,
) -> Vec<(<T as system::Config>::AccountId, bool)> {
    [
        get_member_vote_pairs::<T>(prop_id),
        get_validator_vote_pairs::<T>(prop_id),
    ]
    .concat()
}

fn get_member_vote_pairs<T: xandstrate::Config + network_voting::Config>(
    prop_id: PropIndex,
) -> Vec<(<T as system::Config>::AccountId, bool)> {
    let mut votes = Vec::new();
    for (member, _) in RegisteredMembers::<T>::iter() {
        if Votes::<T>::contains_key(prop_id, &member) {
            let vote = Votes::<T>::get(prop_id, &member);
            votes.push((member, vote));
        }
    }
    votes
}

fn get_validator_vote_pairs<T: xandvalidators::Config + network_voting::Config>(
    prop_id: PropIndex,
) -> Vec<(<T as system::Config>::AccountId, bool)> {
    let mut votes = Vec::new();
    for validator in xandvalidators::Pallet::<T>::validators().iter() {
        if Votes::<T>::contains_key(prop_id, validator) {
            let vote = Votes::<T>::get(prop_id, validator);
            votes.push((validator.clone(), vote));
        }
    }
    votes
}
