use frame_system as system;
use xand_governance::models::role::Role;

use crate::{
    xandstrate::{
        self,
        participants::{is_limited_agent, is_member, is_trust},
    },
    xandvalidators::queryable_xand_validators::QueryableXandValidators,
    Error,
};

pub fn check_role<T: xandstrate::Config>(
    address: &<T as system::Config>::AccountId,
) -> Result<Role, Error<T>> {
    if <T as QueryableXandValidators>::is_validator(address) {
        return Ok(Role::Validator);
    }

    if is_member::<T>(address) {
        return Ok(Role::Member);
    }

    if is_trust::<T>(address) {
        return Ok(Role::Trust);
    }

    if is_limited_agent::<T>(address) {
        return Ok(Role::LimitedAgent);
    }

    Err(Error::RoleNotFoundForAccount)
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use crate::{
        xandstrate::{
            participants::adapters::check_role,
            xandstrate_test_runtime::{
                constants::*,
                test_runtime::{base_test_ext, TestRuntime},
            },
        },
        Error,
    };
    use xand_governance::models::role::Role;

    #[test]
    fn check_role__trust_can_be_translated() {
        base_test_ext().execute_with(|| {
            let role = check_role::<TestRuntime>(&TRUST).unwrap();
            assert_eq!(role, Role::Trust);
        })
    }

    #[test]
    fn check_role__limited_agent_can_be_translated() {
        base_test_ext().execute_with(|| {
            let role = check_role::<TestRuntime>(&LIMITED_AGENT).unwrap();
            assert_eq!(role, Role::LimitedAgent);
        })
    }

    #[test]
    fn check_role__member_can_be_checked() {
        base_test_ext().execute_with(|| {
            let role = check_role::<TestRuntime>(&MEM_1).unwrap();
            assert_eq!(role, Role::Member);
        })
    }

    #[test]
    fn check_role__validator_can_be_checked() {
        base_test_ext().execute_with(|| {
            let role = check_role::<TestRuntime>(&VAL_1).unwrap();
            assert_eq!(role, Role::Validator);
        })
    }

    #[test]
    fn check_role__account_with_no_role_returns_error() {
        base_test_ext().execute_with(|| {
            let res = check_role::<TestRuntime>(&ACCOUNT_NO_ROLE).unwrap_err();
            if let Error::RoleNotFoundForAccount = res {
            } else {
                panic!("unexpected error: {:?}", res)
            }
        })
    }
}
