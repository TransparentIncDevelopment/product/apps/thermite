use parity_scale_codec::{Decode, Encode};
use serde::{Deserialize, Serialize};

use crate::validator_emissions::models::{
    block_completion_count::BlockCompletionCount, validator_emission_rate::ValidatorEmissionRate,
};

#[derive(Default, Clone, PartialEq, Debug, Eq, Encode, Decode, Deserialize, Serialize)]
pub struct ValidatorEmissionProgress {
    pub effective_emission_rate: ValidatorEmissionRate,
    pub blocks_completed_progress: BlockCompletionCount,
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum ValidatorEmissionProgressIncrementOutcome {
    QuotaUnfulfilled(ValidatorEmissionProgress),
    QuotaFulfilled,
}

impl ValidatorEmissionProgress {
    /// Computes an emission progress with incremented "blocks completed" counter. If this
    /// new progress fulfils the effective block quota, returns [ValidatorEmissionProgressIncrementResult::QuotaFulfilled].
    /// Otherwise, returns [ValidatorEmissionProgressIncrementResult::QuotaUnfulfilled],
    /// containing a new [ValidatorEmissionProgress] with incremented completion count.
    pub fn increment(&self) -> ValidatorEmissionProgressIncrementOutcome {
        let new_completion_count = self.blocks_completed_progress.increment();

        let is_fulfilled = self
            .effective_emission_rate
            .block_quota
            .is_fulfilled_by(&new_completion_count);

        if is_fulfilled {
            ValidatorEmissionProgressIncrementOutcome::QuotaFulfilled
        } else {
            ValidatorEmissionProgressIncrementOutcome::QuotaUnfulfilled(ValidatorEmissionProgress {
                blocks_completed_progress: new_completion_count,
                effective_emission_rate: self.effective_emission_rate.clone(),
            })
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]

    use std::convert::TryFrom;

    use crate::validator_emissions::models::{
        block_completion_count::BlockCompletionCount, block_quota::BlockQuota,
        validator_emission_rate::ValidatorEmissionRate,
    };

    use super::{ValidatorEmissionProgress, ValidatorEmissionProgressIncrementOutcome};

    #[test]
    fn with_incremented_blocks_completed__identifies_newly_fulfilled_quotas() {
        // Given
        let progress = ValidatorEmissionProgress {
            effective_emission_rate: ValidatorEmissionRate {
                block_quota: BlockQuota::try_from(3).unwrap(),
                ..ValidatorEmissionRate::default()
            },
            blocks_completed_progress: BlockCompletionCount::from(2),
        };

        // When
        let result = progress.increment();

        // Then
        assert_eq!(
            result,
            ValidatorEmissionProgressIncrementOutcome::QuotaFulfilled
        );
    }

    #[test]
    fn with_incremented_blocks_completed__identifies_already_fulfilled_quotas() {
        // Given
        let progress = ValidatorEmissionProgress {
            effective_emission_rate: ValidatorEmissionRate {
                block_quota: BlockQuota::try_from(3).unwrap(),
                ..ValidatorEmissionRate::default()
            },
            blocks_completed_progress: BlockCompletionCount::from(3),
        };

        // When
        let result = progress.increment();

        // Then
        assert_eq!(
            result,
            ValidatorEmissionProgressIncrementOutcome::QuotaFulfilled
        );
    }

    #[test]
    fn with_incremented_blocks_completed__correctly_increments_unfulfilled_quota() {
        // Given
        let progress = ValidatorEmissionProgress {
            effective_emission_rate: ValidatorEmissionRate {
                block_quota: BlockQuota::try_from(3).unwrap(),
                ..ValidatorEmissionRate::default()
            },
            blocks_completed_progress: BlockCompletionCount::from(1),
        };

        // When
        let result = progress.increment();

        // Then
        assert_eq!(
            result,
            ValidatorEmissionProgressIncrementOutcome::QuotaUnfulfilled(
                ValidatorEmissionProgress {
                    blocks_completed_progress: BlockCompletionCount::from(2),
                    ..progress
                }
            )
        );
    }
}
