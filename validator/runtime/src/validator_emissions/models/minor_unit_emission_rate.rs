use parity_scale_codec::{Decode, Encode};
use serde::{Deserialize, Serialize};

#[derive(Default, Clone, PartialEq, Debug, Eq, Encode, Decode, Deserialize, Serialize)]
/// MinorUnitEmissionRate stores an inner u64 value to be used as an emission amount
/// This structure prevents negative values and allows a reasonable bounds for an emission
pub struct MinorUnitEmissionRate(u64);
impl MinorUnitEmissionRate {
    pub fn value(&self) -> u64 {
        self.0
    }
}

impl From<u32> for MinorUnitEmissionRate {
    fn from(val: u32) -> Self {
        Self(val.into())
    }
}

impl From<u64> for MinorUnitEmissionRate {
    fn from(val: u64) -> Self {
        Self(val)
    }
}

#[allow(non_snake_case)]
#[cfg(test)]
mod tests {
    use crate::validator_emissions::models::minor_unit_emission_rate::MinorUnitEmissionRate;

    #[test]
    fn from__can_be_constructed_from_u32() {
        let value = u32::MAX;

        let rate = MinorUnitEmissionRate::from(value);

        assert_eq!(rate.value(), u32::MAX as u64)
    }

    #[test]
    fn from__can_be_constructed_from_u64() {
        let value = u64::MAX;

        let rate = MinorUnitEmissionRate::from(value);

        assert_eq!(rate.value(), u64::MAX)
    }

    #[test]
    fn value__returns_inner_value() {
        let value = 1u64;
        let rate = MinorUnitEmissionRate::from(1u64);

        let inner_value = rate.value();

        assert_eq!(inner_value, value)
    }
}
