#[derive(Debug, PartialEq, Eq)]
pub enum ModelConstructionFailure {
    ZeroValueSuppliedToNonZeroType,
}
