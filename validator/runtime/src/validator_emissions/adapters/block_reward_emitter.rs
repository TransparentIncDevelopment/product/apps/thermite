use crate::{
    confidentiality::reward::RewardError,
    validator_emissions::{contracts::rewards::BlockRewardEmitter, Config},
};
use core::marker::PhantomData;

pub struct XandBlockRewardEmitter<T>(PhantomData<T>);

impl<T> XandBlockRewardEmitter<T> {
    pub fn new() -> Self {
        Self(PhantomData)
    }
}

impl<T: Config> BlockRewardEmitter<T::AccountId> for XandBlockRewardEmitter<T> {
    type Error = RewardError;

    fn issue_reward(
        &mut self,
        account_id: T::AccountId,
        amount: xand_ledger::MonetaryValue,
    ) -> Result<(), Self::Error> {
        let _ = T::reward_validator_block(&account_id, amount)?;
        Ok(())
    }
}
