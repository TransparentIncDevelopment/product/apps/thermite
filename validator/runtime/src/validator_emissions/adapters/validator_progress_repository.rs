use core::marker::PhantomData;

use crate::validator_emissions::{contracts::rewards::ValidatorProgressRepository, Config, Pallet};

pub struct XandValidatorProgressRepository<T>(PhantomData<T>);

impl<T> XandValidatorProgressRepository<T> {
    pub fn new() -> Self {
        Self(PhantomData)
    }
}

impl<T: Config> ValidatorProgressRepository<T::AccountId> for XandValidatorProgressRepository<T> {
    fn get_validator_emission_progress(
        &mut self,
        account_id: T::AccountId,
    ) -> crate::validator_emissions::models::validator_emission_progress::ValidatorEmissionProgress
    {
        Pallet::<T>::get_validator_emission_progress(&account_id)
    }

    fn set_validator_emission_progress(
        &mut self,
        account_id: T::AccountId,
        progress: crate::validator_emissions::models::validator_emission_progress::ValidatorEmissionProgress,
    ) {
        Pallet::<T>::set_validator_emission_progress(&account_id, progress)
    }

    fn reset_validator_emission_progress_counters(&mut self, account_id: T::AccountId) {
        Pallet::<T>::reset_validator_emission_progress_counters(&account_id)
    }
}
