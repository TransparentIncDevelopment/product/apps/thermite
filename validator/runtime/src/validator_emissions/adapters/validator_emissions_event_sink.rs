use crate::validator_emissions::{
    contracts::events::ValidatorEmissionsEventSink,
    models::minor_unit_emission_rate::MinorUnitEmissionRate, Config, Event, Pallet,
};
use core::marker::PhantomData;

pub struct XandValidatorEmissionsEventSink<T>(PhantomData<T>);

impl<T> XandValidatorEmissionsEventSink<T> {
    pub fn new() -> Self {
        Self(PhantomData)
    }
}

impl<T: Config> ValidatorEmissionsEventSink<T::AccountId> for XandValidatorEmissionsEventSink<T> {
    fn incrementing_counter(&mut self, account_id: T::AccountId) {
        Pallet::<T>::deposit_event(Event::<T>::IncrementingCounter(account_id));
    }

    fn unknown_author_id_in_header(&mut self, account_id: T::AccountId) {
        Pallet::<T>::deposit_event(Event::<T>::UnknownAuthorIdInHeader(account_id));
    }

    fn emission_rate_rejected(&mut self, minor_units_per_emission: u64, block_quota: u32) {
        Pallet::<T>::deposit_event(Event::<T>::EmissionRateRejected(
            minor_units_per_emission,
            block_quota,
        ));
    }

    fn emission_rate_set(
        &mut self,
        rate: crate::validator_emissions::models::validator_emission_rate::ValidatorEmissionRate,
    ) {
        Pallet::<T>::deposit_event(Event::<T>::EmissionRateSet(rate));
    }

    fn award_emitted(&mut self, account_id: T::AccountId, amount: MinorUnitEmissionRate) {
        Pallet::<T>::deposit_event(Event::<T>::AwardEmitted(account_id, amount));
    }

    fn no_award_emitted(&mut self) {
        Pallet::<T>::deposit_event(Event::<T>::NoAwardEmitted);
    }

    fn award_failure(
        &mut self,
        account_id: T::AccountId,
        reason: crate::validator_emissions::EmissionFailureReason,
    ) {
        Pallet::<T>::deposit_event(Event::<T>::AwardFailure(account_id, reason));
    }
}
