use parity_scale_codec::Encode;
use sp_runtime::{testing::Header as TestHeader, traits::Header, DigestItem};

use crate::validator_emissions::validator_emissions_test_runtime::test_runtime::TEST_CONSENSUS_ID;

pub fn build_header(block_num: u64, author: u64) -> TestHeader {
    let mut header = TestHeader::new(
        block_num,
        Default::default(),
        Default::default(),
        Default::default(),
        Default::default(),
    );

    let digest = header.digest_mut();
    digest
        .logs
        .push(DigestItem::PreRuntime(TEST_CONSENSUS_ID, author.encode()));
    header
}
