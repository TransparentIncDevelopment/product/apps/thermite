use core::cell::RefCell;
use std::{collections::HashMap, hash::Hash};

use crate::validator_emissions::{
    contracts::rewards::ValidatorProgressRepository,
    models::validator_emission_progress::ValidatorEmissionProgress,
};

#[derive(Clone)]
pub struct FakeValidatorProgressRepository<AccountId> {
    progress: RefCell<HashMap<AccountId, ValidatorEmissionProgress>>,
}

impl<AccountId> FakeValidatorProgressRepository<AccountId>
where
    AccountId: Eq + Hash,
{
    pub fn with(store: HashMap<AccountId, ValidatorEmissionProgress>) -> Self {
        Self {
            progress: RefCell::new(store),
        }
    }

    pub fn with_account_progress(
        account_id: AccountId,
        progress: ValidatorEmissionProgress,
    ) -> Self {
        let mut store = HashMap::<AccountId, ValidatorEmissionProgress>::new();
        store.insert(account_id, progress);
        Self::with(store)
    }
}

impl<AccountId> ValidatorProgressRepository<AccountId>
    for FakeValidatorProgressRepository<AccountId>
where
    AccountId: Eq + Hash,
{
    fn get_validator_emission_progress(
        &mut self,
        account_id: AccountId,
    ) -> ValidatorEmissionProgress {
        self.progress
            .borrow()
            .get(&account_id)
            .cloned()
            .unwrap_or_default()
    }

    fn set_validator_emission_progress(
        &mut self,
        account_id: AccountId,
        progress: ValidatorEmissionProgress,
    ) {
        self.progress.borrow_mut().insert(account_id, progress);
    }

    fn reset_validator_emission_progress_counters(&mut self, account_id: AccountId) {
        self.set_validator_emission_progress(account_id, ValidatorEmissionProgress::default());
    }
}
