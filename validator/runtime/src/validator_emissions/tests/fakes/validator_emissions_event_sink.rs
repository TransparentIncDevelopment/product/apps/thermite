use crate::validator_emissions::{
    contracts::events::ValidatorEmissionsEventSink,
    models::{
        minor_unit_emission_rate::MinorUnitEmissionRate,
        validator_emission_rate::ValidatorEmissionRate,
    },
    EmissionFailureReason,
};
use core::cell::RefCell;
use core::fmt::Debug;

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum FakeEmissionEvents<AccountId> {
    IncrementingCounter(AccountId),
    UnknownAuthorIdInHeader(AccountId),
    EmissionRateRejected(u64, u32),
    EmissionRateSet(ValidatorEmissionRate),
    AwardEmitted(AccountId, MinorUnitEmissionRate),
    NoAwardEmitted,
    AwardFailure(AccountId, EmissionFailureReason),
}

#[derive(Clone, Default)]
pub struct FakeValidatorEmissionsEventSink<AccountId> {
    events: RefCell<Vec<FakeEmissionEvents<AccountId>>>,
}

impl<AccountId: Eq + Clone + Debug> FakeValidatorEmissionsEventSink<AccountId> {
    pub fn assert_contains(&self, event: FakeEmissionEvents<AccountId>) {
        assert!(
            self.events.borrow().contains(&event),
            "Event '{:?}' not found in '{:?}'",
            event,
            self.events.borrow()
        );
    }

    fn add_event(&mut self, event: FakeEmissionEvents<AccountId>) {
        self.events.borrow_mut().push(event);
    }
}

impl<AccountId: Eq + Clone + Debug> ValidatorEmissionsEventSink<AccountId>
    for FakeValidatorEmissionsEventSink<AccountId>
{
    fn incrementing_counter(&mut self, account_id: AccountId) {
        self.add_event(FakeEmissionEvents::<AccountId>::IncrementingCounter(
            account_id,
        ));
    }

    fn unknown_author_id_in_header(&mut self, account_id: AccountId) {
        self.add_event(FakeEmissionEvents::<AccountId>::UnknownAuthorIdInHeader(
            account_id,
        ));
    }

    fn emission_rate_rejected(&mut self, minor_units_per_emission: u64, block_quota: u32) {
        self.add_event(FakeEmissionEvents::<AccountId>::EmissionRateRejected(
            minor_units_per_emission,
            block_quota,
        ));
    }

    fn emission_rate_set(&mut self, rate: ValidatorEmissionRate) {
        self.add_event(FakeEmissionEvents::<AccountId>::EmissionRateSet(rate));
    }

    fn award_emitted(&mut self, account_id: AccountId, amount: MinorUnitEmissionRate) {
        self.add_event(FakeEmissionEvents::<AccountId>::AwardEmitted(
            account_id, amount,
        ));
    }

    fn no_award_emitted(&mut self) {
        self.add_event(FakeEmissionEvents::<AccountId>::NoAwardEmitted);
    }

    fn award_failure(&mut self, account_id: AccountId, reason: EmissionFailureReason) {
        self.add_event(FakeEmissionEvents::<AccountId>::AwardFailure(
            account_id, reason,
        ));
    }
}
