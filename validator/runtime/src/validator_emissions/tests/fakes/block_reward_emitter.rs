use crate::validator_emissions::contracts::rewards::BlockRewardEmitter;
use core::{cell::RefCell, default::Default, marker::PhantomData};
use std::{collections::HashMap, hash::Hash};
use xand_ledger::MonetaryValue;

#[derive(Clone, Eq, PartialEq)]
pub struct Reward<AccountId> {
    account_id: AccountId,
    amount: MonetaryValue,
}

#[derive(Clone, Default)]
pub struct FakeBlockRewardEmitter<AccountId> {
    awards: RefCell<HashMap<AccountId, Vec<MonetaryValue>>>,
}

impl<AccountId: Eq + Hash> FakeBlockRewardEmitter<AccountId> {
    pub fn get_total_award(&self, account_id: AccountId) -> u64 {
        match self.awards.borrow().get(&account_id) {
            None => 0,
            Some(vec) => vec.iter().map(|m| m.value()).sum(),
        }
    }
}

impl<AccountId: Eq + Hash> BlockRewardEmitter<AccountId> for FakeBlockRewardEmitter<AccountId> {
    type Error = ();

    fn issue_reward(
        &mut self,
        account_id: AccountId,
        amount: MonetaryValue,
    ) -> Result<(), Self::Error> {
        self.awards
            .borrow_mut()
            .entry(account_id)
            .or_insert_with(Default::default)
            .push(amount);
        Ok(())
    }
}

#[derive(Clone, Default)]
pub struct ErroringBlockRewardEmitter<AccountId, Error> {
    pub error: Error,
    pub account: PhantomData<AccountId>,
}

impl<AccountId, Error: Copy> BlockRewardEmitter<AccountId>
    for ErroringBlockRewardEmitter<AccountId, Error>
{
    type Error = Error;

    fn issue_reward(
        &mut self,
        _account_id: AccountId,
        _amount: MonetaryValue,
    ) -> Result<(), Self::Error> {
        Err(self.error)
    }
}
