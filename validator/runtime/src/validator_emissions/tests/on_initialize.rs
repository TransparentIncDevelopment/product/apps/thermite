use core::convert::TryInto;

use sp_runtime::traits::Header;

use super::super::Pallet;
use super::header_util;
use crate::validator_emissions::validator_emissions_test_runtime::{
    builders::base_test_ext,
    constants::{VAL_1, VAL_2},
    test_runtime::TestRuntime,
};
use crate::validator_emissions::{
    models::{
        validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    EmissionProgress, EmissionRateSetting,
};
use frame_support::traits::{OnFinalize, OnInitialize};

type System = frame_system::Module<TestRuntime>;

fn configure_emission_rate_setting(minor_units: u64, block_quota: u32) -> ValidatorEmissionRate {
    let setting = ValidatorEmissionRate {
        block_quota: block_quota.try_into().unwrap(),
        minor_units_per_emission: minor_units.into(),
    };
    EmissionRateSetting::<TestRuntime>::put(&setting);
    setting
}

fn initialize_validator_progress(
    validator_id: u64,
    blocks_completed: u32,
    emission_rate: &ValidatorEmissionRate,
) {
    EmissionProgress::<TestRuntime>::insert(
        validator_id,
        ValidatorEmissionProgress {
            blocks_completed_progress: blocks_completed.into(),
            effective_emission_rate: emission_rate.clone(),
        },
    );
}

/// Metadata for the block currently being executed by the runtime, such as block number and block
/// author, is stored by the various FRAME pallets in normal chain storage. They consider these
/// "temporary", in that their pallets' "initialize" methods for starting a block populate the
/// storage and their "finalize" methods remove it again, avoiding it actually being stored
/// on-chain.
///
/// The tests below rely on changing this metadata: we execute some code in the context of one
/// block and then different code in the context of another, and each has its own author. We rely
/// on the differing authors being visible from one point in time to another.
///
/// As such, this method will initialize the minimal block context such that both the block number
/// and author are available, run the provided "operation" closure, and then tear down that
/// context once complete. The important finalization step is the pallet_authorship "on_finalize",
/// as it clears a memoized author value in the chain storage. Without this, changing the author
/// would have no effect.
fn execute_within_block_environment(block_num: u64, author: u64, operation: impl FnOnce(u64)) {
    let header = header_util::build_header(block_num, author);
    System::initialize(
        &block_num,
        &Default::default(),
        header.digest(),
        Default::default(),
    );

    operation(block_num);

    pallet_authorship::Module::<TestRuntime>::on_finalize(block_num);
    System::finalize();
}

#[test]
fn on_initialize__authoring_validator_increments_block_completion_counter() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 3);
        initialize_validator_progress(VAL_1, 0, &initial_setting);

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 1u32.into(),
                effective_emission_rate: initial_setting,
            }
        );

        assert!(System::events()
            .iter()
            .any(|er| er.event.0.contains("IncrementingCounter")));

        assert!(!System::events()
            .iter()
            .any(|er| er.event.0.contains("AwardEmitted")));
    })
}

#[test]
fn on_initialize__authoring_validator_resets_progress_on_fulfillment() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 3);
        initialize_validator_progress(VAL_1, 2, &initial_setting);

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: initial_setting,
            }
        );
        Pallet::<TestRuntime>::on_finalize(System::block_number());
        assert!(System::events()
            .iter()
            .any(|er| er.event.0.contains("AwardEmitted")));
    })
}

#[test]
fn on_initialize__authoring_validator_adopts_new_emission_rate_on_fulfillment() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 3);
        initialize_validator_progress(VAL_1, 2, &initial_setting);
        let updated_setting = configure_emission_rate_setting(5, 6);

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: updated_setting,
            }
        );
    })
}

#[test]
fn on_initialize__quota_fulfillment_checks_effective_rate_for_authoring_validator() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 5);
        initialize_validator_progress(VAL_1, 2, &initial_setting);
        // After initializing, update quota to be only 3 blocks
        // It should NOT consider this new rate for fulfillment checks
        let updated_setting = configure_emission_rate_setting(10, 3);

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        execute_within_block_environment(101, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        execute_within_block_environment(102, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                // Reset occurred with block 102
                blocks_completed_progress: 0u32.into(),
                // New setting was adopted
                effective_emission_rate: updated_setting,
            }
        );
    })
}

#[test]
fn on_initialize__non_authoring_validator_unaffected() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 3);
        initialize_validator_progress(VAL_1, 2, &initial_setting);
        initialize_validator_progress(VAL_2, 2, &initial_setting);

        // When
        for i in 100..107 {
            execute_within_block_environment(i, VAL_1, |n| {
                Pallet::<TestRuntime>::on_initialize(n);
            });
        }

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_2),
            ValidatorEmissionProgress {
                blocks_completed_progress: 2u32.into(),
                effective_emission_rate: initial_setting,
            }
        );
    })
}

#[test]
fn on_initialize__individual_validators_have_separate_effective_emission_rates() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 5);
        initialize_validator_progress(VAL_1, 2, &initial_setting);
        initialize_validator_progress(VAL_2, 2, &initial_setting);
        let updated_setting = configure_emission_rate_setting(20, 3);

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });
        execute_within_block_environment(101, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });
        execute_within_block_environment(102, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        execute_within_block_environment(103, VAL_2, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });
        execute_within_block_environment(104, VAL_2, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: updated_setting,
            }
        );
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_2),
            ValidatorEmissionProgress {
                // Reset occurred with block 102
                blocks_completed_progress: 4u32.into(),
                // New setting was adopted
                effective_emission_rate: initial_setting,
            }
        );
    })
}

#[test]
fn on_initialize__default_emission_rate_is_inert() {
    base_test_ext().execute_with(|| {
        // Given
        // No global emission rate setting nor existing progress

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: ValidatorEmissionRate {
                    block_quota: 1u32.try_into().unwrap(),
                    minor_units_per_emission: 0u64.into(),
                },
            }
        )
    })
}

#[test]
fn on_initialize__zero_emissions_per_one_block_makes_no_progress() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(0, 1);

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 0u32.into(),
                effective_emission_rate: initial_setting,
            }
        )
    })
}

#[test]
fn on_initialize__initializes_new_progress_for_uninitialized_validator() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 5);
        // No initialization of past state for validators

        // When
        execute_within_block_environment(100, VAL_1, |n| {
            Pallet::<TestRuntime>::on_initialize(n);
        });

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 1u32.into(),
                effective_emission_rate: initial_setting,
            }
        );
    })
}

#[test]
fn on_initialize__multiple_quota_fulfillments_end_with_correct_progress() {
    base_test_ext().execute_with(|| {
        // Given
        let initial_setting = configure_emission_rate_setting(10, 5);
        initialize_validator_progress(VAL_1, 0, &initial_setting);

        // When
        for i in 100..111 {
            execute_within_block_environment(i, VAL_1, |n| {
                Pallet::<TestRuntime>::on_initialize(n);
            });
        }

        // Then
        assert_eq!(
            EmissionProgress::<TestRuntime>::get(VAL_1),
            ValidatorEmissionProgress {
                blocks_completed_progress: 1u32.into(),
                effective_emission_rate: initial_setting,
            }
        );
    })
}
