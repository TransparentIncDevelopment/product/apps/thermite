use sp_runtime::traits::Header;

use super::header_util::build_header;
use crate::validator_emissions::validator_emissions_test_runtime::{
    builders::base_test_ext, constants::VAL_1, test_runtime::TestRuntime,
};
use crate::validator_emissions::Error;

/// Substrate docs on unit-testing
/// https://substrate.dev/docs/en/knowledgebase/runtime/tests
use super::super::Pallet;

type System = frame_system::Module<TestRuntime>;

#[test]
fn get_author__returns_author_in_header() {
    base_test_ext().execute_with(|| {
        // Given
        let block_num = 1;
        let header = build_header(block_num, VAL_1);
        System::initialize(
            &block_num,
            &Default::default(),
            header.digest(),
            Default::default(),
        );

        // When
        let author = Pallet::<TestRuntime>::get_author().unwrap();

        // Then
        assert_eq!(author, VAL_1);
    })
}

#[test]
fn get_author__validator_id_not_in_authority_set_returns_err() {
    base_test_ext().execute_with(|| {
        // Given
        let block_num = 1;
        let invalid_validator: u64 = Default::default();
        let header = build_header(block_num, invalid_validator);
        System::initialize(
            &block_num,
            &Default::default(),
            header.digest(),
            Default::default(),
        );

        // When
        let author = Pallet::<TestRuntime>::get_author().unwrap_err();

        // Then
        assert!(matches!(author, Error::<TestRuntime>::IdNotInAuthoritySet))
    })
}
