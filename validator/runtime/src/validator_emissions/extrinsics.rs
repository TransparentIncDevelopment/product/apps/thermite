use crate::validator_emissions::validation::Validation;
use frame_support::dispatch::DispatchResultWithPostInfo;

pub mod set_validator_emission_rate;

pub trait ValidatorEmissionExtrinsic: Validation {
    type ExtrinsicInput;

    fn tx(input: Self::ExtrinsicInput) -> DispatchResultWithPostInfo;
}
