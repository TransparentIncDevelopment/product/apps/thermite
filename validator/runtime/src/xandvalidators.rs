//! A substrate runtime module for management of the validator set (without using the staking
//! module). All transactions in this module require root-level permissions, how elevation to root
//! happens is up to the caller.

// Annoying, should be scoped to the decl_storage! macro, but that does not work.
#![allow(clippy::string_lit_as_bytes)]
#![allow(clippy::unused_unit)]

use crate::{SessionPeriodOffset, DEFAULT_WEIGHT};
use frame_support::traits::ValidatorRegistration;
use frame_support::{debug, ensure, fail};
use frame_system::{self as system, ensure_root};
use pallet_session::{PeriodicSessions, ShouldEndSession};
use sp_runtime::traits::Convert;
use sp_std::prelude::*;

pub(crate) mod queryable_xand_validators;

pub use pallet::*;
use sp_core::sp_std::marker::PhantomData;

#[frame_support::pallet]
pub mod pallet {
    use super::*;
    use frame_support::pallet_prelude::*;
    use frame_system::pallet_prelude::*;
    use xandstrate_error_derive::ErrorHelper;

    #[pallet::config]
    pub trait Config:
        frame_system::Config + pallet_grandpa::Config + pallet_aura::Config + pallet_session::Config
    {
        type Event: From<Event<Self>> + IsType<<Self as frame_system::Config>::Event>;
        type OnRemoval: OnValidatorRemoved<<Self as system::Config>::AccountId>;
    }

    #[pallet::pallet]
    #[pallet::generate_store(pub (super) trait Store)]
    pub struct Pallet<T>(PhantomData<T>);

    #[pallet::hooks]
    impl<T: Config> Hooks<BlockNumberFor<T>> for Pallet<T> {}

    #[pallet::call]
    impl<T: Config> Pallet<T> {
        /// Add a new authority key so that a new validator can participate in the network.
        /// Note that this validator will need to register the session keys it's going to use
        /// before it can participate in consensus.
        /// It must do so by submitting a
        /// [`session::set_keys`](../srml_session/enum.Call.html#variant.set_keys) transaction
        /// signed with the key whose public half is the parameter to this transaction.
        /// In the normal course of business both that transaction and this one may be submitted
        /// via the polkadot UI.
        ///
        /// **IMPORTANT**: It takes two "sessions" for the change to be fully enacted. This
        /// normally (outside of testing) means 200 blocks or roughly 20 minutes
        ///
        /// **ALSO IMPORTANT**: If this transaction is submitted, the validator being registered
        /// should be ready and waiting to go, as it means they are counted as part of the quorum
        /// two sessions from now. This generally isn't a problem as long as all validators are
        /// operative and your network size is >=3, though.
        ///
        /// In the future this ought to be superseded by network voting
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn add_authority_key(
            origin: OriginFor<T>,
            account_id: T::AccountId,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::add_authority_key_tx(origin, account_id)
        }

        /// Remove an authority key from being considered a validator, so that it can no longer
        /// participate in the network.
        ///
        /// **IMPORTANT**: It takes two "sessions" for the change to be fully enacted. This
        /// normally (outside of testing) means 200 blocks or roughly 20 minutes. It also means that
        /// the removed validator *will continue to be part of the quorum for that long*. If
        /// removing the validator would bring you to less-than-quorum (ie: 3->2) then the network
        /// will stop finalizing if the validator is immediately brought down, so, don't do that.
        /// Wait for two sessions. With networks with N > 3 this isn't a problem as long as you
        /// don't remove multiple validators within two sessions.
        #[pallet::weight(DEFAULT_WEIGHT)]
        pub fn remove_authority_key(
            origin: OriginFor<T>,
            account_id: T::AccountId,
        ) -> DispatchResultWithPostInfo {
            Pallet::<T>::remove_authority_key_tx(origin, account_id)
        }
    }

    #[pallet::event]
    #[pallet::generate_deposit(pub (super) fn deposit_event)]
    pub enum Event<T: Config> {
        /// New validator added
        ValidatorAdded(T::AccountId),
        /// Validator removed
        ValidatorRemoved(T::AccountId),
    }

    #[pallet::error]
    #[derive(ErrorHelper)]
    pub enum Error<T> {
        ValidatorAlreadyInSet,
        ValidatorNotInSet,
        AccountIdConversion,
        TransactionMustBeDoneByRoot,
        TransactionMustBeDoneByRootOrAccount,
        CannotRemoveLastRemainingValidator,
    }

    #[pallet::storage]
    #[pallet::getter(fn validators)]
    // TODO: Should likely be `ValidatorId` -- though they are equivalent in our setup.
    //  Actually expressing that in bounds is of course crazy complex
    pub type Validators<T: Config> = StorageValue<_, Vec<T::AccountId>, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn pending_change)]
    pub type PendingChange<T: Config> = StorageValue<_, bool, ValueQuery>;

    #[pallet::storage]
    #[pallet::getter(fn epoch_duration)]
    pub type EpochDuration<T: Config> = StorageValue<_, T::BlockNumber, ValueQuery>;

    #[pallet::genesis_config]
    pub struct GenesisConfig<T: Config> {
        pub epoch_duration: T::BlockNumber,
        pub validators: Vec<T::AccountId>,
    }

    #[cfg(feature = "std")]
    impl<T: Config> Default for GenesisConfig<T> {
        fn default() -> Self {
            Self {
                epoch_duration: Default::default(),
                validators: Default::default(),
            }
        }
    }

    #[pallet::genesis_build]
    impl<T: Config> GenesisBuild<T> for GenesisConfig<T> {
        fn build(&self) {
            {
                let data = &self.epoch_duration;
                let v: &T::BlockNumber = data;
                <EpochDuration<T> as frame_support::storage::StorageValue<T::BlockNumber>>::put::<
                    &T::BlockNumber,
                >(v);
            }
            let extra_genesis_builder: fn(&Self) = |config| {
                let mut validators = vec![];
                for validator in config.validators.iter().cloned() {
                    system::Pallet::<T>::inc_providers(&validator);
                    validators.push(validator);
                }
                Validators::<T>::put(validators);
            };
            extra_genesis_builder(self);
        }
    }
}

pub type XandValidatorsErr<T> = Error<T>;

pub trait OnValidatorRemoved<AccountId> {
    fn on_validator_removed(
        account: &AccountId,
    ) -> frame_support::pallet_prelude::DispatchResultWithPostInfo;
}

impl<T: Config> Pallet<T> {
    fn add_authority_key_tx(
        origin: T::Origin,
        account_id: T::AccountId,
    ) -> frame_support::pallet_prelude::DispatchResultWithPostInfo {
        let root_res = ensure_root(origin);
        ensure!(
            root_res.is_ok(),
            XandValidatorsErr::<T>::TransactionMustBeDoneByRoot,
        );

        let current_validators = Self::validators();
        // Verify validator isn't already added. Technically this could be made more performant
        // with a map, but this list will never be all that long (almost certainly < 1k).
        for v in &current_validators {
            ensure!(
                v != &account_id,
                XandValidatorsErr::<T>::ValidatorAlreadyInSet
            );
        }

        let mut new_validators = current_validators;
        new_validators.push(account_id.clone());
        system::Pallet::<T>::inc_providers(&account_id);
        <Validators<T>>::put(new_validators);

        let validator_id: T::ValidatorId = match T::ValidatorIdOf::convert(account_id.clone()) {
            Some(v) => v,
            None => return Err(XandValidatorsErr::<T>::AccountIdConversion.into()),
        };

        if pallet_session::Module::<T>::is_registered(&validator_id) {
            PendingChange::<T>::put(true);
        }

        Self::deposit_event(Event::<T>::ValidatorAdded(account_id));
        Ok(().into())
    }

    fn remove_authority_key_tx(
        origin: T::Origin,
        account_id: T::AccountId,
    ) -> frame_support::pallet_prelude::DispatchResultWithPostInfo {
        let root_res = ensure_root(origin);
        ensure!(
            root_res.is_ok(),
            XandValidatorsErr::<T>::TransactionMustBeDoneByRoot,
        );

        let current_validators = Self::validators();
        // Verify the network won't get bricked by removing the last validator
        ensure!(
            current_validators.len() > 1,
            XandValidatorsErr::<T>::CannotRemoveLastRemainingValidator
        );

        // Verify id is actually a current validator
        if let Some(index) = Self::validator_index(&account_id) {
            Self::remove_validator_logic(index, account_id)?;
            Ok(().into())
        } else {
            fail!(XandValidatorsErr::<T>::ValidatorNotInSet)
        }
    }

    pub fn validator_index(account_id: &T::AccountId) -> Option<usize> {
        let current_validators = Self::validators();
        let mut index = None;
        for (i, v) in current_validators.iter().enumerate() {
            if v == account_id {
                index = Some(i);
                break;
            }
        }
        index
    }

    pub fn remove_validator_logic(
        index: usize,
        account_id: T::AccountId,
    ) -> frame_support::pallet_prelude::DispatchResultWithPostInfo {
        let mut new_validators = Self::validators();
        new_validators.remove(index);
        <Validators<T>>::put(new_validators);

        // Triggers the session to bounce
        PendingChange::<T>::put(true);

        T::OnRemoval::on_validator_removed(&account_id)?;
        // Can't dec_providers because session keys still exist (even though they won't be included
        // in validation) and we can't remove them while a session is in progress.
        // There is also a chance additional actions will need to be performed by validator after
        // they have been removed, e.g. redeem emissions.
        Self::deposit_event(Event::<T>::ValidatorRemoved(account_id));
        Ok(().into())
    }
}

impl<T: Config> sp_runtime::BoundToRuntimeAppPublic for Pallet<T> {
    type Public = T::AuthorityId;
}

impl<T: Config> pallet_session::SessionManager<T::ValidatorId> for Pallet<T> {
    fn new_session(_new_index: u32) -> Option<Vec<T::ValidatorId>> {
        let current_validators = Self::validators();
        let next_validators: Vec<T::ValidatorId> = current_validators
            .into_iter()
            .map(|x| T::ValidatorIdOf::convert(x).expect("Must be convertible"))
            .filter(|x| pallet_session::Module::<T>::is_registered(x))
            .collect();
        debug::info!("New session - next validators: {:?}", &next_validators);
        PendingChange::<T>::put(false);
        if next_validators.is_empty() {
            None
        } else {
            Some(next_validators)
        }
    }

    fn end_session(_end_index: u32) {}

    fn start_session(_start_index: u32) {}
}

impl<T: Config> ShouldEndSession<T::BlockNumber> for Pallet<T> {
    fn should_end_session(now: T::BlockNumber) -> bool {
        // We end the session if we have a pending change, or if the epoch duration
        // has elapsed

        PendingChange::<T>::get()
            || <PeriodicSessions<WrapEpochDuration<T>, SessionPeriodOffset> as ShouldEndSession<
                T::BlockNumber,
            >>::should_end_session(now)
    }
}

// Wrap EpochDuration so we can implement the Get trait for it
struct WrapEpochDuration<T: Config> {
    _phantom: PhantomData<T>,
}

impl<T: Config> frame_support::traits::Get<T::BlockNumber> for WrapEpochDuration<T> {
    fn get() -> T::BlockNumber {
        EpochDuration::<T>::get()
    }
}

// Looking for tests? Tests are in the main `xandstrate` module to reduce setup duplication.
