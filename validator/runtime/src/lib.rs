#![forbid(unsafe_code)]

//! The Substrate Node Template runtime. This can be compiled with `#[no_std]`, ready for Wasm.
//! While this was built off of the substrate-node-new template, the template does not include
//! GRANDPA by default.
//! See this MR for changes needed to enable GRANDPA
//! https://gitlab.com/TransparentIncDevelopment/xandstrate/merge_requests/5
//! Relevant code was lifted from this version of Substrate's runtime and service definitions:
//! https://github.com/paritytech/substrate/tree/59d5ec144704cec017a01f3e4dbb6b73c5542bf7

#![cfg_attr(not(feature = "std"), no_std)]
// `construct_runtime!` does a lot of recursion and requires us to increase the limit to 256.
#![recursion_limit = "256"]
// Annoying, should be scoped to the impl_runtime_apis! macro, but that does not work.
#![allow(clippy::large_enum_variant)]
// There's a handful of stuff in the substrate macros that breaks clippy lints:
#![allow(clippy::from_over_into, clippy::type_repetition_in_bounds)]

extern crate alloc;
extern crate core;

#[macro_use]
mod helpers;
pub mod confidentiality;
pub mod network_voting;
pub mod validator_emissions;

#[cfg(feature = "std")]
pub mod extrinsic_builder;
pub mod xandstrate;
pub mod xandvalidators;

#[cfg(feature = "test-upgrade-pallet")]
pub mod test_upgrade;

// Make the WASM binary available.
#[cfg(feature = "std")]
include!(concat!(env!("OUT_DIR"), "/wasm_binary.rs"));

/// Runtime version.
pub const VERSION: RuntimeVersion = RuntimeVersion {
    spec_name: create_runtime_str!("xandstrate"),
    impl_name: create_runtime_str!("xandstrate"),
    authoring_version: 2,
    // Per convention: if the runtime behavior changes, increment spec_version
    // and set impl_version to equal spec_version. If only runtime
    // implementation changes and behavior does not, then leave spec_version as
    // is and increment impl_version.
    spec_version: 2,
    impl_version: 2,
    apis: RUNTIME_API_VERSIONS, // defined in impl_runtime_apis! macro output
    transaction_version: 1,
};
pub const DEFAULT_WEIGHT: u64 = 1000;

/// Native version.
#[cfg(any(feature = "std", test))]
pub fn native_version() -> NativeVersion {
    NativeVersion {
        runtime_version: VERSION,
        can_author_with: Default::default(),
    }
}

pub use confidentiality::Call as ConfidentialityCall;
pub use confidentiality::Event as ConfidentialityEvent;
pub use frame_support::{
    traits::{Randomness, ValidatorSet},
    weights::Weight,
    StorageValue,
};
pub use frame_system::{
    self as system, CheckEra, CheckGenesis, CheckNonce, CheckSpecVersion, CheckTxVersion,
    CheckWeight, Event as SystemEvent, Phase,
};
pub use helpers::*;
pub use network_voting::Call as NetworkVotingCall;
pub use pallet_session::Call as SessionCall;
pub use pallet_timestamp::Call as TimestampCall;
#[cfg(any(feature = "std", test))]
pub use sp_runtime::BuildStorage;
pub use sp_runtime::{
    traits::{ConvertInto, NumberFor},
    DispatchError, Perbill, Permill,
};
pub use sp_transaction_pool;
pub use xandstrate::{AllowListedCidrBlocks, Call as XandCall, Error};
pub use xandstrate_api::ProposalData;
pub use xandvalidators::Call as XandValidatorsCall;

use crate::{
    confidentiality::{
        Config as ConfidentialityTrait, PendingCreateExpireTime, XandstrateStateTrait,
    },
    validator_emissions::models::{
        validator_emission_progress::ValidatorEmissionProgress,
        validator_emission_rate::ValidatorEmissionRate,
    },
    xandstrate::xandstrate_state_adapter::XandstrateStateAdapter,
    xandvalidators::Config as XandValidatorTrait,
};
use confidentiality::api as confidential_api;
use frame_support::{
    construct_runtime, parameter_types,
    traits::KeyOwnerProofSystem,
    weights::constants::{RocksDbWeight, WEIGHT_PER_SECOND},
};
use pallet_grandpa::{
    fg_primitives::{self},
    AuthorityId as GrandpaId, AuthorityList as GrandpaAuthorityList,
};
use pallet_session::PeriodicSessions;
use parity_scale_codec::{Decode, Encode};
use serde_scale_wrap::Wrap;
use sp_api::impl_runtime_apis;
use sp_consensus_aura::sr25519::AuthorityId as AuraId;
use sp_core::{crypto::KeyTypeId, OpaqueMetadata};
use sp_runtime::traits::AccountIdLookup;
use sp_runtime::{
    create_runtime_str, generic, impl_opaque_keys,
    traits::{BlakeTwo256, Block as BlockT, IdentifyAccount, OpaqueKeys, Verify},
    transaction_validity::{TransactionSource, TransactionValidity},
    ApplyExtrinsicResult, MultiSignature,
};
use sp_std::prelude::*;
#[cfg(feature = "std")]
use sp_version::NativeVersion;
use sp_version::RuntimeVersion;
use xand_ledger::{ClearTransactionOutput, CreateRequestRecord, RedeemRequestRecord};
use xand_runtime_models::{CidrBlockArray, FiatReqCorrelationId, X25519PublicKey};
use xandstrate::{
    vote_queries::get_vote_pairs, xand_voting_rules::XandVotingRules, xandstrate_api,
    RegisteredMembers, TrustNodeId,
};

/// An index to a block.
pub type BlockNumber = u32;

/// A number of blocks.
pub type BlockCount = u32;

/// Alias to 512-bit hash when used in the context of a transaction signature on the chain.
pub type Signature = MultiSignature;

/// Some way of identifying an account on the chain. We intentionally make it equivalent
/// to the public key of our transaction signing scheme.
pub type AccountId = <<Signature as Verify>::Signer as IdentifyAccount>::AccountId;

/// Encryption key that will be used by nodes
/// to encrypt bank account metadata to runtime storage
pub type EncryptionPubKey = X25519PublicKey;

/// The type for looking up accounts. We don't expect more than 4 billion of them, but you
/// never know...
pub type AccountIndex = u32;

/// The type for recording an account's balance. u64 gives us enough overhead in cents
/// for $184 × 10^15 dollars (or ~ 184 quadrillion dollars)
pub type Balance = u64;

/// Index of a transaction in the chain.
pub type Index = u32;

/// A hash of some data used by the chain.
pub type Hash = sp_core::H256;

/// Digest item type.
pub type DigestItem = generic::DigestItem<Hash>;
pub type Digest = generic::Digest<Hash>;

/// Event records for the system
pub type EventRecord = frame_system::EventRecord<Event, Hash>;
pub type SudoEvent = pallet_sudo::Event<Runtime>;

/// The address format for describing accounts.
pub type Address = sp_runtime::MultiAddress<AccountId, ()>;
/// Block header type as expected by this runtime.
pub type Header = generic::Header<BlockNumber, BlakeTwo256>;
/// Block type as expected by this runtime.
pub type Block = generic::Block<Header, UncheckedExtrinsic>;
/// A Block signed with a Justification
pub type SignedBlock = generic::SignedBlock<Block>;
/// BlockId type as expected by this runtime.
pub type BlockId = generic::BlockId<Block>;
/// The SignedExtension to the basic transaction logic.
pub type SignedExtra = (
    frame_system::CheckSpecVersion<Runtime>,
    frame_system::CheckTxVersion<Runtime>,
    frame_system::CheckGenesis<Runtime>,
    frame_system::CheckEra<Runtime>,
    frame_system::CheckNonce<Runtime>,
    frame_system::CheckWeight<Runtime>,
);
/// Unchecked extrinsic type as expected by this runtime.
pub type UncheckedExtrinsic = generic::UncheckedExtrinsic<Address, Call, Signature, SignedExtra>;
/// The payload being signed in transactions.
pub type SignedPayload = generic::SignedPayload<Call, SignedExtra>;
/// Extrinsic type that has already been checked.
pub type CheckedExtrinsic = generic::CheckedExtrinsic<AccountId, Call, SignedExtra>;
/// Executive: handles dispatch to the various modules.
pub type Executive =
    frame_executive::Executive<Runtime, Block, system::ChainContext<Runtime>, Runtime, AllModules>;

pub type AccountInfo = system::AccountInfo<Index, AccountData>;
pub type AccountData = ();
pub type AccountStorage = system::Account<Runtime>;
pub type TrustId = xandstrate::TrustNodeId<Runtime>;
pub type CreateExpireTime = PendingCreateExpireTime<Runtime>;
pub type LimitedAgentId = xandstrate::LimitedAgentId<Runtime>;
pub type AllowListedCidrBlockStorage = xandstrate::AllowListedCidrBlocks<Runtime>;
pub type ConfidentialPendingCreatesStorage = confidentiality::PendingCreates<Runtime>;
pub type ConfidentialPendingRedeemsStorage = confidentiality::PendingRedeems<Runtime>;
pub type ConfidentialClearUTxOs = confidentiality::ClearUTXOs<Runtime>;
pub type ValidatorEmissionRateStorage = validator_emissions::EmissionRateSetting<Runtime>;
pub type ValidatorEmissionProgressStorage = validator_emissions::EmissionProgress<Runtime>;

type Proposals = network_voting::Proposals<Runtime>;
type ProposalState = network_voting::ProposalState<Runtime>;

pub type XandstrateEvent = xandstrate::Event<Runtime>;
pub type ValidatorEmissionsEvent<T> = self::validator_emissions::Event<T>;
#[cfg(feature = "test-upgrade-pallet")]
pub type TestUpgradeEvent<T> = self::test_upgrade::Event<T>;

/// Opaque types. These are used by the CLI to instantiate machinery that don't need to know
/// the specifics of the runtime. They can then be made to be agnostic over specific formats
/// of data like extrinsics, allowing for them to continue syncing the network through upgrades
/// to even the core datastructures.
pub mod opaque {
    use super::*;
    use sp_core::sr25519;
    pub use sp_runtime::OpaqueExtrinsic;

    /// Opaque block header type.
    pub type Header = generic::Header<BlockNumber, BlakeTwo256>;
    /// Opaque block type.
    pub type Block = generic::Block<Header, OpaqueExtrinsic>;
    /// Opaque block identifier type.
    pub type BlockId = generic::BlockId<Block>;

    impl_opaque_keys! {
        pub struct SessionKeys {
            pub aura: Aura,
            pub grandpa: Grandpa,
        }
    }

    /// Given an opaque extrinsic, turn it into a more useful runtime-specific extrinsic.
    pub fn unopaqueify_extrinsic(
        op: OpaqueExtrinsic,
    ) -> Result<UncheckedExtrinsic, parity_scale_codec::Error> {
        let bytes = op.encode();
        UncheckedExtrinsic::decode(&mut bytes.as_slice())
    }

    /// Given an extrinsic returned from querying RPC, extract the signer's public key, if possible.
    pub fn get_signer_pubkey_from_extrinsic(
        ext: &super::UncheckedExtrinsic,
    ) -> Option<sr25519::Public> {
        if let Some(addr) = ext.signature.as_ref().map(|x| &x.0) {
            get_pubkey_from_ext_addr(addr)
        } else {
            None
        }
    }

    pub fn get_pubkey_from_ext_addr(addr: &Address) -> Option<sr25519::Public> {
        match addr {
            Address::Id(id) => {
                let raw: &[u8; 32] = id.as_ref();
                Some(sr25519::Public::from_raw(*raw))
            }
            _ => None,
        }
    }

    pub fn bytes_to_extrinsic_addr(bytes: [u8; 32]) -> Address {
        Address::Id(bytes.into())
    }
}

pub const MILLISECS_PER_BLOCK: u64 = 6000;
pub const SLOT_DURATION: u64 = MILLISECS_PER_BLOCK;
pub const SECS_PER_BLOCK: u64 = MILLISECS_PER_BLOCK / 1000;
pub const EPOCH_DURATION_IN_BLOCKS: BlockNumber = 100;
pub const SHORT_TEST_EPOCH_DURATION_IN_BLOCKS: BlockNumber = 3;

const NORMAL_DISPATCH_RATIO: Perbill = Perbill::from_percent(75);

parameter_types! {
    pub const BlockHashCount: BlockNumber = 250;
    pub const Version: RuntimeVersion = VERSION;
    pub const SS58Prefix: u8 = 42;
    pub BlockWeights: frame_system::limits::BlockWeights = frame_system::limits::BlockWeights
        ::with_sensible_defaults(2 * WEIGHT_PER_SECOND, NORMAL_DISPATCH_RATIO);
    pub BlockLength: frame_system::limits::BlockLength = frame_system::limits::BlockLength
        ::max_with_normal_ratio(5 * 1024 * 1024, NORMAL_DISPATCH_RATIO);
}

impl frame_system::Config for Runtime {
    type BaseCallFilter = ();
    /// The ubiquitous origin type.
    type Origin = Origin;
    /// The aggregated dispatch type that is available for extrinsics.
    type Call = Call;
    /// The index type for storing how many extrinsics an account has signed.
    type Index = Index;
    /// The index type for blocks.
    type BlockNumber = BlockNumber;
    /// The type for hashing blocks and tries.
    type Hash = Hash;
    /// The hashing algorithm used.
    type Hashing = BlakeTwo256;
    /// The identifier used to distinguish between accounts.
    type AccountId = AccountId;
    /// The lookup mechanism to get account ID from whatever is passed in dispatchers.
    type Lookup = AccountIdLookup<AccountId, ()>;
    /// The header type.
    type Header = generic::Header<BlockNumber, BlakeTwo256>;
    /// The ubiquitous event type.
    type Event = Event;
    /// Maximum number of block number to block hash mappings to keep (oldest pruned first).
    type BlockHashCount = BlockHashCount;
    /// The weight of database operations that the runtime can invoke.
    type DbWeight = RocksDbWeight;
    /// Version of the runtime.
    type Version = Version;
    /// Converts a module to the index of the module in `construct_runtime!`.
    ///
    /// This type is being generated by `construct_runtime!`.
    type PalletInfo = PalletInfo;
    /// No account data since we use generic asset instead of balance
    type AccountData = ();
    type OnNewAccount = ();
    type OnKilledAccount = ();
    type SystemWeightInfo = ();
    type BlockWeights = BlockWeights;
    type BlockLength = BlockLength;
    type SS58Prefix = SS58Prefix;
}

impl pallet_aura::Config for Runtime {
    type AuthorityId = AuraId;
}

impl pallet_grandpa::Config for Runtime {
    type Event = Event;
    type Call = Call;

    type KeyOwnerProof =
        <Self::KeyOwnerProofSystem as KeyOwnerProofSystem<(KeyTypeId, GrandpaId)>>::Proof;

    type KeyOwnerIdentification = <Self::KeyOwnerProofSystem as KeyOwnerProofSystem<(
        KeyTypeId,
        GrandpaId,
    )>>::IdentificationTuple;

    type KeyOwnerProofSystem = ();

    type HandleEquivocation = ();
    type WeightInfo = ();
}

parameter_types! {
    pub const DisabledValidatorsThreshold: Perbill = Perbill::from_percent(17);
    pub const SessionPeriod: BlockNumber = 100;
    pub const SessionPeriodOffset: BlockNumber = 0;
}

impl pallet_session::Config for Runtime {
    type Event = Event;
    type ValidatorId = <Self as system::Config>::AccountId;
    type ValidatorIdOf = ConvertInto;
    type ShouldEndSession = XandValidators;
    type NextSessionRotation = PeriodicSessions<SessionPeriod, SessionPeriodOffset>;
    type SessionManager = XandValidators;
    type SessionHandler = <opaque::SessionKeys as OpaqueKeys>::KeyTypeIdProviders;
    type Keys = opaque::SessionKeys;
    type DisabledValidatorsThreshold = DisabledValidatorsThreshold;
    type WeightInfo = ();
}

impl XandValidatorTrait for Runtime {
    type Event = Event;
    type OnRemoval = XandStrate;
}

parameter_types! {
    pub const IndexDeposit: Balance = 0;
}

parameter_types! {
    pub const MinimumPeriod: u64 = SLOT_DURATION / 2;
}

impl pallet_timestamp::Config for Runtime {
    /// A timestamp: milliseconds since the unix epoch.
    type Moment = u64;
    type OnTimestampSet = Aura;
    type MinimumPeriod = MinimumPeriod;
    type WeightInfo = ();
}

impl pallet_sudo::Config for Runtime {
    type Event = Event;
    type Call = Call;
}

parameter_types! {
    pub const WindowSize: BlockNumber = 101;
    pub const ReportLatency: BlockNumber = 1000;
}

impl xandstrate::Config for Runtime {
    type Event = Event;
}

impl network_voting::Config for Runtime {
    type Event = Event;
    type Proposal = Call;
    type VotingPermissions = XandVotingRules<Self>;
    type ProposalStatusCheck = XandVotingRules<Self>;
    type Lifecycle = XandVotingRules<Self>;
}

impl validator_emissions::Config for Runtime {
    type Event = Event;
}

#[cfg(feature = "test-upgrade-pallet")]
impl test_upgrade::Config for Runtime {
    type Event = Event;
}

impl pallet_authorship::Config for Runtime {
    type FindAuthor = pallet_session::FindAccountFromAuthorIndex<Self, Aura>;
    type UncleGenerations = ();
    type FilterUncle = ();
    type EventHandler = ();
}

impl_runtime_apis! {
    impl sp_api::Core<Block> for Runtime {
        fn version() -> RuntimeVersion {
            VERSION
        }

        fn execute_block(block: Block) {
            Executive::execute_block(block)
        }

        fn initialize_block(header: &<Block as BlockT>::Header) {
            Executive::initialize_block(header)
        }
    }

    impl sp_api::Metadata<Block> for Runtime {
        fn metadata() -> OpaqueMetadata {
            Runtime::metadata().into()
        }
    }

    impl sp_block_builder::BlockBuilder<Block> for Runtime {
        fn apply_extrinsic(extrinsic: <Block as BlockT>::Extrinsic) -> ApplyExtrinsicResult {
            Executive::apply_extrinsic(extrinsic)
        }

        fn finalize_block() -> <Block as BlockT>::Header {
            Executive::finalize_block()
        }

        fn inherent_extrinsics(data: sp_inherents::InherentData) -> Vec<<Block as BlockT>::Extrinsic> {
            data.create_extrinsics()
        }

        fn check_inherents(
            block: Block,
            data: sp_inherents::InherentData,
        ) -> sp_inherents::CheckInherentsResult {
            data.check_extrinsics(&block)
        }

        fn random_seed() -> <Block as BlockT>::Hash {
            RandomnessCollectiveFlip::random_seed()
        }
    }

    impl sp_transaction_pool::runtime_api::TaggedTransactionQueue<Block> for Runtime {
        fn validate_transaction(
            source: TransactionSource,
            tx: <Block as BlockT>::Extrinsic,
        ) -> TransactionValidity {
            Executive::validate_transaction(source, tx)
        }
    }

    impl sp_offchain::OffchainWorkerApi<Block> for Runtime {
        fn offchain_worker(header: &<Block as BlockT>::Header) {
            Executive::offchain_worker(header)
        }
    }

    impl sp_consensus_aura::AuraApi<Block, AuraId> for Runtime {
        fn slot_duration() -> u64 {
            Aura::slot_duration()
        }

        fn authorities() -> Vec<AuraId> {
            Aura::authorities()
        }
    }

    impl sp_session::SessionKeys<Block> for Runtime {
        fn generate_session_keys(seed: Option<Vec<u8>>) -> Vec<u8> {
            opaque::SessionKeys::generate(seed)
        }

        fn decode_session_keys(
            encoded: Vec<u8>,
        ) -> Option<Vec<(Vec<u8>, sp_core::crypto::KeyTypeId)>> {
            opaque::SessionKeys::decode_into_raw_public_keys(&encoded)
        }
    }

    impl fg_primitives::GrandpaApi<Block> for Runtime {
        fn grandpa_authorities() -> GrandpaAuthorityList {
            Grandpa::grandpa_authorities()
        }

        fn submit_report_equivocation_unsigned_extrinsic(
            _equivocation_proof: fg_primitives::EquivocationProof<
                <Block as BlockT>::Hash,
                NumberFor<Block>,
            >,
            _key_owner_proof: fg_primitives::OpaqueKeyOwnershipProof,
        ) -> Option<()> {
            None
        }

        fn generate_key_ownership_proof(
            _set_id: fg_primitives::SetId,
            _authority_id: GrandpaId,
        ) -> Option<fg_primitives::OpaqueKeyOwnershipProof> {
            // NOTE: this is the only implementation possible since we've
            // defined our key owner proof type as a bottom type (i.e. a type
            // with no values).
            None
        }
    }

    impl frame_system_rpc_runtime_api::AccountNonceApi<Block, AccountId, Index> for Runtime {
        fn account_nonce(account: AccountId) -> Index {
            System::account_nonce(account)
        }
    }

    impl xandstrate_api::AllowlistApi<Block, AccountId> for Runtime {
        fn get_allowlist() -> Vec<(AccountId, CidrBlockArray)> {
            AllowListedCidrBlockStorage::iter().collect()
        }
    }

    impl confidential_api::ConfidentialPendingApi<Block> for Runtime {
        fn get_confidential_pending_creates() -> Vec<(FiatReqCorrelationId, Wrap<CreateRequestRecord>)> {
            ConfidentialPendingCreatesStorage::iter().collect()
        }
        fn get_confidential_pending_redeems() -> Vec<(FiatReqCorrelationId, Wrap<RedeemRequestRecord>)> {
            ConfidentialPendingRedeemsStorage::iter().collect()
        }
        fn get_clear_utxos() -> Vec<Wrap<ClearTransactionOutput>> {
            ConfidentialClearUTxOs::iter().map(|(clear_utxo, _)| clear_utxo).collect()
        }
        fn get_create_expire_time() -> u64 {
            CreateExpireTime::get()
        }
    }

    impl xandstrate_api::ProposalsApi<Block, AccountId, BlockNumber> for Runtime {
        fn get_proposal(
            id: network_voting::PropIndex
        ) -> Option<xandstrate_api::ProposalData<AccountId, BlockNumber>> {
            if let Some((proposed_action, proposer, expiration_block_id)) = Proposals::get(id) {
                let votes = get_vote_pairs::<Runtime>(id);
                let status = ProposalState::get(id);
                let data = xandstrate_api::ProposalData {
                    id,
                    votes,
                    proposer,
                    expiration_block_id,
                    proposed_action: proposed_action.encode(),
                    status,
                };
                Some(data)
            } else {
                None
            }
        }
        fn get_all_proposals() -> Vec<xandstrate_api::ProposalData<AccountId, BlockNumber>> {
            let mut props = Vec::new();
            for (id, (proposed_action, proposer, expiration_block_id)) in Proposals::iter() {
                let votes = get_vote_pairs::<Runtime>(id);
                let status = ProposalState::get(id);
                let data = xandstrate_api::ProposalData {
                    id,
                    votes,
                    proposer,
                    expiration_block_id,
                    proposed_action: proposed_action.encode(),
                    status,
                };
                props.push(data);
             }
             props
        }
    }

    impl xandstrate_api::TrusteeApi<Block, AccountId> for Runtime {
        fn get_trustee() -> AccountId {
            TrustId::get()
        }
    }

    impl xandstrate_api::LimitedAgentApi<Block, AccountId> for Runtime {
        fn get_limited_agent() -> Option<AccountId> {
            LimitedAgentId::get()
        }
    }

    impl xandstrate_api::ValidatorEmissionsApi<Block, AccountId> for Runtime {
        fn get_validator_emission_rate() -> ValidatorEmissionRate {
            ValidatorEmissionRateStorage::get()
        }
        fn get_validator_emission_progress(acct: AccountId) -> Option<ValidatorEmissionProgress> {
            if ValidatorEmissionProgressStorage::contains_key(acct.clone()) {
                Some(ValidatorEmissionProgressStorage::get(acct))
            } else {
                None
            }
        }
    }

    impl xandstrate_api::MemberInfoApi<Block, AccountId> for Runtime {
        fn get_members() -> Vec<AccountId> {
            xandstrate::RegisteredMembers::<Runtime>::iter()
                .filter_map(|(k, v)| Some(k).filter(|_| v))
                .collect()
        }

        fn get_banned_members() -> Vec<AccountId> {
            xandstrate::BannedMembers::<Runtime>::iter()
                .map(|(k, _)| k)
                .collect()
        }
    }

    impl xandstrate_api::AuthorityKeyApi<Block, AccountId> for Runtime {
        fn get_authority_keys() -> Vec<AccountId> {
            xandvalidators::Module::<Runtime>::validators()
        }
    }

    impl xandstrate_api::EncryptionApi<Block, AccountId> for Runtime {
        fn get_encryption_key(acct: AccountId) -> Option<EncryptionPubKey> {
            if xandstrate::NodeEncryptionKey::<Runtime>::contains_key(acct.clone()) {
                Some(xandstrate::NodeEncryptionKey::<Runtime>::get(acct))
            } else {
                None
            }
        }
    }

    impl xandstrate_api::TotalIssuanceApi<Block> for Runtime {
        fn get_total_issuance() -> u64 {
            confidentiality::TotalCreated::<Self>::get() - confidentiality::TotalRedeemed::<Self>::get()
        }
    }

    /// Substrate FRAME runtime benchmarking setup
    /// Enables us to get generated test functions via Substrate macros
    /// Add any benchmarked modules here by their names as defined in `construct_runtime!`
    #[cfg(feature = "runtime-benchmarks")]
    impl frame_benchmarking::Benchmark<Block> for Runtime {
        fn dispatch_benchmark(
            config: frame_benchmarking::BenchmarkConfig,
        ) -> Result<Vec<frame_benchmarking::BenchmarkBatch>, sp_runtime::RuntimeString> {
            use frame_benchmarking::{add_benchmark, Benchmarking, BenchmarkBatch, TrackedStorageKey};
            use frame_system_benchmarking::Module as SystemBench;

            impl frame_system_benchmarking::Config for Runtime {}

            let allowlist: Vec<TrackedStorageKey> = vec![
               // you can allowlist any storage keys you do not want to track here
            ];


            let mut batches = Vec::<BenchmarkBatch>::new();
            let params = (&config, &allowlist);

            // Adding the pallet or module that we will be benchmarking
            add_benchmark!(params, batches, frame_system, SystemBench::<Runtime>);
            add_benchmark!(params, batches, confidentiality, Confidentiality);

            if batches.is_empty() {
                return Err("Benchmark not found for this pallet.".into());
            }
            Ok(batches)
        }
    }
}

impl ConfidentialityTrait for Runtime {
    type Event = Event;
    type XandstrateState = XandstrateStateAdapter<Self>;
}

#[cfg(not(feature = "test-upgrade-pallet"))]
construct_runtime!(
   pub enum Runtime where
       Block = Block,
       NodeBlock = opaque::Block,
       UncheckedExtrinsic = UncheckedExtrinsic
   {
     System: system::{Module, Call, Storage, Config, Event<T>},
     Timestamp: pallet_timestamp::{Module, Call, Storage, Inherent},
     Aura: pallet_aura::{Module, Config<T>},
     Grandpa: pallet_grandpa::{Module, Call, Storage, Config, Event},
     Sudo: pallet_sudo::{Module, Call, Config<T>, Storage, Event<T>},
     RandomnessCollectiveFlip: pallet_randomness_collective_flip::{Module, Call, Storage},
     Authorship: pallet_authorship::{Module, Call, Storage, Inherent},
     // Our runtime(s)
     XandStrate: xandstrate::{Module, Call, Storage, Event<T>, Config<T>},
     XandValidators: xandvalidators::{Module, Call, Storage, Event<T>, Config<T>},
     NetworkVoting: network_voting::{Module, Call, Storage, Event<T>, Config<T>},
     Confidentiality: confidentiality::{Module, Call, Storage, Event<T>, Config<T>, ValidateUnsigned},
     ValidatorEmissions: validator_emissions::{Module, Call, Storage, Event<T>, Config},
     // Including this after XandStrate and XandValidators because this will fail if the accounts
     // aren't already added
     Session: pallet_session::{Module, Call, Storage, Event, Config<T>},
   }
);

#[cfg(feature = "test-upgrade-pallet")]
construct_runtime!(
   pub enum Runtime where
       Block = Block,
       NodeBlock = opaque::Block,
       UncheckedExtrinsic = UncheckedExtrinsic
   {
     System: system::{Module, Call, Storage, Config, Event<T>},
     Timestamp: pallet_timestamp::{Module, Call, Storage, Inherent},
     Aura: pallet_aura::{Module, Config<T>},
     Grandpa: pallet_grandpa::{Module, Call, Storage, Config, Event},
     Sudo: pallet_sudo::{Module, Call, Config<T>, Storage, Event<T>},
     RandomnessCollectiveFlip: pallet_randomness_collective_flip::{Module, Call, Storage},
     Authorship: pallet_authorship::{Module, Call, Storage, Inherent},
     // Our runtime(s)
     XandStrate: xandstrate::{Module, Call, Storage, Event<T>, Config<T>},
     XandValidators: xandvalidators::{Module, Call, Storage, Event<T>, Config<T>},
     NetworkVoting: network_voting::{Module, Call, Storage, Event<T>, Config<T>},
     Confidentiality: confidentiality::{Module, Call, Storage, Event<T>, Config<T>, ValidateUnsigned},
     ValidatorEmissions: validator_emissions::{Module, Call, Storage, Event<T>, Config},
     TestUpgrade: test_upgrade::{Module, Call, Storage, Event<T>},
     // Including this after XandStrate and XandValidators because this will fail if the accounts
     // aren't already added
     Session: pallet_session::{Module, Call, Storage, Event, Config<T>},
   }
);
