//! Various helpers that are all re-exported at the top level

use frame_support::{StorageHasher, Twox128};
use sp_core::storage::StorageKey;

use crate::{Event, Runtime, SystemEvent};
use frame_support::traits::PalletInfo;
use sp_runtime::DispatchError;

lazy_static::lazy_static! {
    static ref XANDSTRATE_MOD_IX: usize =
        crate::PalletInfo::index::<crate::xandstrate::Module<Runtime>>().unwrap();
    static ref NETWORK_MOD_IX: usize =
        crate::PalletInfo::index::<crate::network_voting::Module<Runtime>>().unwrap();
    static ref CONFIDENTIAL_MOD_IX: usize =
        crate::PalletInfo::index::<crate::confidentiality::Module<Runtime>>().unwrap();
    static ref VALIDATOR_EMISSIONS_MOD_IX: usize =
        crate::PalletInfo::index::<crate::validator_emissions::Module<Runtime>>().unwrap();
    static ref XANDVALIDATORS_MOD_IX: usize =
        crate::PalletInfo::index::<crate::xandvalidators::Module<Runtime>>().unwrap();
}

/// Computes the storage key for system events. IE: All events for the block that you are performing
/// the lookup at.
pub fn system_events_storage_key() -> StorageKey {
    storage_key_for("System", "Events")
}

/// Calculate the storage key for something created by `decl_storage!` in the substrate runtime.
/// This is necessary for some modules which do not publicly expose their storage structs, and
/// so `hashed_key` is inaccessible.
///
/// Should not be publicly exposed so consumers don't need to update/change the input strings
///
/// See https://github.com/paritytech/substrate/blob/master/frame/support/src/storage/generator/value.rs#L28
/// for reference
fn storage_key_for(module_prefix: &str, storage_prefix: &str) -> StorageKey {
    let mut final_key = [0u8; 32];
    final_key[0..16].copy_from_slice(&Twox128::hash(module_prefix.as_bytes()));
    final_key[16..32].copy_from_slice(&Twox128::hash(storage_prefix.as_bytes()));
    StorageKey(final_key.to_vec())
}

/// Given an iterator over some (mutable) events, fix them up to contain proper error messages. This
/// is needed because The extrinsic failed event, for reasons that are totally unclear, never
/// actually serializes the error message, so we must re-populate it since we know what it
/// should be.
pub fn fixup_events<'a, T: Iterator<Item = &'a mut Event>>(events: T) {
    for e in events {
        if let Event::system(SystemEvent::<Runtime>::ExtrinsicFailed(ref mut err, _)) = e {
            fixup_err_msg(err)
        }
    }
}

/// If the message is missing in a dispatch error, stringify the enum variant name and use
/// that.
pub fn fixup_err_msg(dispatch_err: &mut DispatchError) {
    if let DispatchError::Module {
        message,
        error,
        index,
    } = dispatch_err
    {
        if message.is_none() {
            // Determine which pallet to use, and use its index->str converter.
            let pallet_index = *index as usize;
            // Variant at index 0 is generated the `__Ignore(_, _)` for PhantomData.
            let variant_index = *error + 1;

            match pallet_index {
                i if i == *XANDSTRATE_MOD_IX => {
                    *message = Some(crate::xandstrate::err_ix_to_str(variant_index))
                }
                i if i == *NETWORK_MOD_IX => {
                    *message = Some(crate::network_voting::err_ix_to_str(variant_index))
                }
                i if i == *CONFIDENTIAL_MOD_IX => {
                    *message = Some(crate::confidentiality::err_ix_to_str(variant_index))
                }
                i if i == *VALIDATOR_EMISSIONS_MOD_IX => {
                    *message = Some(crate::validator_emissions::err_ix_to_str(variant_index))
                }
                i if i == *XANDVALIDATORS_MOD_IX => {
                    *message = Some(crate::xandvalidators::err_ix_to_str(variant_index))
                }
                _ => {}
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fixup_err_msg_emits_expected_error_message() {
        // NOTICE: the `error` is fragile. Currently the `error` index 0 corresponds to
        //   the first of the explicitly listed error variants, namely
        //   `CreateAmountMustBeMoreThanZero`.
        let confidentiality_pallet_index = u8::try_from(*CONFIDENTIAL_MOD_IX).unwrap();
        let create_amount_zero_enum_index = 0;

        let mut dispatch_err = DispatchError::Module {
            index: confidentiality_pallet_index,
            error: create_amount_zero_enum_index,
            message: None,
        };
        fixup_err_msg(&mut dispatch_err);

        match dispatch_err {
            DispatchError::Module { message, .. } => {
                assert_eq!(message, Some("CreateAmountMustBeMoreThanZero"))
            }
            _ => unreachable!(),
        }
    }
}
