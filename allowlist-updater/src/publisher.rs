use crate::errors::Result;
use xand_runtime_models::CidrBlock;

pub trait AllowListPublisher {
    fn publish(&self, blocks: Vec<CidrBlock>) -> Result<()>;
}
