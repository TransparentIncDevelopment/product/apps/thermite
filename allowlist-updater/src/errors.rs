use cfg::ConfigError;
use serde_derive::Serialize;
use snafu::Snafu;
use std::sync::Arc;
use xandstrate_client::SubstrateClientErrors;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum Error {
    #[snafu(display("{}: {:?}", message, source))]
    Io {
        message: String,
        #[snafu(source(from(std::io::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::io::Error>,
    },
    Serialization {
        message: String,
        #[snafu(source(from(std::io::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::io::Error>,
    },
    Configuration {
        message: String,
        #[snafu(source(from(ConfigError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<ConfigError>,
    },
    Rpc {
        source: SubstrateClientErrors,
    },
    Local {
        message: String,
    },
    Glob {
        #[snafu(source(from(glob::PatternError, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<glob::PatternError>,
    },
}

impl From<glob::PatternError> for Error {
    fn from(source: glob::PatternError) -> Self {
        Error::Glob {
            source: Arc::new(source),
        }
    }
}
