use crate::errors::Result;
use xand_runtime_models::CidrBlock;

pub trait CidrBlockRetriever {
    fn retrieve_cidr_blocks(&self) -> Result<Vec<CidrBlock>>;
}
