use crate::{
    errors::*, nginx_config_publisher::NginxConfigPublisher,
    substrate_client_retriever::JsonRpcRetriever, updater::AllowListUpdater,
};
use cfg::{Config, Environment, File as CfgFile};
use futures::executor::block_on;
use glob::glob;
use serde::{Deserialize, Serialize};
use snafu::ResultExt;
use std::env;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::time::Duration;
use url::Url;
use xandstrate_client::SubstrateClient;

/// Config struct specific for running an updater
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SubstrateClientRetriever {
    #[serde(with = "url_serde")]
    validator_endpoint: Url,
    nginx_allowlist_conf_path: String,
    cadence: u64,
}

impl SubstrateClientRetriever {
    /// Create an updater from config
    pub fn create_updater(
        &self,
    ) -> Result<AllowListUpdater<JsonRpcRetriever<SubstrateClient>, NginxConfigPublisher>> {
        let substrate_client =
            block_on(SubstrateClient::connect(self.validator_endpoint.clone())).context(Rpc)?;
        let retriever: JsonRpcRetriever<SubstrateClient> = JsonRpcRetriever::new(substrate_client);
        let allowlist_path = PathBuf::from(&self.nginx_allowlist_conf_path);
        let publisher = NginxConfigPublisher::new(allowlist_path);
        let cadence = Duration::from_secs(self.cadence);
        Ok(AllowListUpdater::new(retriever, publisher, cadence))
    }
}

/// Reads config file at `config_path` and generates a config struct of type `T`
pub fn config_reader<T>(config_path: &str) -> Result<T>
where
    T: Serialize + Deserialize<'static>,
{
    let mut config = Config::default();

    if !Path::new(config_path).exists() {
        let err = Error::Local {
            message: format!("Provided path {} does not exist", config_path),
        };
        return Err(err);
    }

    let config_files = glob(&format!("{}/*.toml", config_path))?
        .chain(glob(&format!("{}/*.yaml", config_path))?)
        .chain(glob(&format!("{}/*.yml", config_path))?)
        .chain(glob(&format!("{}/*.json", config_path))?)
        .map(|path| CfgFile::from(path.unwrap()))
        .collect::<Vec<_>>();

    let current_dir = env::current_dir().context(Io {
        message: "Can not determine current directory",
    })?;
    if config_files.is_empty() {
        let err = Error::Local {
            message: format!(
                "No config files found at provided path {} at current path: {}",
                config_path,
                current_dir.display()
            ),
        };
        return Err(err);
    }

    config.merge(config_files).context(Configuration {
        message: "Trying to merge configs",
    })?;

    config
        .merge(Environment::with_prefix("allowlist_updater").separator("-"))
        .context(Configuration {
            message: "Trying to merge configs",
        })?;

    let cfg: T = serde_path_to_error::deserialize(config).map_err(|err| Error::Configuration {
        message: format!("Deserializing Path: {}", err.path()),
        source: Arc::new(err.into_inner()),
    })?;

    Ok(cfg)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_config_exists() {
        let config: Result<SubstrateClientRetriever> = config_reader("config");
        assert!(config.is_ok())
    }

    #[test]
    fn test_no_config_exists() {
        let config: Result<SubstrateClientRetriever> = config_reader("boo");
        assert!(config.is_err())
    }
}
