use crate::{errors::*, publisher::AllowListPublisher};
use snafu::ResultExt;
use std::{fs::File, io::Write, path::PathBuf};
use xand_runtime_models::CidrBlock;

/// Struct for taking a vector of CIDR blocks, formatting those blocks, and publishing them to a specified
/// nginx config file.
pub struct NginxConfigPublisher {
    path: PathBuf,
}

impl NginxConfigPublisher {
    pub fn new(path: PathBuf) -> Self {
        Self { path }
    }

    fn write_to_file(&self, content: &str) -> Result<()> {
        let mut file = File::create(&self.path).context(Io {
            message: format!("Couldn't create file at {:?}", self.path),
        })?;
        file.write_all(content.as_bytes()).context(Io {
            message: format!("Couldn't write to {:?}", self.path),
        })
    }
}

impl AllowListPublisher for NginxConfigPublisher {
    fn publish(&self, blocks: Vec<CidrBlock>) -> Result<()> {
        let blob = format_nginx_allowlist_config(blocks);
        self.write_to_file(&blob)?;
        Ok(())
    }
}

fn format_nginx_allowlist_config(blocks: Vec<CidrBlock>) -> String {
    let blob = blocks.iter().fold(String::new(), |init, b| {
        let converted = b.to_string();
        format!("{}allow {};\n", init, converted)
    });
    blob
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Read;
    use tempfile::NamedTempFile;

    #[test]
    fn test_publish() {
        let mut file = NamedTempFile::new().unwrap();
        let publisher = NginxConfigPublisher::new(file.path().to_owned());
        let block = CidrBlock([1, 2, 3, 4, 5]);
        let all_blocks = vec![block];
        publisher.publish(all_blocks).unwrap();
        let mut read = String::new();
        file.read_to_string(&mut read).unwrap();
        assert_eq!(read, "allow 1.2.3.4/5;\n");
    }
}
