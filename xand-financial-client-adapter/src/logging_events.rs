use tpfs_logger_port::logging_event;

#[logging_event]
pub enum LoggingEvent {
    KeyParseError(String),
}
