use curve25519_dalek::ristretto::{CompressedRistretto, RistrettoPoint};
use sp_core::{crypto::Ss58Codec, sr25519::Public};
use std::convert::TryFrom;
use tpfs_krypt::{KeyIdentifier, KeyType};
use xand_address::Address;
use xand_ledger::{IdentityTag, PublicKey};
use xand_public_key::ristretto_point_to_address;

pub fn address_to_id_tag(address: &Address) -> Option<IdentityTag> {
    let public_key = Public::from_ss58check(&address.to_string()).ok()?;
    let point = CompressedRistretto::from_slice(&public_key).decompress()?;
    Some(IdentityTag::from_key_with_generator_base(PublicKey::from(
        point,
    )))
}

pub fn id_tag_to_address(id_tag: &IdentityTag) -> Option<Address> {
    Some(ristretto_point_to_address(&RistrettoPoint::from(
        PublicKey::try_from(*id_tag).ok()?,
    )))
}

pub fn address_to_key_id(address: &Address) -> KeyIdentifier {
    KeyIdentifier {
        key_type: KeyType::SubstrateSr25519,
        value: address.to_string(),
    }
}

#[cfg(test)]
mod tests {
    use crate::{address_to_id_tag, id_tag_to_address};
    use curve25519_dalek::ristretto::RistrettoPoint;

    #[test]
    fn address_to_id_tag_roundtrips() {
        let expected_address =
            super::ristretto_point_to_address(&RistrettoPoint::random(&mut rand::thread_rng()));
        assert_eq!(
            id_tag_to_address(&address_to_id_tag(&expected_address).unwrap()).unwrap(),
            expected_address,
        );
    }
}
