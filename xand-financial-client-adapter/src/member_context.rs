use async_trait::async_trait;
use std::sync::Arc;
use xand_financial_client::{errors::FinancialClientError, MemberContext};
use xand_ledger::PublicKey;
use xand_public_key::address_to_public_key;
use xandstrate_client::SubstrateClientInterface;

pub struct SubstrateClientMemberContext<T> {
    pub(crate) substrate_client: Arc<T>,
}

#[async_trait]
impl<T> MemberContext for SubstrateClientMemberContext<T>
where
    T: SubstrateClientInterface,
{
    async fn get_banned_members(&self) -> Result<Vec<PublicKey>, FinancialClientError> {
        let addresses = self
            .substrate_client
            .get_banned_members()
            .await
            .map_err(|err| FinancialClientError::UnexpectedInternalError(Box::new(err)))?;

        let mut pubkeys = Vec::new();
        for address in addresses {
            if let Some(pubkey) = address_to_public_key(&address) {
                pubkeys.push(pubkey);
            } else {
                return Err(FinancialClientError::InvalidAddress {
                    reason: format!("Failed to convert '{:?}' to PublicKey", address),
                });
            }
        }

        Ok(pubkeys)
    }
}

impl<T> From<Arc<T>> for SubstrateClientMemberContext<T>
where
    T: SubstrateClientInterface,
{
    fn from(substrate_client: Arc<T>) -> Self {
        Self { substrate_client }
    }
}
