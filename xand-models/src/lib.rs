//! Chain-agnostic models of blockchain data and business representations
//!
//! Only things below the `xand-api` in our architecture should depend directly on this crate.
//! If you are a user of `xand-api` (IE: Some external service like the trustee), you should
//! depend exclusively on the `xand-api-client` package, which re-exports the things you will
//! need from this crate.
//!
//! Also see the `xand-runtime-models` crate which has some no-std compatible structures
//! for use inside the runtime, but not dependent on it.

#![forbid(unsafe_code)]

pub mod api_client_mapping;
mod correlation_id;
mod financial_client_mapping;
pub mod jwt;
pub mod serde_hex;
#[cfg(feature = "runtime-conversions")]
mod substrate_mapping;
#[cfg(feature = "sp-core")]
mod substrate_primitives;
mod transaction_id;

#[cfg(feature = "sp-core")]
pub use self::substrate_primitives::*;
pub use correlation_id::{CorrelationId, CorrelationIdError};
pub use financial_client_mapping::errors::TxnConversionError;
#[cfg(feature = "runtime-conversions")]
pub use substrate_mapping::{
    decode_non_confidential_call, hash_substrate_encodeable,
    test_helpers as substrate_test_helpers, to_platform::*,
};
pub use transaction_id::{TransactionId, TransactionIdError};
pub use xand_runtime_models::{CidrBlock, CidrBlockParseErr, ProposalStage};

use chrono::{DateTime, TimeZone, Utc};
use derive_more::Constructor;
use serde::{
    Deserialize, Serialize,
    __private::{fmt::Error, Formatter},
};
use snafu::{ResultExt, Snafu};
use std::{
    any::type_name,
    collections::HashMap,
    convert::{TryFrom, TryInto},
    fmt::{Debug, Display},
    str::FromStr,
    sync::Arc,
};
use strum_macros::{EnumString, ToString};
use tpfs_krypt::Encrypted;
use xand_address::Address;
use xand_ledger::{CreateCancellationReason, RedeemCancellationReason};
pub use xand_runtime_models::{FixedSizeFieldError, RuntimeBankAccount, X25519PublicKey};

/// A Encryption key representing the public portion of some private key, and hence a (potential)
/// sender or receiver of encrypted messages.
///
/// Currently, this means a Base58 encoded string
#[derive(Clone, Debug, Deserialize, Hash, Eq, PartialEq, Serialize)]
pub struct EncryptionKey([u8; 32]);

impl EncryptionKey {
    pub fn as_bytes(&self) -> &[u8] {
        &self.0
    }
}

impl Display for EncryptionKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), Error> {
        let encoded_key = bs58::encode(self.0).into_string();
        Display::fmt(&encoded_key, f)
    }
}

impl TryFrom<String> for EncryptionKey {
    type Error = EncryptionKeyError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let key = bs58::decode(&value).into_vec().context(Base58Encode)?;
        EncryptionKey::try_from(key.as_slice())
    }
}

impl FromStr for EncryptionKey {
    type Err = EncryptionKeyError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.to_owned().try_into()
    }
}

impl TryFrom<&[u8]> for EncryptionKey {
    type Error = EncryptionKeyError;

    fn try_from(slice: &[u8]) -> Result<Self, Self::Error> {
        if slice.len() != 32 {
            Err(EncryptionKeyError::InvalidKeyLength)
        } else {
            let mut bytes: [u8; 32] = [0; 32];
            bytes.copy_from_slice(slice);
            Ok(EncryptionKey(bytes))
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, snafu::Snafu)]
pub enum EncryptionKeyError {
    #[snafu(display("Encryption key wasn't proper base 58: {:?}", source))]
    Base58Encode {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: bs58::decode::Error,
    },
    #[snafu(display("Invalid encryption key length, expected 32 bytes"))]
    InvalidKeyLength,
}

/// A chain-implementation agnostic representation of the status of a transaction/extrinsic/whatever
/// that a client has submitted (or tried to submit) to the network.
///
/// From's and to's are implemented for the most common conversion scenarios.
#[derive(Serialize, Deserialize, Debug, EnumString, Hash, PartialEq, Eq, Clone, ToString)]
pub enum TransactionStatus {
    /// The chain and/or validator has never seen the transaction (or refuses to admit it has)
    Unknown,
    /// The chain and/or validator has seen the transaction but has not made a decision about it
    Pending,
    /// The chain and/or validator has decided the transaction is invalid. The parameter is the
    /// reason for rejection.
    Invalid(String),
    /// The transaction is successfully committed to the chain
    Committed,
    /// The transaction is finalized
    Finalized,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct Transaction {
    pub signer_address: Address,
    pub transaction_id: TransactionId,
    pub status: TransactionStatus,
    pub txn: XandTransaction,
    pub timestamp: DateTime<Utc>,
}

impl Transaction {
    pub fn is_financial_event(&self) -> bool {
        matches!(
            self.txn,
            XandTransaction::CreatePendingCreate(_)
                | XandTransaction::FulfillCreate(_)
                | XandTransaction::CreatePendingRedeem(_)
                | XandTransaction::FulfillRedeem(_)
                | XandTransaction::Send(_)
        )
    }

    pub fn from_xand_txn(
        id: TransactionId,
        txn: XandTransaction,
        signer: Address,
        status: TransactionStatus,
        timestamp: DateTime<Utc>,
    ) -> Self {
        Self {
            transaction_id: id,
            signer_address: signer,
            status,
            txn,
            timestamp,
        }
    }

    // These "get_xxx" methods that return an option are fragile and undesirable, they
    // exist because the member api flattens the fields of transactions to one depth level.

    /// If the transaction involves an amount of money, return that amount in minor units
    pub fn get_amount(&self) -> Option<u64> {
        match &self.txn {
            XandTransaction::Send(data) => Some(data.amount_in_minor_unit),
            XandTransaction::CreatePendingCreate(data) => Some(data.amount_in_minor_unit),
            XandTransaction::CreatePendingRedeem(data) => Some(data.amount_in_minor_unit),
            _ => None,
        }
    }

    /// If the transaction involves a correlation_id, return it.
    pub fn get_correlation_id(&self) -> Option<&CorrelationId> {
        match &self.txn {
            XandTransaction::CreatePendingCreate(data) => Some(&data.correlation_id),
            XandTransaction::CreatePendingRedeem(data) => Some(&data.correlation_id),
            XandTransaction::CancelCreate(data) => Some(&data.correlation_id),
            XandTransaction::FulfillCreate(data) => Some(&data.correlation_id),
            XandTransaction::FulfillRedeem(data) => Some(&data.correlation_id),
            XandTransaction::CancelRedeem(data) => Some(&data.correlation_id),
            _ => None,
        }
    }

    /// If the transaction involves some target address for the result of the operation, return it
    pub fn get_destination_addr(&self) -> Option<Address> {
        match &self.txn {
            XandTransaction::RegisterMember(data) => Some(data.address.clone()),
            XandTransaction::SetTrust(data) => Some(data.address.clone()),
            XandTransaction::Send(data) => Some(data.destination_account.clone()),
            XandTransaction::CreatePendingCreate(_) => Some(self.signer_address.clone()),
            _ => None,
        }
    }

    /// If the transaction contains bank account data, return it.
    pub fn get_bank_info(&self) -> Option<&BankAccountInfo> {
        match &self.txn {
            XandTransaction::CreatePendingCreate(data) => Some(&data.account),
            XandTransaction::CreatePendingRedeem(data) => Some(&data.account),
            _ => None,
        }
    }

    pub fn timestamp_from_unix_time_millis(t: i64) -> DateTime<Utc> {
        Utc.timestamp_millis(t)
    }
}

#[derive(
    Clone,
    Debug,
    Hash,
    PartialEq,
    Eq,
    Serialize,
    Deserialize,
    strum_macros::Display,
    strum_macros::EnumDiscriminants,
)]
/// Chain-agnostic versions of all of our transaction types, wrapped up in an enum that provides
/// conversions and deserialization from the specific transaction types in substrate.
#[strum_discriminants(name(TransactionType))]
#[strum_discriminants(derive(Deserialize, Serialize))]
pub enum XandTransaction {
    // NOTE: The serialization names should match the actual transaction names in substrate
    #[strum(serialize = "register_account_as_member")]
    RegisterMember(RegisterAccountAsMember),
    #[strum(serialize = "remove_member")]
    RemoveMember(RemoveMember),
    #[strum(serialize = "exit_member")]
    ExitMember(ExitMember),
    #[strum(serialize = "set_trust_node_id")]
    SetTrust(SetTrustNodeId),
    #[strum(serialize = "set_limited_agent_id")]
    SetLimitedAgent(SetLimitedAgentId),
    #[strum(serialize = "set_validator_emission_rate")]
    SetValidatorEmissionRate(SetValidatorEmissionRate),
    #[strum(serialize = "set_member_encryption_key")]
    SetMemberEncryptionKey(SetMemberEncKey),
    #[strum(serialize = "set_trust_encryption_key")]
    SetTrustEncryptionKey(SetTrustEncKey),
    #[strum(serialize = "set_pending_create_expire_time")]
    SetPendingCreateExpire(SetPendingCreateExpire),
    #[strum(serialize = "send")]
    Send(SendSchema),
    #[strum(serialize = "create_pending_create")]
    CreatePendingCreate(PendingCreate),
    #[strum(serialize = "fulfill_pending_create")]
    FulfillCreate(FulfillPendingCreate),
    #[strum(serialize = "cancel_pending_create")]
    CancelCreate(CancelPendingCreate),
    #[strum(serialize = "cancel_pending_redeem")]
    CancelRedeem(CancelPendingRedeem),
    #[strum(serialize = "redeem")]
    CreatePendingRedeem(PendingRedeem),
    #[strum(serialize = "redeem_fulfillment")]
    FulfillRedeem(FulfillPendingRedeem),
    #[strum(serialize = "add_authority_key")]
    AddAuthorityKey(AddAuthorityKey),
    #[strum(serialize = "remove_authority_key")]
    RemoveAuthorityKey(RemoveAuthorityKey),
    #[strum(serialize = "allowlist_cidr_block")]
    AllowlistCidrBlock(AllowlistCidrBlock),
    #[strum(serialize = "remove_allowlist_cidr_block")]
    RemoveAllowlistCidrBlock(RemoveAllowlistCidrBlock),
    #[strum(serialize = "root_allowlist_cidr_block")]
    RootAllowlistCidrBlock(RootAllowlistCidrBlock),
    #[strum(serialize = "root_remove_allowlist_cidr_block")]
    RootRemoveAllowlistCidrBlock(RootRemoveAllowlistCidrBlock),
    #[strum(serialize = "submit_proposal")]
    SubmitProposal(SubmitProposal),
    #[strum(serialize = "vote_proposal")]
    VoteProposal(VoteProposal),
    #[strum(serialize = "withdraw_from_network")]
    WithdrawFromNetwork,
    #[strum(serialize = "register_session_keys")]
    RegisterSessionKeys(RegisterSessionKeys),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterSessionKeys {
    /// Stringified version of the ss58 session key for block production
    pub block_production_pubkey: Address,
    /// Stringified version of the ss58 session key for block finalization
    pub block_finalization_pubkey: Address,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterAccountAsMember {
    /// Stringified version of the ss58 address to register
    pub address: Address,

    /// Public encryption key
    pub encryption_key: X25519PublicKey,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RemoveMember {
    /// Stringified version of the ss58 address to remove
    pub address: Address,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExitMember {
    /// Stringified version of the ss58 address to exit
    pub address: Address,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetTrustNodeId {
    /// Stringified version of the ss58 address to become the trust
    pub address: Address,

    /// Public encryption key
    pub encryption_key: X25519PublicKey,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetLimitedAgentId {
    /// Optional stringified version of the ss58 address to become the limited agent
    pub address: Option<Address>,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetValidatorEmissionRate {
    /// Amount paid, in minor units, per "emission period" (number of blocks defined below)
    pub minor_units_per_emission: u64,
    /// Interval (number of blocks produced) between each payout
    pub block_quota: u32,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetMemberEncKey {
    /// The new encryption key to set for the member
    pub key: X25519PublicKey,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Blockstamp {
    pub block_number: u64,
    pub unix_timestamp_ms: i64,
    pub is_stale: bool,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetTrustEncKey {
    /// The new encryption key to set for the trust
    pub key: X25519PublicKey,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetPendingCreateExpire {
    /// Value of new timeout in number of blocks
    pub expire_in_milliseconds: u64,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SendSchema {
    /// Stringified version of the public key funds will be sent to
    pub destination_account: Address,
    /// Amount to send in cents
    pub amount_in_minor_unit: u64,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize, Default)]
pub struct BankAccountId {
    pub routing_number: String,
    pub account_number: String,
}

/// Some bank account information associated with creates and redeems. Depending on where in their
/// lifecycle they are, the information may or may not be encrypted yet.
#[derive(Clone, Debug, PartialEq, Hash, Eq, Serialize, Deserialize)]
pub enum BankAccountInfo {
    Unencrypted(BankAccountId),
    Encrypted(Encrypted<BankAccountId>),
    Malformed(Vec<u8>),
}

#[derive(Debug, Snafu, Serialize, Clone)]
pub enum BankAccountConversionErr {
    #[snafu(display("Problem (de)serializing unencrypted bank info: {:?}", source))]
    #[snafu(context(false))]
    BadUnencryptedJson {
        #[snafu(source(from(serde_json::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<serde_json::Error>,
    },
    #[snafu(display("Serialized bank info is too large: {:?}", source))]
    #[snafu(context(false))]
    JsonTooBigError { source: FixedSizeFieldError },
    #[snafu(display("Encrypted bank info is too large"))]
    EncryptedPayloadTooLarge,
    #[snafu(display("Encrypted payload is invalid"))]
    BadEncryptedPayload,
    #[snafu(display("Failed to convert from bytes: {}", message))]
    ByteConversionError { message: String },
}

impl Default for BankAccountInfo {
    /// A default implementation is provided for testing purposes only
    fn default() -> Self {
        BankAccountInfo::Unencrypted(BankAccountId {
            routing_number: "fake".to_string(),
            account_number: "fake".to_string(),
        })
    }
}

impl TryFrom<RuntimeBankAccount> for BankAccountInfo {
    type Error = BankAccountConversionErr;

    fn try_from(v: RuntimeBankAccount) -> Result<Self, Self::Error> {
        match v {
            xand_runtime_models::RuntimeBankAccount::JsonEncoded(dat) => {
                let decoded: BankAccountId = serde_json::from_str(&dat.to_string())?;
                Ok(BankAccountInfo::Unencrypted(decoded))
            }
            xand_runtime_models::RuntimeBankAccount::Encrypted(bytes) => {
                Ok(Encrypted::from_bytes_unchecked(&bytes.0)
                    .map(BankAccountInfo::Encrypted)
                    .unwrap_or_else(|_| BankAccountInfo::Malformed(bytes.0.into())))
            }
        }
    }
}

impl From<BankAccountId> for BankAccountInfo {
    fn from(v: BankAccountId) -> Self {
        BankAccountInfo::Unencrypted(v)
    }
}

impl TryInto<RuntimeBankAccount> for BankAccountInfo {
    type Error = BankAccountConversionErr;

    fn try_into(self) -> Result<RuntimeBankAccount, Self::Error> {
        match self {
            BankAccountInfo::Unencrypted(field) => {
                let stringified = serde_json::to_string(&field)?;
                let payload = stringified.into_bytes().try_into()?;
                let result = xand_runtime_models::RuntimeBankAccount::JsonEncoded(payload);
                Ok(result)
            }
            BankAccountInfo::Encrypted(payload) => {
                let payload = payload
                    .to_bytes()
                    .try_into()
                    .map_err(|_| BankAccountConversionErr::EncryptedPayloadTooLarge)?;
                Ok(xand_runtime_models::RuntimeBankAccount::Encrypted(payload))
            }
            // Make converting malformed bank data into runtime bank data an error to
            // prevent clients from pushing bad data to the chain
            BankAccountInfo::Malformed(_) => Err(BankAccountConversionErr::BadEncryptedPayload),
        }
    }
}

pub struct ConversionFailure {
    pub value: String,
    pub original_type: &'static str,
    pub target_type: &'static str,
}

pub fn convert_type<T, U>(v: T) -> Result<U, ConversionFailure>
where
    T: Copy + Debug,
    U: TryFrom<T>,
{
    v.try_into().map_err(|_| ConversionFailure {
        value: format!("{:?}", v),
        original_type: type_name::<T>(),
        target_type: type_name::<U>(),
    })
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct PendingCreate {
    /// Amount of the pending create, in USD cents
    pub amount_in_minor_unit: u64,
    /// A nonce uniquely identifying this pending create
    pub correlation_id: CorrelationId,
    /// The (possibly decrypted) info about the bank account which will/did provide funds for
    /// this request
    pub account: BankAccountInfo,
    /// ID of the corresponding fulfilling or cancellation transaction if known.
    /// It will always be `None` when submitting a `PendingCreate`.
    pub completing_transaction: Option<CreateCompletion>,
    // TODO: Expire time should to be exposed here, but isn't actually needed by anything at the
    //   moment
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct PendingRedeem {
    /// Amount of the pending redeem, in USD cents
    pub amount_in_minor_unit: u64,
    /// A nonce uniquely identifying this pending redeem
    pub correlation_id: CorrelationId,
    /// The (possibly decrypted) info about the bank account to which the trust should/did
    /// deposit funds
    pub account: BankAccountInfo,
    /// ID of the corresponding fulfilling or cancellation transaction if known.
    /// It will always be `None` when submitting a `PendingRedeem`.
    pub completing_transaction: Option<RedeemCompletion>,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FulfillPendingRedeem {
    /// Nonce of the redeem to mark as fulfilled
    pub correlation_id: CorrelationId,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FulfillPendingCreate {
    /// Nonce of the create to mark as fulfilled
    pub correlation_id: CorrelationId,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CancelPendingCreate {
    /// Nonce of the create to mark as cancelled
    pub correlation_id: CorrelationId,
    /// The reason for the cancellation
    pub reason: CreateCancellationReason,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CancelPendingRedeem {
    /// Nonce of the redeem to mark as cancelled
    pub correlation_id: CorrelationId,
    /// The reason for the cancellation
    pub reason: RedeemCancellationReason,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AddAuthorityKey {
    /// Stringified version of the validator's signing pubkey (ss58 address from sr25519 pubkey)
    pub account_id: Address,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RemoveAuthorityKey {
    /// Stringified version of the validator's signing pubkey (ss58 address from sr25519 pubkey)
    pub account_id: Address,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AllowlistCidrBlock {
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RemoveAllowlistCidrBlock {
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RootAllowlistCidrBlock {
    pub account: Address,
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RootRemoveAllowlistCidrBlock {
    pub account: Address,
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SubmitProposal {
    pub proposed_action: AdministrativeTransaction,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct VoteProposal {
    pub id: u32,
    pub vote: bool,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetCode {
    pub code: Vec<u8>,
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum CreateCompletion {
    Confirmation(TransactionId),
    Cancellation(TransactionId),
    Expiration(TransactionId),
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum RedeemCompletion {
    Confirmation(TransactionId),
    Cancellation(TransactionId),
}

#[derive(Clone, Debug, Default)]
pub struct TransactionFilter {
    pub addresses: Vec<Address>,
    pub types: Vec<TransactionType>,
    pub start_time: Option<DateTime<Utc>>,
    pub end_time: Option<DateTime<Utc>>,
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum AdministrativeTransaction {
    RegisterAccountAsMember(RegisterAccountAsMember),
    SetTrust(SetTrustNodeId),
    SetLimitedAgent(SetLimitedAgentId),
    SetValidatorEmissionRate(SetValidatorEmissionRate),
    AddAuthorityKey(AddAuthorityKey),
    RemoveAuthorityKey(RemoveAuthorityKey),
    RootAllowlistCidrBlock(RootAllowlistCidrBlock),
    RootRemoveAllowlistCidrBlock(RootRemoveAllowlistCidrBlock),
    RemoveMember(RemoveMember),
}

impl AdministrativeTransaction {
    pub fn from_transaction(t: XandTransaction) -> Option<Self> {
        match t {
            XandTransaction::RegisterMember(x) => {
                Some(AdministrativeTransaction::RegisterAccountAsMember(x))
            }
            XandTransaction::RemoveMember(x) => Some(AdministrativeTransaction::RemoveMember(x)),
            XandTransaction::SetTrust(x) => Some(AdministrativeTransaction::SetTrust(x)),
            XandTransaction::SetLimitedAgent(x) => {
                Some(AdministrativeTransaction::SetLimitedAgent(x))
            }
            XandTransaction::SetValidatorEmissionRate(x) => {
                Some(AdministrativeTransaction::SetValidatorEmissionRate(x))
            }
            XandTransaction::AddAuthorityKey(x) => {
                Some(AdministrativeTransaction::AddAuthorityKey(x))
            }
            XandTransaction::RemoveAuthorityKey(x) => {
                Some(AdministrativeTransaction::RemoveAuthorityKey(x))
            }
            XandTransaction::AllowlistCidrBlock(_) => None,
            XandTransaction::RemoveAllowlistCidrBlock(_) => None,
            XandTransaction::RootAllowlistCidrBlock(x) => {
                Some(AdministrativeTransaction::RootAllowlistCidrBlock(x))
            }
            XandTransaction::RootRemoveAllowlistCidrBlock(x) => {
                Some(AdministrativeTransaction::RootRemoveAllowlistCidrBlock(x))
            }
            // These transactions are performed directly by the requesting validator (i.e. root
            // permissions are not required) so there are no corresponding administrative
            // transactions
            XandTransaction::SubmitProposal(_) => None,
            XandTransaction::VoteProposal(_) => None,
            XandTransaction::SetMemberEncryptionKey(_) => None,
            XandTransaction::SetTrustEncryptionKey(_) => None,
            XandTransaction::SetPendingCreateExpire(_) => None,
            XandTransaction::Send(_) => None,
            XandTransaction::CreatePendingCreate(_) => None,
            XandTransaction::FulfillCreate(_) => None,
            XandTransaction::CancelCreate(_) => None,
            XandTransaction::CancelRedeem(_) => None,
            XandTransaction::CreatePendingRedeem(_) => None,
            XandTransaction::FulfillRedeem(_) => None,
            XandTransaction::RegisterSessionKeys(_) => None,
            XandTransaction::ExitMember(_) => None,
            XandTransaction::WithdrawFromNetwork => None,
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Proposal {
    /// Unique proposal id
    pub id: u32,
    /// Map of voters to their votes, true is yes, false is no
    pub votes: HashMap<Address, bool>,
    /// Address of party that created proposal
    pub proposer: Address,
    /// Blockchain block on which proposal will expire and be removed
    pub expiration_block_id: u32,
    /// The action that will be executed if this proposal is accepted
    pub proposed_action: AdministrativeTransaction,
    /// This proposal's current state
    pub status: ProposalStage,
}

#[cfg(test)]
mod test {
    use super::*;
    use chrono::Utc;
    use std::str::FromStr;
    use tpfs_krypt::{Encrypted, KeyType};
    use xand_address::Address;

    fn example_addr() -> Address {
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap()
    }

    fn example_encryption_key() -> X25519PublicKey {
        Default::default()
    }

    fn from_xand_txn(
        id: TransactionId,
        txn: XandTransaction,
        signer: Address,
        status: TransactionStatus,
    ) -> Transaction {
        let timestamp = Utc.ymd(2020, 2, 5).and_hms(6, 0, 0);
        Transaction::from_xand_txn(id, txn, signer, status, timestamp)
    }

    #[test]
    pub fn transaction_size() {
        // This is just a good sanity check to make sure we don't accidentally make these really big
        assert!(std::mem::size_of::<Transaction>() < 300);
    }

    #[test]
    pub fn transaction_status_from_str() {
        let status_str = TransactionStatus::Unknown.to_string();

        let result = TransactionStatus::from_str(&status_str).unwrap();

        assert_eq!(result.to_string(), status_str);
    }

    #[test]
    pub fn unencrypted_bank_info_roundtrips_through_runtime_representation() {
        // Given
        let bank_account = BankAccountId {
            routing_number: "fredbob".to_string(),
            account_number: "foobar".to_string(),
        };

        let input: BankAccountInfo = bank_account.into();

        // When
        let substrate_model: RuntimeBankAccount = input.clone().try_into().unwrap();
        let output: BankAccountInfo = substrate_model.try_into().unwrap();

        assert_eq!(input, output);
    }

    #[test]
    pub fn encrypted_bank_info_roundtrips_through_runtime_representation() {
        let mut key_manager = tpfs_krypt::from_config(tpfs_krypt::config::KryptConfig {
            key_manager_config: tpfs_krypt::config::KeyManagerConfig::InMemoryKeyManager(
                Default::default(),
            ),
        })
        .unwrap();
        let alice = key_manager
            .generate_keypair(KeyType::SharedEncryptionX25519)
            .unwrap();
        let bob = key_manager
            .generate_keypair(KeyType::SharedEncryptionX25519)
            .unwrap();
        let bank_account = BankAccountId {
            routing_number: "123".into(),
            account_number: "456".into(),
        };
        let encrypted_bank_account = Encrypted::encrypt(
            &*key_manager,
            &alice.id,
            &bob.pubkey,
            &bank_account,
            &[0, 1, 2],
        )
        .unwrap();
        let input = BankAccountInfo::Encrypted(encrypted_bank_account);
        let substrate_model: RuntimeBankAccount = input.clone().try_into().unwrap();
        let output: BankAccountInfo = substrate_model.try_into().unwrap();

        assert_eq!(input, output);
    }

    #[test]
    pub fn transaction_is_financial_event_create_request() {
        let transaction = from_xand_txn(
            TransactionId::default(),
            XandTransaction::CreatePendingCreate(PendingCreate {
                account: Default::default(),
                amount_in_minor_unit: 42,
                correlation_id: CorrelationId::gen_random(),
                completing_transaction: None,
            }),
            example_addr(),
            TransactionStatus::Pending,
        );

        assert!(transaction.is_financial_event());
    }

    #[test]
    pub fn transaction_is_financial_event_create() {
        let transaction = from_xand_txn(
            TransactionId::from_str(
                "0x0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF",
            )
            .unwrap(),
            XandTransaction::FulfillCreate(FulfillPendingCreate {
                correlation_id: CorrelationId::gen_random(),
            }),
            example_addr(),
            TransactionStatus::Committed,
        );

        assert!(transaction.is_financial_event());
    }

    #[test]
    pub fn transaction_is_financial_event_redeem_request() {
        let transaction = from_xand_txn(
            TransactionId::default(),
            XandTransaction::CreatePendingRedeem(PendingRedeem {
                account: Default::default(),
                amount_in_minor_unit: 42,
                correlation_id: CorrelationId::gen_random(),
                completing_transaction: None,
            }),
            example_addr(),
            TransactionStatus::Committed,
        );

        assert!(transaction.is_financial_event());
    }

    #[test]
    pub fn transaction_is_financial_event_redeem_fulfillment() {
        let transaction = from_xand_txn(
            TransactionId::from_str(
                "0x0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF",
            )
            .unwrap(),
            XandTransaction::FulfillRedeem(FulfillPendingRedeem {
                correlation_id: CorrelationId::gen_random(),
            }),
            example_addr(),
            TransactionStatus::Committed,
        );

        assert!(transaction.is_financial_event());
    }

    #[test]
    pub fn transaction_is_financial_event_payment() {
        let transaction = from_xand_txn(
            TransactionId::default(),
            XandTransaction::Send(SendSchema {
                amount_in_minor_unit: 42,
                destination_account: example_addr(),
            }),
            example_addr(),
            TransactionStatus::Pending,
        );
        assert!(transaction.is_financial_event());
    }

    #[test]
    pub fn transaction_is_financial_event_some_non_financial() {
        let transaction = from_xand_txn(
            TransactionId::default(),
            XandTransaction::RegisterMember(RegisterAccountAsMember {
                address: example_addr(),
                encryption_key: example_encryption_key(),
            }),
            example_addr(),
            TransactionStatus::Pending,
        );
        assert!(!transaction.is_financial_event());
    }
}
