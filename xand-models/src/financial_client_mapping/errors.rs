use snafu::Snafu;
use std::{fmt::Debug, num::TryFromIntError};

use crate::BankAccountConversionErr;
use crate::TransactionType;
use xand_address::AddressError;
use xand_financial_client::errors::FinancialClientError;
use xand_financial_client::models::FinancialTransaction;

#[derive(Debug, Snafu)]
pub enum TxnConversionError {
    #[snafu(display(
        "Cannot convert because transaction is not a xand transaction {}",
        info
    ))]
    NotAXandTransaction { info: String },

    #[snafu(display("Ints could not be converted from u128s to u64s!"))]
    UnconvertableInts,

    #[snafu(display("Extrinsic did not appear to have a valid signer"))]
    ExtrinsicMissingSigner,

    #[snafu(display("Problem reading transaction address: {:?}", source))]
    #[snafu(context(false))]
    AddressErr { source: AddressError },

    #[snafu(display("Problem fetching bank account information: {:?}", source))]
    #[snafu(context(false))]
    BankAcountInfoErr { source: BankAccountConversionErr },

    #[snafu(display("Expected AdministrativeTransaction but got {:?}", txn_type))]
    ExpectedAdminTransaction { txn_type: TransactionType },

    #[snafu(display("Problem decoding financial transaction: {:?}", source))]
    #[snafu(context(false))]
    FinancialClientError { source: Box<FinancialClientError> },

    #[snafu(display("Unexpected confidential transaction: {:?}", txn))]
    ConfidentialTransactionError { txn: Box<FinancialTransaction> },

    #[snafu(display("Unexpected confidential transaction"))]
    UnexpectedConfidentialTransaction,

    #[snafu(display("Problem getting address from public key"))]
    CouldNotDeriveAddressFromPublicKey,
}

impl From<TryFromIntError> for TxnConversionError {
    fn from(_: TryFromIntError) -> Self {
        TxnConversionError::UnconvertableInts
    }
}

impl From<std::convert::Infallible> for TxnConversionError {
    fn from(_: std::convert::Infallible) -> TxnConversionError {
        TxnConversionError::UnconvertableInts
    }
}
