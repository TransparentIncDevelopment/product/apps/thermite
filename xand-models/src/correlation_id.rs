use hex::FromHex;
use serde::{Deserialize, Serialize};
use snafu::Snafu;
use std::{
    convert::{TryFrom, TryInto},
    fmt::{Debug, Display, Formatter},
    str::FromStr,
};

#[derive(Clone, Debug, Deserialize, PartialEq, Eq, Serialize, Snafu)]
pub enum CorrelationIdError {
    #[snafu(display(
        "The expected correlation_id format is 32 hex characters optionally preceded by '0x': {}",
        msg
    ))]
    Parsing { msg: String },

    #[snafu(display(
    "Cannot construct a correlation_id from the supplied bytes. Must be a byte array with a length of 16 bytes."
    ))]
    Encoding { input: Vec<u8> },
}

/// A correlation_id for correlating a create/redeem. Exactly 16 bytes. Will go away in favor of transaction
/// id based correlation
#[derive(Clone, Hash, PartialEq, Eq, Serialize, Deserialize, Default)]
#[serde(transparent)]
pub struct CorrelationId {
    #[serde(with = "crate::serde_hex")]
    value: [u8; 16],
}

impl CorrelationId {
    pub fn gen_random() -> Self {
        Self {
            value: rand::random(),
        }
    }

    pub fn as_bytes(&self) -> &[u8; 16] {
        &self.value
    }

    pub fn to_bytes(&self) -> [u8; 16] {
        self.value
    }
}

impl FromStr for CorrelationId {
    type Err = CorrelationIdError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut view = s;
        // If ignore the "0x" if the correlation_id starts with it
        if view.starts_with("0x") {
            view = &view[2..];
        }
        let value = <[u8; 16]>::from_hex(view)
            .map_err(|e| CorrelationIdError::Parsing { msg: e.to_string() })?;
        Ok(value.into())
    }
}

impl From<[u8; 16]> for CorrelationId {
    fn from(value: [u8; 16]) -> Self {
        CorrelationId { value }
    }
}

impl TryFrom<Vec<u8>> for CorrelationId {
    type Error = CorrelationIdError;

    fn try_from(input: Vec<u8>) -> Result<CorrelationId, Self::Error> {
        Ok(CorrelationId {
            value: input[..]
                .try_into()
                .map_err(|_| CorrelationIdError::Encoding { input })?,
        })
    }
}

impl Display for CorrelationId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let value_str = hex::encode(self.value);
        write!(f, "0x{}", value_str)
    }
}

impl Debug for CorrelationId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let value_str = hex::encode(self.value);
        write!(f, "Correlation_id(0x{})", value_str)
    }
}

impl From<CorrelationId> for Vec<u8> {
    fn from(input: CorrelationId) -> Vec<u8> {
        input.value.to_vec()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn correlation_id_serializes_to_hex_str() {
        let correlation_id_str = "0x000102030405060708090a0b0c0d0e0f";
        let correlation_id = correlation_id_str.parse::<CorrelationId>().unwrap();
        assert_eq!(
            serde_json::to_string(&correlation_id).unwrap(),
            format!(r#""{}""#, correlation_id_str),
        );
    }

    #[test]
    fn correlation_id_deserializes_from_hex_str() {
        let correlation_id_str = "0x000102030405060708090a0b0c0d0e0f";
        let correlation_id = correlation_id_str.parse::<CorrelationId>().unwrap();
        assert_eq!(
            serde_json::from_str::<CorrelationId>(&format!(r#""{}""#, correlation_id_str)).unwrap(),
            correlation_id,
        );
    }
}
