//! This module provides conversions between the runtime substrate-specific models, and our agnostic
//! versions.
//!
//! The `runtime-conversions` feature of this crate must be enabled to use it. Do not use that
//! feature flag unless you are inside xand-api, or some dev tool that talks directly to validators.

mod runtime;
pub mod test_helpers;

use crate::*;
#[cfg(feature = "parity-scale-codec")]
use parity_scale_codec::Encode;
use sp_core::{blake2_256, H256};
use std::fmt::Debug;

pub use runtime::decode_non_confidential_call;

pub mod to_platform {
    use super::*;
    #[cfg(feature = "xandstrate-runtime")]
    pub use runtime::*;

    pub fn map_h256_to_transaction_id(input: H256) -> TransactionId {
        input.as_fixed_bytes().into()
    }
}

/// This is how substrate hashes extrinsics, at least. There's not really a nice proper publicly
/// exposed API I could find for this. See this for reference:
///
/// Source code where substrate generates the hash that returns for "author_submitExtrinsic"
/// https://github.com/paritytech/substrate/blob/59d5ec144704cec017a01f3e4dbb6b73c5542bf7/core/transaction-pool/src/api.rs#L78
#[cfg(feature = "parity-scale-codec")]
pub fn hash_substrate_encodeable<E: Encode + Debug>(encodeable: &E) -> H256 {
    encodeable.using_encoded(blake2_256).into()
}

#[cfg(test)]
pub mod tests {
    use super::*;

    #[test]
    pub fn map_h256_to_transaction_id() {
        let value_as_bytes: [u8; 32] = [
            18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239, 18, 52, 86,
            120, 144, 171, 205, 239, 18, 52, 86, 120, 144, 171, 205, 239,
        ];

        let value_as_str: String =
            "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef".to_string();

        let input: H256 = H256::from_slice(&value_as_bytes);

        let transaction_id: TransactionId = to_platform::map_h256_to_transaction_id(input);

        assert_eq!(transaction_id.to_string(), value_as_str);
    }
}
