//! This module contains helpers that are useful to any crates that interact directly with substrate
//! types. Primarily, that means exclusive dependencies of the xand-api.

use crate::dev_account_key;
use parity_scale_codec::Encode;
use sp_core::{
    sr25519::{self, Public},
    Pair,
};
use std::convert::Infallible;
use xandstrate_runtime::{
    extrinsic_builder::{CallExt, ExtrinsicSigner},
    opaque::OpaqueExtrinsic,
    UncheckedExtrinsic,
};

/// Transforms a `Call` into a full extrinsic as might be found in a block. Useful for testing.
pub fn call_to_ext(
    call: xandstrate_runtime::Call,
    ix: u32,
    signer: &sr25519::Public,
) -> UncheckedExtrinsic {
    call.into_extrinsic(ix, Default::default(), signer)
        .sign(&FakeSign)
        .unwrap()
}

/// Transforms a `Call` into a full opaque extrinsic as might be found in a block. Useful for testing.
pub fn call_to_opaque_ext(
    call: xandstrate_runtime::Call,
    ix: u32,
    signer: &sr25519::Public,
) -> OpaqueExtrinsic {
    let ext = call_to_ext(call, ix, signer);
    OpaqueExtrinsic::from_bytes(ext.encode().as_slice()).unwrap()
}

struct FakeSign;
impl ExtrinsicSigner for FakeSign {
    type Err = Infallible;

    fn sign(&self, _: &Public, data: &[u8]) -> Result<Vec<u8>, Infallible> {
        let tupac = dev_account_key("2Pac");
        Ok(tupac.sign(data).0.to_vec())
    }
}
