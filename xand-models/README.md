<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [xand-models](#xand-models)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# xand-models

This crate contains shared models for representing the logic / business of
our runtime / blockchain. The crate should have small dependencies (by default).
Optionally, the runtime can be pulled in via a feature flag to enable conversion
between the actual runtime structs and the agnostic models.

See rustdocs for more details.
