#!/usr/bin/env bash

# Obnoxious, hacky script to make sure we start/finish ci stages without process stragglers
set +e

pkill xandstrate 
pkill multinode

set -e

exit 0
