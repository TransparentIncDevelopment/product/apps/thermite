#!/bin/bash

set -e  # fail fast with non-zero exit code when any commands fail
set -x  # print each command before executing it

# Symlink both build caches to their respective /target dirs
ln -s /build-cache/ target
export CARGO_INCREMENTAL=0
# Run Rust tests
cargo make rust-unit-tests-flow
cargo make rust-integ-tests
